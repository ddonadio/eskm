#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include "HarmonicOscillatorSystemDescription.h"

HarmonicOscillatorSystemDescription::~HarmonicOscillatorSystemDescription(void)
{
  //
  // destruct part list
  //
  for ( int i=0 ; i<part_list_.size() ; i++ ) delete part_list_[i];
  //
  // destruct reservoir list
  //
  for ( int i=0 ; i<reservoir_list_.size() ; i++ ) delete reservoir_list_[i];
}

double HarmonicOscillatorSystemDescription::convertEigenvalue(double eigenvalue)
{
  double TWO_PI=2.0*3.1415926535897931;
  return sqrt(eigenvalue)/TWO_PI;
}

// ****************************************************
//
// DefectDescription::readFromFileStream
//
//      parse the content of the filestream and fill the 
//      defect structure
// 
// ****************************************************

void HarmonicOscillatorSystemDescription::readFromFile(char *fileName, bool verbose)
{
  //
  // init cuttoff distance to large value
  //
  cutoff_dist_=-1;
  //
  // start by opening the file
  //
  FileHandler fileHandler;
  fileHandler.open(fileName);
  //
  // buffer for file reading
  //
  char *buff;
  //
  // flag for end of description
  //
  bool endOfSystem=false;
  //
  // while there is something to read
  //
  while ( !endOfSystem )
  {
    //
    // get the current tag
    //
    buff=fileHandler.readNextWord();
    //
    // if we are on the system definition 
    //
    if ( !strcmp( buff,"<system" ) || !strcmp( buff,"<system>" ) )
    {
      //
      // get the label fo this system if any
      //
      if ( !strcmp( buff,"<system" ) )
      {
        buff=fileHandler.readNextWord();
        char *tmp=strstr(buff,">");
        if ( tmp!=NULL ) *tmp = 0;
        label_.assign(buff);
      }
      //
      // parse the system description
      //
      while ( !endOfSystem )
      {
        //
        // get the current tag
        //
        buff=fileHandler.readNextWord();
        //
        // if we are on a part definition
        //
        if ( !strcmp( buff,"<part" ) || !strcmp( buff,"<part>" ) )
        {
          //
          // create a new reservoir description
          //
          PartAtomDescription *part=new PartAtomDescription( fileHandler );
          //
          // divide the representation if needed
          //
          if ( part->hasFlag("subdivide") )
          {
            //
            // subdivide
            //
            vector<PartAtomDescription *> part_list=part->subdivide( verbose );
            //
            // delete the original part
            // 
            delete part;
            //
            // insert the list
            //
            part_list_.insert(part_list_.end(),part_list.begin(),part_list.end());
          }
          else
          {
            //
            // simply insert the part as this
            //
            part_list_.push_back(part);
          }
          usleep(1000000);
        }
        //
        // if we are on a reservoir definition
        //
        if ( !strcmp( buff,"<reservoir" ) || !strcmp( buff,"<reservoir>" ) )
        {
          //
          // create a new reservoir description
          //
          ReservoirAtomDescription *reservoir=new ReservoirAtomDescription( fileHandler , verbose );
          //
          // reduce the representtaion if needed
          //
          if ( reservoir->hasFlag("reduce") )
          {
            reservoir->reduceToMinimumCell( verbose );
          }
          //
          // add reservoir to the list
          //
          reservoir_list_.push_back(reservoir);
        }
        //
        // if we are on the definition of the graph clustering parameters
        //
        if ( !strcmp( buff,"<graphClustering>" ) || !strcmp( buff,"<graphClustering" ) ) 
        {
          //
          // set graph clustering parameters
          //
          clusterParam_.readFromFile( fileHandler );
        }
        //
        // if we are on the cutoff definition
        //
        if ( !strcmp( buff,"<cutoff" ) || !strcmp( buff,"<cutoff>" ) )
        {
          //
          // go to the line if necessary
          //
          if ( !strcmp( buff,"<cutoff" ) ) fileHandler.skipLine();
          //
          // read the cuttoff
          //
          cutoff_dist_=atof(fileHandler.readNextWord());
        }
        //
        // if we are on the cutoff definition
        //
        if ( !strcmp( buff,"<wLinearSet" ) || !strcmp( buff,"<wLinearSet>" ) )
        {
          //
          // go to the line if necessary
          //
          if ( !strcmp( buff,"<wLinearSet" ) ) fileHandler.skipLine();
          //
          // read the cuttoff
          //
          double w_start=atof(fileHandler.readNextWord());
          double w_end=atof(fileHandler.readNextWord());
          int    n_points=atoi(fileHandler.readNextWord());
          //
          // generate the frequency ramp
          //
          if ( n_points > 1 )
          {
            double TWO_PI=2.0*3.1415926535897931;
            for ( int i=0 ; i<n_points ; i++ )
            {
              double w=TWO_PI*(w_start+(w_end-w_start)*i/(double)(n_points-1));
              eigenvalues_.push_back(w*w);
            }
          }
          else if ( n_points == 1 )
          {
            double TWO_PI=2.0*3.1415926535897931;
            double w=TWO_PI*w_start;
            eigenvalues_.push_back(w*w);
          }
          else
          {
            cout << "Error: energy linear set requires at least one point" << endl;
            exit(0);
          }
        }
        //
        // if we are on a flag definition 
        //
        if ( !strcmp( buff,"<flag" ) || !strcmp( buff,"<flags" ) || !strcmp( buff,"<flag>" ) || !strcmp( buff,"<flags>" ) )
        {
          //
          // go to the line
          //
          fileHandler.skipLine();
          //
          // get the flags description
          //
          bool endOfFlags=false;
          //
          while ( !endOfFlags )
          {
            //
            // read the flag
            //
            fileHandler.readNextWord();
            //
            if ( !strcmp( buff,"</flag" ) || !strcmp( buff,"</flag>" ) || !strcmp( buff,"</flags" ) || !strcmp( buff,"</flags>" ) )
            {
              endOfFlags=true;
            } 
            else
            {
              //
              // get the flag in a string
              //
              string flag;
              flag.assign(buff);
              //
              // append the string to the flag list
              //
              flags_.push_back(flag);
            } 
          }
        }
        //
        // if we hit the end of the defect description 
        //   
        if ( !strcmp( buff,"</system" ) || !strcmp( buff,"</system>" ) ) endOfSystem=true; 
        //
        // if we hit the end of the file, there is a problem
        //
        if ( fileHandler.eof() )
        {
          cout << "Error while reading defect description" << endl;
          exit(0);
        }
      }
    }
  }
  //
  // check that the description is complete
  //
  if ( reservoir_list_.size()==0 )
  {
    cout << "Error: system definition requires definition of at least one reservoir" << endl;
    exit(0);
  }
  if ( cutoff_dist_<0.0 )
  {
    cout << "Error: no cutoff distance has been specified for system descritpion" << endl;
    exit(0);
  }
  if ( eigenvalues_.size()==0 )
  {
    cout << "Error: no frequencies has been specified for transmission calculation, detected in - system descritpion -" << endl;
    exit(0);
  }
  //
  // recapitulate
  //
  if ( verbose )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "HarmonicOscillatorSystemDescription:" << endl << endl;
    cout << "    read from file " << fileName << endl;
    cout << "    found " << part_list_.size() << " part(s) " << endl; 
    cout << "    found " << reservoir_list_.size() << " reservoir(s) " << endl; 
    for ( int i=0 ; i<flags_.size() ; i++ )
    cout << "    found flag " << flags_[i] << endl;
    cout << "    using cutoff " << cutoff_dist_ << endl; 
    cout << "-------------------------------------------------------------------------" << endl;
  }
}

bool HarmonicOscillatorSystemDescription::hasFlag(string flag)
{
  for ( int i=0 ; i<flags_.size() ; i++ ) if ( flags_[i]==flag ) return true;
  return false;
}

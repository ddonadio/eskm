/*! \file Flux.h
    \brief Flux class and related function declarations
*/

#ifndef FLUX_H
#define FLUX_H

#include "Sort.h"

//! Flux Class.
/// \brief This class template is intended to help manipulating fluxes. 

template <class ObjectType>
class Flux
{
  public:
  
    ObjectType fromObject;        ///< object (atom for example) from wich the flux start
    ObjectType toObject;          ///< object (atom for example) to wich the flux goes
    double     value;             ///< value of the flux
    int        fromGlobalIndex;   ///< global index of the initial state
    int        toGlobalIndex;     ///< global index of the final state
};

//! \brief Save fluxes into a file
/// \param flux reference a vector of Flux object
/// \param fileName name of the file in wich the fluxes are saved

template <class ObjectType>
void saveFluxes( string fileName, vector<Flux<ObjectType> > &flux )
{
  //
  // open file for writing 
  //
  ofstream filestr;
  filestr.open(fileName.data());
  //
  // loop on the fluxes
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) 
  {
    filestr << flux[iflux].fromGlobalIndex << "     "  << flux[iflux].toGlobalIndex << "     " << flux[iflux].fromObject << "    " << flux[iflux].toObject << "     " << flux[iflux].value << endl;
  }
}

//! \brief Load fluxes from a file
/// \param flux reference a vector of Flux object
/// \param fileName name of the file from wich the fluxes are read

template <class ObjectType>
void loadFluxes( string fileName, vector<Flux<ObjectType> > &flux )
{
  //
  // open file for reading
  //
  FileHandler file;
  file.open(fileName.data());
  //
  // loop on the fluxes
  //
  while (1)
  {
    char* buff=file.readNextWord();
    //
    // check that there is something to read
    //
    if (file.eof()) break;
    //
    // init the flux object
    //
    Flux<ObjectType> myflux;
    //
    // get global indices
    //
    myflux.fromGlobalIndex=atoi(buff);
    myflux.toGlobalIndex=atoi(file.readNextWord());
    //
    // set fromObject name
    //
    myflux.fromObject << file;    
    //
    // read toObject 
    //
    myflux.toObject << file;
    //
    // read value of the flux
    //
    myflux.value=atof(file.readNextWord());
    //
    // push this flux in the list
    //
    flux.push_back(myflux);
  }
  //
  // inform 
  //
  cout << "read " << flux.size() << " fluxes from file " << fileName << endl;
}

//! \brief Remove from the list the fluxes which values are == 0
/// \param flux reference a vector of Flux object
/// \retval flux is freed from fluxes with null component 

template <class ObjectType>
void cleanEmptyFluxes( vector<Flux<ObjectType> > &flux )
{
  //
  // loop on the fluxes to keep only relevant ones
  //
  size_t jflux=0;
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) 
  {
    if ( flux[iflux].value!=0.0 )
    {
      flux[jflux].fromObject     =flux[iflux].fromObject;
      flux[jflux].toObject       =flux[iflux].toObject;
      flux[jflux].fromGlobalIndex=flux[iflux].fromGlobalIndex;
      flux[jflux].toGlobalIndex  =flux[iflux].toGlobalIndex;
      flux[jflux].value          =flux[iflux].value;
      jflux++;
    }
  }
  //
  // readjust the new flux list
  //
  flux.resize(jflux);
}

//! \brief Return rank according to fromObject ordering. 
/// \param flux reference a vector of Flux object
/// \return return the indices of fluxes sorted according to toObject ordering  

template <class ObjectType>
vector<size_t> rankByFromObject( vector<Flux<ObjectType> > &flux )
{
  //
  // allocate memory for index and object lists
  //
  vector<size_t> object_indices; object_indices.reserve(flux.size());
  vector<ObjectType> object_list; object_list.reserve(flux.size());
  //
  // fill index and object lists
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) 
  {
    object_indices.push_back(iflux);
    object_list.push_back(flux[iflux].fromObject);
  }
  //
  // sort 
  //
  sort<ObjectType>(&object_list[0],object_list.size(),&object_indices[0]);
  //
  // return ranking
  //
  return object_indices;
}

//! \brief Return rank according to toObject ordering. 
/// \param flux reference a vector of Flux object
/// \return return the indices of fluxes sorted according to toObject ordering  

template <class ObjectType>
vector<size_t> rankByToObject( vector<Flux<ObjectType> > &flux )
{
  //
  // allocate memory for index and object lists
  //
  vector<size_t> object_indices; object_indices.reserve(flux.size());
  vector<ObjectType> object_list; object_list.reserve(flux.size());
  //
  // fill index and object lists
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) 
  {
    object_indices.push_back(iflux);
    object_list.push_back(flux[iflux].toObject);
  }
  //
  // sort 
  //
  sort<ObjectType>(&object_list[0],object_list.size(),&object_indices[0]);
  //
  // return ranking
  //
  return object_indices;
}

//! \brief Sort fluxes according to fromObject. 
/// \param flux reference a vector of Flux object
/// \retval flux the list is sorted such that i<j => flux[i].fromObject<=flux[j].fromObject

template <class ObjectType>
void sortByFromObject( vector<Flux<ObjectType> > &flux )
{
  //
  // allocate memory for object and index lists
  //
  vector<size_t> index_list; index_list.reserve(flux.size());
  vector<ObjectType> object_list; object_list.reserve(flux.size());
  //
  // fill the lists
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) 
  {
    index_list.push_back(iflux);
    object_list.push_back(flux[iflux].fromObject);
  }
  //
  // sort
  //
  sort<ObjectType>(&object_list[0],object_list.size(),&index_list[0]);
  //
  // reorder the fluxes
  //
  vector<Flux<ObjectType> > sorted_flux; sorted_flux.reserve(flux.size());
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) sorted_flux.push_back(flux[index_list[iflux]]);
  //
  // set order flux list
  //
  flux=sorted_flux;
}

//! \brief Sort fluxes according to toObject. 
/// \param flux reference a vector of Flux object
/// \retval flux the list is sorted such that i<j => flux[i].toObject<=flux[j].toObject

template <class ObjectType>
void sortByToObject( vector<Flux<ObjectType> > &flux )
{
  //
  // allocate memory for object and index lists
  //
  vector<size_t> index_list; index_list.reserve(flux.size());
  vector<ObjectType> object_list; object_list.reserve(flux.size());
  //
  // fill the lists
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) 
  {
    index_list.push_back(iflux);
    object_list.push_back(flux[iflux].toObject);
  }
  //
  // sort
  //
  sort<ObjectType>(&object_list[0],object_list.size(),&index_list[0]);
  //
  // reorder the fluxes
  //
  vector<Flux<ObjectType> > sorted_flux; sorted_flux.reserve(flux.size());
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ ) sorted_flux.push_back(flux[index_list[iflux]]);
  //
  // set order flux list
  //
  flux=sorted_flux;
}

//! \brief Remove duplicatas in the flux list.
/// Keep only fluxes from object 1 -> object 2 and 
/// remove the ones from object 2 -> object 1. 
/// The order is defined by the first occurence in the list.
/// \param flux reference a vector of Flux object
/// \retval flux the list contains at more one flux between two given object

template <class ObjectType>
void removeFluxCopies( vector<Flux<ObjectType> > &flux )
{
  int init_size=flux.size();
  //
  // sort flux list according to formObject order
  //
  sortByFromObject( flux );
  //
  // remove the duplicate fluxes with similar orientation, that is with
  // fromObject1=fromObject2 and  toObject1=toObject2
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ )
  {
    size_t jflux=iflux+1;
    //
    while ( jflux<flux.size() && flux[iflux].fromObject==flux[jflux].fromObject ) 
    {
      if ( flux[iflux].toObject==flux[jflux].toObject ) flux[jflux].value=0.0; 
      //
      jflux++; 
    }
  }
  //
  // clean from null fluxes
  //
  cleanEmptyFluxes( flux );
  //
  cout << "removed " << init_size-flux.size() << " duplicatas in - removeFluxCopies -" << endl;
  //
  // restore order
  //
  sortByFromObject( flux );
  //
  // get ordering according to toObject
  //
  vector<size_t> sorted_second=rankByToObject<ObjectType>( flux );    
  //
  // remove the fluxes that are present twice in the list with opposite orientation, that is
  // fromObject1=toObject2 and  toObject1=fromObject2
  //
  for ( size_t iflux=0 , isecond=0 ; iflux<flux.size() ; iflux++ )
  {    
    ObjectType fromObject=flux[iflux].fromObject;
    ObjectType toObject=flux[iflux].toObject;
    //
    // position of the first similar second atom
    //
    while ( isecond<sorted_second.size() && flux[sorted_second[isecond]].toObject<fromObject ) isecond++; 
    size_t jflux=isecond;
    //
    // loop on the second atoms
    //
    while ( jflux<sorted_second.size() && flux[sorted_second[jflux]].toObject==fromObject && flux[iflux].value!=0.0 )
    {
      if ( flux[sorted_second[jflux]].fromObject==toObject ) flux[sorted_second[jflux]].value=0.0;
      //
      jflux++;
    }
  }
  //
  // clean from null fluxes
  //
  cleanEmptyFluxes( flux );
  //
  // inform
  //
  cout << "removed " << init_size-flux.size() << " duplicatas in - removeFluxCopies -" << endl;
}

//! \brief Fold fluxes on the objects.
/// Add all fluxes from object 1 -> object 2. 
/// \param flux reference a vector of Flux object
/// \retval flux the list contains at more one flux from object 1 -> object 2

template <class ObjectType>
void foldFluxes( vector<Flux<ObjectType> > &flux )
{
  //
  // start by sorting the flux list according to from object
  //
  sortByFromObject<ObjectType>( flux );
  //
  // loop on the fluxes
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ )
  {
    ObjectType fromObject=flux[iflux].fromObject;
    ObjectType toObject=flux[iflux].toObject;
    //
    // start second loop from next object
    //
    size_t jflux=iflux+1; 
    //
    // loop on the following objects
    // 
    while ( jflux<flux.size() && fromObject==flux[jflux].fromObject && flux[iflux].value!=0.0 )
    {
      //
      // if the two atoms are identical
      //
      if ( toObject==flux[jflux].toObject )
      {
        //
        // add flux j to flux i
        //
        flux[iflux].value+=flux[jflux].value;
        //
        // remove flux j from the list
        //
        flux[jflux].value=0.0;
      }
      //
      // increment index
      //    
      jflux++;
    }
  }
  //
  // remove empty fluxes from the list
  //
  cleanEmptyFluxes( flux );
}

//! \brief Construct object list from the fluxes.
/// \param flux reference a vector of Flux object
/// \return return the list of object constructed from fromObject attributes 

template <class ObjectType>
vector<ObjectType> fluxObjectList( vector<Flux<ObjectType> > &flux )
{
  //
  // start by sorting the flux list according to from object
  //
  sortByFromObject<ObjectType>( flux );
  //
  // allocate object list
  //
  vector<ObjectType> object_list;
  //
  // init object list
  //
  if ( flux.size()>0 ) object_list.push_back(flux[0].fromObject);
  //
  // loop on the remaining fluxes
  //
  for ( size_t iflux=1 , stop=flux.size() ; iflux<stop ; iflux++ )
  {
    if ( !( object_list.back() == flux[iflux].fromObject ) ) object_list.push_back(flux[iflux].fromObject);
  }
  //
  // return the list of objects
  //
  return object_list;
}

//! \brief Construct list of global indices from the fluxes.
/// \param flux reference a vector of Flux object
/// \retval fromGlobalIndex contains the list of indexeds constructed from fromGlobalIndex attribute 
/// \retval toGlobalIndex contains the list of indexeds constructed from toGlobalIndex attribute 

template <class ObjectType>
void fluxGlobalIndexLists( vector<Flux<ObjectType> > &flux, vector<int> &fromGlobalIndex, vector<int> &toGlobalIndex )
{
  //
  // reset lists of global indices 
  //
  fromGlobalIndex.resize(0);
  toGlobalIndex.resize(0);
  //
  // reserve memory
  //
  fromGlobalIndex.reserve(flux.size());
  toGlobalIndex.reserve(flux.size());
  //
  // loop on fluxes to construct list
  //
  for ( size_t iflux=0 ; iflux<flux.size() ; iflux++ )
  {
    fromGlobalIndex.push_back(flux[iflux].fromGlobalIndex);
    toGlobalIndex.push_back(flux[iflux].toGlobalIndex);
  }
}

#endif

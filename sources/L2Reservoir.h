#ifndef L2RESERVOIR_H
#define L2RESERVOIR_H

#include "Timer.h"
#include "L2Basis.h"
#include "Reservoir.h"

#define REAL_SYSTEM_TYPE    1
#define COMPLEX_SYSTEM_TYPE 2
#define TWO_PI              6.283185307179586

#define TIMING
//#define DEBUG

template <class Precision>
class L2Reservoir : public Reservoir<Precision>
{
  private:
    
    // arrays for folding the system
    vector<Precision>           real_folded_system_;
    vector<complex<Precision> > complex_folded_system_; 
    
    // array for the current operator
    vector<complex<Precision> > current_operator_;
    
    // folding method
    void foldSystem_(Precision E, Precision phi1, Precision Phi2, Precision noise_level);
    
    // current operator
    void setCurrentOperator_(void);
    
    // chanel definition
    bool getChannelsForRealSystem_(void);
    bool getChannelsForComplexSystem_(void);
    
    // orthonormalization of the channels
    void orthonormalizeChannels_(void);
    
    // balancing of the channels
    void balanceChannels_(void);
    
    // sort of the channels
    void sortsChannels_(int iq1, int iq2);
    
    // definition of the duals
    bool findDuals_(int iq1, int iq2);
    
    // probability current computation
    complex<Precision> computeProbabilityCurrent_(int i_channel);
    complex<Precision> computeProbabilityCurrent_(int i_channel, int j_channel);
                       
    // currents array
    vector<Precision> currents_;
    
    // channels expression for one q point sampling
    vector<complex<Precision> > this_q_channels_; 
    
    // propagation constant
    vector<complex<Precision> > this_q_constants_;
    
    // channels count
    vector<vector<int> > n_explosive_channels_;
    vector<vector<int> > n_evanescent_channels_;
    vector<vector<int> > n_incoming_channels_;
    vector<vector<int> > n_outgoing_channels_;
    
    // total numbers of channel by type
    int n_ex_;
    int n_ev_;
    int n_in_;
    int n_out_;
    
    // index lists of first and second half
    vector<int> local_first_half_indices_;
    vector<int> local_second_half_indices_;
    
    // indices for extraction of coefficient from periodic solution array
    int         istart_center_;
    vector<int> local_src_first_half_;
    vector<int> local_src_second_half_;
    vector<int> local_dest_first_half_;
    vector<int> local_dest_second_half_;
    
    // trivial solution of the kernel equation
    vector<int> trivial_indices_;
    
    // flag to indicate that channels are set
    bool flag_reduced_interactions_;
    
    // flag to indicate that channels are set
    bool flag_channel_set;
    
    // flag to indicate that channels are synchro
    bool flag_channel_sync;
    
    // flag to indicate wich folded matrix is set
    bool real_folded_system_set_;
    
    // timers   
    TimerMap tmap_;
       
  protected:
    
    // index of working processor
    int working_proc_;
    
    // number of repetition for this reservoir
    int nrep1_;
    int nrep2_;
        
    // list of the indices for each repetition of the unit cell
    // the unit cell count 3 slices of the electrode  
    vector<vector<vector<int> > > cell_indices_;
    
    // interaction and overlaps between each repetition of
    // the unit cell and the (0,0) unit cell 
    vector<vector<vector<complex<Precision> > > > H_coeff_;   // coefficients are stored transposed
    vector<vector<vector<complex<Precision> > > > S_coeff_;   // coefficients are stored transposed
    vector<int>                      active_folded_domain_;   // local domain indices of active states 
    
    // channel duals and coeffs for each Q sampling (overload the base class names)
    vector<vector<vector<complex<Precision> > > >   dual_coeff_;     // local coefficients for the channels duals : Cij is the ith coeff of channel j dual. 
    vector<vector<vector<complex<Precision> > > >   channel_coeff_;  // local coefficients for the channels : Cij is the is the jth coeff of channel i.
    
    // flag to indicate that the Hamiltonian and overlaps are real
    int  system_type_;
    
    // flag to indicate that we want to keep the coefficients of the channels, not only the duals
    bool keepChannels_; 
    
  public:

    // constructor
    template <class L2BasisType>
    L2Reservoir(L2BasisType &basis, Context &context, int ireservoir);
    
    // resolution of the channels 
    virtual bool findChannelSolutions(double Energy);
    
    // translation into channel representation
    virtual void moveToChannelRepresentation(Subspace<complex<Precision> > &subspace);
    virtual Subspace<complex<Precision> >* moveToChannelRepresentation(Subspace<Precision> &subspace);
    
    // translation from channel to local representation
    virtual void moveToLocalRepresentation(Subspace<complex<Precision> > &subspace);
    
    // synchronization of channels indices on all the procs of the context
    virtual void synchronizeChannelsIndices(void);
    
    // reduction of the interaction matrices
    void reduce(double cutoff_H, double cutoff_S);
    
    // diagnostic tools
    void randomize(bool complex);
    void symmetrize(void);
    bool checkSymmetry(void);
    void printChannels(string comment);
    void printInteractions(string comment);
    void printComplexArrayInFile_(string fileName, string comment, complex<Precision> *A, int LDA, int count_from, int count_to) ;
    
    
    // destructor
    ~L2Reservoir();
};

// reduce removes the non-interacting domain states  
template <class Precision>
void L2Reservoir<Precision>::reduce(double cutoff_H, double cutoff_S)
{
  //
  // get a copy of the current active domain
  //
  vector<int> old_active_domain=active_folded_domain_;
  //
  // init active domain indices of folded coeffs
  //
  active_folded_domain_.resize(0);
  //
  // dimensions
  //
  int size_folded=cell_indices_[0][0].size();
  int size_center=cell_indices_[0][0].size()/3;
  int size_coeffs=size_folded*size_center;
  //
  // square the cutoff once for all
  //
  Precision sqr_H_cutoff=cutoff_H*cutoff_H;
  Precision sqr_S_cutoff=cutoff_S*cutoff_S;
  //
  // check if overlaps are present
  //
  bool has_overlaps = (  S_coeff_.size()==nrep1_ );
  //
  // compute norms
  //
  complex<Precision> trace=0.0;
  complex<Precision> norm2=0.0;
  for ( int i=0 ; i<size_center ; i++ )
  {
    // trace
    trace+=H_coeff_[0][0][size_center+i*(size_folded+1)]*conj(H_coeff_[0][0][size_center+i*(size_folded+1)]);
    // norm2
    for ( int j=0 ; j<size_folded ; j++ ) norm2+=H_coeff_[0][0][i*size_folded+j]*conj(H_coeff_[0][0][i*size_folded+j]);
  }
  //
  // print norms
  //
  cout << "Reservoir " << this->label_ << " checks:" << endl;
  cout << "  trace = " << trace << endl;
  cout << "  norm2 = " << norm2 << endl;
  //
  // loop on the domain states of the folded cell to determine the interacting ones
  //
  for ( int i=0 ; i<size_folded ; i++ )
  {
    //
    // init interaction flag
    //
    bool interact=false;
    //
    // loop on the different cells
    //
    for ( int irep1=0 ; irep1<nrep1_ && !interact ; irep1++ )
    for ( int irep2=0 ; irep2<nrep2_ && !interact ; irep2++ )
    {
      //
      // look for interaction
      //
      for ( complex<Precision> *H_ptr=&H_coeff_[irep1][irep2][i] , 
                               *stop=&H_coeff_[irep1][irep2][i]+size_coeffs ; H_ptr<stop ; H_ptr+=size_folded )
      {
        if ( norm(*H_ptr)>sqr_H_cutoff ) { interact=true; break; }
      } 
    }
    //
    // check the overlaps if necessary
    //
    if ( has_overlaps )
    {
      //
      // loop on the different cells
      //
      for ( int irep1=0 ; irep1<nrep1_ && !interact ; irep1++ )
      for ( int irep2=0 ; irep2<nrep2_ && !interact ; irep2++ )
      {
        //
        // look for interaction
        //
        for ( complex<Precision> *S_ptr=&S_coeff_[irep1][irep2][i] , 
                                 *stop=&S_coeff_[irep1][irep2][i]+size_coeffs ; S_ptr<stop ; S_ptr+=size_folded )
        {
          if ( norm(*S_ptr)>sqr_S_cutoff ) { interact=true; break; }
        } 
      }
    }
    //
    // if this state interact, add it to the list
    //
    if ( interact ) active_folded_domain_.push_back(i);
  }
  //
  // reduce the interactions
  //
  if ( active_folded_domain_.size()<size_folded )
  {
    int size_reduced=active_folded_domain_.size();
    //
    // loop on the different cells
    //
    for ( int irep1=0 ; irep1<nrep1_ ; irep1++ )
    for ( int irep2=0 ; irep2<nrep2_ ; irep2++ )
    {
      //
      // remove null lines in H and S:
      //
      if ( has_overlaps )
      {
        //
        // outer loop on the columns 
        // 
        for ( int icol=0 ; icol<size_center ; icol++ )
        {
          //
          // get pointer on this column
          //
          complex<Precision> *S_ptr=&S_coeff_[irep1][irep2][icol*size_reduced];
          complex<Precision> *H_ptr=&H_coeff_[irep1][irep2][icol*size_reduced];
          complex<Precision> *S_ptr_start=&S_coeff_[irep1][irep2][icol*size_folded];
          complex<Precision> *H_ptr_start=&H_coeff_[irep1][irep2][icol*size_folded];
          //
          // inner loops on the lines
          // 
          for ( int *iact=&active_folded_domain_[0] , 
                    *stop=&active_folded_domain_[0]+active_folded_domain_.size() ; iact<stop ; iact++ , H_ptr++ , S_ptr++ )
          {
            (*H_ptr)=(*(H_ptr_start+(*iact)));
            (*S_ptr)=(*(S_ptr_start+(*iact)));
          }
        }
        //
        // resize the interaction matrix
        //
        H_coeff_[irep1][irep2].resize(active_folded_domain_.size()*size_center);
        S_coeff_[irep1][irep2].resize(active_folded_domain_.size()*size_center);
      }
      else
      {
        //
        // outer loop on the columns 
        // 
        for ( int icol=0 ; icol<size_center ; icol++ )
        {
          //
          // get pointer on this column
          //
          complex<Precision> *H_ptr=&H_coeff_[irep1][irep2][icol*size_reduced];
          complex<Precision> *H_ptr_start=&H_coeff_[irep1][irep2][icol*size_folded];
          //
          // inner loops on the lines
          // 
          for ( int *iact=&active_folded_domain_[0] , 
                    *stop=&active_folded_domain_[0]+active_folded_domain_.size() ; iact<stop ; iact++ , H_ptr++ )
          {
            (*H_ptr)=(*(H_ptr_start+(*iact)));
          }
        }
        //
        // resize the interaction matrix
        //
        H_coeff_[irep1][irep2].resize(active_folded_domain_.size()*size_center);
      }
    }
  }
  //
  // get a copy of the first half indices
  //
  vector<int> first_half_copy=local_first_half_indices_;
  //
  // form index lists of first and second half
  //
  local_first_half_indices_.resize(0);
  local_second_half_indices_.resize(0);
  int first_i_center=size_center<=active_folded_domain_;
  //
  for ( int i=0 ; i<first_i_center ; i++ )
  {
    local_first_half_indices_.push_back(i);
    local_second_half_indices_.push_back(first_i_center+active_folded_domain_[i]);
  }
  //
  int offset=first_i_center-2*size_center;
  for ( int i=size_center+first_i_center , stop=active_folded_domain_.size() ; i<stop ; i++ )
  {
    local_first_half_indices_.push_back(active_folded_domain_[i]+offset);
    local_second_half_indices_.push_back(i);
  }
  //
  // form index list for solution extraction 
  //
  local_src_first_half_.resize(0);
  local_dest_first_half_.resize(0);
  local_src_second_half_.resize(0);
  local_dest_second_half_.resize(0);
  //
  for ( int i=0 , size_channel=2*size_center ; i<active_folded_domain_.size() ; i++ )
  {
    if ( active_folded_domain_[i]<size_channel )
    {
      local_src_first_half_.push_back(i);
      local_dest_first_half_.push_back(active_folded_domain_[i]);
    }
    if ( active_folded_domain_[i]>=size_center )
    {
      local_src_second_half_.push_back(i);
      local_dest_second_half_.push_back(active_folded_domain_[i]-size_center);
    }
  }
  //
  // form the list of trivial solutions indices
  //
  trivial_indices_ = old_active_domain != active_folded_domain_;
  //
  // turn on flag to indicate that the reservoir has been reduced
  //
  flag_reduced_interactions_=true;
  //
  // get starting index of the center
  //
  istart_center_=size_center<=active_folded_domain_;
}
   
// destructor
template <class Precision>
L2Reservoir<Precision>::~L2Reservoir(void)
{
#ifdef TIMING
  //
  // stop global timer
  //
  tmap_["total life time"].stop();
  //
  // print the different timers
  //
  tmap_.print(cout,"L2Reservoir:",this->context_,working_proc_);
#endif
}   
   
    
//
// constructor
// make sure that we got the references to 
// all the elements of the base class
//
template <class Precision>
template <class L2BasisType>
L2Reservoir<Precision>::L2Reservoir(L2BasisType &basis, Context &context, int ireservoir) :
Reservoir<Precision>(basis,context,ireservoir)
{
#ifdef TIMING 
  tmap_["total life time"].start();
#endif
  //
  // get centers of the states for this reservoir
  //
  vector<int> rep=basis.getReservoirRepetitions(ireservoir);
  //
  // keep number of repetitions for this reservoir
  //
  nrep1_=rep[0];
  nrep2_=rep[1];
  //
  // fill the cell indices array
  //
  cell_indices_.resize(nrep1_);
  for ( int irep1=0 ; irep1<nrep1_ ; irep1++ )
  { 
    cell_indices_[irep1].resize(nrep2_);
    for ( int irep2=0 ; irep2<nrep2_ ; irep2++ ) 
    {
      cell_indices_[irep1][irep2]=basis.getReservoirCellIndices(ireservoir,irep1,irep2);
      //
      // check the dimension
      // 
      if ( (cell_indices_[irep1][irep2].size()%3) != 0 || cell_indices_[irep1][irep2].size() != cell_indices_[0][0].size() )
      {
        cout << "Error: inconsistent cell dimension in - L2Reservoir<Precision>::L2Reservoir -" << endl;
        exit(0); 
      }
    }
  }
  //
  // allocat ememory the channels count
  //
  n_explosive_channels_.resize(nrep1_);
  n_evanescent_channels_.resize(nrep1_);
  n_incoming_channels_.resize(nrep1_);
  n_outgoing_channels_.resize(nrep1_);
  for ( int irep1=0 ; irep1<nrep1_ ; irep1++ )
  { 
    n_explosive_channels_[irep1].resize(nrep2_);
    n_evanescent_channels_[irep1].resize(nrep2_);
    n_incoming_channels_[irep1].resize(nrep2_);
    n_outgoing_channels_[irep1].resize(nrep2_);
  }
  //
  // set the channel flag to true by default
  //
  keepChannels_=true;
  //
  // set system type to 0
  //
  system_type_=0;
  //
  // decide the working proc for this reservoir
  //
  working_proc_=-1;
  int nmax=0;
  for ( map<int,vector<int> >::iterator it=this->local_indices_.begin() , stop=this->local_indices_.end() ; it!=stop ; it++ )
  {
    if ( it->second.size()>nmax ) 
    {
      nmax=it->second.size();
      working_proc_=it->first;
    }
  }
  //
  // set flag to indicate that channels are no set yet
  //
  flag_channel_set=false;
  //
  // set flag to indicate that the description is complete
  //
  flag_reduced_interactions_=false;
  //
  // set domain indices of the folded coeffs
  // 
  for ( int i=0 , stop=cell_indices_[0][0].size() ; i<stop ; i++ ) active_folded_domain_.push_back(i);
  //
  // form indices list for first and second half of non periodic solutions
  //
  for ( int i=0 , j=cell_indices_[0][0].size()/3 , stop=cell_indices_[0][0].size() ; j<stop ; i++ , j++ )
  {
    local_first_half_indices_.push_back(i);
    local_second_half_indices_.push_back(j);
  }
  //
  // form index list for periodic solution extraction 
  //
  {
    int size_center=cell_indices_[0][0].size()/3;
    int size_channel=2*size_center;
    //
    for ( int i=0 ; i<active_folded_domain_.size() ; i++ )
    {
      if ( active_folded_domain_[i]<size_channel )
      {
        local_src_first_half_.push_back(i);
        local_dest_first_half_.push_back(active_folded_domain_[i]);
      }
      if ( active_folded_domain_[i]>=size_center )
      {
        local_src_second_half_.push_back(i);
        local_dest_second_half_.push_back(active_folded_domain_[i]-size_center);
      } 
    }
    //
    // get starting index of the center
    //
    istart_center_=size_center;
  }
}    

template <class Precision>
bool L2Reservoir<Precision>::findChannelSolutions(double energy)
{
  //
  // turn on the flags
  //
  flag_channel_set=true;
  flag_channel_sync=false;
  //
  // get reference of base class attribute
  //
  Context   &context_=this->context_;
  int       &my_proc_=this->my_proc_;
  int       &n_procs_=this->n_procs_;
  //
  // if we are on the working processor 
  //
  if ( my_proc_==working_proc_ )
  {
    // range/domain indices
    vector<int>           &global_indices_=this->global_indices_;    // indices of this reservoir
    map<int,vector<int> > &local_indices_=this->local_indices_;      // repartition of the indices on the proc grid
    // range proc index lists  
    vector<int> &list_proc_=this->list_proc_;              // list of processors supporting the reservoir 
    vector<int> &Nloc_=this->Nloc_;                        // local dimension on supporting processors
    int         &my_Nloc_=this->my_Nloc_;                  // local dimension on this processor
    // channel indices by type
    vector<int>    &explosive_channel_indices_=this->explosive_channel_indices_;   // indices of explosive channels
    vector<int>    &evanescent_channel_indices_=this->evanescent_channel_indices_; // indices of evanescent channels
    vector<int>    &incoming_channel_indices_=this->incoming_channel_indices_;     // indices of incoming channels
    vector<int>    &outgoing_channel_indices_=this->outgoing_channel_indices_;   // indices of outcoming channels
    //
    // check that the interactions and overlaps have been initialized
    //
    if ( H_coeff_.size()!=nrep1_ )
    {
      cout << "Error: in - L2Reservoir::findChannelSolutions - interactions have not been correctly initialized" << endl;
      exit(0);
    }
    else if ( H_coeff_[0].size()!=nrep2_ )
    {
      cout << "Error: in - L2Reservoir::findChannelSolutions - interactions have not been correctly initialized" << endl;
      exit(0);
    }
    //
    // check that the system type has been set
    //
    if ( system_type_!=REAL_SYSTEM_TYPE && system_type_!=COMPLEX_SYSTEM_TYPE )
    {
      cout << "Error: in - L2Reservoir::findChannelSolutions - system_type_ have not been correctly initialized" << endl;
      exit(0);
    }
    //
    // allocate memory for channels and duals
    //
    if ( keepChannels_ )
    {
      channel_coeff_.resize(nrep1_);
      for ( int i=0 ; i<nrep1_ ; i++ ) channel_coeff_[i].resize(nrep2_);
    }
    dual_coeff_.resize(nrep1_);
    for ( int i=0 ; i<nrep1_ ; i++ ) dual_coeff_[i].resize(nrep2_);  
    //
    // determine if the system of equation is real or complex
    //
    bool real_system_matrix = system_type_==REAL_SYSTEM_TYPE && nrep1_==1 && nrep2_==1; 
    //
    // loop on the Q point sampling
    //
    for ( int iq2=0 ; iq2<nrep2_ ; iq2++ ) 
    for ( int iq1=0 ; iq1<nrep1_ ; iq1++ ) 
    {
      Precision noise_level=0.0;
      bool channel_ok=false;
      bool dual_ok=false;
      //
      // loop while we cannot get the channels
      //
      while ( ( !channel_ok || !dual_ok ) && noise_level<1E-6 )
      {
        //
        // get the corresponding phases
        //
        Precision phi1=iq1*TWO_PI/nrep1_;
        Precision phi2=iq2*TWO_PI/nrep2_;
        //
        // increase progressively the level of noise
        // 
        channel_ok=false;
        while ( !channel_ok && noise_level<1E-6 )
        {
#ifdef TIMING 
          tmap_["1 - system folding"].start();
#endif
          //
          // fold the matrix system
          // 
          foldSystem_(energy,phi1,phi2,noise_level);
#ifdef TIMING 
          tmap_["1 - system folding"].stop();
#endif
#ifdef TIMING 
          tmap_["2 - setting current operator"].start();
#endif
          //
          // set the current operator
          //
          setCurrentOperator_();
#ifdef TIMING 
          tmap_["2 - setting current operator"].stop();
#endif
#ifdef TIMING 
          tmap_["3 - getting channels"].start();
#endif
          //
          // get channels
          //
          if ( real_folded_system_set_ )
          {
            channel_ok=getChannelsForRealSystem_();
          }
          else
          {
            channel_ok=getChannelsForComplexSystem_();
          }
#ifdef TIMING 
          tmap_["3 - getting channels"].stop();
#endif
          //
          // increase noise level
          //
          noise_level*=10.0;
          noise_level+=1E-10;
        }
        if ( channel_ok )
        {
          dual_ok=false;
#ifdef TIMING 
          tmap_["4 - orthonormalize channels"].start();
#endif
          //
          // orthonormalize periodic solutions according to probability current
          //
          orthonormalizeChannels_();
#ifdef TIMING 
          tmap_["4 - orthonormalize channels"].stop();
#endif
#ifdef TIMING 
          tmap_["5 - balance channels"].start();
#endif
          //
          // balance non propagative channels
          // 
          balanceChannels_();
#ifdef TIMING 
          tmap_["5 - balance channels"].stop();
#endif
#ifdef TIMING 
          tmap_["6 - sort channels"].start();
#endif
          //
          // sort the channels
          //
          sortsChannels_(iq1,iq2);
#ifdef TIMING 
          tmap_["6 - sort channels"].stop();
#endif
#ifdef TIMING 
          tmap_["7 - find duals"].start();
#endif
          //
          // find duals
          //
          if ( findDuals_(iq1,iq2) ) dual_ok=true;
#ifdef TIMING 
          tmap_["7 - find duals"].stop();
#endif
        }
      }
      //
      // check if we found the channels
      //
      if ( !channel_ok )
      {
        cout << "*** WARNING: could not converge channels for energy " << energy << " with q sampling (" << iq1 << "," << iq2 << ") ***" << endl;
        return false;
      }
      //
      // check if we found the channels
      //
      if ( !dual_ok )
      {
        cout << "*** WARNING: could not compute dual channels for energy " << energy << " with q sampling (" << iq1 << "," << iq2 << ") ***" << endl;
        return false;
      }
    }  
#ifdef TIMING 
    tmap_["8 - form channel index lists"].start();
#endif    
    //
    // count total number of channels by type
    //
    n_ev_=0;
    n_ex_=0;
    n_in_=0;
    n_out_=0;
    for ( int iq2=0 ; iq2<nrep2_ ; iq2++ ) 
    for ( int iq1=0 ; iq1<nrep1_ ; iq1++ ) 
    {
      n_ex_+=n_explosive_channels_[iq1][iq2];
      n_ev_+=n_evanescent_channels_[iq1][iq2];
      n_in_ +=n_incoming_channels_[iq1][iq2];
      n_out_+=n_outgoing_channels_[iq1][iq2];
    }
    //
    // check that the numbers correspond
    //
    for ( int i=0 ; i<nrep1_ ; i++ ) 
    for ( int j=0 ; j<nrep2_ ; j++ )
    {
      int n_ev=n_evanescent_channels_[i][j];
      int n_ex=n_explosive_channels_[i][j];
      int n_in=n_incoming_channels_[i][j];
      int n_out=n_outgoing_channels_[i][j];
      if (n_ev_!=n_ex_)
      {
        cout << "L2Reservoir::findChannelSolutions : explosive and evanescent channel numbers differ" << endl;
        return false; 
      }
      if (n_in_!=n_out_)
      {
        cout << "L2Reservoir::findChannelSolutions : incoming and outgoing channel numbers differ" << endl;
        return false; 
      }
      if ( n_ev_+n_ex_+n_in_+n_out_ != 2*global_indices_.size()/3 )
      {
        cout << "L2Reservoir::findChannelSolutions : lost " << global_indices_.size()-(n_ev_+n_ex_+n_in_+n_out_) << " channel(s) during the sort" << endl;
        return false; 
      }
    }
#ifdef TIMING 
      tmap_["8 - form channel index lists"].stop();
#endif    
  }
  //
  // indicate that all went well
  //
  return true;    
}

template <class Precision>
void L2Reservoir<Precision>::synchronizeChannelsIndices(void)
{    
  //
  // check that channels are set
  //
  if ( !flag_channel_set )
  {
    cout << "Error: trying to synchronize channels before calling L2Reservoir::findChannelSolutions, detected in - L2Reservoir::synchronizeChannelsIndices -" << endl;
    exit(0);
  }
  //
  // fast return if possible
  //
  if ( flag_channel_sync )
  {
    return;
  }
  //
  // get reference of base class attribute
  //
  Context               &context_=this->context_;
  int                   &my_proc_=this->my_proc_;
  int                   &n_procs_=this->n_procs_;
  vector<int>           &global_indices_=this->global_indices_;   
  map<int,vector<int> > &local_indices_=this->local_indices_;      
  vector<int>           &explosive_channel_indices_=this->explosive_channel_indices_;   
  vector<int>           &evanescent_channel_indices_=this->evanescent_channel_indices_; 
  vector<int>           &incoming_channel_indices_=this->incoming_channel_indices_;     
  vector<int>           &outgoing_channel_indices_=this->outgoing_channel_indices_;   
  //
  // form index lists
  //
  if ( my_proc_==working_proc_ )
  {
    //
    // form an index list by stacking all the local_indices
    //
    vector<int> ordered_domain_indices;
    for ( map<int,vector<int> >::iterator it=local_indices_.begin() , stop=local_indices_.end() ; it!=stop ; it++ )
    {
      ordered_domain_indices.insert(ordered_domain_indices.end(),it->second.begin(),it->second.end());
    }
    //
    // reset index lists
    //
    explosive_channel_indices_.resize(0);
    evanescent_channel_indices_.resize(0);
    incoming_channel_indices_.resize(0);
    outgoing_channel_indices_.resize(0);
    //
    // loop over q sampling
    //
    int  istate=0; 
    for ( int iq2=0 ; iq2<nrep2_ ; iq2++ ) 
    for ( int iq1=0 ; iq1<nrep1_ ; iq1++ ) 
    {
      for ( int i=0 ; i<n_explosive_channels_[iq1][iq2] ; i++ , istate++ ) explosive_channel_indices_.push_back(ordered_domain_indices[istate]);
      for ( int i=0 ; i<n_evanescent_channels_[iq1][iq2] ; i++ , istate++ ) evanescent_channel_indices_.push_back(ordered_domain_indices[istate]);
      for ( int i=0 ; i<n_incoming_channels_[iq1][iq2] ; i++ , istate++ ) incoming_channel_indices_.push_back(ordered_domain_indices[istate]);
      for ( int i=0 ; i<n_outgoing_channels_[iq1][iq2] ; i++ , istate++ ) outgoing_channel_indices_.push_back(ordered_domain_indices[istate]);
    }
  }
  //
  // broadcast indices for the different types of channel
  //
#ifdef WITH_MPI
  if ( n_procs_>1 )
  {
    if ( my_proc_==working_proc_ )
    {
      //
      // store info on indices
      //
      vector<int> buffer;
      buffer.push_back(n_ex_);
      buffer.push_back(n_ev_);
      buffer.push_back(n_in_);
      buffer.push_back(n_out_);
      buffer.insert(buffer.end(),explosive_channel_indices_.begin(),explosive_channel_indices_.end());
      buffer.insert(buffer.end(),evanescent_channel_indices_.begin(),evanescent_channel_indices_.end());
      buffer.insert(buffer.end(),incoming_channel_indices_.begin(),incoming_channel_indices_.end());
      buffer.insert(buffer.end(),outgoing_channel_indices_.begin(),outgoing_channel_indices_.end());
      //
      // broadcast indices
      //
      MPI_Bcast(&buffer[0],buffer.size(),MPI_INT,working_proc_,context_.comm());
    }
    else
    {
      //
      // receive indices
      //
      vector<int> buffer(2*global_indices_.size()/3+4);
      MPI_Bcast(&buffer[0],buffer.size(),MPI_INT,working_proc_,context_.comm());
      //
      // extract indices
      //
      n_ex_=buffer[0];
      n_ev_=buffer[1];
      n_in_=buffer[2];
      n_out_=buffer[3];
      explosive_channel_indices_.resize(n_ex_);
      evanescent_channel_indices_.resize(n_ev_);
      incoming_channel_indices_.resize(n_in_);
      outgoing_channel_indices_.resize(n_out_);
      int ibuff=4;
      for ( int i=0 ; i<n_ex_  ; i++ , ibuff++ ) explosive_channel_indices_[i]=buffer[ibuff];
      for ( int i=0 ; i<n_ev_  ; i++ , ibuff++ ) evanescent_channel_indices_[i]=buffer[ibuff];
      for ( int i=0 ; i<n_in_  ; i++ , ibuff++ ) incoming_channel_indices_[i]=buffer[ibuff];
      for ( int i=0 ; i<n_out_ ; i++ , ibuff++ ) outgoing_channel_indices_[i]=buffer[ibuff];
    }
  }
#endif      
  //
  // indicate that channels are synchronized now
  //  
  flag_channel_sync=true;
  //
  // return
  //
  return;
}

template <class Precision>
void L2Reservoir<Precision>::printChannels(string comment)
{
  //
  // dimensions
  //
  int size_folded=cell_indices_[0][0].size();
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // if this is the proc containing the reservoir
  //
  if ( working_proc_==this->my_proc_ )
  {
    //
    // get domain indices
    //
    vector<int> domain_indices;
    domain_indices.insert(domain_indices.end(),cell_indices_[0][0].begin(),cell_indices_[0][0].begin()+LDB);
    //  
    // print header
    //
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "L2Reservoir::printChannels : " << comment << endl << endl;
    //
    // loop on q sampling
    //
    for ( int i=0 ; i<nrep1_ ; i++ ) 
    for ( int j=0 ; j<nrep2_ ; j++ )
    {
      cout << "*******************************" << endl;
      cout << "* unit cell repetition (" << i << "," << j << "):" << endl;
      cout << "*******************************" << endl << endl;
      //
      // print channels
      //
      cout << "  reduced chanels:" << endl;
      cout << "  ----------------" << endl;
      //
      // if we have the channels
      //
      if ( channel_coeff_.size()==nrep1_ )
      {
        int istart=0;
        while (istart<LDB)
        {
          //
          // print header for this subset of vector
          //
          cout << "vectors: "; 
          //
          // adjust spacing for first vector index
          //
          if (istart<10) cout.width(11);
          else if (istart<100) cout.width(10);
          else if (istart<1000) cout.width(9);
          //
          // print vector indices
          //
          for ( int ivector=istart ; ivector<LDB && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            //
            // print type of channel
            //
            if      ( ivector<n_explosive_channels_[i][j] ) 
              cout << "(ex  " << ivector << ")";
            else if ( ivector<n_explosive_channels_[i][j]+n_evanescent_channels_[i][j] ) 
              cout << "(ev  " << ivector << ")";
            else if ( ivector<n_explosive_channels_[i][j]+n_evanescent_channels_[i][j]+n_incoming_channels_[i][j] ) 
              cout << "(in  " << ivector << ")";
            else
              cout << "(out " << ivector << ")";
            //
            // adjust spacing for next vector index
            //
            if (ivector<9) cout.width(23);
            else if (ivector<99) cout.width(22);
            else if (ivector<999) cout.width(21);
          }
          //
          // flush
          //
          cout << endl;
          // 
          // print coefficients
          //
          for ( int icoeff=0 ; icoeff<LDB ; icoeff++ )
          {
            //
            // set spacing for coefficient index
            //
            if (domain_indices[icoeff]<10) cout.width(4);
            else if (domain_indices[icoeff]<100) cout.width(3);
            else if (domain_indices[icoeff]<1000) cout.width(2);
            else if (domain_indices[icoeff]<10000) cout.width(1);
            //
            // print coefficient index
            //
            cout << "(" << domain_indices[icoeff] << ") :";
            cout.precision(5);
            for ( int ivector=istart ; ivector<LDB && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
            {
              cout.width(14);
              cout << scientific << channel_coeff_[i][j][icoeff+ivector*LDB].real();
              if ( imag(channel_coeff_[i][j][icoeff+ivector*LDB])<0.0 )
              {
                cout.width(12);
              }
              else
              {
                cout << "+";
                cout.width(11);
              }
              cout << scientific << imag(channel_coeff_[i][j][icoeff+ivector*LDB]) << "*%i" ;
            } 
            cout << endl;
          }  
          //
          // increment istart
          //
          istart+=N_VECTORS_PER_PAGE;
        } 
        cout << endl;
      }   
      //
      // print duals
      //
      cout << "  reduced duals:" << endl;
      cout << "  --------------" << endl;
      //
      int istart=0;
      while (istart<LDB)
      {
        //
        // print header for this subset of vector
        //
        cout << "vectors: "; 
        //
        // adjust spacing for first vector index
        //
        if (istart<10) cout.width(11);
        else if (istart<100) cout.width(10);
        else if (istart<1000) cout.width(9);
        //
        // print vector indices
        //
        for ( int ivector=istart ; ivector<LDB && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
        {
          //
          // print type of channel
          //
          if      ( ivector<n_explosive_channels_[i][j] ) 
            cout << "(ex  " << ivector << ")";
          else if ( ivector<n_explosive_channels_[i][j]+n_evanescent_channels_[i][j] ) 
            cout << "(ev  " << ivector << ")";
          else if ( ivector<n_explosive_channels_[i][j]+n_evanescent_channels_[i][j]+n_incoming_channels_[i][j] ) 
            cout << "(in  " << ivector << ")";
          else
            cout << "(out " << ivector << ")";
          //
          // adjust spacing for next vector index
          //
          if (ivector<9) cout.width(23);
          else if (ivector<99) cout.width(22);
          else if (ivector<999) cout.width(21);
        }
        //
        // flush
        //
        cout << endl;
        // 
        // print coefficients
        //
        for ( int icoeff=0 ; icoeff<LDB ; icoeff++ )
        {
          //
          // set spacing for coefficient index
          //
          if (domain_indices[icoeff]<10) cout.width(4);
          else if (domain_indices[icoeff]<100) cout.width(3);
          else if (domain_indices[icoeff]<1000) cout.width(2);
          else if (domain_indices[icoeff]<10000) cout.width(1);
          //
          // print coefficient index
          //
          cout << "(" << domain_indices[icoeff] << ") :";
          cout.precision(5);
          for ( int ivector=istart ; ivector<LDB && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            cout.width(14);
            cout << scientific << dual_coeff_[i][j][icoeff+ivector*LDB].real();
            if ( imag(dual_coeff_[i][j][icoeff+ivector*LDB])<0.0 )
            {
              cout.width(12);
            }
            else
            {
              cout << "+";
              cout.width(11);
            }
            cout << scientific << imag(dual_coeff_[i][j][icoeff+ivector*LDB]) << "*%i" ;
          } 
          cout << endl;
        }  
        //
        // increment istart
        //
        istart+=N_VECTORS_PER_PAGE;
      } 
      cout << endl;
    }
  }
}

template <class Precision>
void L2Reservoir<Precision>::printInteractions(string comment)
{
  //
  // dimensions
  //
  int size_folded=cell_indices_[0][0].size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // if this is the proc containing the reservoir
  //
  if ( working_proc_==this->my_proc_ )
  {
    //
    // get range indices
    //
    vector<int> range_indices;
    range_indices.insert(range_indices.end(),cell_indices_[0][0].begin()+size_center,cell_indices_[0][0].begin()+2*size_center);
    //  
    // print header
    //
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "L2Reservoir::printInteractions : " << comment << endl << endl;
    //
    // loop on q sampling
    //
    for ( int i=0 ; i<nrep1_ ; i++ ) 
    for ( int j=0 ; j<nrep2_ ; j++ )
    {
      cout << "*******************************" << endl;
      cout << "* unit cell repetition (" << i << "," << j << "):" << endl;
      cout << "*******************************" << endl << endl;
      //
      // print hamiltonian
      //
      cout << "  Interactions:" << endl;
      cout << "  -------------" << endl;
      //
      // get the domain indices for this repetition
      //
      vector<int> domain_indices=cell_indices_[i][j];
      int istart=0;
      while (istart<domain_indices.size())
      {
        //
        // print header for this subset of vector
        //
        cout << "   r/d   "; 
        //
        // adjust spacing for first vector index
        //
        if (domain_indices[istart]<10) cout.width(13);
        else if (domain_indices[istart]<100) cout.width(12);
        else if (domain_indices[istart]<1000) cout.width(11);
        //
        // print vector indices
        //
        for ( int ivector=istart ; ivector<domain_indices.size() && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
        {
          cout << "(" << domain_indices[ivector] << ")";
          //
          // adjust spacing for next vector index
          //
          if (domain_indices[ivector]<9) cout.width(27);
          else if (domain_indices[ivector]<99) cout.width(26);
          else if (domain_indices[ivector]<999) cout.width(25);
        }
        //
        // flush
        //
        cout << endl;
        // 
        // print coefficients
        //
        for ( int irow=0 ; irow<range_indices.size() ; irow++ )
        {
          //
          // set spacing for coefficient index
          //
          if (range_indices[irow]<10) cout.width(4);
          else if (range_indices[irow]<100) cout.width(3);
          else if (range_indices[irow]<1000) cout.width(2);
          else if (range_indices[irow]<10000) cout.width(1);
          //
          // print coefficient index
          //
          cout << "(" << range_indices[irow] << ") :";
          cout.precision(5);
          for ( int icol=istart ; icol<domain_indices.size() && icol-istart<N_VECTORS_PER_PAGE ; icol++ )
          {
            cout.width(14);
            cout << scientific << H_coeff_[i][j][icol+irow*size_folded].real();
            if ( H_coeff_[i][j][icol+irow*size_folded].imag()<0.0 )
            {
              cout.width(12);
            }
            else
            {
              cout << "+";
              cout.width(11);
            }
            cout << scientific << H_coeff_[i][j][icol+irow*size_folded].imag() << "*%i" ;
          } 
          cout << endl;
        }  
        //
        // increment istart
        //
        istart+=N_VECTORS_PER_PAGE;
      }
      cout << endl;
      //
      // print overlaps if any
      //
      if ( S_coeff_.size()==nrep1_ && S_coeff_[0].size()==nrep2_ )
      {
        cout << "  Overlaps:" << endl;
        cout << "  ---------" << endl;
        //
        // get the domain indices for this repetition
        //
        vector<int> domain_indices=cell_indices_[i][j];
        int istart=0;
        while (istart<domain_indices.size())
        {
          //
          // print header for this subset of vector
          //
          cout << "   r/d   "; 
          //
          // adjust spacing for first vector index
          //
          if (domain_indices[istart]<10) cout.width(13);
          else if (domain_indices[istart]<100) cout.width(12);
          else if (domain_indices[istart]<1000) cout.width(11);
          //
          // print vector indices
          //
          for ( int ivector=istart ; ivector<domain_indices.size() && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            cout << "(" << domain_indices[ivector] << ")";
            //
            // adjust spacing for next vector index
            //
            if (domain_indices[ivector]<9) cout.width(27);
            else if (domain_indices[ivector]<99) cout.width(26);
            else if (domain_indices[ivector]<999) cout.width(25);
          }
          //
          // flush
          //
          cout << endl;
          // 
          // print coefficients
          //
          for ( int irow=0 ; irow<range_indices.size() ; irow++ )
          {
            //
            // set spacing for coefficient index
            //
            if (range_indices[irow]<10) cout.width(4);
            else if (range_indices[irow]<100) cout.width(3);
            else if (range_indices[irow]<1000) cout.width(2);
            else if (range_indices[irow]<10000) cout.width(1);
            //
            // print coefficient index
            //
            cout << "(" << range_indices[irow] << ") :";
            cout.precision(5);
            for ( int icol=istart ; icol<domain_indices.size() && icol-istart<N_VECTORS_PER_PAGE ; icol++ )
            {
              cout.width(14);
              cout << scientific << S_coeff_[i][j][icol+irow*size_folded].real();
              if ( S_coeff_[i][j][icol+irow*size_folded].imag()<0.0 )
              {
                cout.width(12);
              }
              else
              {
                cout << "+";
                cout.width(11);
              }
              cout << scientific << S_coeff_[i][j][icol+irow*size_folded].imag() << "*%i" ;
            } 
            cout << endl;
          }  
          //
          // increment istart
          //
          istart+=N_VECTORS_PER_PAGE;
        }
        cout << endl;
      } 
    }
    cout << "-------------------------------------------------------------------------" << endl;
    cout << endl;
  }
}

template <class Precision>
void L2Reservoir<Precision>::printComplexArrayInFile_(string fileName, string comment, complex<Precision> *A, int LDA, int count_from, int count_to) 
{
  //
  // open file
  //
  ofstream filestr;
  filestr.open (fileName.data());
  //
  // insert header
  //
  filestr << comment << "=[" << endl;
  //
  // loop on rows
  //
  filestr.precision(16);
  for ( int i_to=0 ; i_to<count_to ; i_to++ )
  {
    for ( int i_from=0 ; i_from<count_from ; i_from++ )
    {
      filestr.width(14);
      filestr << scientific << A[i_from*count_to+i_to].real();
      if ( A[i_from*count_to+i_to].imag()<0.0 )
      {
        filestr.width(12);
      }
      else
      {
        filestr << "+";
        filestr.width(11);
      }
      filestr << scientific << A[i_from*count_to+i_to].imag() << "*%i ..." << endl;
    }
    filestr << ";" << endl;
  }
  filestr << "]" << endl;
  filestr.close(); 
}

template <class Precision>
void L2Reservoir<Precision>::randomize(bool complex_reservoir)
{
  //
  // dimensions
  //
  int size_folded=cell_indices_[0][0].size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // make sure that at least H_coeff_ has been allocated
  //
  if ( H_coeff_.size()==0 || H_coeff_[0].size()==0 || H_coeff_[0][0].size()==0 )
  {
    cout << "Error: interaction coefficient array is not yet allocated in - L2Reservoir::randomize" << endl;
  }
  //
  // randomize interactions
  //
  for ( int i=0 ; i<nrep1_ ; i++ ) 
  for ( int j=0 ; j<nrep2_ ; j++ )
  {
    if ( complex_reservoir )
    for ( int k=0 , stop=H_coeff_[i][j].size() ; k<stop ; k++ ) H_coeff_[i][j][k]=complex<Precision>(rand()/(Precision)RAND_MAX,rand()/(Precision)RAND_MAX);
    else
    for ( int k=0 , stop=H_coeff_[i][j].size() ; k<stop ; k++ ) H_coeff_[i][j][k]=rand()/(Precision)RAND_MAX;
  }
  //
  // randomize overlaps if present
  //
  if ( S_coeff_.size()==nrep1_ && S_coeff_[0].size()==nrep2_ )
  {
    for ( int i=0 ; i<nrep1_ ; i++ ) 
    for ( int j=0 ; j<nrep2_ ; j++ )
    {
      //
      // randomize coeff
      //
      if ( complex_reservoir )
      for ( int k=0 , stop=H_coeff_[i][j].size() ; k<stop ; k++ ) S_coeff_[i][j][k]=complex<Precision>(rand()/(Precision)RAND_MAX,rand()/(Precision)RAND_MAX);
      else
      for ( int k=0 , stop=H_coeff_[i][j].size() ; k<stop ; k++ ) S_coeff_[i][j][k]=rand()/(Precision)RAND_MAX;
      // 
      // renorm
      //
      for ( int k=0 , stop=H_coeff_[i][j].size() ; k<stop ; k++ ) S_coeff_[i][j][k]/=(Precision)10.0;
    }
    //
    // set diagonal
    //
    for ( int k=0 ; k<size_center ; k++ ) S_coeff_[0][0][(size_folded+1)*k+size_center]=(Precision)1.0;
  }
  //
  // symetrize interactions
  //
  for ( int i=0 ; i<nrep1_ ; i++ ) 
  for ( int j=0 ; j<nrep2_ ; j++ )
  {
    //
    // find the corresponding symmetric indices
    //
    int i_=(nrep1_-i)%nrep1_;
    int j_=(nrep2_-j)%nrep2_;
    //
    // check the interactions
    //
    for ( int irow=0 ; irow<size_center ; irow++ )
    for ( int icol=0 ; icol<size_center ; icol++ )
    {
      //
      // get coeffs
      //
      complex<Precision> coeff_1 =H_coeff_[i][j][irow+icol*size_folded];
      complex<Precision> coeff_1_=H_coeff_[i_][j_][2*size_center+icol+irow*size_folded];
      //
      complex<Precision> coeff_2 =H_coeff_[i_][j_][irow+icol*size_folded];
      complex<Precision> coeff_2_=H_coeff_[i][j][2*size_center+icol+irow*size_folded];
      //
      complex<Precision> coeff_3 =H_coeff_[i_][j_][size_center+irow+icol*size_folded];
      complex<Precision> coeff_3_=H_coeff_[i][j][size_center+icol+irow*size_folded];
      //
      // symmetrize
      //
      complex<Precision> coeff_tmp;
      //
      coeff_tmp=(coeff_1+conj(coeff_1_))/(Precision)2.0;
      H_coeff_[i][j][irow+icol*size_folded]=coeff_tmp;
      H_coeff_[i_][j_][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
      //
      coeff_tmp=(coeff_2+conj(coeff_2_))/(Precision)2.0;
      H_coeff_[i_][j_][irow+icol*size_folded]=coeff_tmp;
      H_coeff_[i][j][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
      //
      coeff_tmp=(coeff_3+conj(coeff_3_))/(Precision)2.0;
      H_coeff_[i_][j_][size_center+irow+icol*size_folded]=coeff_tmp;
      H_coeff_[i][j][size_center+icol+irow*size_folded]=conj(coeff_tmp);
    }
    //
    // if there are overlaps
    //
    if ( S_coeff_.size()==nrep1_ && S_coeff_[0].size()==nrep2_ )
    {
      for ( int irow=0 ; irow<size_center ; irow++ )
      for ( int icol=0 ; icol<size_center ; icol++ )
      {
        //
        // get coeffs
        //
        complex<Precision> coeff_1 =S_coeff_[i][j][irow+icol*size_folded];
        complex<Precision> coeff_1_=S_coeff_[i_][j_][2*size_center+icol+irow*size_folded];
        //
        complex<Precision> coeff_2 =S_coeff_[i_][j_][irow+icol*size_folded];
        complex<Precision> coeff_2_=S_coeff_[i][j][2*size_center+icol+irow*size_folded];
        //
        complex<Precision> coeff_3 =S_coeff_[i_][j_][size_center+irow+icol*size_folded];
        complex<Precision> coeff_3_=S_coeff_[i][j][size_center+icol+irow*size_folded];
        //
        // symmetrize
        //
        complex<Precision> coeff_tmp;
        //
        coeff_tmp=(coeff_1+conj(coeff_1_))/(Precision)2.0;
        S_coeff_[i][j][irow+icol*size_folded]=coeff_tmp;
        S_coeff_[i_][j_][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
        //
        coeff_tmp=(coeff_2+conj(coeff_2_))/(Precision)2.0;
        S_coeff_[i_][j_][irow+icol*size_folded]=coeff_tmp;
        S_coeff_[i][j][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
        //
        coeff_tmp=(coeff_3+conj(coeff_3_))/(Precision)2.0;
        S_coeff_[i_][j_][size_center+irow+icol*size_folded]=coeff_tmp;
        S_coeff_[i][j][size_center+icol+irow*size_folded]=conj(coeff_tmp);
      }
    } 
  }
}

template <class Precision>
bool L2Reservoir<Precision>::checkSymmetry(void)
{
  //
  // dimensions
  //
  int size_folded=cell_indices_[0][0].size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // caracterisation of the folded system
  //
  Precision norm_H=0.0; 
  Precision norm_S=0.0; 
  Precision disbalance_H=0.0;
  Precision disbalance_S=0.0;
  //
  // make sure that at least H_coeff_ has been allocated
  //
  if ( H_coeff_.size()==0 || H_coeff_[0].size()==0 || H_coeff_[0][0].size()==0 )
  {
    cout << "Error: interaction coefficient array is not yet allocated in - L2Reservoir::randomize" << endl;
  }
  //
  // compute frobenius norm of the reservoirs matrices
  //
  for ( int i=0 ; i<nrep1_ ; i++ ) 
  for ( int j=0 ; j<nrep2_ ; j++ )
  {
    // 
    // interaction first
    //
    for ( int k=0 , stop=H_coeff_[i][j].size() ; k<stop ; k++ ) norm_H+=norm(H_coeff_[i][j][k]);
    //
    // overlaps if necessary
    //
    if ( S_coeff_.size()==nrep1_ && S_coeff_[0].size()==nrep2_ )
      for ( int k=0 , stop=S_coeff_[i][j].size() ; k<stop ; k++ ) norm_S+=norm(S_coeff_[i][j][k]);
  }
  norm_H=sqrt(norm_H);
  norm_S=sqrt(norm_S);
  //
  // check symmetry of interactions and overlaps
  //
  for ( int i=0 ; i<nrep1_ ; i++ ) 
  for ( int j=0 ; j<nrep2_ ; j++ )
  {
    //
    // find the corresponding symmetric indices
    //
    int i_=(nrep1_-i)%nrep1_;
    int j_=(nrep2_-j)%nrep2_;
    //
    // check the interactions
    //
    for ( int irow=0 ; irow<size_center ; irow++ )
    for ( int icol=0 ; icol<size_center ; icol++ )
    {
      //
      // get coeffs
      //
      complex<Precision> coeff_1 =H_coeff_[i][j][irow+icol*size_folded];
      complex<Precision> coeff_1_=H_coeff_[i_][j_][2*size_center+icol+irow*size_folded];
      //
      complex<Precision> coeff_2 =H_coeff_[i_][j_][irow+icol*size_folded];
      complex<Precision> coeff_2_=H_coeff_[i][j][2*size_center+icol+irow*size_folded];
      //
      complex<Precision> coeff_3 =H_coeff_[i_][j_][size_center+irow+icol*size_folded];
      complex<Precision> coeff_3_=H_coeff_[i][j][size_center+icol+irow*size_folded];
      //
      // compute percentage of disbalance
      //
      if ( sqrt(norm(coeff_1))/norm_H>1E-10 || sqrt(norm(coeff_1_))/norm_H>1E-10 )
      {
        disbalance_H = max( disbalance_H , (Precision)2.0*norm(coeff_1-conj(coeff_1_))/norm(coeff_1+conj(coeff_1_)) );
      }
      if ( sqrt(norm(coeff_2))/norm_H>1E-10 || sqrt(norm(coeff_2_))/norm_H>1E-10 )
      {
        disbalance_H = max( disbalance_H , (Precision)2.0*norm(coeff_2-conj(coeff_2_))/norm(coeff_2+conj(coeff_2_)) );
      }
      if ( sqrt(norm(coeff_3))/norm_H>1E-10 || sqrt(norm(coeff_3_))/norm_H>1E-10 )
      {
        disbalance_H = max( disbalance_H , (Precision)2.0*norm(coeff_3-conj(coeff_3_))/norm(coeff_3+conj(coeff_3_)) );
      }
    }
    //
    // if there are overlaps
    //
    if ( S_coeff_.size()==nrep1_ && S_coeff_[0].size()==nrep2_ )
    {
      for ( int irow=0 ; irow<size_center ; irow++ )
      for ( int icol=0 ; icol<size_center ; icol++ )
      {
        //
        // get coeffs
        //
        complex<Precision> coeff_1 =S_coeff_[i][j][irow+icol*size_folded];
        complex<Precision> coeff_1_=S_coeff_[i_][j_][2*size_center+icol+irow*size_folded];
        //
        complex<Precision> coeff_2 =S_coeff_[i_][j_][irow+icol*size_folded];
        complex<Precision> coeff_2_=S_coeff_[i][j][2*size_center+icol+irow*size_folded];
        //
        complex<Precision> coeff_3 =S_coeff_[i_][j_][size_center+irow+icol*size_folded];
        complex<Precision> coeff_3_=S_coeff_[i][j][size_center+icol+irow*size_folded];
        //
        // compute percentage of disbalance
        //
        if ( sqrt(norm(coeff_1))/norm_S>1E-10 || sqrt(norm(coeff_1_))/norm_S>1E-10 )
        {
          disbalance_S = max( disbalance_S , (Precision)2.0*norm(coeff_1-conj(coeff_1_))/norm(coeff_1+conj(coeff_1_)) );
        }
        if ( sqrt(norm(coeff_2))/norm_S>1E-10 || sqrt(norm(coeff_2_))/norm_S>1E-10 )
        {
          disbalance_S = max( disbalance_S , (Precision)2.0*norm(coeff_2-conj(coeff_2_))/norm(coeff_2+conj(coeff_2_)) );
        }
        if ( sqrt(norm(coeff_3))/norm_S>1E-10 || sqrt(norm(coeff_3_))/norm_S>1E-10 )
        {
          disbalance_S = max( disbalance_S , (Precision)2.0*norm(coeff_3-conj(coeff_3_))/norm(coeff_3+conj(coeff_3_)) );
        }
      }
    } 
  }
  //
  // inform in case of problem
  // 
  if ( disbalance_H>1E-4 )
  {
    cout << endl << endl; 
    cout << " *********************************************************************" << endl;
    cout << " WARNING !!! Important disbalance found in reservoir interactions:" << endl;
    cout << "       reservoir Frobenius norm: " << norm_H << endl;
    cout << "       reservoir maximun disbalance: " << disbalance_H*100 << "%" << endl;
    cout << " *********************************************************************" << endl; 
    cout << endl << endl;
    //
    // indicate a problem
    //
    return false;
  }
  if ( disbalance_S>1E-4 )
  {
    cout << endl << endl; 
    cout << " *********************************************************************" << endl;
    cout << " WARNING !!! Important disbalance found in reservoir interactions:" << endl;
    cout << "       reservoir Frobenius norm: " << norm_S << endl;
    cout << "       reservoir maximun disbalance: " << disbalance_S*100 << "%" << endl;
    cout << " *********************************************************************" << endl; 
    cout << endl << endl;
    //
    // indicate a problem
    //
    return false;
  }
  //
  // indicate that everithing is fine
  //
  return true;
}


template <class Precision>
void L2Reservoir<Precision>::symmetrize(void)
{
  //
  // dimensions
  //
  int size_folded=cell_indices_[0][0].size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // make sure that at least H_coeff_ has been allocated
  //
  if ( H_coeff_.size()==0 || H_coeff_[0].size()==0 || H_coeff_[0][0].size()==0 )
  {
    cout << "Error: interaction coefficient array is not yet allocated in - L2Reservoir::randomize" << endl;
  }
  //
  // symetrize interactions
  //
  for ( int i=0 ; i<nrep1_ ; i++ ) 
  for ( int j=0 ; j<nrep2_ ; j++ )
  {
    //
    // find the corresponding symmetric indices
    //
    int i_=(nrep1_-i)%nrep1_;
    int j_=(nrep2_-j)%nrep2_;
    //
    // check the interactions
    //
    for ( int irow=0 ; irow<size_center ; irow++ )
    for ( int icol=0 ; icol<size_center ; icol++ )
    {
      //
      // get coeffs
      //
      complex<Precision> coeff_1 =H_coeff_[i][j][irow+icol*size_folded];
      complex<Precision> coeff_1_=H_coeff_[i_][j_][2*size_center+icol+irow*size_folded];
      //
      complex<Precision> coeff_2 =H_coeff_[i_][j_][irow+icol*size_folded];
      complex<Precision> coeff_2_=H_coeff_[i][j][2*size_center+icol+irow*size_folded];
      //
      complex<Precision> coeff_3 =H_coeff_[i_][j_][size_center+irow+icol*size_folded];
      complex<Precision> coeff_3_=H_coeff_[i][j][size_center+icol+irow*size_folded];
      //
      // symmetrize
      //
      complex<Precision> coeff_tmp;
      //
      coeff_tmp=(coeff_1+conj(coeff_1_))/(Precision)2.0;
      H_coeff_[i][j][irow+icol*size_folded]=coeff_tmp;
      H_coeff_[i_][j_][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
      //
      coeff_tmp=(coeff_2+conj(coeff_2_))/(Precision)2.0;
      H_coeff_[i_][j_][irow+icol*size_folded]=coeff_tmp;
      H_coeff_[i][j][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
      //
      coeff_tmp=(coeff_3+conj(coeff_3_))/(Precision)2.0;
      H_coeff_[i_][j_][size_center+irow+icol*size_folded]=coeff_tmp;
      H_coeff_[i][j][size_center+icol+irow*size_folded]=conj(coeff_tmp);
    }
    //
    // if there are overlaps
    //
    if ( S_coeff_.size()==nrep1_ && S_coeff_[0].size()==nrep2_ )
    {
      for ( int irow=0 ; irow<size_center ; irow++ )
      for ( int icol=0 ; icol<size_center ; icol++ )
      {
        //
        // get coeffs
        //
        complex<Precision> coeff_1 =S_coeff_[i][j][irow+icol*size_folded];
        complex<Precision> coeff_1_=S_coeff_[i_][j_][2*size_center+icol+irow*size_folded];
        //
        complex<Precision> coeff_2 =S_coeff_[i_][j_][irow+icol*size_folded];
        complex<Precision> coeff_2_=S_coeff_[i][j][2*size_center+icol+irow*size_folded];
        //
        complex<Precision> coeff_3 =S_coeff_[i_][j_][size_center+irow+icol*size_folded];
        complex<Precision> coeff_3_=S_coeff_[i][j][size_center+icol+irow*size_folded];
        //
        // symmetrize
        //
        complex<Precision> coeff_tmp;
        //
        coeff_tmp=(coeff_1+conj(coeff_1_))/(Precision)2.0;
        S_coeff_[i][j][irow+icol*size_folded]=coeff_tmp;
        S_coeff_[i_][j_][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
        //
        coeff_tmp=(coeff_2+conj(coeff_2_))/(Precision)2.0;
        S_coeff_[i_][j_][irow+icol*size_folded]=coeff_tmp;
        S_coeff_[i][j][2*size_center+icol+irow*size_folded]=conj(coeff_tmp);
        //
        coeff_tmp=(coeff_3+conj(coeff_3_))/(Precision)2.0;
        S_coeff_[i_][j_][size_center+irow+icol*size_folded]=coeff_tmp;
        S_coeff_[i][j][size_center+icol+irow*size_folded]=conj(coeff_tmp);
      }
    } 
  }
}

template <class Precision>
void L2Reservoir<Precision>::foldSystem_(Precision E, Precision phi1, Precision phi2, Precision noise_level)
{
  //
  // reset flags
  //
  real_folded_system_set_=false;
  bool only_real_phases=true;
  //
  // dimensions
  //
  int size_folded=active_folded_domain_.size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // caracterisation of the folded system
  //
  Precision norm_coeff_max=0.0;
  Precision system_norm=0.0; 
  Precision system_disbalance=0.0;
  //
  // fill the folded system matrix
  //
  if ( system_type_==REAL_SYSTEM_TYPE && nrep1_==1 && nrep2_==1 ) 
  {
    //
    // allocate real array.
    // we allocate more because the Q matrix will be stored in this array.
    //
    real_folded_system_.resize(size_folded*size_folded); 
    //
    // if there are overlaps
    //
    if (  S_coeff_.size()==nrep1_ )
    {    
      //
      // fill system matrix
      //
      for ( int k=0 , stop=H_coeff_[0][0].size() ; k<stop ; k++ ) real_folded_system_[k]=real(H_coeff_[0][0][k]-E*S_coeff_[0][0][k]);
    }
    // 
    // if the overlap matrix is unitary
    //
    else
    {
      //
      // copy interaction coefficients
      //
      for ( int k=0 , stop=H_coeff_[0][0].size() ; k<stop ; k++ ) real_folded_system_[k]=real(H_coeff_[0][0][k]);
      //
      // remove E*overlaps
      //
      for ( int i_col=0 , i_row=istart_center_ ; i_col<size_center ; i_col++, i_row++ ) real_folded_system_[i_col*size_folded+i_row]-=E;
    }
    //
    // stabilize the system if necessary
    //
    if ( noise_level>0.0 )
    {
      //
      // get the maximum coefficient 
      //
      for ( int k=0 , stop=real_folded_system_.size() ; k<stop ; k++ ) 
      {
        Precision norm_coeff=(real_folded_system_[k]*real_folded_system_[k]);
        if ( norm_coeff>norm_coeff_max ) norm_coeff_max=norm_coeff;
      }
      //
      // noise level to stabilize the system
      //
      Precision noise=sqrt(norm_coeff_max)*noise_level;
      //
      // add noise to the center
      //
      for ( int row=0 , i_row=istart_center_ ; row<size_center ; row++, i_row++ )
      {
        for ( int col=0 , i_col=istart_center_ ; col<size_center ; col++, i_col++ )
        {
          //
          // get conjugated pair of indices 
          //
          Precision &coeffij=real_folded_system_[i_row+col*size_folded];
          Precision &coeffji=real_folded_system_[i_col+row*size_folded];
          //
          // stabilize => add real noise only to the central part,
          // this have less impact on the precision than adding noise
          // on the interaction parts.
          //
          Precision rand_noise=noise*(rand()-rand())/(Precision)RAND_MAX;
          coeffij+=rand_noise;
          coeffji+=rand_noise;
        }
      }
    }
    //
    // set flag
    //
    real_folded_system_set_=true;
  }
  //
  // if the system is complex
  //
  else
  {
    int size_array=size_folded*size_center;
    //
    // reinit the folded representation
    //
    complex_folded_system_.resize(size_folded*size_folded); 
    for ( int k=0 , stop=size_folded*size_folded ; k<stop ; k++ ) complex_folded_system_[k]=0.0;
    //
    // for each repetitions in the orthogonal directions
    // 
    for ( int irep2=0 ; irep2<nrep2_ ; irep2++ ) 
    for ( int irep1=0 ; irep1<nrep1_ ; irep1++ ) 
    {
      //
      // compute local phase
      //
      complex<Precision> local_phase = exp( complex<Precision>(0.0,irep1*phi1+irep2*phi2) );  
      complex<Precision> E_times_local_phase( E * local_phase );
      //
      // keep track of complex phases
      //
      if ( fabs(local_phase.imag())>1E-5 ) only_real_phases=false;
      //
      // if there are overlaps
      //
      if (  S_coeff_.size()==nrep1_ )
      {    
        //
        //  get pointers on the system 
        //
        complex<Precision> *__restrict__ H_ptr=&H_coeff_[irep1][irep2][0];
        complex<Precision> *__restrict__ S_ptr=&S_coeff_[irep1][irep2][0];
        complex<Precision> *__restrict__ F_ptr=&complex_folded_system_[0];
        complex<Precision> *stop_F=&complex_folded_system_[0]+size_array;
        //
        // fill system matrix
        //
        for ( ; F_ptr!=stop_F ; F_ptr++ , H_ptr++ , S_ptr++ ) (*F_ptr)+=conj(local_phase*(*H_ptr)-E_times_local_phase*(*S_ptr));
      }
      //
      // if there are no overlaps
      //
      else
      {
        //
        //  get pointers on the system 
        //
        complex<Precision> *__restrict__ H_ptr=&H_coeff_[irep1][irep2][0];
        complex<Precision> *__restrict__ F_ptr=&complex_folded_system_[0];
        complex<Precision> *stop_F=&complex_folded_system_[0]+size_array;
        //
        // add interactions
        //
        for ( ; F_ptr!=stop_F ; F_ptr++ , H_ptr++ ) (*F_ptr)+=conj(local_phase*(*H_ptr));
      }
    }
    //
    // remove overlaps if necessary
    // 
    if ( S_coeff_.size()==0 )
    {
      for ( int i_col=0 , i_row=istart_center_ ; i_col<size_center ; i_col++, i_row++ ) complex_folded_system_[i_col*size_folded+i_row]-=E;
    }   
    //
    // stabilize the system if necessary
    //
    if ( noise_level>0.0 )
    {
      //
      // get the maximum coefficient 
      //
      for ( int k=0 , stop=real_folded_system_.size() ; k<stop ; k++ ) 
      {
        Precision norm_coeff=norm(complex_folded_system_[k]);
        if ( norm_coeff>norm_coeff_max ) norm_coeff_max=norm_coeff;
      }
      //
      // noise level to stabilize the system
      //
      Precision noise=sqrt(norm_coeff_max)*noise_level;
      //
      // add noise to the center
      //
      for ( int row=0 , i_row=istart_center_ ; row<size_center ; row++, i_row++ )
      {
        for ( int col=0 , i_col=istart_center_ ; col<size_center ; col++, i_col++ )
        {
          //
          // get conjugated pair of indices 
          //
          complex<Precision> &coeffij=complex_folded_system_[i_row+col*size_folded];
          complex<Precision> &coeffji=complex_folded_system_[i_col+row*size_folded];
          //
          // stabilize => add real noise only to the central part,
          // this have less impact on the precision than adding noise
          // on the interaction parts.
          //
          complex<Precision> rand_noise=complex<Precision>(noise*(rand()-rand())/(Precision)RAND_MAX,
                                                           noise*(rand()-rand())/(Precision)RAND_MAX);
          coeffij+=rand_noise;
          coeffji+=conj(rand_noise);   
        }
      }
    }
  } 
  //
  // check if the system matrix is real and stored in the complex array
  //
  if ( ( nrep1_>1 || nrep2_>1 ) && system_type_==REAL_SYSTEM_TYPE && only_real_phases )
  {
    //
    // allocate real matrix
    //
    real_folded_system_.resize(size_folded*size_folded); 
    //
    // keep the coefficients in the real folded system
    //
    for ( int k=0 , stop=complex_folded_system_.size() ; k<stop ; k++ ) real_folded_system_[k]=complex_folded_system_[k].real();
    //
    // set flags
    //
    real_folded_system_set_=true;
    //
    // deallocate complex matrix
    //
    complex_folded_system_.resize(0);
  }  
  //
  // inform in case of problem
  // 
  if ( !flag_reduced_interactions_ && system_disbalance>1E-6 )
  {
    cout << endl << endl; 
    cout << " *********************************************************************" << endl;
    cout << " WARNING !!! Important disbalance found in reservoir interactions:" << endl;
    cout << "       reservoir Frobenius norm: " << system_norm << endl;
    cout << "       reservoir maximun disbalance: " << system_disbalance*100 << "% of coeff max" << endl;
    cout << " *********************************************************************" << endl; 
    cout << endl << endl;
  }  
}

template <class Precision>
void L2Reservoir<Precision>::setCurrentOperator_(void)
{
  //
  // dimensions
  //
  int size_folded=active_folded_domain_.size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // allocate memory for the current operator
  //
  current_operator_.resize(size_center*size_center); 
  //
  // reset current operator
  //
  for ( complex<Precision> *__restrict__ p=&current_operator_[0] , 
                           *stop=&current_operator_[0]+current_operator_.size() ; p!=stop ; p++ ) (*p)=0.0;
  //
  // form the operator measuring the current from the surface of the reservoir to the center 
  //
  if ( system_type_==REAL_SYSTEM_TYPE && nrep1_==1 && nrep2_==1 )
  {
    //
    // copy coefficients from the folded hamiltonian
    //
    for ( int i_col=0 ; i_col<size_center ; i_col++ )
    {
      //
      // point on the first element of the source column
      //
      Precision *__restrict__ p_source=&real_folded_system_[i_col*size_folded];
      //
      // point on the first element of the destination column
      //
      complex<Precision> *__restrict__ p_destination=&current_operator_[i_col*size_center];
      //
      // copy the coefficients of this column
      //
      for ( int *__restrict__ i_row=&active_folded_domain_[0] ; (*i_row)<size_center ; i_row++ , p_source++ ) 
      {
        (*(p_destination+(*i_row)))=(*p_source); 
      }
    }
  }
  else
  {
    //
    // copy coefficients from the folded hamiltonian
    //
    for ( int i_col=0 ; i_col<size_center ; i_col++ )
    {
      //
      // point on the first element of the source column
      //
      complex<Precision> *__restrict__ p_source=&complex_folded_system_[i_col*size_folded];
      //
      // point on the first element of the destination column
      //
      complex<Precision> *__restrict__ p_destination=&current_operator_[i_col*size_center];
      //
      // copy the coefficients of this column
      //
      for ( int *__restrict__ i_row=&active_folded_domain_[0] ; (*i_row)<size_center ; i_row++ , p_source++ ) 
      {
        (*(p_destination+(*i_row)))=(*p_source); 
      }
    }
  }
}

template <class Precision>
bool L2Reservoir<Precision>::getChannelsForRealSystem_(void)
{
  //
  // dimensions
  //
  int size_folded=active_folded_domain_.size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // lapack variables
  //
  int M=size_folded;
  int N=size_center;
  int LDA=M;
  int LWORK;
  int INFO;
  int K=min(M,N); // number of reflectors for QR factorisation
  vector<Precision> REAL_TAU(K,0.0);
  vector<Precision> REAL_WORK(10,0.0);
  //
  // query for memory needs
  //
  LWORK=-1;
  lapack::geqrf<Precision>( &M, &N, &real_folded_system_[0], &LDA, &REAL_TAU[0], &REAL_WORK[0], &LWORK, &INFO );
  //
  // allocate memory for work array
  //
  REAL_WORK.resize((int)REAL_WORK[0]);
  LWORK=REAL_WORK.size();
  //
  // perform QR factorisation
  //
  lapack::geqrf<Precision>( &M, &N, &real_folded_system_[0], &LDA, &REAL_TAU[0], &REAL_WORK[0], &LWORK, &INFO );
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::getChanelsForRealSystem_ : problem during QR factorisation in xGEQRF" << endl;
    return false; 
  }
  //
  // query for memory needs
  //
  LWORK=-1;
  lapack::gqr<Precision>( &M, &M, &K, &real_folded_system_[0], &LDA, &REAL_TAU[0], &REAL_WORK[0], &LWORK, &INFO );
  //
  // reallocate if more work space is needed
  //
  if ( (int)REAL_WORK[0] > REAL_WORK.size() ) REAL_WORK.resize((int)REAL_WORK[0]);
  LWORK=REAL_WORK.size();
  //
  // compute Q matrix => folded_system
  //
  lapack::gqr<Precision>( &M, &M, &K, &real_folded_system_[0], &LDA, &REAL_TAU[0], &REAL_WORK[0], &LWORK, &INFO );
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::getChanelsForRealSystem_ : problem during Q matrix forming in xORGQR" << endl;
    return false; 
  }
  //
  // dimension of the first and seconf half arrays
  //
  int LDB=active_folded_domain_.size()-size_center;
  //
  // allocate arrays for first and second half of the kernel
  //
  vector<Precision> first_half(LDB*LDB);
  vector<Precision> second_half(LDB*LDB); 
  //
  // extract the coefficients on the first and second half
  // 
  for ( int i_col=0 , stop=LDB ; i_col<stop ; i_col++ )
  {  
    //
    // point on the first element of this column of the kernel
    //
    Precision *__restrict__ p_source=&real_folded_system_[(size_center+i_col)*LDA];
    //
    // point on corresponding element of the first half array 
    //
    Precision *__restrict__ p_destination_first=&first_half[i_col*LDB];
    //
    // point on corresponding element of the second half array 
    //
    Precision *__restrict__ p_destination_second=&second_half[i_col*LDB];
    //
    // copy corresponding coefficients
    //
    for ( int *__restrict__ i_row_first=&local_first_half_indices_[0] , 
              *__restrict__ i_row_second=&local_second_half_indices_[0] , 
              *stop_first=&local_first_half_indices_[0]+local_first_half_indices_.size() ; 
              i_row_first!=stop_first ; 
              i_row_first++ , i_row_second++ , p_destination_first++ , p_destination_second++ )
    {
      (*p_destination_first)=(*(p_source+(*i_row_first)));
      (*p_destination_second)=(*(p_source+(*i_row_second)));
    }
  }
  //
  // work arrays for lapack
  //
  vector<Precision> REAL_VR(LDB*LDB);
  vector<Precision> REAL_BETA(LDB);
  vector<Precision> REAL_ALPHAR(LDB);
  vector<Precision> REAL_ALPHAI(LDB);
  //
  // parameters for lapack call
  //
  N=LDB;
  char JOBVL='N';
  char JOBVR='V';
  int LDVL=LDB;
  int LDVR=LDB;
  //
  // querry for the optimal work array size
  //	
  LWORK=-1;  
  lapack::ggev_real<Precision>( &JOBVL, &JOBVR, &N, &second_half[0], &LDB, &first_half[0], &LDB, &REAL_ALPHAR[0], &REAL_ALPHAI[0], &REAL_BETA[0], NULL, &LDVL, &REAL_VR[0], &LDVR, &REAL_WORK[0], &LWORK, &INFO );		
  //
  // reallocate if more work space is needed
  //
  if ( (int)REAL_WORK[0] > REAL_WORK.size() ) REAL_WORK.resize((int)REAL_WORK[0]);
  LWORK=REAL_WORK.size();
  //
  // compute transform to periodic solutions
  //
  lapack::ggev_real<Precision>( &JOBVL, &JOBVR, &N, &second_half[0], &LDB, &first_half[0], &LDB, &REAL_ALPHAR[0], &REAL_ALPHAI[0], &REAL_BETA[0], NULL, &LDVL, &REAL_VR[0], &LDVR, &REAL_WORK[0], &LWORK, &INFO );		
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::getChanelsForRealSystem_ : problem during generalized eigen solving in xGGEV" << endl;
    return false; 
  }  
  //
  // release memory on the second half
  //
  second_half.resize(0);
  //
  //  resize first_half to receive the transformed kernel
  //
  first_half.resize(size_folded*LDB);
  //
  // multiply the kernel by VR
  //
  { 
    char TRANSA='N';
    char TRANSB='N';
    Precision ALPHA=1.0;
    Precision BETA=0.0;
    lapack::gemm<Precision>(&TRANSA, &TRANSB, &size_folded, &LDB, &LDB, &ALPHA, &real_folded_system_[size_center*size_folded], &size_folded, &REAL_VR[0], &LDB, &BETA, &first_half[0], &size_folded);
  }
  //
  // allocate proper size for the complete set of channels
  //
  int size_channel=2*size_center;
  this_q_channels_.resize(size_channel*size_channel);
  //
  // form propagation constants
  //
  this_q_constants_.resize(size_channel);
  for ( int k=0 ; k<LDB ; k++ ) 
  {
    this_q_constants_[k]=complex<Precision>(REAL_ALPHAR[k],REAL_ALPHAI[k])/(Precision)(REAL_BETA[k]+1.0E-16);
  } 
  //
  // set channels to 0
  //
  memset(&this_q_channels_[0],0,size_channel*size_channel*sizeof(complex<Precision>));
  //
  // for the appropriate solutions
  //
  for ( int i_col=0 ; i_col<LDB ; i_col++ )
  {
    //
    // if this constant is real
    //
    if ( REAL_ALPHAI[i_col]==0.0 )
    {
      //
      // point on this channel
      //
      Precision          *p_channel_src=&first_half[i_col*size_folded];
      complex<Precision> *p_channel_dest=&this_q_channels_[i_col*size_channel];
      //
      // if the chanel is seriously explosive, we construct it
      // from the second half (it should be close to 0 on the first one).
      //
      if ( norm(this_q_constants_[i_col])>1E4 )
      {
        for ( int i=0 , stop=local_src_second_half_.size() ; i<stop ; i++ ) 
        {
          p_channel_dest[local_dest_second_half_[i]]=p_channel_src[local_src_second_half_[i]];
        }
      }
      else
      {
        for ( int i=0 , stop=local_src_first_half_.size() ; i<stop ; i++ ) 
        {
          p_channel_dest[local_dest_first_half_[i]]=p_channel_src[local_src_first_half_[i]];
        }
      }
      //
      // if the channel is well conditioned, we complete the trivial indices
      // the threshold used guarantees that the state was defined from the first half
      // so the second part of the state is completely defined
      //
      if ( norm(this_q_constants_[i_col])<1E2 && norm(this_q_constants_[i_col])>1E-2 && trivial_indices_.size()>0 )
      {
        //
        // inverse constant propagation
        //
        complex<Precision> inv_q=complex<Precision>(1.0,0.0)/this_q_constants_[i_col];
        //
        // loop on trivial indices
        //
        for ( int i=0 ; trivial_indices_[i]<size_center ; i++ ) 
        {
          p_channel_dest[trivial_indices_[i]]=p_channel_dest[trivial_indices_[i]+size_center]*inv_q; 
        }
      }
    }
    else
    {
      //
      // point on this channel
      //
      Precision          *p_real_src=&first_half[i_col*size_folded];
      Precision          *p_imag_src=&first_half[(i_col+1)*size_folded];
      complex<Precision> *p_channel1_dest=&this_q_channels_[i_col*size_channel];
      complex<Precision> *p_channel2_dest=&this_q_channels_[(i_col+1)*size_channel];
      //
      // if the chanel is seriously explosive, we construct it
      // from the second half (it should be close to 0 on the first one).
      //
      if ( norm(this_q_constants_[i_col])>1E4 )
      {
        for ( int i=0 , stop=local_src_second_half_.size() ; i<stop ; i++ ) 
        {
          p_channel1_dest[local_dest_second_half_[i]]=complex<Precision>(p_real_src[local_src_second_half_[i]], p_imag_src[local_src_second_half_[i]]);
          p_channel2_dest[local_dest_second_half_[i]]=complex<Precision>(p_real_src[local_src_second_half_[i]],-p_imag_src[local_src_second_half_[i]]);
        }
      }
      else
      {
        for ( int i=0 , stop=local_src_first_half_.size() ; i<stop ; i++ ) 
        {
          p_channel1_dest[local_dest_first_half_[i]]=complex<Precision>(p_real_src[local_src_first_half_[i]], p_imag_src[local_src_first_half_[i]]);
          p_channel2_dest[local_dest_first_half_[i]]=complex<Precision>(p_real_src[local_src_first_half_[i]],-p_imag_src[local_src_first_half_[i]]);
        }
      }
      //
      // if the channel is well conditioned, we complete the trivial indices
      // the threshold used guarantees that the state was defined from the first half
      // so the second part of the state is completely defined
      //
      if ( norm(this_q_constants_[i_col])<1E2 && norm(this_q_constants_[i_col])>1E-2 && trivial_indices_.size()>0 )
      {
        //
        // inverse constant propagation
        //
        complex<Precision> inv_q1=complex<Precision>(1.0,0.0)/this_q_constants_[i_col];
        complex<Precision> inv_q2=complex<Precision>(1.0,0.0)/this_q_constants_[i_col+1];
        //
        // loop on trivial indices
        //
        for ( int i=0 ; trivial_indices_[i]<size_center ; i++ ) 
        {
          p_channel1_dest[trivial_indices_[i]]=p_channel1_dest[trivial_indices_[i]+size_center]*inv_q1; 
          p_channel2_dest[trivial_indices_[i]]=p_channel2_dest[trivial_indices_[i]+size_center]*inv_q2; 
        }
      }
      //
      // skip two column instead of one 
      //
      i_col++;
    }
  }
  //
  // add trivial solutions
  //
  for ( int i=0 , i_col=LDB ; i_col<size_channel ; i_col++ , i++ )
  {
    //
    // determine if it is an explosive or an evanescent channel
    //
    bool is_explosive = ( trivial_indices_[i] >= 2*size_center );
    //
    // find corresponding index
    //
    int icoeff = is_explosive ? trivial_indices_[i]-size_center : trivial_indices_[i];
    //
    // set coeff 
    //
    this_q_channels_[i_col*size_channel+icoeff]=1.0;
    //
    // set propagation constant
    //
    this_q_constants_[i_col] = is_explosive ? 1E16 : 1E-16; 
  }
  //
  // indicate that everything went fine
  //
  return true;
}

template <class Precision>
bool L2Reservoir<Precision>::getChannelsForComplexSystem_(void)
{
  //
  // dimensions
  //
  int size_folded=active_folded_domain_.size();
  int size_center=cell_indices_[0][0].size()/3;
  //
  // lapack variables
  //
  int M=size_folded;
  int N=size_center;
  int LDA=M;
  int LWORK;
  int INFO;
  int K=min(M,N); // number of reflectors for QR factorisation
  vector<Precision> REAL_WORK(8*N);
  vector<complex<Precision> > COMPLEX_TAU(K);
  vector<complex<Precision> > COMPLEX_WORK(1);
  //
  // query for memory needs
  //
  LWORK=-1;
  lapack::geqrf<complex<Precision> >( &M, &N, &complex_folded_system_[0], &LDA, &COMPLEX_TAU[0], &COMPLEX_WORK[0], &LWORK, &INFO );
  //
  // allocate memory fo work array
  //
  COMPLEX_WORK.resize((int)COMPLEX_WORK[0].real());
  LWORK=COMPLEX_WORK.size();
  //
  // perform QR factorisation
  //
  lapack::geqrf<complex<Precision> >( &M, &N, &complex_folded_system_[0], &LDA, &COMPLEX_TAU[0], &COMPLEX_WORK[0], &LWORK, &INFO );
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::getChanelsForComplexSystem_ : problem during QR factorisation in xGEQRF" << endl;
    return false; 
  }
  //
  // query for memory needs
  //
  LWORK=-1;
  lapack::gqr<complex<Precision> >( &M, &M, &K, &complex_folded_system_[0], &LDA, &COMPLEX_TAU[0], &COMPLEX_WORK[0], &LWORK, &INFO );
  //
  // reallocate if more work space is needed
  //
  if ( (int)COMPLEX_WORK[0].real() > COMPLEX_WORK.size() ) COMPLEX_WORK.resize((int)COMPLEX_WORK[0].real());
  LWORK=COMPLEX_WORK.size();
  //
  // compute Q matrix => folded_system
  //
  lapack::gqr<complex<Precision> >( &M, &M, &K, &complex_folded_system_[0], &LDA, &COMPLEX_TAU[0], &COMPLEX_WORK[0], &LWORK, &INFO );
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::getChanelsForComplexSystem_ : problem during Q matrix forming in xUNGQR" << endl;
    return false; 
  }
  //
  // dimension of the first and seconf half arrays
  //
  int LDB=active_folded_domain_.size()-size_center;
  //
  // allocate arrays for first and second half of the kernel
  //
  vector<complex<Precision> > first_half(LDB*LDB);
  vector<complex<Precision> > second_half(LDB*LDB); 
  //
  // extract the coefficients on the first and second half
  // 
  for ( int i_col=0 , stop=LDB ; i_col<stop ; i_col++ )
  {  
    //
    // point on the first element of this column of the kernel
    //
    complex<Precision> *__restrict__ p_source=&complex_folded_system_[(size_center+i_col)*LDA];
    //
    // point on corresponding element of the first half array 
    //
    complex<Precision> *__restrict__ p_destination_first=&first_half[i_col*LDB];
    //
    // point on corresponding element of the second half array 
    //
    complex<Precision> *__restrict__ p_destination_second=&second_half[i_col*LDB];
    //
    // copy corresponding coefficients
    //
    for ( int *__restrict__ i_row_first=&local_first_half_indices_[0] , 
              *__restrict__ i_row_second=&local_second_half_indices_[0] , 
              *stop_first=&local_first_half_indices_[0]+local_first_half_indices_.size() ; 
              i_row_first!=stop_first ; 
              i_row_first++ , i_row_second++ , p_destination_first++ , p_destination_second++ )
    {
      (*p_destination_first)=(*(p_source+(*i_row_first)));
      (*p_destination_second)=(*(p_source+(*i_row_second)));
    }
  }
  //
  // lapack work arrays
  //
  vector<complex<Precision> > COMPLEX_VR(LDB*LDB);
  vector<complex<Precision> > COMPLEX_ALPHA(LDB);
  vector<complex<Precision> > COMPLEX_BETA(LDB);
  //
  // parameters for lapack call
  //
  N=LDB;
  char JOBVL='N';
  char JOBVR='V';
  int LDVL=LDB;
  int LDVR=LDB;
  //
  // querry for the optimal work array size
  //	
  LWORK=-1;  
  lapack::ggev_complex<Precision>( &JOBVL, &JOBVR, &N, &second_half[0], &LDB, &first_half[0], &LDB, &COMPLEX_ALPHA[0], &COMPLEX_BETA[0], NULL, &LDVL, &COMPLEX_VR[0], &LDVR, &COMPLEX_WORK[0], &LWORK, &REAL_WORK[0], &INFO );		
  //
  // reallocate if more work space is needed
  //
  if ( (int)COMPLEX_WORK[0].real() > COMPLEX_WORK.size() ) COMPLEX_WORK.resize((int)COMPLEX_WORK[0].real());
  LWORK=COMPLEX_WORK.size();
  //
  // compute transform to periodic solutions
  //
  lapack::ggev_complex<Precision>( &JOBVL, &JOBVR, &N, &second_half[0], &LDB, &first_half[0], &LDB, &COMPLEX_ALPHA[0], &COMPLEX_BETA[0], NULL, &LDVL, &COMPLEX_VR[0], &LDVR, &COMPLEX_WORK[0], &LWORK, &REAL_WORK[0], &INFO );		
  //
  // check status
  //
  if ( INFO!=0 )
  {
    if ( INFO<0 )
    cout << "L2Reservoir::getChanelsForComplexSystem_ : problem during generalized eigen solving in ZGGEV, INFO = " << INFO << endl;
    return false; 
  }        
  //
  // release memory on the second half
  //
  second_half.resize(0);
  //
  //  resize first_half to receive the transformed kernel
  //
  first_half.resize(size_folded*LDB);
  //
  // multiply the kernel by VR
  //
  { 
    char TRANSA='N';
    char TRANSB='N';
    complex<Precision> ALPHA=(Precision)1.0;
    complex<Precision> BETA=(Precision)0.0;
    lapack::gemm<complex<Precision> >(&TRANSA, &TRANSB, &size_folded, &LDB, &LDB, &ALPHA, &complex_folded_system_[size_center*size_folded], &size_folded, &COMPLEX_VR[0], &LDB, &BETA, &first_half[0], &size_folded);
  }
  //
  // allocate proper size for the complete set of channels
  //
  int size_channel=2*size_center;
  this_q_channels_.resize(size_channel*size_channel);
  //
  // form propagation constants
  //
  this_q_constants_.resize(size_channel);
  for ( int k=0 ; k<LDB ; k++ ) 
  {
    this_q_constants_[k]=COMPLEX_ALPHA[k]/(COMPLEX_BETA[k]+(Precision)1.0E-16);
  } 
  //
  // set channels to 0
  //
  memset(&this_q_channels_[0],0,size_channel*size_channel*sizeof(complex<Precision>));
  //
  // extract channels
  //
  for ( int i_col=0 ; i_col<LDB ; i_col++ )
  {
    //
    // point on this channel
    //
    complex<Precision> *p_channel_src=&first_half[i_col*size_folded];
    complex<Precision> *p_channel_dest=&this_q_channels_[i_col*size_channel];
    //
    // if the chanel is seriously explosive, we construct it
    // from the second half (it should be close to 0 on the first one).
    //
    if ( norm(this_q_constants_[i_col])>1E4 )
    {
      for ( int i=0 , stop=local_src_second_half_.size() ; i<stop ; i++ ) 
      {
        p_channel_dest[local_dest_second_half_[i]]=p_channel_src[local_src_second_half_[i]];
      }
    }
    else
    {
      for ( int i=0 , stop=local_src_first_half_.size() ; i<stop ; i++ ) 
      {
        p_channel_dest[local_dest_first_half_[i]]=p_channel_src[local_src_first_half_[i]];
      }
    }
    //
    // if the channel is well conditioned, we complete the trivial indices
    // the threshold used guarantees that the state was defined from the first half
    // so the second part of the state is completely defined
    //
    if ( norm(this_q_constants_[i_col])<1E2 && norm(this_q_constants_[i_col])>1E-2 && trivial_indices_.size()>0 )
    {
      //
      // inverse constant propagation
      //
      complex<Precision> inv_q=complex<Precision>(1.0,0.0)/this_q_constants_[i_col];
      //
      // loop on trivial indices
      //
      for ( int i=0 ; trivial_indices_[i]<size_center ; i++ ) 
      {
        p_channel_dest[trivial_indices_[i]]=p_channel_dest[trivial_indices_[i]+size_center]*inv_q; 
      }
    }
  }
  //
  // add trivial solutions
  //
  for ( int i=0 , i_col=LDB ; i_col<size_channel ; i_col++ , i++ )
  {
    //
    // determine if it is an explosive or an evanescent channel
    //
    bool is_explosive = ( trivial_indices_[i] >= 2*size_center );
    //
    // find corresponding index
    //
    int icoeff = is_explosive ? trivial_indices_[i]-size_center : trivial_indices_[i];
    //
    // set coeff 
    //
    this_q_channels_[i_col*size_channel+icoeff]=1.0;
    //
    // set propagation constant
    //
    this_q_constants_[i_col] = is_explosive ? 1E16 : 1E-16; 
  }
  //
  // indicate that everything went fine
  //
  return true;
}

template <class Precision>
void L2Reservoir<Precision>::orthonormalizeChannels_(void)
{
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // vector increment for blas calls
  //
  int INC=1;
  //
  // loop on the channels
  //
  currents_.resize(LDB);
  for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
  {
    //
    // get module and argument of the corresponding propagative constant
    //
    Precision module=sqrt(norm(this_q_constants_[i_channel]));
    Precision argument=arg(this_q_constants_[i_channel]);
    //
    // try to determine wich kind of channel this is
    //
    bool propagative_channel=false;
#if Precision == float
    //
    // first test the norm of the propagative vector within the safe zone
    //
    propagative_channel = propagative_channel || ( fabs(module-1.0) < 1E-5 );
    //
    // test the arg if norm is within the red zone 
    //
    propagative_channel = propagative_channel || ( fabs(module-1.0) < 5E-4 && fabs(argument) > 1E-4 );
#else
    //
    // first test the norm of the propagative vector within the safe zone
    //
    propagative_channel = propagative_channel || ( fabs(module-1.0) < 1E-6 );
    //
    // test the arg if norm is within the red zone 
    //
    propagative_channel = propagative_channel || ( fabs(module-1.0) < 1E-4 && fabs(argument) > 1E-6 );
#endif
    //
    // if this is possibly a propagative channel
    //
    if ( propagative_channel )
    {
      //
      // compute current associated with this channel
      //
      currents_[i_channel] = imag( computeProbabilityCurrent_(i_channel,i_channel) );
      //
      // flag to avoid computing twice the current
      //
      bool channel_modif=false;  
      //
      // point to first element of channel i
      //
      complex<Precision> *p_i=&this_q_channels_[i_channel*LDB];
      //
      // orthonormalize with the previous chanels 
      //
      for ( int j_channel=0 ; j_channel<i_channel ; j_channel++ )
      {
        // 
        // if this chanel is also propagative and the propagation constant are close to be the same
        //
        if ( currents_[j_channel]!=0.0 && norm(this_q_constants_[i_channel]-this_q_constants_[j_channel])<1.0E-3 )
        {
          //
          // compute current betwen the two chanels
          //
          complex<Precision> alpha=computeProbabilityCurrent_(i_channel, j_channel);
          //
          // remove j_channel componant 
          //
          if ( norm(alpha)>1.0E-16 )
          {
            //
            // point to first element of chanel j
            //
            complex<Precision> *p_j=&this_q_channels_[j_channel*LDB];
            //
            // compute correction constant
            //
/*
            complex<Precision> beta(-alpha.imag()*currents_[j_channel],-alpha.real()*currents_[j_channel]);
            //
            // correct |i>=|i>+beta|j>
            //
            lapack::axpy<complex<Precision> >(&LDB,&beta,p_j,&INC,p_i,&INC);
*/
            //
            // substract
            //
            complex<Precision> beta(alpha.imag()*currents_[j_channel],alpha.real()*currents_[j_channel]);
            for ( int k=0 ; k<LDB ; k++ ) p_i[k]-=beta*p_j[k];
            //
            // inform that we modified the chanel
            // 
            channel_modif=true;
          } 
        }
      } 
      //
      // if necessary recompute the probability current associated to this chanel
      //
      if ( channel_modif ) 
      {
        currents_[i_channel] = imag( computeProbabilityCurrent_(i_channel,i_channel) );
      }
      //
      // renorm the chanel to get a unitary probability current
      //
      if ( fabs(currents_[i_channel])>1E-8 )
      {
        complex<Precision> alpha=1.0/sqrt(fabs(currents_[i_channel]));
//      lapack::scal<complex<Precision> >(&LDB,&alpha,p_i,&INC);
        //
        for ( int k=0 ; k<LDB ; k++ ) p_i[k]*=alpha;
        //
        // keep track of the current with the sign
        //
        currents_[i_channel]=currents_[i_channel]/fabs(currents_[i_channel]);
      }
      else
      {
        currents_[i_channel]=0.0;
      }
    }
    else
    {
      //
      // set current to 0
      //
      currents_[i_channel]=0.0;
    }
  } 
}

template <class Precision>
void L2Reservoir<Precision>::balanceChannels_(void)
{
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // average norm and number of propagative states
  //
  Precision average_norm=0.0;
  int       n_propagative=0;
  //
  for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
  {
    if ( currents_[i_channel]!=0.0 )
    {
      //
      // point to first element of chanel i
      //
      complex<Precision> *p_i=&this_q_channels_[i_channel*LDB];
      //
      // compute norm
      //
      Precision norm_channel=0.0;
      for ( int k=0 ; k<LDB ; k++ ) norm_channel += norm(p_i[k]);
      //
      // add to norm
      //
      average_norm+=norm_channel;
      //
      // increment number of propagative chanels found
      //
      n_propagative++;
    }
  }
  //
  // average
  //
  if (n_propagative>0)
  {
    average_norm/=n_propagative;
    //
    // norm the rest of the chanels
    //
    for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
    {
      if ( currents_[i_channel]==0.0 )
      {
        //
        // point to first element of channel i
        //
        complex<Precision> *p_i=&this_q_channels_[i_channel*LDB];
        //
        // compute norm
        //
        Precision norm_channel=0.0;
        for ( int k=0 ; k<LDB ; k++ ) norm_channel += norm(p_i[k]);
        //
        // compute factor
        //
        Precision alpha = sqrt(average_norm) / sqrt(norm_channel);
        //
        // norm state
        //
        for ( int k=0 ; k<LDB ; k++ ) p_i[k] *= alpha;
      }
    }
  }
}

template <class Precision>
void L2Reservoir<Precision>::sortsChannels_(int iq1, int iq2)
{
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // allocate memory according to the needs
  //
  if ( keepChannels_ ) channel_coeff_[iq1][iq2].resize(LDB*LDB);
  dual_coeff_[iq1][iq2].resize(LDB*LDB);
  //
  // inser explosive chanels first
  //
  int n_channels=0;
  n_explosive_channels_[iq1][iq2]=0;
  //
  for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
  {
    //
    // if this chanel is explosive
    //
    if ( currents_[i_channel]==0.0 && norm(this_q_constants_[i_channel])>1.0 )
    {
      // 
      // point on first element of source column
      //
      complex<Precision> *p_source=&this_q_channels_[i_channel*LDB];
      // 
      // point on first element of destination column
      //
      complex<Precision> *p_destination = ( keepChannels_ ) ? &(channel_coeff_[iq1][iq2][n_channels*LDB]) : &(dual_coeff_[iq1][iq2][n_channels*LDB]);
      //
      // copy coefficients
      //
      for ( int i_coeff=0 ; i_coeff<LDB ; i_coeff++ ) p_destination[i_coeff]=p_source[i_coeff];
      //
      // increment the number of chanels
      //
      n_channels++;
      n_explosive_channels_[iq1][iq2]++;
    }
  }
  //
  // then evanescent chanels
  //
  n_evanescent_channels_[iq1][iq2]=0;
  for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
  {
    //
    // if this channel is evanescent
    //
    if ( currents_[i_channel]==0.0 && norm(this_q_constants_[i_channel])<1.0 )
    {
      // 
      // point on first element of source column
      //
      complex<Precision> *p_source=&this_q_channels_[i_channel*LDB];
      // 
      // point on first element of destination column
      //
      complex<Precision> *p_destination = ( keepChannels_ ) ? &(channel_coeff_[iq1][iq2][n_channels*LDB]) : &(dual_coeff_[iq1][iq2][n_channels*LDB]);
      //
      // copy coefficients
      //
      for ( int i_coeff=0 ; i_coeff<LDB ; i_coeff++ ) p_destination[i_coeff]=p_source[i_coeff];
      //
      // increment the number of chanels
      //
      n_channels++;
      n_evanescent_channels_[iq1][iq2]++;
    }
  }
  //
  // then incoming chanels
  //
  n_incoming_channels_[iq1][iq2]=0;
  for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
  {
    //
    // if this channel is evanescent
    //
    if ( currents_[i_channel]<0.0 )
    {
      // 
      // point on first element of source column
      //
      complex<Precision> *p_source=&this_q_channels_[i_channel*LDB];
      // 
      // point on first element of destination column
      //
      complex<Precision> *p_destination = ( keepChannels_ ) ? &(channel_coeff_[iq1][iq2][n_channels*LDB]) : &(dual_coeff_[iq1][iq2][n_channels*LDB]);
      //
      // copy coefficients
      //
      for ( int i_coeff=0 ; i_coeff<LDB ; i_coeff++ ) p_destination[i_coeff]=p_source[i_coeff];
      //
      // increment the number of chanels
      //
      n_channels++;
      n_incoming_channels_[iq1][iq2]++;
    }
  }
  //
  // then outgoing chanels
  //
  n_outgoing_channels_[iq1][iq2]=0;
  for ( int i_channel=0 ; i_channel<LDB ; i_channel++ )
  {
    //
    // if this channel is evanescent
    //
    if ( currents_[i_channel]>0.0 )
    {
      // 
      // point on first element of source column
      //
      complex<Precision> *p_source=&this_q_channels_[i_channel*LDB];
      // 
      // point on first element of destination column
      //
      complex<Precision> *p_destination = ( keepChannels_ ) ? &(channel_coeff_[iq1][iq2][n_channels*LDB]) : &(dual_coeff_[iq1][iq2][n_channels*LDB]);
      //
      // copy coefficients
      //
      for ( int i_coeff=0 ; i_coeff<LDB ; i_coeff++ ) p_destination[i_coeff]=p_source[i_coeff];
      //
      // increment the number of chanels
      //
      n_channels++;
      n_outgoing_channels_[iq1][iq2]++;
    }
  }
}

template <class Precision>
bool L2Reservoir<Precision>::findDuals_(int iq1, int iq2)
{
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // copy chanels into dual forms if needed
  //
  if ( keepChannels_ ) dual_coeff_[iq1][iq2]=channel_coeff_[iq1][iq2];
  //
  // inverse the chanel matrix to find the dual forms on the chanels
  //
  vector<int> IPIV(LDB);
  int M=LDB;
  int N=LDB;
  int INFO;
  int LWORK;
  vector<complex<Precision> > COMPLEX_WORK(1);
  //
  // first factorize
  //
  lapack::getrf<complex<Precision> >( &M, &N, &(dual_coeff_[iq1][iq2][0]), &LDB, &IPIV[0], &INFO );
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::findDuals_ : problem during dual inversion in xGETRF, INFO = " << INFO << endl;
    return false; 
  }
  //
  // optimal memory request
  //
  LWORK=-1;
  lapack::getri<complex<Precision> >( &N, &(dual_coeff_[iq1][iq2][0]), &LDB, &IPIV[0], &COMPLEX_WORK[0], &LWORK, &INFO );
  //
  // allocate memory if necessary
  //
  if ( (int)COMPLEX_WORK[0].real()>COMPLEX_WORK.size() ) COMPLEX_WORK.resize((int)COMPLEX_WORK[0].real());
  LWORK=COMPLEX_WORK.size();
  //
  // compute inverse
  //
  lapack::getri<complex<Precision> >( &N, &(dual_coeff_[iq1][iq2][0]), &LDB, &IPIV[0], &COMPLEX_WORK[0], &LWORK, &INFO );
  //
  // check status
  //
  if ( INFO!=0 )
  {
    cout << "L2Reservoir::findDuals_ : problem during dual inversion in xGETRI, INFO = " << INFO << endl;
    return false; 
  }
  //
  // indicate that everything went fine
  //
  return true;
}

template <class Precision>
complex<Precision> L2Reservoir<Precision>::computeProbabilityCurrent_(int i_channel)
{
  complex<Precision> current=0.0;
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // allocate memory for temporary variables
  //
  static vector<complex<Precision> > product_temp;
  if ( product_temp.size()<size_center ) product_temp.resize(size_center);
  //
  // point on first element of product temp
  //
  complex<Precision> * p_prod_tmp=&product_temp[0];  
  //
  // lapack variables
  //
  char TRANSA='C';
  complex<Precision> ALPHA=1.0;
  complex<Precision> BETA =0.0;
  int DIM=size_center;
  int INC=1;
  //
  // compute first <i|P1.H.P2|i>
  //
  {
    //
    // point on first coefficient of the channel i on slice 0
    //
    complex<Precision> *p_channel_i=&this_q_channels_[i_channel*LDB];
    //
    // point on first coefficient of the channel j on slice 1
    //
    complex<Precision> *p_channel_j=&this_q_channels_[i_channel*LDB+size_center];
    //
    // compute VR([first part])' * current_operator first
    //
    lapack::gemv<complex<Precision> >(&TRANSA,&DIM,&DIM,&ALPHA,&current_operator_[0],&DIM,p_channel_i,&INC,&BETA,p_prod_tmp,&INC);   
    //
    // compute then VR([first part])' * current_operator * VR([center]) and add to current
    //
//    current += lapack::dot<complex<Precision> >(&DIM,p_prod_tmp,&INC,p_channel_j,&INC);
    complex<Precision> value_temp=0.0;
    for ( int i_col=0 ; i_col<size_center ; i_col++ ) value_temp +=  conj(product_temp[i_col]) * p_channel_j[i_col]; 
    current += value_temp;
  }
  //
  // remove then -<i|P2.H.P1|i>
  //
  current -= conj(current);
  //
  // return result
  //
  return  current;
}


template <class Precision>
complex<Precision> L2Reservoir<Precision>::computeProbabilityCurrent_(int i_channel, int j_channel)
{
  complex<Precision> current=0.0;
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // allocate memory for temporary variables
  //
  static vector<complex<Precision> > product_temp;
  if ( product_temp.size()<size_center ) product_temp.resize(size_center);
  //
  // point on first element of product temp
  //
  complex<Precision> * p_prod_tmp=&product_temp[0];  
  //
  // lapack variables
  //
  char TRANSA='C';
  complex<Precision> ALPHA=1.0;
  complex<Precision> BETA =0.0;
  int DIM=size_center;
  int INC=1;
  //
  // compute first <i|P1.H.P2|j>
  //
  {
    //
    // point on first coefficient of the channel i on slice 0
    //
    complex<Precision> *p_channel_i=&this_q_channels_[i_channel*LDB];
    //
    // point on first coefficient of the channel j on slice 1
    //
    complex<Precision> *p_channel_j=&this_q_channels_[j_channel*LDB+size_center];
    //
    // compute VR([first part])' * current_operator first
    //
    lapack::gemv<complex<Precision> >(&TRANSA,&DIM,&DIM,&ALPHA,&current_operator_[0],&DIM,p_channel_i,&INC,&BETA,p_prod_tmp,&INC);   
    //
    // compute then VR([first part])' * current_operator * VR([center]) and add to current
    //
//    current += lapack::dot<complex<Precision> >(&DIM,p_prod_tmp,&INC,p_channel_j,&INC);
    complex<Precision> value_temp=0.0;
    for ( int i_col=0 ; i_col<size_center ; i_col++ ) value_temp +=  conj(product_temp[i_col]) * p_channel_j[i_col]; 
    current += value_temp;
  }
  //
  // compute then -<i|P2.H.P1|j>
  //
  {
    //
    // point on first coefficient of the channel i on slice 1
    //
    complex<Precision> *p_channel_i=&this_q_channels_[i_channel*LDB+size_center];
    //
    // point on first coefficient of the channel j on slice 0
    //
    complex<Precision> *p_channel_j=&this_q_channels_[j_channel*LDB];
    //
    // compute VR([first part])' * current_operator first
    //
    lapack::gemv<complex<Precision> >(&TRANSA,&DIM,&DIM,&ALPHA,&current_operator_[0],&DIM,p_channel_j,&INC,&BETA,p_prod_tmp,&INC);
    //
    // compute then VR([first part])' * current_operator ' * VR([center]) and substract from current
    //
//    current -= lapack::dot<complex<Precision> >(&DIM,p_channel_i,&INC,p_prod_tmp,&INC);
    complex<Precision> value_temp=0.0;
    for ( int i_col=0 ; i_col<size_center ; i_col++ ) value_temp +=  product_temp[i_col] * conj(p_channel_i[i_col]); 
    current -= value_temp;
  }
  //
  // return result
  //
  return  current;
}


/*
template <class Precision>
complex<Precision> L2Reservoir<Precision>::computeProbabilityCurrent_(int i_channel)
{
  return computeProbabilityCurrent_(i_channel,i_channel);
}
*/
/*
template <class Precision>
complex<Precision> L2Reservoir<Precision>::computeProbabilityCurrent_(int i_channel, int j_channel)
{
  complex<Precision> current=0.0;
  //
  // dimensions
  //
  int size_center=cell_indices_[0][0].size()/3;
  int LDB=2*size_center;
  //
  // allocate memory for temporary variables
  //
  vector<complex<Precision> > product_temp(size_center);
  //
  // compute first <i|P1.H.P2|j>
  //
  {
    //
    // point on first coefficient of the channel i on slice 0
    //
    complex<Precision> *p_channel_i=&this_q_channels_[i_channel*LDB];
    //
    // point on first coefficient of the channel j on slice 1
    //
    complex<Precision> *p_channel_j=&this_q_channels_[j_channel*LDB+size_center];
    //
    // compute VR([first part])' * current_operator first
    //
    for ( int i_col=0 ; i_col<size_center ; i_col++ )
    {
      //
      // point on the first element of this column of current operator
      //
      complex<Precision> *p_V=&current_operator_[i_col*size_center];
      //
      // compute coefficient
      //
      complex<Precision> value_temp=0.0;
      for ( int i_row=0 ; i_row<size_center ; i_row++ ) value_temp+= ( p_V[i_row] * conj( p_channel_i[i_row] ) );
      //
      // store result of the product
      //
      product_temp[i_col]=value_temp;
    }
    //
    // compute then VR([first part])' * current_operator * VR([center])
    //
    complex<Precision> value_temp=0.0;
    //
    for ( int i_col=0 ; i_col<size_center ; i_col++ )
    {
      value_temp +=  product_temp[i_col] * p_channel_j[i_col]; 
    }
    //
    // add contribution
    //
    current += value_temp;
  }
  //
  // compute then -<i|P2.H.P1|j>
  //
  {
    //
    // point on first coefficient of the channel i on slice 1
    //
    complex<Precision> *p_channel_i=&this_q_channels_[i_channel*LDB+size_center];
    //
    // point on first coefficient of the channel j on slice 0
    //
    complex<Precision> *p_channel_j=&this_q_channels_[j_channel*LDB];
    //
    // compute VR([first part])' * current_operator first
    //
    for ( int i_col=0 ; i_col<size_center ; i_col++ )
    {
      //
      // point on the first element of this column of current operator
      //
      complex<Precision> *p_V=&current_operator_[i_col*size_center];
      //
      // compute coefficient
      //
      complex<Precision> value_temp=0.0;
      for ( int i_row=0 ; i_row<size_center ; i_row++ ) value_temp+=conj(p_V[i_row])* p_channel_j[i_row];
      //
      // strore result of the product
      //
      product_temp[i_col]=value_temp;
    }
    //
    // compute then VR([first part])' * current_operator ' * VR([center])
    //
    complex<Precision> value_temp=0.0;
    for ( int i_col=0 ; i_col<size_center ; i_col++ ) value_temp +=  product_temp[i_col] * conj(p_channel_i[i_col]); 
    //
    // add contribution
    //
    current -= value_temp;
  }
  //
  // return result
  //
  return  current;
}
*/

template <class Precision>
Subspace<complex<Precision> >* L2Reservoir<Precision>::moveToChannelRepresentation(Subspace<Precision> &subspace)
{
  //
  // create a complex subspace as a copy of the real one
  //
  Subspace<complex<Precision> > *complex_subspace=new Subspace<complex<Precision> >(this->context_);
  *complex_subspace=subspace;
  //
  // apply transfo to the complex subspace
  // 
  moveToChannelRepresentation(*complex_subspace);
  //
  // return pointer on the newly created subspace
  //
  return complex_subspace;
}

template <class Precision>
void L2Reservoir<Precision>::moveToChannelRepresentation(Subspace<complex<Precision> > &subspace)
{
  //
  // get reference of base class attribute
  //
  Context   &context_=this->context_;
  int       &my_proc_=this->my_proc_;
  int       &n_procs_=this->n_procs_;
  // range/domain indices
  vector<int>           &global_indices_=this->global_indices_;    // indices of this reservoir
  map<int,vector<int> > &local_indices_=this->local_indices_;      // repartition of the indices on the proc grid
  // range proc index lists 
  vector<int> &list_proc_=this->list_proc_;              // list of processors supporting the reservoir 
  vector<int> &Nloc_=this->Nloc_;                        // local dimension on supporting processors
  int         &my_Nloc_=this->my_Nloc_;                  // local dimension on this processor
  // channel indices by type
  vector<int>    &explosive_channel_indices_=this->explosive_channel_indices_;   // indices of explosive channels
  vector<int>    &evanescent_channel_indices_=this->evanescent_channel_indices_; // indices of evanescent channels
  vector<int>    &incoming_channel_indices_=this->incoming_channel_indices_;     // indices of incoming channels
  vector<int>    &outgoing_channel_indices_=this->outgoing_channel_indices_;     // indices of outcoming channels
  //
  // check that we have the dimensions 
  // 
  if ( global_indices_.size()==0 ) 
  {
    cout << "Error: int - L2Reservoir::moveToChannelRepresentation - global_indices_ is not set on proc " << my_proc_ << endl;
    exit(0);
  }
  //
  // compute dimension of the system
  //
  int size_center=global_indices_.size()/(3*nrep1_*nrep2_);
  int LDB=2*size_center;
  int nVectors=subspace.nVectors();
  //
  // generate list of ordered indices for the repetitions of the cells
  //
  vector<int> ordered_cell_indices;
  for ( int iq2=0 ; iq2<nrep2_ ; iq2++ ) 
  for ( int iq1=0 ; iq1<nrep1_ ; iq1++ ) 
  {
    ordered_cell_indices.insert(ordered_cell_indices.end(),cell_indices_[iq1][iq2].begin(),cell_indices_[iq1][iq2].begin()+LDB);
  }
  //
  // gather subspace coefficients on range processor
  //
  vector<complex<Precision> > coeff;
  vector<int> domain_indices=ordered_cell_indices;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_,coeff); // <= domain_indices contains now the indices of the gathered coeffs
  //
  // make sure that we found all the coeff of the contact for that subspace
  // or none at all
  //
  if ( ordered_cell_indices.size()!=domain_indices.size() && domain_indices.size()!=0 )
  {
    cout << "Error: trying to move an incomplete contact subspace to channel representation in - moveToChannelRepresentation -" << endl;
    exit(0);
  } 
  //
  // synchronize channels if not done already
  // 
  synchronizeChannelsIndices();
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_indices_.count(my_proc_) )
  {
    //
    // allocat memory for the local duals
    //
    vector<complex<Precision> > local_duals;
    //
    // get the counts of coeff (=nduals*LDB) received by each procs
    //
    vector<int> count;
    for ( map<int,vector<int> >::iterator it=local_indices_.begin() , stop=local_indices_.end() ; it!=stop ; it++ ) count.push_back(it->second.size()*LDB);
    //
    // allocate memory for reception
    //
    local_duals.resize(local_indices_[my_proc_].size()*LDB);
    //
    // scatter the states
    //
    if ( my_proc_==working_proc_ )
    {
      //
      // allocate memory for the send buffer
      //
      vector<complex<Precision> > sendbuf((n_ex_+n_ev_+n_in_+n_out_)*LDB);
      //
      // copy the dual forms into send buffer,
      // transpose the form as they are currently
      // stocked row-wise
      //
      complex<Precision> *__restrict__ p_destination=&sendbuf[0];
      for ( int iq2=0 ; iq2<nrep2_ ; iq2++ ) 
      for ( int iq1=0 ; iq1<nrep1_ ; iq1++ ) 
      {
        for ( int i=0 ; i<LDB ; i++ )
        {
          complex<Precision> *__restrict__ p_source=&dual_coeff_[iq1][iq2][i];
          for ( int j=0 , stop=LDB*LDB ; j<stop ; j+=LDB , p_destination++ ) (*p_destination)=p_source[j];
        }
      }
      //
      // Scatter the duals
      //
      communication::scatter<complex<Precision> >( &sendbuf[0], &local_duals[0], count, working_proc_, list_proc_, context_ );
    }
    else
    {
      //
      // receive the duals
      //
      vector<complex<Precision> > sendbuf(1);
      communication::scatter<complex<Precision> >( &sendbuf[0], &local_duals[0], count, working_proc_, list_proc_, context_ );
    }
    //
    // align the subspaces:
    // find local indices correspondance between subspace coeffs and dual coeffs
    //  
    vector<int> local_indices = ordered_cell_indices <= domain_indices;
    //
    // keep dimensions of the subspace
    //
    int ncols=local_indices.size();
    int nrows=subspace.nVectors();
    //
    // reorder subspace coeffs to match the one of the duals
    //
    for ( int icol=0 ; icol<ncols ; icol++ )
    {
      //
      // if there is something to be done
      //
      if ( local_indices[icol]!=icol )
      {
        // 
        // swap coefficients 
        //
        complex<Precision> tmp;
        complex<Precision> *p1=&coeff[icol*nrows];
        complex<Precision> *p2=&coeff[local_indices[icol]*nrows];
        for ( int i=0 ; i<nrows ; i++ )
        {
          tmp=p1[i];
          p1[i]=p2[i];
          p2[i]=tmp;
        }
        int i=domain_indices[local_indices[icol]];
        domain_indices[local_indices[icol]]=domain_indices[icol];
        domain_indices[icol]=i;
        //
        // find index irow position in local_indices 
        //
        int index = icol <= local_indices;
        //
        // correct order in local_indices
        //
        local_indices[index]=local_indices[icol];
        local_indices[icol]=icol;
      } 
    }
    //
    // align subspace indices with local channel indices
    //
    subspace.setCoeffOnTop(local_indices_[my_proc_]);
    //
    // determine starting and ending indices for duals on this part
    //
    int start_index=0;
    int end_index=count[0]/LDB;
    for ( int i=0 ; list_proc_[i]!=my_proc_ ; i++ ) 
    {
      start_index=end_index;
      end_index+=count[i+1]/LDB;
    }
    //
    // allocate memory for the folded vectors
    //
    vector<complex<Precision> > first_folding(nVectors*LDB*nrep1_);
    vector<complex<Precision> > folded_coeff(nVectors*LDB); 
    //
    // loop on second q sampling direction
    //
    for ( int iq2=0 , first_index=0 , start_dual=0 ; iq2<nrep2_ ; iq2++ ) 
    {
      //
      // determine if this part count some of those q points
      //
      if ( start_index < first_index+nrep1_*LDB && end_index > first_index )
      {
        //
        // compute the phase
        // 
        Precision phi2=-iq2*TWO_PI/nrep2_;
        //
        // reinit the folded coeff
        //
        for ( int i=0 , stop=first_folding.size()  ; i<stop ; i++ ) first_folding[i]=0.0;
        //
        // prefold in this direction
        //
        for ( int _iq2_=0 ; _iq2_<nrep2_ ; _iq2_++ )
        {
          //
          // compute the local phase asscociated to this repetition
          // and norm to conserve probability current
          // 
          complex<Precision> local_phase = exp(complex<Precision>((Precision)0.0,_iq2_*phi2)) * (Precision)(1.0/sqrt((Precision)nrep2_));  
          //
          // loop on first direction 
          //
          for ( int _iq1_=0 ; _iq1_<nrep1_ ; _iq1_++ )
          {
            //
            // point source and destination
            //
            complex<Precision> *p_source=&coeff[LDB*(_iq2_*nrep1_+_iq1_)*nVectors];
            complex<Precision> *p_destination=&first_folding[_iq1_*LDB*nVectors];
            //
            // fold
            //
            for ( int k=0 , stop=LDB*nVectors ; k<stop ; k++ ) p_destination[k]+=local_phase*p_source[k];
          }
        }
        //
        // loop on first q sampling direction
        // 
        for ( int iq1=0 ; iq1<nrep1_ ; iq1++ , first_index+=LDB ) 
        {
          //
          // determine if this part count some of this q point
          //
          if ( start_index < first_index+LDB && end_index > first_index )
          {
            //
            // reinit the folded coeff
            //
            for ( int i=0 , stop=folded_coeff.size()  ; i<stop ; i++ ) folded_coeff[i]=0.0;
            //
            // compute the phase
            // 
            Precision phi1=-iq1*TWO_PI/nrep1_;
            //
            // fold in the first direction
            //
            for ( int _iq1_=0 ; _iq1_<nrep1_ ; _iq1_++ )
            {
              //
              // compute local phase
              //
              complex<Precision> local_phase = exp(complex<Precision>((Precision)0.0,_iq1_*phi1)) * (Precision)(1.0/sqrt((Precision)nrep1_));
              //
              // point source and destination
              //
              complex<Precision> *p_source=&first_folding[_iq1_*LDB*nVectors];
              complex<Precision> *p_destination=&folded_coeff[0];
              //
              // fold
              //
              for ( int k=0 , stop=LDB*nVectors ; k<stop ; k++ ) p_destination[k]+=local_phase*p_source[k];
            }
            // 
            // get the numbers of duals for this q point
            // 
            int nduals= ( first_index<start_index ) ? first_index+LDB-start_index : min( end_index-first_index , LDB ); 
            //
            // in the case both starting and ending indices arise within this q point
            //
            nduals=min(end_index-start_index,nduals);
            //
            // multiply with the concerned duals
            //
            {
#ifdef DEBUG
              usleep(100000*my_proc_);
              cout << "proc " << my_proc_ << " moveToChannel: start_dual=" << start_dual << endl;
#endif
              char TRANSA='T';            //  
              char TRANSB='T';            // 
              int M=nduals;               // number of chanel coeff on this proc   
              int N=nVectors;             // number of vector in subspace
              int K=LDB;                  // dimension of the reservoir 
              complex<Precision> ALPHA=1.0;
              complex<Precision> *A=&local_duals[start_dual*LDB];
              int LDA=LDB;//local_indices.size();
              complex<Precision> *B=&folded_coeff[0];
              int LDB_loc=nVectors;
              complex<Precision> BETA=0.0;
              complex<Precision> *C=subspace.coeffPtr()+start_dual;
              int LDC=subspace.localDomain().size();
#ifdef DEBUG
              cout << "proc " << my_proc_ << " moveToChannel gemm: TRANSA=T TRANSB=T M=" << M << " N=" << N << " K=" << K 
                   << " LDA=" << LDA << " LDB_loc=" << LDB_loc << " LDC=" << LDC << endl; 
#endif
              lapack::gemm<complex<Precision> >(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB_loc, &BETA, C, &LDC);
#ifdef DEBUG
              usleep(100000*(n_procs_-my_proc_));
#endif
            }
            //
            // increment staring index of dual forms
            //
            start_dual+=nduals;        
          }
        }
      }
      //
      // if this part did not count some of those q points
      //
      else
      {
        //
        // simply increment first index for the next iteration
        //
        first_index+=nrep1_*LDB; 
      }
    }
  }
}



template <class Precision>
void L2Reservoir<Precision>::moveToLocalRepresentation(Subspace<complex<Precision> > &subspace)
{
  //
  // get reference of base class attribute
  //
  Context   &context_=this->context_;
  int       &my_proc_=this->my_proc_;
  int       &n_procs_=this->n_procs_;
  // range/domain indices
  vector<int>           &global_indices_=this->global_indices_;    // indices of this reservoir
  map<int,vector<int> > &local_indices_=this->local_indices_;      // repartition of the indices on the proc grid
  // range proc index lists 
  vector<int> &list_proc_=this->list_proc_;              // list of processors supporting the reservoir 
  vector<int> &Nloc_=this->Nloc_;                        // local dimension on supporting processors
  int         &my_Nloc_=this->my_Nloc_;                  // local dimension on this processor
  // channel indices by type
  vector<int>    &explosive_channel_indices_=this->explosive_channel_indices_;   // indices of explosive channels
  vector<int>    &evanescent_channel_indices_=this->evanescent_channel_indices_; // indices of evanescent channels
  vector<int>    &incoming_channel_indices_=this->incoming_channel_indices_;     // indices of incoming channels
  vector<int>    &outgoing_channel_indices_=this->outgoing_channel_indices_;   // indices of outcoming channels
  //
  // check that we have the dimensions 
  // 
  if ( global_indices_.size()==0 ) 
  {
    cout << "Error: in - L2Reservoir::moveToLocalRepresentation - global_indices_ is not set on proc " << my_proc_ << endl;
    exit(0);
  }
  //
  // check that we have the expression of the channels
  //
  if ( !keepChannels_ )
  {
    cout << "Error: in - L2Reservoir::moveToLocalRepresentation - channels definition is not kept, turn on keepChannels_ at instanciation of L2Reservoir class" << endl;
  }
  //
  // compute dimension of the system
  //
  int size_center=global_indices_.size()/(3*nrep1_*nrep2_);
  int LDB=2*size_center;
  int nVectors=subspace.nVectors();
  int nChannelsTot=2*global_indices_.size()/3;
  //
  // form an index list of channels indices
  //
  vector<int> ordered_channel_indices;
  for ( map<int,vector<int> >::iterator it=local_indices_.begin() , stop=local_indices_.end() ; it!=stop ; it++ )
  {
    ordered_channel_indices.insert(ordered_channel_indices.end(),it->second.begin(),it->second.end());
  }
  //
  // gather subspace coefficients on range processor
  //
  vector<complex<Precision> > coeff;
  vector<int> domain_indices=ordered_channel_indices;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_,coeff); // <= domain_indices contains now the indices of the gathered coeffs
  //
  // make sure that we found all the coeff of the contact for that subspace
  // or none at all
  //
  if ( ordered_channel_indices.size()!=domain_indices.size() && domain_indices.size()!=0 )
  {
    cout << "Error: trying to move an incomplete contact subspace to channel representation in - moveToChannelRepresentation -" << endl;
    exit(0);
  }
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_indices_.count(my_proc_) )
  {
    //
    // allocat memory for the local channels
    //
    vector<complex<Precision> > local_channels(nChannelsTot*LDB);
    //
    // broadcast the states
    //
    if ( my_proc_==working_proc_ )
    {
      //
      // copy the channels into the send buffer,
      // transpose the form as they are currently
      // stocked row-wise
      //
      for ( int iq2=0 , ichannel=0 ; iq2<nrep2_ ; iq2++ ) 
      for ( int iq1=0 ; iq1<nrep1_ ; iq1++ ) 
      {
        for ( int i=0 ; i<LDB ; i++ , ichannel++ )
        {
          for ( int j=0 ; j<LDB ; j++ )         
            local_channels[ichannel+j*nChannelsTot]=channel_coeff_[iq1][iq2][i*LDB+j];
        }
      }
      //
      // Broadcast the channels
      //broadcast( Type *buff , int count, int iroot, vector<int> idest, Context &context )
      communication::broadcast<complex<Precision> >( &local_channels[0], local_channels.size(), working_proc_, list_proc_, context_ );
    }
    else
    {
      //
      // receive the duals
      //
      communication::broadcast<complex<Precision> >( &local_channels[0], local_channels.size(), working_proc_, list_proc_, context_ );
    }
    //
    // align the subspaces:
    // find local indices correspondance between subspace coeffs and dual coeffs
    //  
    vector<int> local_indices = ordered_channel_indices <= domain_indices;
    //
    // keep dimensions of the subspace
    //
    int ncols=local_indices.size();
    int nrows=subspace.nVectors();
    //
    // reorder subspace coeffs tho match the one of the channels
    //
    for ( int icol=0 ; icol<ncols ; icol++ )
    {
      //
      // if there is something to be done
      //
      if ( local_indices[icol]!=icol )
      {
        // 
        // swap coefficients 
        //
        complex<Precision> tmp;
        complex<Precision> *p1=&coeff[icol*nrows];
        complex<Precision> *p2=&coeff[local_indices[icol]*nrows];
        for ( int i=0 ; i<nrows ; i++ )
        {
          tmp=p1[i];
          p1[i]=p2[i];
          p2[i]=tmp;
        }
        //
        // find index irow position in local_indices 
        //
        int index = icol <= local_indices;
        //
        // correct order in local_indices
        //
        local_indices[index]=local_indices[icol];
        local_indices[icol]=icol;
      } 
    }
    //
    // allocate memory for the expression of the channels on local states
    //
    vector<complex<Precision> > local_channel_coeff(nChannelsTot*LDB); 
    //
    // loop on second q sampling direction
    //
    for ( int iq2=0 , first_index=0 , start_channel=0 ; iq2<nrep2_ ; iq2++ ) 
    {
      //
      // loop on first q sampling direction
      // 
      for ( int iq1=0 ; iq1<nrep1_ ; iq1++ , first_index+=LDB ) 
      {
        //
        // get the indices for this cell
        //
        vector<int> cell_indices=cell_indices_[iq1][iq2];
        //
        // compute intersection of the cell indices and local indices
        //
        vector<int> intersection=cell_indices&&local_indices_[my_proc_];
        //
        // if this part support this cell
        //
        if ( intersection.size()>0 )
        {
          //
          // compute the expression of the channels on the corresponding cell
          //
          vector<int> correspondance=intersection<=cell_indices;
          //
          // for the channels for those states
          //
          for ( int _iq2_=0 ; _iq2_<nrep2_ ; _iq2_++ )
          for ( int _iq1_=0 ; _iq1_<nrep1_ ; _iq1_++ )
          {
            //
            // compute local phase
            //
            Precision phi1=_iq1_*TWO_PI/nrep1_;
            Precision phi2=_iq2_*TWO_PI/nrep2_;
            complex<Precision> local_phase = exp(complex<Precision>((Precision)0.0,iq1*phi1)) * (Precision)(1.0/sqrt((Precision)nrep1_))
                                           * exp(complex<Precision>((Precision)0.0,iq2*phi2)) * (Precision)(1.0/sqrt((Precision)nrep2_));
            for ( int i=0 ; i<correspondance.size() ; i++ )
            {
              complex<Precision> *p_source=&local_channels[(_iq1_+_iq2_*nrep1_)*LDB+correspondance[i]*nChannelsTot];
              complex<Precision> *p_destination=&local_channel_coeff[(_iq1_+_iq2_*nrep1_)*LDB+i*nChannelsTot];
            
              for ( int j=0 ; j<LDB ; j++ )
              {
                p_destination[j]=p_source[j]*local_phase;
              }
            }        
          }
          //
          // align indices of the intersection
          //
          subspace.setCoeffOnTop(intersection);
          //
          // multiply with the concerned duals
          //
          {
            char TRANSA='T';            //  
            char TRANSB='T';            // 
            int M=intersection.size();  // number of channels coeff on this proc   
            int N=nVectors;             // number of vector in subspace
            int K=nChannelsTot;         // dimension of the reservoir 
            complex<Precision> ALPHA=1.0;
            complex<Precision> *A=&local_channel_coeff[0];
            int LDA=nChannelsTot;
            complex<Precision> *B=&coeff[0];
            int LDB=nVectors;
            complex<Precision> BETA=0.0;
            complex<Precision> *C=subspace.coeffPtr();
            int LDC=subspace.localDomain().size();
            lapack::gemm<complex<Precision> >(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
          }
        }
      }
    }
  }
}

#ifdef TIMING
  #undef TIMING
#endif 

#endif

#ifndef HARMONICOSCILLATORSYSTEMDESCRIPTION_H
#define HARMONICOSCILLATORSYSTEMDESCRIPTION_H

using namespace std;

#include <unistd.h>
#include "PartAtomDescription.h"
#include "ReservoirAtomDescription.h"
#include "GraphClusteringParameters.h"

class HarmonicOscillatorSystemDescription
{
  private:
    
    // system properties
    double                             cutoff_dist_;
    vector<double>                     eigenvalues_; 
    string                             label_;
    vector<string>                     flags_;
    vector<PartAtomDescription *>      part_list_;
    vector<ReservoirAtomDescription *> reservoir_list_;
    
    // clustering parameters
    GraphClusteringParameters clusterParam_;
    
  public:
  
    // constructor
    HarmonicOscillatorSystemDescription(void) {}
    
    // virtual method
    void readFromFile(char *fileName, bool verbose=true);
    
    // cutoff flags and labels handling
    double cutoff(void) { return cutoff_dist_; }
    bool   hasFlag(string flag);
    string label(void) { return label_; }
    vector<string> flags(void) { return flags_; }
    
    // member access
    int nReservoirs(void) { return reservoir_list_.size(); }
    ReservoirAtomDescription* reservoir( int i_reservoir ) { return reservoir_list_[i_reservoir]; }
    int nParts(void) { return part_list_.size(); }
    PartAtomDescription* part( int i_part ) { return part_list_[i_part]; }
    vector<double> eigenvalues(void) { return eigenvalues_; }
    
    // clustering parameters access
    GraphClusteringParameters clusteringParameters(void) {return clusterParam_;}
        
    // conversion method for the eigen values
    static double convertEigenvalue(double eigenvalue);
    
    //destructor
    ~HarmonicOscillatorSystemDescription(void);
};

#endif

#ifndef RESERVOIRATOMDESCRIPTION_H
#define RESERVOIRATOMDESCRIPTION_H

#include <fstream>

#include "Atom.h"
#include "FileHandler.h"
#include "PartAtomDescription.h"

class ReservoirAtomDescription
{
  private:
    
    // repetitions attributes
    int nrep1_;
    int nrep2_;
    
    // list of atoms for each cell
    vector<vector<vector<Atom<double> > > > cell_atoms_;
    
    // translation symmetry
    vector<Vector3D<double> > translation_;
    
    // propagation direction
    Vector3D<double> propagation_;  
    
    // label and flags
    string         label_;
    vector<string> flags_;
    
    // contact parts 
    vector<PartAtomDescription *> contact_list_;
    
  public:
    
    // methods
    int nrep1(void) { return nrep1_; }
    int nrep2(void) { return nrep2_; }
    vector<Atom<double> > atoms(int irep1,int irep2) { return cell_atoms_[irep1][irep2]; }
    
    // reduction of the reservoir
    void reduceToMinimumCell(bool verbose=true);
    
    // flags and labels handling
    bool hasFlag(string flag);
    string label(void) { return label_; }
    vector<string> flags(void) { return flags_; }
    
    // translation access
    vector<Vector3D<double> > translation(void) { return translation_; }
    
    // propagation access
    Vector3D<double> propagation(void) { return propagation_; }
    
    // contact access
    int nContacts(void) { return contact_list_.size(); }
    PartAtomDescription* contact( int i_contact ) { return contact_list_[i_contact]; }
    
    // constructor
    ReservoirAtomDescription(FileHandler &fileHandler, bool verbose=true);
    
    // destructor
    ~ReservoirAtomDescription(void);
};


#endif



#ifndef L2STATE_H
#define L2STATE_H

using namespace std;

#include <string>
#include "Vector3D.h"

class L2State
{
  protected:
    
    // atributes
    Vector3D<double> center_;
    int global_index_;
    
  public:

    // default constructor
    L2State(void) {}

    // copy constructor
    L2State(const L2State &other)
    {
      center_=other.center_;
      global_index_=other.global_index_;
    }

    // standard construtor
    L2State(const Vector3D<double> &center, const int global_index)
    {
      center_=center;
      global_index_=global_index;
    }
    
    // method
    int globalIndex(void) { return global_index_; }
    Vector3D<double> center(void) { return center_; }
    
    // assignement operator
    L2State& operator=(const L2State &other)
    {
      center_=other.center_;
      global_index_=other.global_index_;
      //
      // return refrence
      //
      return *this;
    } 

    // destructor
    ~L2State(void) {}
};

#endif

#ifndef INDEXLIST_H
#define INDEXLIST_H

#define SMALL_ARRAY_THRESHOLD 32
//256

#include <vector>
#include <iostream>
#include "Sort.h"

using namespace std;

///////////////////////////////////////////
// 
// index printing
//
///////////////////////////////////////////

static ostream& operator << ( ostream& os , vector<int> indices )
{
  // null size
  if ( indices.size()==0 ) { os << "[]"; return os; }
  // print n-1 first index
  int i,stop;
  os << "[ ";
  for ( i=0 , stop=indices.size()-1 ; i<stop ; i++ ) os << indices[i] << " ";
  // print last index
  os << indices[i] << " ]";
  return os;
}

///////////////////////////////////////////
// 
// index list intersection
//
//   return the list of indices present 
//   in both lists
//
///////////////////////////////////////////

static vector<int> operator&&( vector<int> &left, vector<int> &right )
{
  //
  // create an empty list
  //
  vector<int> result;
  //
  // fast return if possible
  //
  if ( left.size()==0 || right.size()==0 )
  {
    return result;
  }
  //
  // search mas and min for each vectors to test if arrays overlap
  //
  int min_left=left[0];
  int max_left=left[0];
  int min_right=right[0];
  int max_right=right[0];
  //
  for ( int *__restrict__ ptr_left=&left[0] , *borne=&left[0]+left.size() ; ptr_left<borne ; ptr_left++ )
  {
    if ( max_left<*ptr_left ) max_left=*ptr_left;
    else if ( min_left>*ptr_left ) min_left=*ptr_left;
  }
  //
  for ( int *__restrict__ ptr_right=&right[0] , *borne=&right[0]+right.size() ; ptr_right<borne ; ptr_right++ )
  {
    if ( max_right<*ptr_right ) max_right=*ptr_right;
    else if ( min_right>*ptr_right ) min_right=*ptr_right;
  }
  //
  // if the two lists are not disjoints
  //
  if ( !( min_right > max_left || min_left > max_right ) )
  {
    //
    // reserve memory 
    //
    result.resize( min(left.size(),right.size()) );
    //
    // determine if one of the list is a segment
    //
    bool left_is_segment  = ( max_left-min_left   == left.size()-1  );
    bool right_is_segment = ( max_right-min_right == right.size()-1 );
    //
    // if left or right is segment
    //
    if ( left_is_segment && right_is_segment )
    {
      int imin = max( min_left,min_right );
      int imax = min( max_left,max_right ); 
      //
      result.resize(max(imax-imin+1,0));
      //
      for ( int i=imin , j=0 ; i<=imax ; i++ , j++ ) result[j]=i;
      //
      return result;
    }
    else if ( left_is_segment )
    {
      result.resize(right.size());
      //
      register int n=0;
      int *__restrict__ p_result=&result[0];
      //
      const int *end_right=&right[0]+right.size();
      //
      for ( int *__restrict__ j=&right[0] ; j<end_right ; j++ ) if ( (*j)<=max_left && (*j)>=min_left ) { (*p_result)=(*j); p_result++; n++; }
      //
      result.resize(n);
      //
      return result;
    }
    else if ( right_is_segment )
    {
      result.resize(left.size());
      //
      register int n=0;
      int *__restrict__ p_result=&result[0];
      //
      const int *end_left=&left[0]+left.size();
      //
      for ( int *__restrict__ j=&left[0] ; j<end_left ; j++ ) if ( (*j)<=max_right && (*j)>=min_right ) { (*p_result)=(*j); p_result++; n++; }
      //
      result.resize(n);
      //
      return result;
    }
    //
    // case 1: two small arrays => intersect without sorting  
    //
    if ( left.size()<SMALL_ARRAY_THRESHOLD && right.size()<SMALL_ARRAY_THRESHOLD )
    {
      register int n=0;
      int *__restrict__ p_result=&result[0];
      int *start_left=&left[0];
      int *start_right=&right[0];
      const int *end_left=&left[0]+left.size();
      const int *end_right=&right[0]+right.size();
      //
      // loop on the lists
      //
      for ( int *__restrict__ i=start_left ; i!=end_left ; i++ )
      {
        for ( int *__restrict__ j=start_right ; j!=end_right ; j++ )
        {
          if ( (*i)==(*j) ) { (*p_result)=(*i); p_result++; n++; break; }
        }
      }
      //
      // set intersection size
      //
      result.resize(n);
    }
    //
    // case 2: at least one large array => sort and intersect 
    //
    else
    {
      //
      // get a copy of the vectors
      //
      vector<int> left_=left;
      vector<int> right_=right;
      //
      // sort the indices by cressent order
      //
      if ( left_.size()>0 )  sort<int>( &left_[0], (int)left_.size() );
      if ( right_.size()>0 ) sort<int>( &right_[0], (int)right_.size() );
      //
      // intersect
      //
      register int n=0;
      int *__restrict__ p_result=&result[0];
      int *__restrict__ il=&left_[0];
      int *__restrict__ ir=&right_[0];
      const int *end_left=&left_[0]+left_.size();
      const int *end_right=&right_[0]+right_.size();
      //
      while ( il<end_left && ir<end_right )
      {
        while ( il<end_left && (*il)<(*ir) ) il++;
        //
        if ( il<end_left )
        {
          while ( ir<end_right && (*il)>(*ir) ) ir++;
          //
          if ( ir<end_right && (*il)==(*ir) ) 
          {
            (*p_result)=(*il);
            p_result++;
            n++;
            il++;
            ir++;
          }
        }
      }
      //
      // set intersection size
      //
      result.resize(n);
    }
  }
  //
  // return the list
  //
  return result;
}

///////////////////////////////////////////
// 
// index list union
//
//   return the list of indices present 
//   in at least one of lists
//
///////////////////////////////////////////

static vector<int> operator||( vector<int> &left , vector<int> &right )
{
  //
  // create a list form this one
  //
  vector<int> result;
  //
  // fast return if possible
  //
  if ( left.size()==0 || right.size()==0 || &left==&right )
  {
    return ( left.size()==0 ) ? right : left;
  }
  //
  // reserve memory 
  //
  result.reserve( left.size()+right.size() );
  //
  // search mas and min for each vectors to test if arrays overlap
  //
  int min_left=left[0];
  int max_left=left[0];
  int min_right=right[0];
  int max_right=right[0];
  //
  for ( int *__restrict__ ptr_left=&left[0] , *borne=&left[0]+left.size() ; ptr_left<borne ; ptr_left++ )
  {
    if ( max_left<*ptr_left ) max_left=*ptr_left;
    else if ( min_left>*ptr_left ) min_left=*ptr_left;
  }
  //
  for ( int *__restrict__ ptr_right=&right[0] , *borne=&right[0]+right.size() ; ptr_right<borne ; ptr_right++ )
  {
    if ( max_right<*ptr_right ) max_right=*ptr_right;
    else if ( min_right>*ptr_right ) min_right=*ptr_right;
  }
  //
  // if the two lists are disjoints
  //
  if ( min_right > max_left || min_left > max_right )
  {
    //
    // append the two lists in results
    //
    result=left;
    result.insert(result.end(),right.begin(),right.end());
  }
  //
  // if the two lists are not disjoints
  //
  else
  {
    //
    // case 1: two small arrays => unite without sorting  
    //
    if ( left.size()<SMALL_ARRAY_THRESHOLD && right.size()<SMALL_ARRAY_THRESHOLD )
    { 
      // 
      // init results with left indices
      //
      result=left;
      //
      // get reference pointers 
      //
      int *start_left=&left[0];
      int *start_right=&right[0];
      int *end_left=&left[0]+left.size();
      int *end_right=&right[0]+right.size();
      //
      // loop on the other list
      //
      for ( int *__restrict__ i=start_right ; i<end_right ; i++ )
      {
        //
        // search this index in the left list
        //
        int *__restrict__ j=start_left;
        for ( ; j<end_left ; j++ ) if ( (*j)==(*i) ) break;
        //
        // add if the index if not found 
        //
        if ( j>=end_left ) result.push_back((*i));
      }
    }
    //
    // cas 2
    //
    else
    {
      //
      // get a copy of the vectors
      //
      vector<int> left_=left;
      vector<int> right_=right;
      //
      // sort the indices by cressent order
      //
      if ( left_.size()>0 )  sort<int>( &left_[0], (int)left_.size() );
      if ( right_.size()>0 ) sort<int>( &right_[0], (int)right_.size() );
      //
      // unite
      //
      int *__restrict__ il=&left_[0];
      int *__restrict__ ir=&right_[0];
      int *end_left=&left_[0]+left_.size();
      int *end_right=&right_[0]+right_.size();
      //
      while ( il<end_left && ir<end_right )
      {
        while ( il<end_left && (*il)<(*ir) ) 
        {
          result.push_back((*il));
          il++;
        }
        if ( il<end_left )
        {
          while ( ir<end_right && (*il)>(*ir) )
          {
            result.push_back((*ir));
            ir++;
          }
          if ( ir<end_right && (*il)==(*ir) ) 
          {
            result.push_back((*il));
            il++;
            ir++;
          }
        }
      }
      //
      // complete with remaining coeffs
      //
      while ( il<end_left )
      {
        result.push_back((*il));
        il++;
      } 
      while ( ir<end_right )
      {
        result.push_back((*ir));
        ir++;
      } 
    }
  }
  //
  // readjust memory occupation
  //
  result.reserve( result.size() );
  //
  // return the list
  //
  return result; 
}

///////////////////////////////////////////
// 
// index list difference
//
//   return the list of indices in left list 
//   that are not in right list
//
///////////////////////////////////////////

static vector<int> operator!=( vector<int> &left , vector<int> &right )
{
  //
  // create a list form this one
  //
  vector<int> result;
  //
  // reserve memory 
  //
  result.reserve( left.size() );
  //
  // search mas and min for each vectors to test if arrays overlap
  //
  int min_left=left[0];
  int max_left=left[0];
  int min_right=right[0];
  int max_right=right[0];
  //
  for ( int *ptr_left=&left[0] , *borne=&left[0]+left.size() ; ptr_left<borne ; ptr_left++ )
  {
    if ( max_left<*ptr_left ) max_left=*ptr_left;
    else if ( min_left>*ptr_left ) min_left=*ptr_left;
  }
  //
  for ( int *ptr_right=&right[0] , *borne=&right[0]+right.size() ; ptr_right<borne ; ptr_right++ )
  {
    if ( max_right<*ptr_right ) max_right=*ptr_right;
    else if ( min_right>*ptr_right ) min_right=*ptr_right;
  }
  //
  // if the two lists are disjoints
  //
  if ( min_right > max_left || min_left > max_right )
  {
    //
    // result is the left list
    //
    result=left;
  }
  //
  // if the two lists are not disjoints
  //
  else
  {
    //
    // case 1: two small arrays => unite without sorting  
    //
    if ( left.size()<SMALL_ARRAY_THRESHOLD && right.size()<SMALL_ARRAY_THRESHOLD )
    {
      //
      // loop on the left list indices
      //
      for ( int i=0 , stop_i=left.size()   ; i<stop_i ; i++ )
      {
        //
        // search this index in the right list
        //
        int j=0;
        int index=left[i];
        //
        for ( int stop_j=right.size() ; j<stop_j ; j++ ) if ( right[j]==index ) break;
        //
        // add if the index is not found 
        //
        if ( j>=right.size() ) result.push_back( index );
      }
    }
    //
    // cas 2
    //
    else
    {
      //
      // get a copy of the vectors
      //
      vector<int> left_=left;
      vector<int> right_=right;
      //
      // sort the indices by cressent order
      //
      if ( left_.size()>0 )  sort<int>( &left_[0], (int)left_.size() );
      if ( right_.size()>0 ) sort<int>( &right_[0], (int)right_.size() );
      //
      // get difference
      //
      int i1=0;
      int i2=0;
      int count1=left_.size();
      int count2=right_.size();
      //
      while ( i1<count1 && i2<count2 )
      {
        while ( i1<count1 && left_[i1]<right_[i2] ) 
        {
          result.push_back(left_[i1]);
          i1++;
        }
        if ( i1<count1 )
        {
          while ( i2<count2 && left_[i1]>right_[i2] )
          {
            i2++;
          }
          if ( i2<count2 && left_[i1]==right_[i2] ) 
          {
            i1++;
            i2++;
          }
        }
      }
      //
      // complete with remaining coeff
      //
      while ( i1<count1 )
      {
        result.push_back(left_[i1]);
        i1++;
      } 
    }
  }
  //
  // readjust memory occupation
  //
  result.reserve( result.size() );
  //
  // return the list
  //
  return result; 
}

///////////////////////////////////////////
// 
// index finding
//
//   return the positions in the right list  
//   of item in the left list
//
///////////////////////////////////////////

/*
static vector<int> operator<=( vector<int> &left , vector<int> &right )
{
  //
  // create an empty list
  //
  vector<int> result;
  //
  // loop on the left list
  //
  for ( int i=0 ; i<left.size() ; i++ )
  {
    //
    // get corresponding index in the right list
    //
    int j=0;
    int index=left[i];
    //
    for ( int stop=right.size() ; j<stop ; j++ ) if ( right[j]==index )  break;
    //
    // verify that we found the corresponding index
    //
    if ( j>=right.size() )
    {
      cout << "Error: cannot find index " << index << " during left_list>>right_list operation" << endl;
      exit(0);
    }
    //
    // add this index to the list
    //
    result.push_back( j );
  }
  //
  // return the index list
  //
  return result;
}
*/

static vector<int> operator<=( vector<int> &left, vector<int> &right )
{
  //
  // create the list
  //
  vector<int> result(left.size());
  //
  // fast return if possible
  //
  if ( left.size()==0 )
  {
    return result;
  }
  //
  // case 1: two small arrays => extract without sorting  
  //
  if ( left.size()<SMALL_ARRAY_THRESHOLD && right.size()<SMALL_ARRAY_THRESHOLD )
  {
    int size_right=right.size();
    int *__restrict__ p_result=&result[0];
    int *start_left=&left[0];
    int *start_right=&right[0];
    const int *end_left=&left[0]+left.size();
    const int *end_right=&right[0]+right.size();
    //
    // loop on the lists
    //
    for ( int *__restrict__ i=start_left ; i!=end_left ; i++ )
    {
      register int n=0;
      //
      for ( int *__restrict__ j=start_right ; j!=end_right ; j++ , n++ )
      {
        if ( (*i)==(*j) ) { (*p_result)=n; p_result++;  break; }
      }
      //
      if ( n>=size_right )
      {
        cout << "Error: cannot find index " << (*i) << " during left_list<=right_list indices extraction" << endl;
        exit(0);
      }
    }
  }
  //
  // case 2: at least one large array => sort and intersect 
  //
  else
  {
    int size_left=left.size();
    int size_right=right.size();
    //
    // get a copy of the vectors
    //
    vector<int> left_=left;
    vector<int> right_=right;
    vector<int> trans_l(left.size());
    vector<int> trans_r(right.size());
    for ( int i=0 ; i<size_left  ; i++ ) trans_l[i]=i;
    for ( int i=0 ; i<size_right ; i++ ) trans_r[i]=i;
    //
    // sort the indices by cressent order
    //
    if ( left_.size()>0 )  sort<int>( &left_[0], (int)left_.size(), &trans_l[0] );
    if ( right_.size()>0 ) sort<int>( &right_[0], (int)right_.size(), &trans_r[0] );
    //
    // intersect
    //
    int *__restrict__ p_result=&result[0];
    int *__restrict__ il=&left_[0];
    int *__restrict__ ir=&right_[0];
    int *__restrict__ tl=&trans_l[0];
    int *__restrict__ tr=&trans_r[0];
    const int *end_left=&left_[0]+left_.size();
    const int *end_right=&right_[0]+right_.size();
    //
    while ( il<end_left && ir<end_right )
    {
      while ( ir<end_right && (*ir)<(*il) ) { ir++; tr++; }
      //
      if ( ir<end_right && (*il)==(*ir) ) 
      {
        (*(p_result+(*tl)))=(*tr);
        il++;
        ir++;
        tl++;
        tr++;
      }
      else
      {
        cout << "Error: cannot find index " << (*il) << " during left_list<=right_list indices extraction" << endl;
        exit(0);
      }
    }
  }
  //
  // return the list
  //
  return result;
}

// same as <=, but set index to 0 if cannot find an element
//
static vector<int> tryExtraction( vector<int> &left, vector<int> &right )
{
  //
  // create the list
  //
  vector<int> result(left.size());
  //
  // fast return if possible
  //
  if ( left.size()==0 )
  {
    return result;
  }
  //
  // case 1: two small arrays => extract without sorting  
  //
  if ( left.size()<SMALL_ARRAY_THRESHOLD && right.size()<SMALL_ARRAY_THRESHOLD )
  {
    int size_right=right.size();
    int *__restrict__ p_result=&result[0];
    int *start_left=&left[0];
    int *start_right=&right[0];
    const int *end_left=&left[0]+left.size();
    const int *end_right=&right[0]+right.size();
    //
    // loop on the lists
    //
    for ( int *__restrict__ i=start_left ; i!=end_left ; i++ )
    {
      register int n=0;
      //
      for ( int *__restrict__ j=start_right ; j!=end_right ; j++ , n++ )
      {
        if ( (*i)==(*j) ) { (*p_result)=n; p_result++;  break; }
      }
      //
      if ( n>=size_right )
      {
        (*p_result)=-1; p_result++;
      }
    }
  }
  //
  // case 2: at least one large array => sort and intersect 
  //
  else
  {
    int size_left=left.size();
    int size_right=right.size();
    //
    // get a copy of the vectors
    //
    vector<int> left_=left;
    vector<int> right_=right;
    vector<int> trans_l(left.size());
    vector<int> trans_r(right.size());
    for ( int i=0 ; i<size_left  ; i++ ) trans_l[i]=i;
    for ( int i=0 ; i<size_right ; i++ ) trans_r[i]=i;
    //
    // sort the indices by cressent order
    //
    if ( left_.size()>0 )  sort<int>( &left_[0], (int)left_.size(), &trans_l[0] );
    if ( right_.size()>0 ) sort<int>( &right_[0], (int)right_.size(), &trans_r[0] );
    //
    // intersect
    //
    int *__restrict__ p_result=&result[0];
    int *__restrict__ il=&left_[0];
    int *__restrict__ ir=&right_[0];
    int *__restrict__ tl=&trans_l[0];
    int *__restrict__ tr=&trans_r[0];
    const int *end_left=&left_[0]+left_.size();
    const int *end_right=&right_[0]+right_.size();
    //
    while ( il<end_left && ir<end_right )
    {
      while ( ir<end_right && (*ir)<(*il) ) { ir++; tr++; }
      //
      if ( ir<end_right && (*il)==(*ir) ) 
      {
        (*(p_result+(*tl)))=(*tr);
        il++;
        ir++;
        tl++;
        tr++;
      }
      else
      {
        (*(p_result+(*tl)))=-1;
        il++;
        ir++;
        tl++;
        tr++;
      }
    }
  }
  //
  // return the list
  //
  return result;
}


//
// same as <= extraction, but does not complain if an index of left is not found
// and also return multiplicity of occurences in the right list
//
static vector<int> weakExtraction( vector<int> &left, vector<int> &right )
{
  //
  // create the list
  //
  int size_result=0;
  vector<int> result(right.size());
  //
  // fast return if possible
  //
  if ( left.size()==0 )
  {
    return result;
  }
  //
  // case 1: two small arrays => extract without sorting  
  //
  if ( left.size()<SMALL_ARRAY_THRESHOLD && right.size()<SMALL_ARRAY_THRESHOLD )
  {
    int size_right=right.size();
    int *__restrict__ p_result=&result[0];
    int *start_left=&left[0];
    int *start_right=&right[0];
    const int *end_left=&left[0]+left.size();
    const int *end_right=&right[0]+right.size();
    //
    // loop on the lists
    //
    for ( int *__restrict__ i=start_left ; i!=end_left ; i++ )
    {
      register int n=0;
      //
      for ( int *__restrict__ j=start_right ; j!=end_right ; j++ , n++ )
      {
        if ( (*i)==(*j) ) { (*p_result)=n; p_result++; size_result++; }
      }
    }
  }
  //
  // case 2: at least one large array => sort and intersect 
  //
  else
  {
    int size_left=left.size();
    int size_right=right.size();
    //
    // get a copy of the vectors
    //
    vector<int> left_=left;
    vector<int> right_=right;
    vector<int> trans_l(left.size());
    vector<int> trans_r(right.size());
    for ( int i=0 ; i<size_left  ; i++ ) trans_l[i]=i;
    for ( int i=0 ; i<size_right ; i++ ) trans_r[i]=i;
    //
    // sort the indices by cressent order
    //
    if ( left_.size()>0 )  sort<int>( &left_[0], (int)left_.size(), &trans_l[0] );
    if ( right_.size()>0 ) sort<int>( &right_[0], (int)right_.size(), &trans_r[0] );
    //
    // intersect
    //
    int *__restrict__ p_result=&result[0];
    int *__restrict__ il=&left_[0];
    int *__restrict__ ir=&right_[0];
    int *__restrict__ tl=&trans_l[0];
    int *__restrict__ tr=&trans_r[0];
    const int *end_left=&left_[0]+left_.size();
    const int *end_right=&right_[0]+right_.size();
    //
    while ( il<end_left && ir<end_right )
    {
      while ( ir<end_right && (*ir)<(*il) ) { ir++; tr++; }
      //
      if ( ir<end_right && (*il)==(*ir) ) 
      {
        (*p_result)=(*tr);
        ir++;
        tr++;
        p_result++;
        size_result++;
      }
      else
      {
        il++;
        tl++;
      }
    }
  }
  //
  // resize
  //
  result.resize(size_result);
  //
  // return the list
  //
  return result;
}

static int operator<=( int left , vector<int> &right )
{
  //
  // loop on the right list to find corresponding index
  //
  for ( int i=0 ; i<right.size() ; i++ ) if ( right[i]==left ) return i;
  //
  // verify that we found the corresponding index
  //
  cout << "Error: cannot find index " << left << " during index<=list_index extraction" << endl;
  exit(0);
  return -1;
}

#endif

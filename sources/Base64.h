#ifndef BASE64_H
#define BASE64_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#ifdef CSTDLIB
  #include <cstdlib>
#endif  
#define TCHAR char    //Not unicode
#define TEXT(x) x     //Not unicode
#define DWORD long
#define BYTE unsigned char

//Lookup table for encoding
const static TCHAR encodeLookup[] = TEXT("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
const static TCHAR padCharacter = TEXT('=');

using namespace std;

/*
// packing and unpacking functions
std::vector<BYTE> pack2LittleEndian(std::vector<double> &inputBuffer);
std::vector<double> unpack2LocalEndian(std::vector<BYTE> &inputBuffer);

// enconding and decoding functions
std::basic_string<TCHAR> base64Encode(std::vector<BYTE> &inputBuffer);
std::vector<BYTE> base64Decode(const std::basic_string<TCHAR>& input);
*/

// template for packing function
template <class Type>
std::vector<BYTE> orderAsLittleEndian(Type *inputBuffer, int N)
{
  std::vector<BYTE> outputBuffer;
  // test the endianness and reorder to litle indian if necessary
  long one= 1;
  // if the arch is big endian
  if (!(*((char *)(&one))))
  {
    // reserve output buffer
    outputBuffer.reserve(N*sizeof(double));
    // get size for the type of data
    int size_data=sizeof(double);
    int size_data_m1=size_data-1;
    // loop on the data
    for ( int i=0 , stop=N ; i<stop ; i++ )
    {
      // coonvert value to double
      double value=(double)(*(double *)&inputBuffer[i]);
      // get pointer on this value
      BYTE *psrc=(BYTE *)&value;
      // loop on the bytes
      for ( int j=size_data_m1 ; j>=0 ; j-- ) outputBuffer.push_back(psrc[j]);
    }
  }
  else
  {
    // allocate output buffer
    outputBuffer.resize(N*sizeof(double));
    // get size for the type of data
    int size_data=sizeof(double);
    // loop on the data
    for ( int i=0 , stop=N ; i<stop ; i++ )
    {
      // coonvert value to double
      double value=(double)(*(double *)&inputBuffer[i]);
      // get pointer on this value
      BYTE *psrc=(BYTE *)&value;
      // loop on the bytes
      for ( int j=0 ; j<size_data ; j++ ) outputBuffer.push_back(psrc[j]);
    }
  }
  // return the buffer
  return outputBuffer;
}


#define SIZE_INPUT_CHUNK 2048

// base64 encoding
template <class Type>
void base64EncodeOnTheFly(std::string fileName, Type *coeff, int N)
{
  // 
  // allocate
  //
  DWORD temp;
  std::vector<BYTE> inputBuffer;
  inputBuffer.reserve(SIZE_INPUT_CHUNK*sizeof(double));
  //
  std::basic_string<TCHAR> encodedString;
  encodedString.reserve(SIZE_INPUT_CHUNK*2);
  //
  long one= 1;
  bool bigEndian=(!(*((char *)(&one))));
  //
  // open the file
  //
  ofstream filestr;
  filestr.open (fileName.data());
  //
  // translate
  //
  int i_input=0;
  while ( i_input<N )
  {
    // 
    // read a chunk of the input and order it in the byte buffer
    //
    if ( bigEndian )
    {
      // get size for the type of data
      int size_data=sizeof(double);
      int size_data_m1=size_data-1;
      // loop on the data
      for ( int i=0 ; i<SIZE_INPUT_CHUNK && i_input<N ; i++ , i_input++ )
      {
        // coonvert value to double
        double value=(double)(*(double *)&coeff[i_input]);
        // get pointer on this value
        BYTE *psrc=(BYTE *)&value;
        // loop on the bytes
        for ( int j=size_data_m1 ; j>=0 ; j-- ) inputBuffer.push_back(psrc[j]);
      }
    }
    else
    {
      // get size for the type of data
      int size_data=sizeof(double);
      // loop on the data
      for ( int i=0 ; i<SIZE_INPUT_CHUNK && i_input<N ; i++ , i_input++ )
      {
        // coonvert value to double
        double value=(double)(*(double *)&coeff[i_input]);
        // get pointer on this value
        BYTE *psrc=(BYTE *)&value;
        // loop on the bytes
        for ( int j=0 ; j<size_data ; j++ ) inputBuffer.push_back(psrc[j]);
      }
    }
    //
    // translate the content of the buffer in a file
    //
    size_t idx,stop;
    //
    for( idx = 0 , stop=inputBuffer.size()-2 ; idx < stop ; idx+=3)
    {
      temp  = inputBuffer[idx]   << 16; //Convert to big endian
      temp += inputBuffer[idx+1] << 8;
      temp += inputBuffer[idx+2];
      encodedString.append(1,encodeLookup[(temp & 0x00FC0000) >> 18]);
      encodedString.append(1,encodeLookup[(temp & 0x0003F000) >> 12]);
      encodedString.append(1,encodeLookup[(temp & 0x00000FC0) >> 6 ]);
      encodedString.append(1,encodeLookup[(temp & 0x0000003F)      ]);

    }
    //
    // print the content of the encoding
    //
    filestr << encodedString;
    encodedString="";
    //
    // remove the bytes that have been translated
    //
    inputBuffer.erase(inputBuffer.begin(),inputBuffer.begin()+idx);
  }
  //
  // finish to tranlate the end of the byte buffer
  //
  switch(inputBuffer.size() % 3)
  {
    case 1:
      temp  = inputBuffer[0] << 16; //Convert to big endian
      encodedString.append(1,encodeLookup[(temp & 0x00FC0000) >> 18]);
      encodedString.append(1,encodeLookup[(temp & 0x0003F000) >> 12]);
      encodedString.append(2,padCharacter);
      
      
      break;
    case 2:
      temp  = inputBuffer[0] << 16; //Convert to big endian
      temp += inputBuffer[1] << 8;
      encodedString.append(1,encodeLookup[(temp & 0x00FC0000) >> 18]);
      encodedString.append(1,encodeLookup[(temp & 0x0003F000) >> 12]);
      encodedString.append(1,encodeLookup[(temp & 0x00000FC0) >> 6 ]);
      encodedString.append(1,padCharacter);
      break;
  }
  //
  // print the content of the encoding
  //
  filestr << encodedString;
  //
  // close the file
  //
  filestr.close();
}

#define SIZE_BYTE_INPUT_CHUNK 4096

// base64 encoding
template <class Type>
void base64DecodeOnTheFly(std::string fileName, Type *coeff, int N)
{
  //Sanity check
  if (SIZE_BYTE_INPUT_CHUNK % 4)
  {
    std::cout << "Non-Valid size for SIZE_BYTE_INPUT_CHUNK detected in base64Decode!" << std::endl;
    exit(0);
  }
  
  // declare and reserve arrays
  std::basic_string<TCHAR> encodedChars;
  encodedChars.reserve(SIZE_BYTE_INPUT_CHUNK);
  std::vector<BYTE> decodedBytes;
  decodedBytes.reserve(SIZE_BYTE_INPUT_CHUNK);
  DWORD temp=0; //Holds decoded quanta
  
  // detect the architecture
  long one= 1;
  bool bigEndian = (!(*((char *)(&one))));
  
  // open the file
  ifstream filestr;
  filestr.open (fileName.data());
  //
  // check the opening
  //  
  if ( filestr.fail() )
  {
    cout << "Error: cannot open file " << fileName << " for reading interactions" << endl;
    exit (0);
  }
  
  //
  // decode on the fly
  //
  int  i_coeff=0;
  bool end_reached=false;
  char buff[SIZE_BYTE_INPUT_CHUNK+1];
  buff[SIZE_BYTE_INPUT_CHUNK]=0;
  //
  while ( i_coeff<N )
  {
    //
    // read a buch of bytes from the file
    //
    filestr.read(buff,SIZE_BYTE_INPUT_CHUNK);
    int nchar=filestr.gcount();
    //
    // sanity check
    //
    if (nchar % 4)
    {
      std::cout << "Non-Valid base64 detected in base64Decode! nChar=" << nchar << std::endl;
      exit(0);
    }
    //
    // add those chars to the char buffer
    //
    for ( int i=0 ; i<nchar ; i++ ) encodedChars.push_back(buff[i]);
    //
    // loop on the curent encoded characters
    //
    std::basic_string<TCHAR>::const_iterator cursor = encodedChars.begin();
    while (cursor < encodedChars.end())
    {
      for (size_t quantumPosition = 0; quantumPosition < 4; quantumPosition++)
      {
        temp <<= 6;
        if       (*cursor >= 0x41 && *cursor <= 0x5A) // This area will need tweaking if
          temp |= *cursor - 0x41;                     // you are using an alternate alphabet
        else if  (*cursor >= 0x61 && *cursor <= 0x7A)
          temp |= *cursor - 0x47;
        else if  (*cursor >= 0x30 && *cursor <= 0x39)
          temp |= *cursor + 0x04;
        else if  (*cursor == 0x2B)
          temp |= 0x3E; //change to 0x2D for URL alphabet
        else if  (*cursor == 0x2F)
          temp |= 0x3F; //change to 0x5F for URL alphabet
        else if  (*cursor == padCharacter) //pad
        {
          switch( encodedChars.end() - cursor )
          {
            case 1: //One pad character
              decodedBytes.push_back((temp >> 16) & 0x000000FF);
              decodedBytes.push_back((temp >> 8 ) & 0x000000FF);
              end_reached=true;
              break;
            case 2: //Two pad characters
              decodedBytes.push_back((temp >> 10) & 0x000000FF);
              end_reached=true;
              break;
            default:
              std::cout << "Invalid Padding in Base 64 detected during base64 on the fly extraction!" << std::endl;
              exit(0);
          }
        }  
        else
        {
          std::cout << "Non-Valid Character in Base 64 detected during base64 on the fly extraction!" << std::endl;
          exit(0);
        }
        cursor++;
      }
      if ( !end_reached )
      {
        decodedBytes.push_back((temp >> 16) & 0x000000FF);
        decodedBytes.push_back((temp >> 8 ) & 0x000000FF);
        decodedBytes.push_back((temp      ) & 0x000000FF);
      }
    }
    //
    // reset the list of encoded characters
    //
    encodedChars.resize(0);
    //
    // check that we have something to fill the array with
    //
    if ( i_coeff<N && decodedBytes.size()==0 )
    {
      cout << "Error: incomplete base64 file encoutered during on the fly base64 decoding" << endl;
      exit(0);
    }
    //
    // fill the array
    //
    int i,stop;
    //
    if ( bigEndian )
    {
      int size_data=sizeof(double);
      int size_data_m1=size_data-1;
      //
      // loop on the data
      //
      for ( i=0 , stop=decodedBytes.size()-size_data_m1 ; i<stop ; i+=size_data , i_coeff++ )
      {
        //
        // loop on the bytes
        //
        BYTE *pdest=(BYTE *)&coeff[i_coeff];
        BYTE *psrc =(BYTE *)&decodedBytes[i];
        for ( int j=size_data_m1 ; j>=0 ; j-- , pdest++ ) (*pdest)=psrc[j];
      }
    }
    else
    {
      int size_data=sizeof(double);
      int size_data_m1=size_data-1;
      //
      // loop on the data
      //
      for ( i=0 , stop=decodedBytes.size()-size_data_m1 ; i<stop ; i+=size_data , i_coeff++ )
      {
        //
        // loop on the bytes
        //
        BYTE *pdest=(BYTE *)&coeff[i_coeff];
        BYTE *psrc =(BYTE *)&decodedBytes[i];
        for ( int j=0 ; j<size_data ; j++ , pdest++ ) (*pdest)=psrc[j];
      }
    }
    //
    // remove the bytes that has been stored
    //
    decodedBytes.erase(decodedBytes.begin(),decodedBytes.begin()+i);
  }
  //
  // check that everything has been translated
  //
  if ( decodedBytes.size()!=0 ) 
  {
    cout << "Error: problem during base64 on the fly extraction" << endl;
    exit(0);
  }
  //
  // close the file
  //
  filestr.close();
}

#endif


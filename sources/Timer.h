#ifndef TIMER_H
#define TIMER_H

#include <map>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <unistd.h>

#include "Context.h"

class Timer
{
  private:

  clock_t clk;
  double t,total_cpu,total_real;
  int running_;

  public:

  Timer() : total_cpu(0.0), total_real(0.0), running_(0) {};

  void reset() { total_cpu = 0.0; total_real = 0.0; running_ = 0; };

  void start()
  {
    clk = clock();
    t = gtod();
    running_ = 1;
  };

  int running() { return running_; };

  void stop()
  {
    if ( running_ )
    {
      total_cpu += ((double)(clock()-clk))/CLOCKS_PER_SEC;
      total_real += gtod()-t;
      running_ = 0;
    }
  };

  double cpu()
  {
    if ( running_ )
    {
      return total_cpu + ((double)(clock()-clk))/CLOCKS_PER_SEC;
    }
    else
    {
      return total_cpu;
    }
  };

  double real()
  {
    if ( running_ )
    {
      return total_real + gtod()-t;
    }
    else
    {
      return total_real;
    }
  };

  double gtod(void)
  {
    static struct timeval tv;
    static struct timezone tz;
    gettimeofday(&tv,&tz);
    return tv.tv_sec + 1.e-6*tv.tv_usec;
  }
};

class TimerMap
{
  private:
    
    std::map<std::string,Timer> tmap_;
  
  public:
  
    // access to the map
    Timer& operator[](std::string str) { return tmap_[str]; }
    
    // display timing method
    void print(std::ostream &str, std::string comment, Context &context, int source=-1) 
    {
      //
      // wait for everybody in this context
      //
      MPI_Barrier(context.comm());
      //
      // if we are on the source
      //
      if ( source==-1 && context.myContext()<=0 )
      {
        //
        // print header
        //
        if ( context.myProc()==0 ) 
        {
          str << endl;
          str << "-------------------------------------------------------------------------" << endl;
          str << comment << endl;
        }
        //
        // loop on times
        //
        for ( std::map<std::string,Timer>::iterator i = tmap_.begin(); i != tmap_.end(); i++ )
        {
          //
          // get total number of procs
          //
          int i_procs;
          int n_procs;
          MPI_Comm_size( MPI_COMM_WORLD, &n_procs);
          //
          // reduce timer values
          //
          double min_cpu;
          double max_cpu;
          double tot_cpu;
          double min_real;
          double max_real;
          double tot_real;
          double loc_cpu=(*i).second.cpu();
          double loc_real=(*i).second.real();
          MPI_Allreduce( (void *) &loc_cpu, (void *) &min_cpu, 1, MPI_DOUBLE, MPI_MIN, context.comm());
          MPI_Allreduce( (void *) &loc_cpu, (void *) &max_cpu, 1, MPI_DOUBLE, MPI_MAX, context.comm());
          MPI_Allreduce( (void *) &loc_cpu, (void *) &tot_cpu, 1, MPI_DOUBLE, MPI_SUM, context.comm());
          MPI_Allreduce( (void *) &loc_real, (void *) &min_real, 1, MPI_DOUBLE, MPI_MIN, context.comm());
          MPI_Allreduce( (void *) &loc_real, (void *) &max_real, 1, MPI_DOUBLE, MPI_MAX, context.comm());
          MPI_Allreduce( (void *) &loc_real, (void *) &tot_real, 1, MPI_DOUBLE, MPI_SUM, context.comm());
          //
          // print values
          //
          if ( context.myProc()==0 ) 
          {
            str.precision(4);
            str << "<timing name=\"";
            str << (*i).first << "\""; 
            str.width(52-(*i).first.size());
            str << " cpu =\"(min/max/av.) ";
            str.width(9);
            str << min_cpu << "s / ";
            str.width(9);
            str << max_cpu << "s / ";
            str.width(9);
            str << tot_cpu/context.nProcs() << "s\"" << endl;
            str << "              ";
            str.width(32);
            str << " ";
            str << " real=\"(min/max/av.) ";
            str.width(9);
            str << min_real << "s / ";
            str.width(9);
            str << max_real << "s / ";
            str.width(9);
            str << tot_real/context.nProcs() << "s\">" << endl; 
          }   
        }
        //
        // close field
        //   
        if ( context.myProc()==0 ) 
        {
          str << "-------------------------------------------------------------------------" << endl;
        }
      }
      //
      // if we are on the source
      //
      else if ( context.myProc()==source && context.myContext()<=0 )
      {
        //
        // print header
        //
        str << endl;
        str << "-------------------------------------------------------------------------" << endl;
        str << comment << endl;
        //
        // loop on times
        //
        for ( std::map<std::string,Timer>::iterator i = tmap_.begin(); i != tmap_.end(); i++ )
        {
          //
          // print values
          //
          str.precision(4);
          str << "<timing name=\"";
          str << (*i).first << "\""; 
          str.width(45-(*i).first.size());
          str << " t=\"";
          str.width(9);
          str << (*i).second.real() << "\"s/>" << endl;
        }
        //
        // close field
        //   
        str << "-------------------------------------------------------------------------" << endl;
      }      
      //
      // let time for the system to flush
      //
      usleep(100000);
    }
      
    // constructor
    TimerMap(void) {}
    
    // destructor
    ~TimerMap(void) {} 
};

#endif

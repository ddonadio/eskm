#ifndef HARMONICOSCILLATORPART_H
#define HARMONICOSCILLATORPART_H

#include "L2Part.h"
#include "DlpolyInterface.h"
#include "ExternalCoeffInterface.h"
#include "HarmonicOscillatorBasis.h"

template <class SolutionType>
class HarmonicOscillatorPart : public L2Part<SolutionType,HarmonicOscillatorState>
{
  private: 
  
    // reference to the basis
    HarmonicOscillatorBasis &basis_;
    
    // index of this part in the basis
    int my_part_;
    
  public:
    
    // checking method
    virtual bool check(void);
    
    // constructor
    HarmonicOscillatorPart(HarmonicOscillatorBasis &basis, Context &context, int ipart, string tmpPath="");
    
    // destructor
    ~HarmonicOscillatorPart(){}
};

// constructor
template <class SolutionType>
HarmonicOscillatorPart<SolutionType>::HarmonicOscillatorPart(HarmonicOscillatorBasis &basis, Context &context, int ipart, string tmpPath) :
L2Part<SolutionType,HarmonicOscillatorState>(basis,context,ipart) , basis_(basis) , my_part_(ipart)
{
  char ID[256];
  //
  // get local range and domain 
  //
  vector<int> range =this->matrix_pencil_->localRange();
  vector<int> domain=this->matrix_pencil_->domain();
  //
  // fast return if possible
  //
  if ( range.size()==0 ) return;
  //
  // if we use external forces
  //
  if ( basis.hasDefectPartFlag(ipart,"externalForces") )
  {
    //
    // form the whole defect index list
    //
    vector<int> full_index_list = basis.getExtendedDefectIndices(); 
    //
    // fill coeffs
    //
#ifndef DUMMY
    ExternalCoeffInterface<SolutionType>("data/defect.dyn", range, domain, full_index_list, this->matrix_pencil_->coeffPtr());
    //
    // if this part is a contact
    //
    int i_reservoir;
    if ( basis.isContact(ipart, &i_reservoir) )
    {
      //
      // forge reservoir name
      //
      char buff[1024];
      sprintf(buff,"data/reservoir_%i.dyn",i_reservoir);
      //
      // get reservoir index list
      //
      vector<int> res_index_list = basis.getReservoirIndices(i_reservoir); 
      //
      // fill coeffs from reservoir file
      //
      ExternalCoeffInterface<SolutionType>(buff, range, domain, res_index_list, this->matrix_pencil_->coeffPtr());
    }
#else
    //
    // allocate pencil memory
    //
    this->matrix_pencil_->coeffVector().resize(this->matrix_pencil_->domain().size()*this->matrix_pencil_->localRange().size());
    //
    // fill coeffs
    //
    ExternalCoeffInterface<SolutionType>("data/defect.dyn", range, domain, full_index_list, this->matrix_pencil_->coeffPtr());
    //
    // if this part is a contact
    //
    int i_reservoir;
    if ( basis.isContact(ipart, &i_reservoir) )
    {
      //
      // forge reservoir name
      //
      char buff[1024];
      sprintf(buff,"data/reservoir_%i.dyn",i_reservoir);
      //
      // get reservoir index list
      //
      vector<int> res_index_list = basis.getReservoirIndices(i_reservoir); 
      //
      // fill coeffs from reservoir file
      //
      ExternalCoeffInterface<SolutionType>(buff, range, domain, res_index_list, this->matrix_pencil_->coeffPtr());
    }
    //
    // reduce pencil as it would be during check 
    // (memory is released during reduce operation)
    //
    this->matrix_pencil_->reduce(0.0,0.0); 
#endif
  }
  //
  // otherwise use DLpoly
  //
  else
  {
    //
    // join the two lists
    //
    vector<int> indices=range||domain;
    //
    // check that the range is included in the domain
    //
    if ( indices.size()!=domain.size() )
    {
      cout << "Error: inconsistent domain in HarmonicOscillatorPart::HarmonicOscillatorPart, part range should be included in its domain." << endl;
      exit(0);
    }
    //
    // print ID for those interactions
    //
    sprintf(ID,"HOP_%i",ipart);
    //
    // get the atom list corresponding the the index list
    //
    vector<Atom<double> > list_atom=basis.getAtomList(indices); 
    //
    // call the interface with DLpoly
    //
#ifndef DUMMY
    DlpolyInterface<SolutionType>(ID, range, domain, indices, list_atom, basis.getPartTranslations(ipart), this->matrix_pencil_->coeffPtr());
#else
    //
    // allocate pencil memory
    //
    this->matrix_pencil_->coeffVector().resize(this->matrix_pencil_->domain().size()*this->matrix_pencil_->localRange().size());
    //
    // fill coeffs
    //
    DlpolyInterface<SolutionType>(ID, range, domain, indices, list_atom, basis.getPartTranslations(ipart), this->matrix_pencil_->coeffPtr());
    //
    // reduce pencil as it would be during check 
    // (memory is released during reduce operation)
    //
    this->matrix_pencil_->reduce(0.0,0.0); 
#endif
  }
  //
  // if we are saving memory space
  //
  if ( tmpPath!="" ) 
  {
    //
    // move content of matrix pencil to disc
    //
    this->matrix_pencil_->moveToDisc(tmpPath);
  }
}

template <class SolutionType>
bool HarmonicOscillatorPart<SolutionType>::check(void)
{
  //
  // remove all empty colums for the matrix pencil:
  //
  this->matrix_pencil_->reduce(0.0,0.0);
  //
  // return good operation
  //
  return true;
}



#endif

#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <stdlib.h>
#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

class FileHandler
{
  private:
    
    // stream references
    vector<ifstream *> filestreampile_;
    
    // buffer
    char buff_[1024];
    int  last_int;
    
    // end of file flag
    bool eof_;
    
  public:
    
    // constructor
    FileHandler(void) {}
    
    // methods
    bool  eof(void);
    void  close(void);
    void  open(const char *fileName);
    char* readNextWord(void);
    char* lastWord(void) { return buff_; }
    void  skipLine(void);
    ifstream* ifstreamPtr() { return filestreampile_.back(); }
    
    // destructor
    ~FileHandler(void);
};

#endif



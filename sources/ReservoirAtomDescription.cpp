#include <iostream>
#include <math.h>
#include <string>

#include "Sort.h"
#include "ReservoirAtomDescription.h"

#define EPSILON_T 1.0e-2

ReservoirAtomDescription::ReservoirAtomDescription(FileHandler &fileHandler, bool verbose)
{
  int  num,i_cell;
  char *buff;
  //
  // get reservoir label if any
  //
  buff=fileHandler.lastWord();
  //
  if ( !strcmp( buff,"<reservoir" ) )
  {
    buff=fileHandler.readNextWord();
    char *tmp=strstr(buff,">");
    if ( tmp!=NULL ) 
    { 
      *tmp = 0;
      label_.assign(buff);
    }
  } 
  //
  // allocate memory for list of atoms
  //
  vector<vector<Atom<double> > > list_atoms_cell(2);
  //
  // flag for end of description
  //
  bool endOfReservoir=false;
  //
  // while there is something to read
  //
  while ( !endOfReservoir )
  {
    //
    // get the current tag
    //
    buff=fileHandler.readNextWord();
    //
    // if we are on a cell definition 
    //
    if ( !strcmp( buff,"<cell" ) )
    {
      //
      // keep cell index
      //
      num=atoi(fileHandler.readNextWord());
      //
      // check that the index is valid
      //
      if ( num>0 && num<3 )
      {
        i_cell=num-1; 
      }
      else
      {
        cout << "Error: invalid cell number found while reading reservoir description" << endl;
        exit(0);
      }
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // get the atom description
      //
      bool endOfCell=false;
      //
      while ( !endOfCell )
      {        
        buff=fileHandler.readNextWord();
        //
        if ( !strcmp( buff,"</cell>" ) || !strcmp( buff,"</cell" ) )
        {
          endOfCell=true;
        }
        else
        {
          //
          // generate atom
          //
          Atom<double> atom;
          atom.name.assign(buff);
          atom.x_pos=atof(fileHandler.readNextWord());
          atom.y_pos=atof(fileHandler.readNextWord());
          atom.z_pos=atof(fileHandler.readNextWord());  
          //
          // add atoms to list
          //        
          list_atoms_cell[i_cell].push_back(atom);
        }
      }
    }
    //
    // if we are on a repetition definition 
    //
    if ( !strcmp( buff,"<repetition" ) || !strcmp( buff,"<repetition>" ) || !strcmp( buff,"<repetitions" ) || !strcmp( buff,"<repetitions>" ) )
    {
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // get the flags description
      //
      bool endOfRepetitions=false;
      //
      while ( !endOfRepetitions )
      {
        buff=fileHandler.readNextWord();
        //
        if ( !strcmp( buff,"</repetition" ) || !strcmp( buff,"</repetition>" ) || !strcmp( buff,"</repetitions" ) || !strcmp( buff,"</repetitions>" ) )
        {
          endOfRepetitions=true;
        }
        else
        {
          //
          // check that we have less than 2 repetitions
          //
          if ( translation_.size() == 2 )
          {
            cout << "Error: more than 2 repetitions were found in resevoir definition" << endl;
            exit(0);
          }
          //
          // read the repetition
          //
          double x,y,z;
          x=atof(buff);
          y=atof(fileHandler.readNextWord());
          z=atof(fileHandler.readNextWord());
          //
          // add to the list of repetition
          //
          translation_.push_back(Vector3D<double>(x,y,z));  
        }
      }
    }
    //
    // if we are on the propagation vector definition 
    //
    if ( !strcmp( buff,"<propagation" ) || !strcmp( buff,"<propagation>" ) )
    {
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // read the repetition
      //
      double x,y,z;
      x=atof(fileHandler.readNextWord());
      y=atof(fileHandler.readNextWord());
      z=atof(fileHandler.readNextWord());
      //
      // keep propagation direction 
      //
      propagation_=Vector3D<double>(x,y,z);
      //
      // go to the line
      //
      fileHandler.skipLine();
    } 
    //
    // if we are on a flag definition 
    //
    if ( !strcmp( buff,"<flag" ) || !strcmp( buff,"<flags" ) || !strcmp( buff,"<flag>" ) || !strcmp( buff,"<flags>" ) )
    {
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // get the flags description
      //
      bool endOfFlags=false;
      //
      while ( !endOfFlags )
      {
        //
        // read the flag
        //
        fileHandler.readNextWord();
        //
        if ( !strcmp( buff,"</flag" ) || !strcmp( buff,"</flag>" ) || !strcmp( buff,"</flags" ) || !strcmp( buff,"</flags>" ) )
        {
          endOfFlags=true;
        } 
        else
        {
          //
          // get the flag in a string
          //
          string flag;
          flag.assign(buff);
          //
          // append the string to the flag list
          //
          flags_.push_back(flag);
        } 
      }
    }
    //
    // if we hit the end of the reservoir description 
    //   
    if ( !strcmp( buff,"</reservoir" ) || !strcmp( buff,"</reservoir>" ) ) endOfReservoir=true; 
    //
    // if we hit the end of the file, there is a problem
    //
    if ( fileHandler.eof() )
    {
      cout << "Error while reading reservoir description" << endl;
      exit(0);
    }
  }
  //
  // check that the description is complete
  //
  if ( list_atoms_cell[0].size()==0 )
  {
    cout << "Error: resevoir definition requires definition of the first cell" << endl;
    exit(0);
  }
  if ( list_atoms_cell[1].size()==0 && propagation_==Vector3D<double>(0,0,0) )
  {
    cout << "Error: resevoir definition requires either a propagation vector or a second cell" << endl;
    exit(0);
  }
  if ( list_atoms_cell[0].size()!=list_atoms_cell[1].size() && list_atoms_cell[1].size()>0 )
  {
    cout << "Error: diffent numbers of atoms found for the cells in reservoir description" << endl;
    exit(0);
  }
  //
  // generate a contact part from first cell
  //
  contact_list_.push_back( new PartAtomDescription );
  contact_list_.back()->atoms_=list_atoms_cell[0];
  contact_list_.back()->translation_=translation_;
  contact_list_.back()->label_=label_+"_contact";
  contact_list_.back()->flags_=flags_;
  //
  // subdivide the part if necessary
  //
  PartAtomDescription *contact=contact_list_[0];
  contact_list_=contact->subdivide( verbose );
  if ( contact_list_[0]!=contact ) delete contact;
  //
  // compute propagation if necessary
  //
  if ( propagation_==Vector3D<double>(0,0,0) )
  {
    double x=list_atoms_cell[1][0].x_pos-list_atoms_cell[0][0].x_pos;
    double y=list_atoms_cell[1][0].y_pos-list_atoms_cell[0][0].y_pos;
    double z=list_atoms_cell[1][0].z_pos-list_atoms_cell[0][0].z_pos;
    propagation_=Vector3D<double>(x,y,z);
  }
  //
  // check propagation translation if necessary
  //
  if ( list_atoms_cell[1].size()>0 )
  {
    bool translationOK=true;
    //
    for ( int i=0 ; i<list_atoms_cell[1].size() && translationOK ; i++ )
    {
      double x=list_atoms_cell[1][i].x_pos-list_atoms_cell[0][i].x_pos;
      double y=list_atoms_cell[1][i].y_pos-list_atoms_cell[0][i].y_pos;
      double z=list_atoms_cell[1][i].z_pos-list_atoms_cell[0][i].z_pos;
      translationOK = translationOK && ( fabs(propagation_[0]-x)<EPSILON_T );
      translationOK = translationOK && ( fabs(propagation_[1]-y)<EPSILON_T );
      translationOK = translationOK && ( fabs(propagation_[2]-z)<EPSILON_T );
      translationOK = translationOK && list_atoms_cell[0][i].name==list_atoms_cell[1][i].name;
    }
    //
    // if the propagation translation is not ok
    //
    if ( !translationOK )
    {
      cout << "Error: the two cells defined in reservoir description are not identical" << endl;
      exit(0);
    }
  }
  //
  // check that repetition directions and propagation direction are orthogonal
  //
  for ( int i=0 ; i<translation_.size() ; i++ )
  {
    if ( fabs( translation_[i][0]*propagation_[0] + translation_[i][1]*propagation_[1] + translation_[i][2]*propagation_[2] ) > EPSILON )
    {
      cout << "Warning: the propagation direction and repetition directions should be orthogonal in reservoir description" << endl;
      exit(0);
    }
  }
  //
  // recapitulate
  //
  if ( verbose )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "Reservoir description:" << endl << endl;
    cout << "    found " << list_atoms_cell[0].size()+list_atoms_cell[1].size() << " atoms describing " 
         << ( ( list_atoms_cell[1].size()>0 ) ? 2 : 1 ) << " cell" << ( ( list_atoms_cell[1].size()>0 ) ? "s" : " " ) << endl;
    if ( translation_.size()>0 )
    cout << "    found " << translation_[0] << " as repetition vector" << endl;
    if ( translation_.size()>1 )
    cout << "    found " << translation_[1] << " as repetition vector" << endl;
    if ( propagation_[0]!=0 || propagation_[1]!=0 || propagation_[2]!=0 )
    cout << "    found " << propagation_ << " as propagation vector" << endl;
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
  }
  //
  // allocate memory for cell repetitions
  //
  cell_atoms_.resize(1);
  cell_atoms_[0].resize(1);
  //
  // generate the list of atoms
  //
  cell_atoms_[0][0]=list_atoms_cell[0];
  for ( int i_translation=1 ; i_translation<3 ; i_translation++ )
  {
    //
    // get the corresponding translation
    //
    double T[3];
    T[0]=i_translation*propagation_[0];
    T[1]=i_translation*propagation_[1];
    T[2]=i_translation*propagation_[2];
    //
    // add the cell to the list
    //
    for ( int i=0 ; i<list_atoms_cell[0].size() ; i++ )
    {
      //
      // generate atom i
      //
      Atom<double> atom;
      atom.name=cell_atoms_[0][0][i].name;
      atom.x_pos=cell_atoms_[0][0][i].x_pos+T[0];
      atom.y_pos=cell_atoms_[0][0][i].y_pos+T[1];
      atom.z_pos=cell_atoms_[0][0][i].z_pos+T[2];
      //
      // add atom to the list
      // 
      cell_atoms_[0][0].push_back(atom);
    }
  }
  //
  // initialize repetition numbers
  //
  nrep1_=1;
  nrep2_=1;
}

bool ReservoirAtomDescription::hasFlag(string flag)
{
  for ( int i=0 ; i<flags_.size() ; i++ ) if ( flags_[i]==flag ) return true;
  return false;
}

// *************************************************
// ReservoirAtomDescription::reduceToMinimumCell
// 
//   from the list of atoms, try to find repetitions
//   in the orthogonal directions     
//
// *************************************************

void ReservoirAtomDescription::reduceToMinimumCell(bool verbose)
{
  //
  // make sure the cell is not reduced already
  //
  if ( nrep1_!=1 || nrep2_!=1 )
  {
    cout << "Error: reservoir description has already been reduced to minimal cell, detected in - ReservoirAtomDescription::reduceToMinimumCell -" << endl;
    exit(0);
  }
  //
  // get number of atoms in the first slice
  //
  int n_atoms=cell_atoms_[0][0].size()/3;
  //
  // copy atoms of the first cell
  //
  vector<Atom<double> > atoms_;
  atoms_.insert(atoms_.end(),cell_atoms_[0][0].begin(),cell_atoms_[0][0].begin()+n_atoms);
  //
  // check that we can do something
  //
  if ( translation_.size()==0 ) 
  {
    cout << "Warning: reservoir is not periodic, so cannot be reduced" << endl;
    return;
  } 
  //
  // get size of the translation
  //
  double size_T1= sqrt(translation_[0]*translation_[0]);
  double size_T2= translation_.size()==2 ? sqrt(translation_[1]*translation_[1]) : 0.0 ;
  //
  // form a basis with repetition vectors
  //
  Vector3D<double> V1;
  Vector3D<double> V2; 
  Vector3D<double> V3;
  //
  if ( translation_.size()==1 )
  {
    //
    // use translation_[1] as the first vector
    //
    V1=translation_[0]/sqrt(translation_[0]*translation_[0]);
    //
    // use propagation as the third vector
    //
    V3=propagation_/sqrt(propagation_*propagation_);
    //
    // form V2
    //
    V2=V3^V1;
    V2=V2/sqrt(V2*V2); 
  }
  else
  {
    //
    // use translation_[0] as the first vector
    //
    V1=translation_[0]/sqrt(translation_[0]*translation_[0]);
    //
    // use translation_[1] as the second vector
    //
    V2=translation_[1]/sqrt(translation_[1]*translation_[1]);
    //
    // check that the two vectors are orthogonal
    //
    if ( fabs( V1*V2 ) > EPSILON )
    {
      cout << "Warning: reservoir repetitions are not orthogonals, so reservoir cannot be reduced" << endl;
      return;
    }
    //
    // form V3
    //
    V3=V1^V2; 
    V3=V3/sqrt(V3*V3);
    //
    // check V[3] sign
    //
    if ( V3*propagation_<0.0 ) V3=-((double)1.0)*V3;
  }
  //
  // flag to set wich direction to work on
  //
  bool only_x = (translation_.size()==1);
  //
  // move atom list in the new coordinates
  //  
  for ( int i=0 ; i<n_atoms ; i++ ) atoms_[i].changeBasis(V1,V2,V3);
  //
  // loop on the list of atoms to connect the atoms with their neigborhs
  //
  vector<int> prev_x(n_atoms,-1);
  vector<int> next_x(n_atoms,-1);
  vector<int> prev_y(n_atoms,-1);
  vector<int> next_y(n_atoms,-1);
  //
  for ( int i=0 ; i<n_atoms ; i++ ) 
  {
    //
    // find the pervious and next atom in each direction
    //
    double dmin_prev_x=-1E10;
    double dmin_next_x=1E10;
    double dmin_prev_y=-1E10;
    double dmin_next_y=1E10;
    //
    // loop on the atoms
    //
    for ( int j=0 ; j<n_atoms ; j++ )
    {
      //
      // if those two atoms are identical
      //
      if (  atoms_[j].name==atoms_[i].name )
      {
        double dx=atoms_[j].x_pos-atoms_[i].x_pos;
        double dy=atoms_[j].y_pos-atoms_[i].y_pos;
        double dz=atoms_[j].z_pos-atoms_[i].z_pos;
        //
        // search for aligned atoms
        //
        if ( fabs(dx)<=EPSILON )
        {
          if ( fabs(dy)>EPSILON && fabs(dz)<=EPSILON)
          {
            //
            // case in wich atom is aligned along y
            //
            if ( dmin_prev_y<dy && dy<0 )
            {
              dmin_prev_y=dy;
              prev_y[i]=j;
            }
            if ( dmin_next_y>dy && dy>0 )
            {
              dmin_next_y=dy;
              next_y[i]=j;
            }
          }
        }
        else
        {
          if ( fabs(dy)<=EPSILON && fabs(dz)<=EPSILON)
          {
            //
            // case in wich atom is aligned along x
            //
            if ( dmin_prev_x<dx && dx<0 )
            {
              dmin_prev_x=dx;
              prev_x[i]=j;
            }
            if ( dmin_next_x>dx && dx>0 )
            {
              dmin_next_x=dx;
              next_x[i]=j;
            }
          }
        }
      }
    }
    //
    // fast return if possible
    //
    if ( dmin_prev_x==-1E10 && dmin_next_x==1E10 && ( ( dmin_prev_y==-1E10 && dmin_next_y==1E10 ) || only_x ) )
    {
      //
      // recapitulate
      //
      if ( verbose )
      {
        cout << endl;
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "Reservoir description reduction:" << endl << endl;
        for ( int i=0 ; i<translation_.size() ; i++ )
        cout << "    found " <<  ( (i==1) ? nrep2_ : nrep1_ ) << " unit cell" 
             << ( ( (i==1) ? nrep2_>1 : nrep1_>1 ) ? "s" : " " ) << " along repetition direction [ " 
             << translation_[i][0] << " , " << translation_[i][1] << " , " << translation_[i][2] << " ]" << endl;
        cout << endl;
        cout << "-------------------------------------------------------------------------" << endl;
      }
      //
      // return here
      //
      return;
    }
  }
  //
  // find the atoms that don't have previous neighbors
  //
  vector<int> list_pcell;
  //
  for ( int i=0 ; i<n_atoms ; i++ ) 
  {
    if ( prev_x[i]<0 && ( prev_y[i]<0 || only_x ) ) list_pcell.push_back(i);
  }
  //
  // find the biggest distance between atoms of the primitive cell and their neigborh
  //
  double dx_max=0.0;
  double dy_max=0.0;
  //
  for ( int i=0 ; i<list_pcell.size() ; i++ )
  {
    //
    // get index of this atom
    //
    int iat=list_pcell[i];
    //
    // find distances with next neighbor along x
    //
    double dx;
    if ( next_x[iat]<0 ) dx=size_T1;
    else
    {
      Vector3D<double> v=atoms_[next_x[iat]].coordinates()-atoms_[iat].coordinates();
      dx=sqrt(v*v);
    }
    if ( dx>dx_max) dx_max=dx;
    //
    // find distances with next neighbor along y only if necessary
    //
    if ( !only_x )
    {
      double dy;
      if ( next_y[iat]<0 ) dy=size_T2;
      else
      {
        Vector3D<double> v=atoms_[next_y[iat]].coordinates()-atoms_[iat].coordinates();
        dy=sqrt(v*v);
      }
      if ( dy>dy_max) dy_max=dy;
    }
    //
    // fast return if possible
    //
    if ( dx_max==size_T1 && dy_max==size_T2 )
    {
      //
      // recapitulate
      //
      if ( verbose )
      {
        cout << endl;
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "Reservoir description reduction:" << endl << endl;
        for ( int i=0 ; i<translation_.size() ; i++ )
        cout << "    found " <<  ( (i==1) ? nrep2_ : nrep1_ ) << " unit cell" 
             << ( ( (i==1) ? nrep2_>1 : nrep1_>1 ) ? "s" : " " ) << " along repetition direction [ " 
             << translation_[i][0] << " , " << translation_[i][1] << " , " << translation_[i][2] << " ]" << endl;
        cout << endl;
        cout << "-------------------------------------------------------------------------" << endl;
      }
      //
      // return here
      //
      return;
    }
  }
  //
  // check that the distance we found corresponds to an integer fraction of the translation 
  //
  if ( fabs(round(size_T1/dx_max)-size_T1/dx_max)>EPSILON )
  {
    cout << "Error: problem while trying to reduce reservoir in T1 direction" << endl;
    cout << "dx = " << fabs(round(size_T1/dx_max)-size_T1/dx_max) << endl;
    exit(0);
  }
  if ( fabs(round(size_T2/dy_max)-size_T2/dy_max)>EPSILON )
  {
    cout << "Error: problem while trying to reduce reservoir in T2 direction" << endl;
    cout << "dy = " << fabs(round(size_T2/dy_max)-size_T2/dy_max) << endl;
    exit(0);
  }
  //
  // get the number of repetitions
  //
  nrep1_= round(size_T1/dx_max);
  nrep2_= dy_max>0 ? round(size_T2/dy_max) : 1;
  //
  // get sizes of the unit cell translations
  //
  double unit_cell_translation_1= size_T1/nrep1_;
  double unit_cell_translation_2= size_T2/nrep2_;
  //
  // construct the primitive cell for direction 1
  //
  vector<int> pcell=list_pcell;
  //
  for ( int i=0 ; i<list_pcell.size() ; i++ )
  {
    //
    // get atom index
    //
    int iat=list_pcell[i];
    //
    // get next neighbor index
    //
    int inext=next_x[iat];
    //
    // integrate intermediate atoms
    //
    while ( inext>=0 && fabs(atoms_[inext].x_pos-atoms_[iat].x_pos-unit_cell_translation_1)>EPSILON )
    {
      //
      // add atom to the primitive cell only if 
      // it does not have previous neighbors in y direction
      // or if we are working only along first translation
      //
      if ( prev_y[inext]<0 || only_x ) pcell.push_back(inext);
      //
      // chose next atom
      //
      inext=next_x[inext];
    } 
  }
  //
  list_pcell=pcell;
  //
  // work along second direction if necessary
  //
  if ( !only_x )
  {
  
    for ( int i=0 ; i<list_pcell.size() ; i++ )
    {
      //
      // get atom index
      //
      int iat=list_pcell[i];
      //
      // get next neighbor index
      //
      int inext=next_y[iat];
      //
      // integrate intermediate atoms
      //
      while ( inext>=0 && fabs(atoms_[inext].y_pos-atoms_[iat].y_pos-unit_cell_translation_2)>EPSILON )
      {
        //
        // add atom to the primitive cell 
        //
        pcell.push_back(inext);
        //
        // chose next atom
        //
        inext=next_y[inext];
      } 
    }
    //
    list_pcell=pcell;  
  }
  //
  // check the size of the unit cell
  //
  if ( list_pcell.size()*nrep1_*nrep2_!=n_atoms ) 
  {
    cout << "Error: problem while reducing reservoir: size of unit cell = " << list_pcell.size() 
         << " times repetitions = " << nrep1_ << " * " << nrep2_ 
         << " isn't equal to total number of first slice atoms = "  << n_atoms << endl;
    exit(0); 
  }
  //
  // construct the list of cell atoms
  //
  vector<vector<vector<Atom<double> > > > cell_atoms(nrep1_);
  for ( int irep1=0 ; irep1<nrep1_ ; irep1++ )
  {
    //
    // allocate memory
    //
    cell_atoms[irep1].resize(nrep2_);
    //
    // compute actual translation
    //
    Vector3D<double> T1=translation_[0]/(double)nrep1_*(double)irep1;
    //
    // loop on second direction
    //
    for ( int irep2=0 ; irep2<nrep2_ ; irep2++ )
    {
      //
      // compute actual translation
      //
      Vector3D<double> T2;
      if ( translation_.size()==2 ) T2=translation_[1]/(double)nrep2_*(double)irep2;
      //
      // loop on propagation direction
      //
      for ( int irep3=0 ; irep3<3 ; irep3++ )
      {
        //
        // compute actual translation
        //
        Vector3D<double> T3=propagation_*(double)irep3;
        //
        // computet total translation
        //
        Vector3D<double> T=T1+T2+T3;
        //
        // copy translated unit cell atoms
        //
        for ( int i=0 ; i<list_pcell.size() ; i++ )
        {
          cell_atoms[irep1][irep2].push_back( cell_atoms_[0][0][list_pcell[i]]+T );
        }
      }
    }
  }
  //
  // keep the new cell atom lists
  //
  cell_atoms_=cell_atoms;
  //
  // recapitulate
  //
  if ( verbose )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "Reservoir description reduction:" << endl << endl;
    for ( int i=0 ; i<translation_.size() ; i++ )
    cout << "    found " <<  ( (i==1) ? nrep2_ : nrep1_ ) << " unit cell" 
         << ( ( (i==1) ? nrep2_>1 : nrep1_>1 ) ? "s" : " " ) << " along repetition direction [ " 
         << translation_[i][0] << " , " << translation_[i][1] << " , " << translation_[i][2] << " ]" << endl;
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
  }
}

// destructtor
ReservoirAtomDescription::~ReservoirAtomDescription(void)
{
  //
  // release contact list
  //
  for ( int i=0 ; i<contact_list_.size() ; i++ ) delete contact_list_[i];
}


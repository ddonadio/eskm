#include "Base64.h"
 
//Lookup table for encoding
//const static TCHAR encodeLookup[] = TEXT("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
//const static TCHAR padCharacter = TEXT('=');

// packing function
std::vector<BYTE> pack2LittleEndian(std::vector<double> &inputBuffer)
{
  std::vector<BYTE> outputBuffer;
  // test the endianness and reorder to litle indian if necessary
  long one= 1;
  // if the arch is big endian
  if (!(*((char *)(&one))))
  {
    // reserve output buffer
    outputBuffer.reserve(inputBuffer.size()*sizeof(double));
    // get size for the type of data
    int size_data=sizeof(double);
    int size_data_m1=size_data-1;
    // loop on the data
    for ( int i=0 , stop=inputBuffer.size() ; i<stop ; i++ )
    {
      // get pointer on this value
      BYTE *psrc=(BYTE *)&inputBuffer[i];
      // loop on the bytes
      for ( int j=size_data_m1 ; j>=0 ; j-- ) outputBuffer.push_back(psrc[j]);
    }
  }
  else
  {
    // allocate output buffer
    outputBuffer.resize(inputBuffer.size()*sizeof(double));
    // simply copy the content of the input buffer
    for ( BYTE *psrc=(BYTE *)&inputBuffer[0] , 
               *ptrgt=&outputBuffer[0] , 
               *stop=&outputBuffer[0]+outputBuffer.size() ; ptrgt<stop ; ptrgt++ , psrc++ ) (*ptrgt)=(*psrc);
  }
  // return the buffer
  return outputBuffer;
}

// unpacking function
std::vector<double> unpack2LocalEndian(std::vector<BYTE> &inputBuffer)
{
  std::vector<double> outputBuffer;
  // test the endianness and reorder to big indian if necessary
  long one= 1;
  // if the arch is big endian
  if (!(*((char *)(&one))))
  {
    // reserve output buffer
    outputBuffer.reserve(inputBuffer.size()/sizeof(double));
    // get size for the type of data
    int size_data=sizeof(double);
    int size_data_m1=size_data-1;
    // loop on the data
    for ( int i=0 , stop=inputBuffer.size() ; i<stop ; )
    {
      // add a value to the buffer
      outputBuffer.push_back(0.0);
      // get pointer on this value
      BYTE *pdest=(BYTE *)&outputBuffer.back();
      // loop on the bytes
      for ( int j=size_data_m1 ; j>=0 ; j-- , i++ ) pdest[j]=inputBuffer[i];
    }
    
  }
  else
  {
    // allocate output buffer
    outputBuffer.resize(inputBuffer.size()/sizeof(double));
    // simply copy the content of the input buffer
    for ( BYTE *psrc=&inputBuffer[0] , 
               *ptrgt=(BYTE *)&outputBuffer[0] , 
               *stop=(BYTE *)(&outputBuffer[0]+outputBuffer.size()) ; ptrgt<stop ; ptrgt++ , psrc++ ) (*ptrgt)=(*psrc);
  }
  // return the buffer
  return outputBuffer;
}


// base64 encoding
std::basic_string<TCHAR> base64Encode(std::vector<BYTE> &inputBuffer)
{
  std::basic_string<TCHAR> encodedString;
  encodedString.reserve(((inputBuffer.size()/3) + (inputBuffer.size() % 3 > 0)) * 4);
  DWORD temp;
  std::vector<BYTE>::iterator cursor = inputBuffer.begin();
  for(size_t idx = 0; idx < inputBuffer.size()/3; idx++)
  {
    temp  = (*cursor++) << 16; //Convert to big endian
    temp += (*cursor++) << 8;
    temp += (*cursor++);
    encodedString.append(1,encodeLookup[(temp & 0x00FC0000) >> 18]);
    encodedString.append(1,encodeLookup[(temp & 0x0003F000) >> 12]);
    encodedString.append(1,encodeLookup[(temp & 0x00000FC0) >> 6 ]);
    encodedString.append(1,encodeLookup[(temp & 0x0000003F)      ]);
  }
  switch(inputBuffer.size() % 3)
  {
  case 1:
    temp  = (*cursor++) << 16; //Convert to big endian
    encodedString.append(1,encodeLookup[(temp & 0x00FC0000) >> 18]);
    encodedString.append(1,encodeLookup[(temp & 0x0003F000) >> 12]);
    encodedString.append(2,padCharacter);
    break;
  case 2:
    temp  = (*cursor++) << 16; //Convert to big endian
    temp += (*cursor++) << 8;
    encodedString.append(1,encodeLookup[(temp & 0x00FC0000) >> 18]);
    encodedString.append(1,encodeLookup[(temp & 0x0003F000) >> 12]);
    encodedString.append(1,encodeLookup[(temp & 0x00000FC0) >> 6 ]);
    encodedString.append(1,padCharacter);
    break;
  }
  return encodedString;
}

// base64 decoding
std::vector<BYTE> base64Decode(const std::basic_string<TCHAR> &input)
{
  if (input.length() % 4) //Sanity check
  {
    std::cout << "Non-Valid base64 detected in base64Decode!" << std::endl;
    exit(0);
  }
  size_t padding = 0;
  if (input.length())
  {
    if (input[input.length()-1] == padCharacter)
      padding++;
    if (input[input.length()-2] == padCharacter)
      padding++;
  }
  //Setup a vector to hold the result
  std::vector<BYTE> decodedBytes;
  decodedBytes.reserve(((input.length()/4)*3) - padding);
  DWORD temp=0; //Holds decoded quanta
  std::basic_string<TCHAR>::const_iterator cursor = input.begin();
  while (cursor < input.end())
  {
    for (size_t quantumPosition = 0; quantumPosition < 4; quantumPosition++)
    {
      temp <<= 6;
      if       (*cursor >= 0x41 && *cursor <= 0x5A) // This area will need tweaking if
        temp |= *cursor - 0x41;                  // you are using an alternate alphabet
      else if  (*cursor >= 0x61 && *cursor <= 0x7A)
        temp |= *cursor - 0x47;
      else if  (*cursor >= 0x30 && *cursor <= 0x39)
        temp |= *cursor + 0x04;
      else if  (*cursor == 0x2B)
        temp |= 0x3E; //change to 0x2D for URL alphabet
      else if  (*cursor == 0x2F)
        temp |= 0x3F; //change to 0x5F for URL alphabet
      else if  (*cursor == padCharacter) //pad
      {
        switch( input.end() - cursor )
        {
        case 1: //One pad character
          decodedBytes.push_back((temp >> 16) & 0x000000FF);
          decodedBytes.push_back((temp >> 8 ) & 0x000000FF);
          return decodedBytes;
        case 2: //Two pad characters
          decodedBytes.push_back((temp >> 10) & 0x000000FF);
          return decodedBytes;
        default:
          std::cout << "Invalid Padding in Base 64 detected in base64Decode!" << std::endl;
          exit(0);
        }
      }  
      else
      {
        std::cout << "Non-Valid Character in Base 64 detected in base64Decode!" << std::endl;
        exit(0);
      }
      cursor++;
    }
    decodedBytes.push_back((temp >> 16) & 0x000000FF);
    decodedBytes.push_back((temp >> 8 ) & 0x000000FF);
    decodedBytes.push_back((temp      ) & 0x000000FF);
  }
  return decodedBytes;
}



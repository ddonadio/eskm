#ifndef SUBOPERATOR_H
#define SUBOPERATOR_H

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <math.h>
#include <typeinfo>

#include "mpi.h"
#include "Timer.h"
#include "Context.h"
#include "Subspace.h"
#include "Lapack.h"
#include "IndexList.h"
#include "QRFactorization.h"
#include "Communication.h"
#include "VectorUtils.h"
#include "WorkArrays.h"

using namespace std;

template <class Type>
class MatrixPencil
{
  private :
    
    // communicator 
    Context   &context_;
    int       my_proc_;
    int       n_procs_;
    
    // comunication request
    vector<MPI_Request> request_recv_coeff_;   
    vector<MPI_Request> request_send_coeff_;   
    
    // range/domain indices
    vector<int>           global_range_indices_;     // indices of the domain of the sub operator
    vector<int>           global_domain_indices_;    // indices of the range of the sub operator
    map<int,vector<int> > local_range_indices_;      // repartition of the range on the proc grid
    
    // range proc index lists and corresponding dimension of the local ranges
    vector<int> list_proc_range_;
    vector<int> Nloc_;
    int         my_Nloc_;
    
    // coefficients and overlaps arrays
    vector<Type> coeff_;              // contains the transpose of the subpart of the operator
    vector<Type> overlaps_;           // contains the transpose of the overlaps
    bool         hasOverlaps_;        // flag to indicate wether the overlaps are unitary or not
    
    // Q matrix
//    vector<vector<Type> >   BUFF_;      // send buffer for repartition of Q
//    vector<Type>            Q_;         // contains some columns of the Q transform
    vector<vector<Type> >  &BUFF_;      // send buffer for repartition of Q (use a common work array for all pencils
    vector<Type>           &Q_;         // contains some columns of the Q transform (use a common work array for all pencils)
    int                     NQ_;        // global number of columns of Q matrix 
    int                     MQ_;        // number of rows of the Q matrix
    int                     my_NQloc_;  // local number of column of the Q matrix on this proc
    int                     my_IQloc_;  // index of the first local colum of Q on this proc
    vector<int>             NQloc_;     // local number of column of the Q matrix on range processors
    vector<int>             IQloc_;     // index of the first local colum of Q on range processors
    
    // reference to subspace object containing the kernel
    Subspace<Type>* kernel_;
    
    // timer map for kernel resolution
    TimerMap tmap_;
    
    // on disc storage attribute
    string tempPath_;
    char   tempFileName_[1024];
    bool   onDisc_;
    
    // on disc storage methods
    void   release_(void);
    void   purgeTemporaryFiles_(void);
    void   save_(void);
    void   load_(void);
    
    // methods
    void fillFrom_( MatrixPencil &matrix_pencil );
    void (*fillingFunction_)(int *coeff_from, int *count_from, int *coeff_to, int *count_to, Type *coefficients, Type *overlaps);
        
  public :
    
    // standard constructor
    MatrixPencil(vector<int> global_range_indices, vector<int> global_domain_indices, Context &context, bool hasOverlaps=false);
    
    // filling method
    void setFillingFunction(void (*fillingFunction_)(int *coeff_from, int *count_from, int *coeff_to, int *count_to, Type *coefficients, Type *overlaps));
    void fill(void);
    void randomize(void);
    
    // attribute access methods
    bool hasOverlaps(void) { return hasOverlaps_;}
    Type *coeffPtr(int i=0) {return &coeff_[i];}
    Type *overlapsPtr(int i=0) {return &overlaps_[i];}
    vector<int> domain(void) { return global_domain_indices_; }
    vector<int> localRange(void); 
    vector<int> localRange(int i_proc); 
    vector<int> range(void) { return global_range_indices_; }
    void print(string comments);

    // add an access to coeff in case of dummy pencil
#ifdef DUMMY
    vector<Type>& coeffVector(void) { return coeff_; }
#endif    
    
    // kernel method
    void startKernel(double E);
    Subspace<Type>* endKernel(vector<int> intersection_indices=vector<int>(0,-1));
    void reduce(double energy_cutoff, double overlaps_cutoff, vector<int> except_indices=vector<int>(0,-1));
    
    // operator
    void      operator&=( MatrixPencil<Type> &matrix_pencil );             // merging operator
    void      operator =( MatrixPencil<Type> &matrix_pencil );             // copy operator
    template <class Type_other>
    void      operator =( MatrixPencil<Type_other> &matrix_pencil );       // copy operator<complex>=operator<real>
    
    // subspace projection
    template <class Type_proj, class Type_sub>
    Subspace<Type_proj>* projectSubspace( Subspace<Type_sub> &subspace, double energy );   // subspace projection where subspace is real and matrix_pencil complex
    
    // memory saving method
    void moveToDisc(string path);
    
    // destructor
    ~MatrixPencil(void);
  
  friend class MatrixPencil<float>;
  friend class MatrixPencil<double>;
  friend class MatrixPencil<complex<float> >;
  friend class MatrixPencil<complex<double> >;
};

// double * complex<float> definition 
#ifndef DOUBLETIMESCOMPLEXFLOAT
#define DOUBLETIMESCOMPLEXFLOAT

complex<float> operator *(double a, complex<float> b)
{
  return ((float)a)*b;
}

complex<float> operator *(complex<float> a, double b)
{
  return a*((float)b);
}

#endif

// standard constructor 
template <class Type>
MatrixPencil<Type>::MatrixPencil(vector<int> global_range_indices, vector<int> global_domain_indices, Context &context, bool hasOverlaps) : 
  global_range_indices_(global_range_indices) , global_domain_indices_(global_domain_indices) , context_(context) ,
  Q_(Q_MatrixPencil_WorkArray<Type>()) , BUFF_(BUFF_MatrixPencil_WorkArray<Type>()) 
{
  //
  // verify indices
  //
  if ( global_range_indices.size()==0 || global_domain_indices.size()==0 )
  {
    cout << "Error: null size index array in suoboperator instanciation" << endl;
    exit(0);
  }
  if ( global_range_indices.size()>=global_domain_indices.size() )
  {
    cout << "Error: dimension of the matrix_pencil should be such that range<domain in - MatrixPencil::MatrixPencil - " << endl;
    if ( context.myProc()==0 ) cout << "global_range_indices = " << global_range_indices << endl;
    if ( context.myProc()==0 ) cout << "global_domain_indices = " << global_domain_indices << endl;
    exit(0);
  }
  //
  // keep localisation within the context grid
  //  
  my_proc_=context.myProc();
  n_procs_=context.nProcs();
  //
  // verify context
  //
  {
    vector<int> global_indices=context.getIndicesOnProc(my_proc_); 
    if ( global_indices.size()==0 )
    {
      cout << "Error: context has no been properly initialized, detected in - MatrixPencil::MatrixPencil" << endl;
      exit(0);
    }
  }
  //
  // map the range indices on the proc array
  //
  for ( int i_proc=0 ; i_proc<n_procs_ ; i_proc++ )
  {
    //
    // get global indices on processor i_proc
    //
    vector<int> global_indices=context.getIndicesOnProc(i_proc);
    //
    // intersect range indices with the global indices on processor i_proc
    //
    vector<int> intersection_range=global_indices&&global_range_indices;
    //
    // if the intersection if non null
    //
    if ( intersection_range.size()>0 )
    {
      //
      // map the indices 
      //
      local_range_indices_[i_proc]=intersection_range;
      //
      // add this proc to the list of proc suporting the range
      //
      list_proc_range_.push_back( i_proc );
      //
      // keep the corresponding number of range indices
      //
      Nloc_.push_back( intersection_range.size() );
    }
    //
    // keep the dimension of the local range
    //
    if ( my_proc_==i_proc ) my_Nloc_=intersection_range.size();
  }
  //
  // allocate memory for coefficients: 
  // 
#ifndef DUMMY
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    //
    // allocate memory for overlaps if needed
    //
    if (hasOverlaps) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
#else
  cout << "Warning: using dummy MatrixPencil" << endl;
#endif  
  //
  // set the overlap flag
  //
  if (hasOverlaps)
  {
    hasOverlaps_=true;
  }
  else
  {
    hasOverlaps_=false;
  }
  //
  // global dimensions of the kernel
  //
  NQ_ = max( (int) ( global_domain_indices_.size() - global_range_indices_.size() ) , 0 );
  MQ_ = global_domain_indices_.size();
  //
  // spread the kernel on each range processor
  //
  {
    int  size_chunk=NQ_/local_range_indices_.size();
    int IQ = size_chunk*local_range_indices_.size();
    NQloc_.resize(n_procs_,0);
    for ( int i_proc=0 ; i_proc<n_procs_ ; i_proc++ )
    {
      if ( local_range_indices_.count(i_proc) ) { NQloc_[i_proc]+=size_chunk; }
    } 
    //
    while ( IQ<NQ_ )
    {
      for ( int i_proc=0 ; i_proc<n_procs_ && IQ<NQ_ ; i_proc++ )
      {
        if ( local_range_indices_.count(i_proc) ) { NQloc_[i_proc]++; IQ++; }
      }
    }
  }
  //
  // determine starting indices on kernel on range processors
  //
  IQloc_.resize(n_procs_);
  IQloc_[0]=0;
  for ( int i_proc=1 ; i_proc<n_procs_ ; i_proc++ ) IQloc_[i_proc]=IQloc_[i_proc-1]+NQloc_[i_proc-1];
  //
  // keep local value
  //
  for ( int i_proc=0 ; i_proc<n_procs_ ; i_proc++ )
  {
    if ( my_proc_==i_proc )
    {
      my_NQloc_=NQloc_[i_proc];
      my_IQloc_=IQloc_[i_proc];
      break;
    }
  }
  //
  // set flag to indicate that interactions are in memory only
  //
  onDisc_=false;
  tempPath_="";
  tempFileName_[0]=0;
}

// destructor
template <class Type>
MatrixPencil<Type>::~MatrixPencil<Type>(void)
{
  //
  // remove temporary files if necessary
  //
  purgeTemporaryFiles_();
}

// ****************************************************
//
// space saving method
// 
// ****************************************************

template <class Type>
void MatrixPencil<Type>::moveToDisc(string path)
{
  //
  // keep path
  //
  tempPath_=path;
  //
  // check if the pencil has been stored already
  //
  if ( onDisc_ )
  {
    //
    // just release memory
    //
//    coeff_.resize(0);
    clear(coeff_);
    //
//    if (hasOverlaps_) overlaps_.resize(0);
    if (hasOverlaps_) clear(overlaps_);
    //
    // return
    //
    return;
  }
  //
  // forge a name for the temporary file
  //
  sprintf(tempFileName_,"%s/tmp_%i%i%i",path.data(),rand(),rand(),rand());
  //
  // save the pencil on the disc
  //
  save_();
}

template <class Type>
void MatrixPencil<Type>::release_(void)
{
  //
  // check if the pencil has been stored already
  //
  if ( onDisc_ )
  {
    //
    // release memory
    //
//    coeff_.resize(0);
    clear(coeff_);
    //
//    if (hasOverlaps_) overlaps_.resize(0);
    if (hasOverlaps_) clear(overlaps_);
  }
}

template <class Type>
void MatrixPencil<Type>::purgeTemporaryFiles_(void)
{    
  //
  // if there was something stored
  //
  if ( onDisc_ )
  {
#ifndef DUMMY
    //
    // remove temporary file storage
    //
    char buff[1024];
    sprintf(buff,"rm -f %s_coeff",tempFileName_);
    system(buff);
    //
    // remove overlaps if necessary
    //
    if ( hasOverlaps_ )
    {
      char buff[1024];
      sprintf(buff,"rm -f %s_overlaps",tempFileName_);
      system(buff);
    }
    //
    // set flag for on disc storage
    //
#endif    
    onDisc_=false;
  }
}

template <class Type>
void MatrixPencil<Type>::save_(void)
{
#ifndef DUMMY
  //
  // if this proc is concerned
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    char buff[1024];
    //
    // form pathname
    //
    sprintf(buff,"%s_coeff",tempFileName_);
    //
    // open the file for writting coefficients
    //
    ofstream myFile;
    myFile.open(buff, ios::out | ios::binary); 
    //
    // check that the file has been opened
    //
    if ( myFile.fail() )
    {
      cout << "Error: couldn't open file " << buff << " for writing, detected in - MatrixPencil::save_ -" << endl;
      exit(0);
    }
    //
    // write data to the file
    //
    myFile.write ((char *)&coeff_[0], coeff_.size()*sizeof(Type));
    //
    // close the file
    //
    myFile.close();
    //
    // release memory
    //
//    coeff_.resize(0);
    clear(coeff_);
    //
    // save overlaps if necessary
    //
    if (hasOverlaps_)
    {
      //
      // form pathname
      //
      sprintf(buff,"%s_overlaps",tempFileName_);
      //
      // open the file for writting coefficients
      //
      myFile.open(buff, ios::out | ios::binary); 
      if ( myFile.fail() )
      {
        cout << "Error: couldn't open file " << buff << " for writing, detected in - MatrixPencil::save_ -" << endl;
        exit(0);
      }
      //
      // write data to the file
      //
      myFile.write ((char *)&overlaps_[0], overlaps_.size()*sizeof(Type));
      //
      // close the file
      //
      myFile.close();
      //
      // release memory
      //
//      overlaps_.resize(0);
      clear(overlaps_);
    }
  }
#endif  
  //
  // set flag to indicate that interactions are on disc
  //
  onDisc_=true;
}

template <class Type>
void MatrixPencil<Type>::load_(void)
{
#ifndef DUMMY
  if ( local_range_indices_.count(my_proc_) ) 
  {
    //
    // check if interactions are present in memory
    //
    if ( coeff_.size()!=0 ) return;
    //
    // check if the interactions are stored on disc
    //
    if ( !onDisc_ )
    {
      cout << "Error: trying to load interactions that are not on dics" << endl;
      exit(0);
    }
    //
    // forge path name
    //
    char buff[1024];
    sprintf(buff,"%s_coeff",tempFileName_);
    //
    // open file for reading
    //
    ifstream myFile;
    myFile.open(buff, ios::in | ios::binary); 
    //
    // check that the file has been opened
    //
    if ( myFile.fail() )
    {
      cout << "Error: couldn't open file " << buff << " for reading, detected in - MatrixPencil::load_ -" << endl;
      exit(0);
    }
    //
    // allocate memory for coefficients
    // 
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    //
    // write data to the file
    //
    myFile.read((char *)&coeff_[0], coeff_.size()*sizeof(Type));
    //
    // close the file
    //
    myFile.close();
    //
    // load overlaps if necessary
    //
    if (hasOverlaps_)
    {
      //
      // form pathname
      //
      sprintf(buff,"%s_overlaps",tempFileName_);
      //
      // open file for reading
      //
      myFile.open(buff, ios::in | ios::binary); 
      //
      // check that the file has been opened
      //
      if ( myFile.fail() )
      {
        cout << "Error: couldn't open file " << buff << " for reading, detected in - MatrixPencil::load_ -" << endl;
        exit(0);
      }
      //
      // allocate memory for coefficients
      // 
      overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
      //
      // write data to the file
      //
      myFile.read((char *)&overlaps_[0], overlaps_.size()*sizeof(Type));
      //
      // close the file
      //
      myFile.close();
    }
  } 
#endif   
}

// ****************************************************
//
// access to local indices
// 
// ****************************************************

template <class Type>
vector<int> MatrixPencil<Type>::localRange(void)
{
  //
  // return the list of indices if any
  //
  if ( local_range_indices_.count(my_proc_) ) return local_range_indices_[my_proc_];
  //
  // else, return an empty list
  //
  vector<int> empty_list;
  return empty_list;
}

template <class Type>
vector<int> MatrixPencil<Type>::localRange(int i_proc) 
{
  //
  // return the list of indices if any
  //
  if ( local_range_indices_.count(i_proc) ) return local_range_indices_[i_proc];
  //
  // else, return an empty list
  //
  vector<int> empty_list;
  return empty_list;
}
  
// ****************************************************
//
// MatrixPencil::setFillingFunction
//
//   set the function to use for filling the operator
//   this function should:
//     - return void
//     - take 4 pointers on int defining in this order
//               
//            the index of the coefficient along the rows
//            the number of row coefficients
//            the index of the coefficient along the column
//            the number of column coefficients
// 
//     - take 2 pointers on double defining in this order
//            
//            the adress of first element of the operator coefficients
//            the adress of first element of the overlaps coefficients
//
// ****************************************************

template <class Type>
void MatrixPencil<Type>::setFillingFunction(void (*function)(int *coeff_from, int *count_from, int *coeff_to, int *count_to, Type *coefficients, Type *overlaps))
{
  fillingFunction_=function;
}

// ****************************************************
//
// MatrixPencil::fill
//
//   call the filling function to set the coefficient of the operator 
//   and the overlaps
//
// ****************************************************

template <class Type>
void MatrixPencil<Type>::fill(void)
{
  //
  // make sure the array are allocated
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    if ( hasOverlaps_ ) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
  //
  // get dimensions of the lists
  //
  int count_from = global_domain_indices_.size();
  int count_to   = ( local_range_indices_.count(my_proc_) ) ? local_range_indices_[my_proc_].size() : 0;
  //
  // call the filling function if necessary. Fill as the transpose 
  // to facilitate the kernel resolution later on
  //
  if ( count_to>0 ) fillingFunction_( &local_range_indices_[my_proc_][0], &count_to, &global_domain_indices_[0], &count_from, &coeff_[0], &overlaps_[0]);
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_); 
  }
}

// ****************************************************
//
// MatrixPencil::randomize
//
//   randomize the coefficients of the Suboperator
//
// ****************************************************

template <>
void MatrixPencil<float>::randomize(void)
{
  //
  // make sure the array are allocated
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    if ( hasOverlaps_ ) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
  //
  // randomize coeff
  //
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=rand()/(1.0*RAND_MAX);
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_); 
  }
}

template <>
void MatrixPencil<double>::randomize(void)
{
  //
  // make sure the array are allocated
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    if ( hasOverlaps_ ) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
  //
  // randomize coeff
  //
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=rand()/(1.0*RAND_MAX);
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_); 
  }
}

template <>
void MatrixPencil<complex<float> >::randomize(void)
{
  //
  // make sure the array are allocated
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    if ( hasOverlaps_ ) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
  //
  // randomize coeff
  //
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=complex<float>(rand()/(1.0*RAND_MAX),rand()/(1.0*RAND_MAX));
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_); 
  }
}

template <>
void MatrixPencil<complex<double> >::randomize(void)
{
  //
  // make sure the array are allocated
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    if ( hasOverlaps_ ) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
  //
  // randomize coeff
  //
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=complex<double>(rand()/(1.0*RAND_MAX),rand()/(1.0*RAND_MAX));
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_); 
  }
}


// ****************************************************
//
// MatrixPencil::startkernel
//
//   return the definition of the non trivial elements 
//   of the kernel of this operator:
//
//   V in Ker => (H-E*S)V=0
//
// ****************************************************

template <class Type>
void MatrixPencil<Type>::startKernel(double E)
{
#ifdef TIMING
  tmap_["total time"].start();
#endif
  //
  // make sure we have the coefficients
  //
  load_();
  //
  // get the dimension of the system
  // N    = global number of column of operator transpose
  // M    = number of rows of operator transpose
  //
  int N    = global_range_indices_.size(); 
  int M    = global_domain_indices_.size(); 
  //
  // check that the mapping is correct
  //
  vector<int> Nloc;
  for ( int i_proc=0 ; i_proc<list_proc_range_.size() ; i_proc++ )
  {
    if ( !local_range_indices_.count(list_proc_range_[i_proc]) )
    {
      cout << "Error: list_proc_range_ and local_range_indices_ doesn't correspond in MatrixPencil::startKernel" << endl;
      exit(0);
    }
    if ( local_range_indices_[list_proc_range_[i_proc]].size()==0 )
    {
      cout << "Error: local_range_indices_ is empty on proc " << i_proc << " in MatrixPencil::startKernel" << endl;
      exit(0);
    }
  }
  //
  // make sure request list are empty
  //
  if ( request_recv_coeff_.size()>0 || request_send_coeff_.size()>0 )
  {
    cout << "Error: communication request lists are not empty in MatrixPencil::startKernel" << endl;
    exit(0);
  } 
  // 
  // generate a new Subspace
  //
  kernel_ = new Subspace<Type>( global_domain_indices_, context_ );


#ifdef DUMMY
  //
  // simply set kernel size
  //
  kernel_->setSize( max(0,M-N) );
  //
  // return
  //
  return;
#endif


  //
  // if there is nothing else to do for this proc
  //
  if ( ! ( local_range_indices_.count(my_proc_) || kernel_->localDomain().size()>0 ))
  {
    //
    // set dimension of the kernel
    //
    kernel_->setSize( max(0,M-N) );
    //
    // return
    //
    return;
  }
  //
  // if this proc is working for the Suboperator
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // initialize the Q matrix among working procs
    //
    Q_.resize(my_NQloc_*MQ_);
    for ( int i=0 , stop=Q_.size() ; i<stop ; i++ ) Q_[i]=0.0;
    for ( int i=0 ; i<my_NQloc_ ; i++ ) Q_[i*MQ_+i+my_IQloc_+N]=1.0;
    //
    // compute H-E*S => W (conjigate if complex)
    //
    vector<Type> W(M*my_Nloc_);
    //
    if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
    {
      if ( hasOverlaps_ )
      {
        for ( int i=0 , stop=M*my_Nloc_ ; i<stop ; i++ ) W[i]=coeff_[i]+E*overlaps_[i];
      }
      else
      {
        //
        // copy interactions first
        //
        for ( int i=0 , stop=M*my_Nloc_ ; i<stop ; i++ ) W[i]=coeff_[i];
        //
        // remove E*overlaps
        //
        for ( int i=0 ; i<my_Nloc_ ; i++ )
        {
          int i_range_loc=local_range_indices_[my_proc_][i];
          //
          for ( int j=0 ; j<M ; j++ )
          {
            if ( i_range_loc==global_domain_indices_[j] )
            {
              W[i*M+j]+=E;
              break;
            }
          }
        }
      }
    }
    //
    // complex case
    //
    else
    {
      if ( hasOverlaps_ )
      {
        for ( int i=0 , stop=M*my_Nloc_ ; i<stop ; i++ ) W[i]=conj(coeff_[i]+E*overlaps_[i]);
      }
      else
      {
        //
        // copy interactions first
        //
        for ( int i=0 , stop=M*my_Nloc_ ; i<stop ; i++ ) W[i]=conj(coeff_[i]);
        //
        // remove E*overlaps
        //
        for ( int i=0 ; i<my_Nloc_ ; i++ )
        {
          int i_range_loc=local_range_indices_[my_proc_][i];
          //
          for ( int j=0 ; j<M ; j++ )
          {
            if ( i_range_loc==global_domain_indices_[j] )
            {
              W[i*M+j]+=E;
              break;
            }
          }
        }
      }
    }
    //
    // conpute kernel through QR decomposition
    //
    QRFactorization<Type>( my_proc_, list_proc_range_, list_proc_range_, context_, &W[0], M, Nloc_, &Q_[0], MQ_, my_NQloc_ );
    //
    // get the list of processors supporting the kernel domain 
    //
    vector<int> list_proc_domain=kernel_->listProcDomain();
    //
    // allocate BUFF for sending the coeff to the proc supporting the domain
    //
    BUFF_.resize(list_proc_domain.size());
    //
    // start sending the coefficients
    //
    for ( int index_destination_list=0 ; index_destination_list<list_proc_domain.size() ; index_destination_list++ )
    {
      //
      // get corresponding proc index
      //
      int proc_destination=list_proc_domain[index_destination_list];
      //
      // get the corresponding domain indices
      //
      vector<int> domain_indices=kernel_->localDomain(proc_destination);
      //
      int size_local_domain=domain_indices.size();
      //
      // if it is a different proc than this one
      //
      if ( proc_destination!=my_proc_ )
      {
#ifdef WITH_MPI
        //
        // allocate BUFF for ordering the Q coefficients
        //
        BUFF_[index_destination_list].resize(size_local_domain*my_NQloc_);
        //
        // copy the coefficients of Q in WBUFF
        //
        for ( int i_row=0 ; i_row<size_local_domain ; i_row++ )
        {
          //
          // find the corresponding local index
          //
          int i=0;
          int index=domain_indices[i_row];
          //
          for ( int stop=global_domain_indices_.size() ; i<stop ; i++ ) if ( global_domain_indices_[i]==index ) break;
          //
          // check that we found the index
          //
          if ( i>=global_domain_indices_.size() )
          {
            cout << "Error: cannot find global domain index " << domain_indices[i_row] << " in MatrixPencil::startKernel" << endl;
            exit(0);
          }
          //
          // point on corresponding element of the first source column
          //
          Type *p_source=&Q_[i];
          //
          // point on corresponding element of the first destination column
          //
          Type *p_destination=&BUFF_[index_destination_list][i_row];
          //
          // copy coeff
          //
          for ( int i_col=0 ; i_col<my_NQloc_ ; i_col++ ) p_destination[i_col*size_local_domain]=p_source[i_col*M];
        }
        //
        // send to the destination proc
        //
        MPI_Request request;
        communication::immediate_send<Type>(&BUFF_[index_destination_list][0], size_local_domain*my_NQloc_, proc_destination, TAG_KERNEL_COEFF, context_.comm(), &request);      
        request_send_coeff_.push_back(request);
#else  
        //
        // should not arrive here
        //
        cout << "Error: reach a point not suposed to in MatrixPencil::startKernel serial build" << endl;
        exit(0);
#endif 
      }
      else
      {
        //
        // allocate memory for kernel coefficients
        //
        kernel_->setSize( max(0,M-N) );
        //
        // simply copy local coefficients into subspace array
        //
        for ( int i_row=0 ; i_row<size_local_domain ; i_row++ )
        {
          //
          // find the corresponding local index
          //
          int i=0;
          int index=domain_indices[i_row];
          //
          for ( int stop=global_domain_indices_.size() ; i<stop ; i++ ) if ( global_domain_indices_[i]==index ) break;
          //
          // check that we found the index
          //
          if ( i>=global_domain_indices_.size() )
          {
            cout << "irow=" << i_row << endl;
            cout << "size_local_domain=" << size_local_domain << endl;
            cout << "Error: cannot find global domain index " << domain_indices[i_row] << " in MatrixPencil::startKernel" << endl;
            usleep(1000000);
            exit(0);
          }
          //
          // point on corresponding element of the first source column
          //
          Type *p_source=&Q_[i];
          //
          // point on corresponding element of the first destination column
          //
          Type *p_destination=kernel_->coeffPtr(my_IQloc_*size_local_domain+i_row);
          //
          // copy coeff
          //
          for ( int i_col=0 ; i_col<my_NQloc_ ; i_col++ ) p_destination[i_col*size_local_domain]=p_source[i_col*M];
        }
      }
    }
    //
    // release the Q matrix
    //
//    Q_.resize(0);
    clear(Q_);
  } 
  //
  // if this proc has something to receive
  //
  if ( kernel_->localDomain().size()>0 )
  {
    //
    // allocate memory for kernel coefficients if not done yet
    //
    if ( kernel_->nVectors()==0 ) kernel_->setSize( max(0,M-N) );
    //
    // get the local domain size
    //
    int size_local_domain=kernel_->localDomain().size();    
    //
    // start reception of the coefficients
    //
    for ( int index_source_list=0 ; index_source_list<list_proc_range_.size() ; index_source_list++ )
    {
      //
      // get corresponding proc index
      //
      int proc_source=list_proc_range_[index_source_list];
      //
      // if the source is not the current proc
      //
      if ( proc_source!=my_proc_ )
      {
#ifdef WITH_MPI
        //
        // receive the coefficients directly into the subspace array
        //
        int iStart=IQloc_[proc_source]*size_local_domain;
        int nCoeff=size_local_domain*NQloc_[proc_source];
        //
        MPI_Request request;
        communication::immediate_recv<Type>(kernel_->coeffPtr(iStart), nCoeff, proc_source, TAG_KERNEL_COEFF, context_.comm(), &request);
        request_recv_coeff_.push_back(request);
#else
        //
        // should not arrive here
        //
        cout << "Error: reach a point not suposed to in MatrixPencil::startKernel serial build" << endl;
        exit(0);
#endif
      }
    }
  }
  //
  // release memory for coefficients if necessary
  //
  release_();
}  

template <class Type>  
Subspace<Type>* MatrixPencil<Type>::endKernel(vector<int> intersection_indices)
{
#ifndef DUMMY
 #ifdef WITH_MPI
  //
  // wait for end of transmission request
  //
  MPI_Status status;
  //
  // end of send requests
  //
  for ( int i=0 ; i<request_send_coeff_.size() ; i++ ) MPI_Wait(&request_send_coeff_[i], &status);
  //
  // end of receive requests
  //
  for ( int i=0 ; i<request_recv_coeff_.size() ; i++ ) MPI_Wait(&request_recv_coeff_[i], &status);
 #endif
  //
  // release the send buffer
  //
//  BUFF_.resize(0);
  clear(BUFF_);
  //
  // reset request lists
  //
  request_recv_coeff_.resize(0);
  request_send_coeff_.resize(0);
#endif
  //
  // discard useless coeff if necessary
  //
  if ( intersection_indices.size()>0 )
  {
    //
    // reduce kernel
    //
    kernel_->keepCoeffs( intersection_indices );
  }
  //
  // timing
  //
#ifdef TIMING
  tmap_["total time"].stop();
  //
  // print the different timers
  //
  cout << endl;communication::immediate_send<Type>
  cout << "-------------------------------------------------------------------------" << endl;
  cout << "MatrixPencil::kernel: " << endl;
  for ( TimerMap::iterator i = tmap_.begin(); i != tmap_.end(); i++ )
  {
    cout.precision(3);
    cout << "<timing name=\"";
    cout.width(25);
    cout << (*i).first << "\" time=\"";
    cout.width(9);
    cout << (*i).second.real() << "\"/>" << endl;
  }
  cout << "-------------------------------------------------------------------------" << endl;
  cout << endl;
#endif
  //
  // return kernel address
  //
  return kernel_;
}

template <class Type>
void MatrixPencil<Type>::operator&=( MatrixPencil<Type> &other )
{
  //
  // verify that we can merge the two operators
  //
  if ( hasOverlaps_!=other.hasOverlaps_ )
  {
    cout << "Error: trying to merge operators with incosistent overlaps" << endl;
    exit(0);
  }
  //
  // start creating the domain index list
  //
  vector<int> new_domain = global_domain_indices_ || other.global_domain_indices_;
  //
  // create the range index list
  //
  vector<int> new_range = global_range_indices_ || other.global_range_indices_;
  //
  // create a new matrix_pencil object
  //
  MatrixPencil matrix_pencil(new_range,  new_domain, context_, hasOverlaps_);
  //
  // fill the new matrix_pencil from the two others
  //
  matrix_pencil.fillFrom_( *this );
  matrix_pencil.fillFrom_( other );
  //
  // assign the new matrix_pencil to *this
  //
  *this = matrix_pencil;
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_);
  }
}

template <class Type>
void MatrixPencil<Type>::fillFrom_( MatrixPencil<Type> &matrix_pencil )
{
#ifndef DUMMY
  //
  // make sure we have the coefficients
  //
  matrix_pencil.load_();  
  //
  // make sure the array are allocated
  //
  if ( local_range_indices_.count(my_proc_) ) 
  {
    coeff_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
    if ( hasOverlaps_ ) overlaps_.resize(global_domain_indices_.size()*local_range_indices_[my_proc_].size());
  }
  //
  // if there is something to do
  //
  if ( matrix_pencil.local_range_indices_.count(my_proc_) && local_range_indices_.count(my_proc_) )
  {
    //
    // find local correspondance of indices
    //
    vector<int> local_domain_source;
    vector<int> local_domain_destination;
    {
      vector<int> domain_source=matrix_pencil.global_domain_indices_;
      vector<int> domain_destination=global_domain_indices_;
      //
      for ( int i=0 ; i<domain_source.size() ; i++ )
      for ( int j=0 ; j<domain_destination.size() ; j++ )
      { 
        if ( domain_source[i]==domain_destination[j] ) 
        {
          local_domain_source.push_back(i);
          local_domain_destination.push_back(j);
        }
      }
    }
    //
    vector<int> local_range_source;
    vector<int> local_range_destination;
    {
      vector<int> range_source=matrix_pencil.local_range_indices_[my_proc_];
      vector<int> range_destination=local_range_indices_[my_proc_];
      //
      for ( int i=0 ; i<range_source.size() ; i++ )
      for ( int j=0 ; j<range_destination.size() ; j++ )
      { 
        if ( range_source[i]==range_destination[j] ) 
        {
          local_range_source.push_back(i);
          local_range_destination.push_back(j);
        }
      }
    }
    //
    // get local leading dimensions
    //
    int size_domain_destination=global_domain_indices_.size();
    int size_domain_source=matrix_pencil.global_domain_indices_.size();
    //
    // loop on the correspondaces
    //
    for ( int i=0 ; i<local_domain_source.size() ; i++ )
    for ( int j=0 ; j<local_range_source.size()  ; j++ )
    {
      int i_source=local_range_source[j]*size_domain_source+local_domain_source[i];
      int i_destination=local_range_destination[j]*size_domain_destination+local_domain_destination[i];
      //
      coeff_[i_destination]=matrix_pencil.coeff_[i_source];
    }
    //
    // copy overlaps if any
    //
    if ( hasOverlaps_ )
    {
      //
      // loop on the correspondaces
      //
      for ( int i=0 ; i<local_domain_source.size() ; i++ )
      for ( int j=0 ; j<local_range_source.size()  ; j++ )
      {
        int i_source=local_range_source[j]*size_domain_source+local_domain_source[i];
        int i_destination=local_range_destination[j]*size_domain_destination+local_domain_destination[i];
        //
        overlaps_[i_destination]=matrix_pencil.overlaps_[i_source];
      }
    }
  }
  //
  // manage on disc storage 
  //
  if ( onDisc_ )
  {
    purgeTemporaryFiles_();
    //
    // store new coeffs
    //    
    moveToDisc(tempPath_);
  }
  //
  // release memory for coefficients if necessary
  //
  release_();
#endif
}  

template <class Type>
void MatrixPencil<Type>::operator=( MatrixPencil<Type> &other )
{
  // verify that the context corresponds
  if ( &context_!=&other.context_ )
  {
    cout << "Error: illegal call to subOperator::operator=, the two subspaces have different contexts" << endl;
  }
  
  // range/domain indices
  global_range_indices_=other.global_range_indices_;     
  global_domain_indices_=other.global_domain_indices_;    
  local_range_indices_=other.local_range_indices_;      
  
  // range/domain proc index lists
  list_proc_range_=other.list_proc_range_;
  Nloc_=other.Nloc_;
  my_Nloc_=other.my_Nloc_;
  
  // coefficient and overlaps arrays
#ifndef DUMMY
  coeff_=other.coeff_;              
  overlaps_=other.overlaps_;           
#endif
  hasOverlaps_=other.hasOverlaps_;        
  
  // Q matrix
  NQ_=other.NQ_;                 
  MQ_=other.MQ_;                 
  NQloc_=other.NQloc_;              
  IQloc_=other.IQloc_;              
  my_NQloc_=other.my_NQloc_;              
  my_IQloc_=other.my_IQloc_;              
  
  // reference to subspace object containing the kernel
  kernel_=other.kernel_;
  
  // methods
  fillingFunction_=other.fillingFunction_;
  
  // disc storage atribute
  sprintf(tempFileName_,"%s",other.tempFileName_);
  onDisc_=other.onDisc_;
}

template <> template <> 
void MatrixPencil<complex<float> >::operator=( MatrixPencil<float> &other )
{
  // verify that the context corresponds
  if ( &context_!=&other.context_ )
  {
    cout << "Error: illegal call to subOperator::operator=, the two subspaces have different contexts" << endl;
  }
  
  // range/domain indices
  global_range_indices_=other.global_range_indices_;     
  global_domain_indices_=other.global_domain_indices_;    
  local_range_indices_=other.local_range_indices_;      
  
  // range/domain proc index lists
  list_proc_range_=other.list_proc_range_;
  Nloc_=other.Nloc_;
  my_Nloc_=other.my_Nloc_;
  
  // make sure we have the coefficients
  other.load_();    
  
  // remove temp files if any
  purgeTemporaryFiles_();
  
  // coefficient and overlaps arrays
#ifndef DUMMY
  coeff_.resize(other.coeff_.size());
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=other.coeff_[i];              
  overlaps_.resize(other.overlaps_.size());
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) overlaps_[i]=other.overlaps_[i];           
#endif
  hasOverlaps_=other.hasOverlaps_;        
  
  // Q matrix
  NQ_=other.NQ_;                 
  MQ_=other.MQ_;                 
  NQloc_=other.NQloc_;              
  IQloc_=other.IQloc_;              
  my_NQloc_=other.my_NQloc_;              
  my_IQloc_=other.my_IQloc_;     
  
  // release memory for coefficients if necessary
  other.release_();       
}

template <> template <> 
void MatrixPencil<complex<double> >::operator=( MatrixPencil<double> &other )
{
  // verify that the context corresponds
  if ( &context_!=&other.context_ )
  {
    cout << "Error: illegal call to subOperator::operator=, the two subspaces have different contexts" << endl;
  }
  
  // range/domain indices
  global_range_indices_=other.global_range_indices_;     
  global_domain_indices_=other.global_domain_indices_;    
  local_range_indices_=other.local_range_indices_;      
  
  // range/domain proc index lists
  list_proc_range_=other.list_proc_range_;
  Nloc_=other.Nloc_;
  my_Nloc_=other.my_Nloc_;

  // make sure we have the coefficients
  other.load_();    
  
  // remove temp files if any
  purgeTemporaryFiles_();
  
  // coefficient and overlaps arrays
#ifndef DUMMY
  coeff_.resize(other.coeff_.size());
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=other.coeff_[i];              
  overlaps_.resize(other.overlaps_.size());
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) overlaps_[i]=other.overlaps_[i];           
#endif
  hasOverlaps_=other.hasOverlaps_;        
  
  // Q matrix
  NQ_=other.NQ_;                 
  MQ_=other.MQ_;                 
  NQloc_=other.NQloc_;              
  IQloc_=other.IQloc_;              
  my_NQloc_=other.my_NQloc_;              
  my_IQloc_=other.my_IQloc_;              

  // release memory for coefficients if necessary
  other.release_();       
}

template <class Type>
void MatrixPencil<Type>::reduce(double energy_cutoff, double overlaps_cutoff, vector<int> except_indices)
{
  //
  // check that we have coeffs
  //
  load_();
  //
  // save memory managment attribute
  //
  bool   onDisc=onDisc_;
  string tempPath=tempPath_;
  //
  // purge temporary files if any
  //
  purgeTemporaryFiles_();
  //
  // first of all, set coeff <= energy cutoff to 0
  //
  if ( hasOverlaps_ )
  {
    for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) 
    {
      if ( fabs(coeff_[i])<=energy_cutoff ) coeff_[i]=0.0;
      if ( fabs(overlaps_[i])<=overlaps_cutoff ) overlaps_[i]=0.0;
    }
  }
  else
  {
    for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) 
    {
      if ( fabs(coeff_[i])<=energy_cutoff ) coeff_[i]=0.0;
    }
  }
  //
  // get the indices that we should keep absolutely
  //
  vector<int> keep_indices = global_domain_indices_ && except_indices;
  //
  // get size of domain and local range
  //
  int size_range  = local_range_indices_.count(my_proc_) ? local_range_indices_[my_proc_].size() : 0;
  int size_domain = global_domain_indices_.size();

#ifdef DUMMY
  bool is_dummy = true;
  bool is_allocated = local_range_indices_.count(my_proc_) ? coeff_.size()==global_domain_indices_.size()*local_range_indices_[my_proc_].size() : true;
#else
  bool is_dummy = false;
  bool is_allocated = true;
#endif
  //
  // construct the list of domain indices with interactions
  //  
  vector<int> new_domain;
  vector<int> my_interactions;
  // 
  for ( int i=0 ; i<size_domain ; i++ )
  {
    //
    // init flag 
    //
    int has_interaction = false || ( is_dummy && !is_allocated );
    //
    // if there is something to do for this proc
    //
    if ( local_range_indices_.count(my_proc_) && is_allocated )
    {
      //
      // point first element of coefficient row
      //
      Type *p_coeff=&coeff_[i];
      //
      // point first element of overlaps row
      //
      Type *p_overlaps= ( hasOverlaps_ ) ? &overlaps_[i] : NULL;
      //
      // determine if this row has an interaction
      //
      if ( hasOverlaps_ )
      {
        for ( int j=0 ; j<size_range && !has_interaction ; j++ )
        {
          if ( p_coeff[j*size_domain]!=0.0 )    has_interaction=true; 
          if ( p_overlaps[j*size_domain]!=0.0 ) has_interaction=true;     
        }
      }
      else
      {
        for ( int j=0 ; j<size_range && !has_interaction ; j++ )
        {
          if ( p_coeff[j*size_domain]!=0.0 ) has_interaction=true; 
        }
      }
    }
    //
    // keep interaction flag for this coeff
    //
    my_interactions.push_back(has_interaction);
  }
  //
  // enforce interaction for range coefficients 
  //
  vector<int> range_indices_positions = global_range_indices_ <= global_domain_indices_;
  for ( int i=0 ; i<range_indices_positions.size() ; i++ ) my_interactions[range_indices_positions[i]]=true;
  //
  // get global interactions
  //
  vector<int> global_interactions=my_interactions;
#ifdef WITH_MPI
  //
  // reduce the result of the search
  //
  MPI_Allreduce( (void *) &my_interactions[0], (void *) &global_interactions[0], my_interactions.size(), MPI_INT, MPI_MAX, context_.comm());
#endif
  //
  // create the new domain
  //
  for ( int i=0 ; i<size_domain ; i++ )
  {
    //
    // if there is an interaction, keep the index
    //
    if ( global_interactions[i] ) new_domain.push_back(global_domain_indices_[i]);
  }
  //
  // add the list of indices to be kept
  //
  new_domain = new_domain || keep_indices;
  //
  // reduce the operator if needed
  //
  if ( new_domain.size()<global_domain_indices_.size() )
  {
    //
    // create a matrix_pencil object for the index list
    //
    MatrixPencil<Type> matrix_pencil( global_range_indices_, new_domain, context_, hasOverlaps_);
    //
    // fill the new matrix_pencil object form the actual coeff
    //
#ifndef DUMMY    
    matrix_pencil.fillFrom_( *this );
#else
    //
    // make sure we throw the coeff away
    //
    clear(matrix_pencil.coeff_);
    clear(coeff_);
#endif
    //
    // copy the new matrix_pencil 
    //
    *this = matrix_pencil;
  }
  //
  // manage on disc storage 
  //
  if ( onDisc )
  {
    //
    // store new coeffs
    //    
    moveToDisc(tempPath);
  }
}

#ifndef REALIMAG_H
#define REALIMAG_H

template <class Type> 
Type real( Type a)
{
  cout << "Error try to extract real/imag values from real number" << endl;
  exit(0);
  //
  return a;
} 

template <class Type> 
Type imag( Type a)
{
  cout << "Error try to extract real/imag values from real number" << endl;
  exit(0);
  //
  return a;
}

template <class Type> 
Type real( complex<Type> )
{
  return a.real();
}

template <class Type> 
Type imag( complex<Type> )
{
  return a.imag();
}

#endif

#define N_VECTORS_PER_PAGE 5
template <class Type>
void MatrixPencil<Type>::print(string comments)
{
#ifndef DUMMY
  //
  // make sure we have the coeffs
  //
  load_();
  //
  // if this proc is concerned
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // start gathering the indices
    //
    vector<int> size_local_range;
    vector<int> size_local_coeff;
    //
    for ( int index_list_proc=0 ; index_list_proc<list_proc_range_.size() ; index_list_proc++ )
    {
      //
      // find the corresponding processor index
      // 
      int i_proc=list_proc_range_[index_list_proc];
      //
      // get the size of range indices on this proc
      //
      size_local_range.push_back( local_range_indices_[i_proc].size() );
      size_local_coeff.push_back( local_range_indices_[i_proc].size()*global_domain_indices_.size() );
    }
    //
    // compute the displacements
    //
    vector<int> disp_indices(1,0);
    vector<int> disp_coeff(1,0);
    //
    for ( int i=0 ; i<size_local_range.size()-1 ; i++ ) 
    {
      disp_indices.push_back( disp_indices[i]+size_local_range[i] );
      disp_coeff.push_back( disp_coeff[i]+size_local_coeff[i] );
    }
    //
    // allocate W only if necessary
    //
    vector<Type> W;
    if ( my_proc_==list_proc_range_[0] ) W.resize(global_domain_indices_.size()*global_range_indices_.size());
    //
    // if there is several processors suporting the domain
    //
    if ( list_proc_range_.size()>1 )
    {
#ifdef WITH_MPI  
      //
      // gather the indices
      //
      communication::gather<int>( &local_range_indices_[my_proc_][0], &global_range_indices_[0], size_local_range, list_proc_range_[0], list_proc_range_, context_ ); 
      //
      // gather the coefficients
      //
      communication::gather<Type>( &coeff_[0], &W[0], size_local_coeff, list_proc_range_[0], list_proc_range_, context_ );   
#endif
    }
    else
    {
      //
      // simply update global_range_indices
      //
      for ( int i=0 ; i<local_range_indices_[my_proc_].size() ; i++ ) global_range_indices_[i]=local_range_indices_[my_proc_][i];
      //
      // copy the coefficient in W
      //
      int my_size=coeff_.size();
      //
      for ( int i=0 , stop=coeff_.size() ; i<stop   ; i++ )
      {
        W[i]=coeff_[i];
      } 
    }        
    //
    // print the set from proc list_proc_range_[0]
    //    
    if ( my_proc_==list_proc_range_[0] )
    {
      int nCoeff=global_range_indices_.size();
      int nCols=global_domain_indices_.size();
      //
      // print global header + comments firt
      //
      cout << endl;
      cout << "-------------------------------------------------------------------------" << endl;
      cout << "MatrixPencil : " << comments << endl;
      //
      // print vector N_VECTORS_PER_PAGE by N_VECTORS_PER_PAGE
      //
      int istart=0;
      //
      // real case
      //
      if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
      {
        while (istart<nCols)
        {
          //
          // print header for this subset of vector
          //
          cout << "   r/d   "; 
          //
          // adjust spacing for first vector index
          //
          if (global_domain_indices_[istart]<10) cout.width(8);
          else if (global_domain_indices_[istart]<100) cout.width(7);
          else if (global_domain_indices_[istart]<1000) cout.width(6);
          //
          // print vector indices
          //
          for ( int ivector=istart ; ivector<nCols && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            cout << "(" << global_domain_indices_[ivector] << ")";
            //
            // adjust spacing for next vector index
            //
            if (global_domain_indices_[ivector]<9) cout.width(12);
            else if (global_domain_indices_[ivector]<99) cout.width(11);
            else if (global_domain_indices_[ivector]<999) cout.width(10);
          }
          //
          // flush
          //
          cout << endl;
          // 
          // print coefficients
          //
          for ( int icoeff=0 ; icoeff<nCoeff ; icoeff++ )
          {
            //
            // set spacing for coefficient index
            //
            if (global_range_indices_[icoeff]<10) cout.width(4);
            else if (global_range_indices_[icoeff]<100) cout.width(3);
            else if (global_range_indices_[icoeff]<1000) cout.width(2);
            else if (global_range_indices_[icoeff]<10000) cout.width(1);
            //
            // print coefficient index
            //
            cout << "(" << global_range_indices_[icoeff] << ") :";
            cout.precision(5);
            for ( int ivector=istart ; ivector<nCols && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
            {
              cout.width(14);
              cout << scientific << W[ivector+icoeff*nCols];
            } 
            cout << endl;
          }  
          //
          // increment istart
          //
          istart+=N_VECTORS_PER_PAGE;
        }
      }
      //
      // complex case
      //
      else
      {
        while (istart<nCols)
        {
          //
          // print header for this subset of vector
          //
          cout << "   r/d   "; 
          //
          // adjust spacing for first vector index
          //
          if (global_domain_indices_[istart]<10) cout.width(8);
          else if (global_domain_indices_[istart]<100) cout.width(7);
          else if (global_domain_indices_[istart]<1000) cout.width(6);
          //
          // print vector indices
          //
          for ( int ivector=istart ; ivector<nCols && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            cout << "(" << global_domain_indices_[ivector] << ")";
            //
            // adjust spacing for next vector index
            //
            if (global_domain_indices_[ivector]<9) cout.width(12);
            else if (global_domain_indices_[ivector]<99) cout.width(11);
            else if (global_domain_indices_[ivector]<999) cout.width(10);
          }
          //
          // flush
          //
          cout << endl;
          // 
          // print coefficients
          //
          for ( int icoeff=0 ; icoeff<nCoeff ; icoeff++ )
          {
            //
            // set spacing for coefficient index
            //
            if (global_range_indices_[icoeff]<10) cout.width(4);
            else if (global_range_indices_[icoeff]<100) cout.width(3);
            else if (global_range_indices_[icoeff]<1000) cout.width(2);
            else if (global_range_indices_[icoeff]<10000) cout.width(1);
            //
            // print coefficient index
            //
            cout << "(" << global_range_indices_[icoeff] << ") :";
            cout.precision(5);
            for ( int ivector=istart ; ivector<nCols && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
            {
              cout.width(14);
              cout << scientific << real(W[ivector+icoeff*nCols]);
              if ( imag(W[ivector+icoeff*nCols])<0.0 )
              {
                cout.width(12);
              }
              else
              {
                cout << "+";
                cout.width(11);
              }
              cout << scientific << imag(W[ivector+icoeff*nCols]) << "*%i" ;
            } 
            cout << endl;
          }  
          //
          // increment istart
          //
          istart+=N_VECTORS_PER_PAGE;
        }
      }
      cout << "-------------------------------------------------------------------------" << endl;
      cout << endl;
    }
  }
  //
  // release memory for coefficients if necessary
  //
  release_();   
#else
  cout << "Warning: trying to print a dummy MatixPencil object" << endl;
#endif    
}

template <> 
template <>  
Subspace<complex<float> >* MatrixPencil<complex<float> >::projectSubspace( Subspace<float> &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<complex<float> > *sub = new Subspace<complex<float> >( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<float> coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    complex<float> *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<float> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<float> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<complex<double> >* MatrixPencil<complex<double> >::projectSubspace( Subspace<double> &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<complex<double> > *sub = new Subspace<complex<double> >( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<double> coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    complex<double> *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<double> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<double> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<complex<float> >* MatrixPencil<float>::projectSubspace( Subspace<complex<float> > &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<complex<float> > *sub = new Subspace<complex<float> >( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<complex<float> > coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    complex<float> *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<float> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<float> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<complex<double> >* MatrixPencil<double>::projectSubspace( Subspace<complex<double> > &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<complex<double> > *sub = new Subspace<complex<double> >( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<complex<double> > coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    complex<double> *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<double> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<double> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<float>* MatrixPencil<float>::projectSubspace( Subspace<float> &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<float> *sub = new Subspace<float>( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<float> coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    float *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          float c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          float c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<complex<float> >* MatrixPencil<complex<float> >::projectSubspace( Subspace<complex<float> > &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<complex<float> > *sub = new Subspace<complex<float> >( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<complex<float> > coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    complex<float> *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<float> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<float> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<double>* MatrixPencil<double>::projectSubspace( Subspace<double> &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<double> *sub = new Subspace<double>( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<double> coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    double *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          double c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          double c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

template <> template <> 
Subspace<complex<double> >* MatrixPencil<complex<double> >::projectSubspace( Subspace<complex<double> > &subspace, double energy )
{
  //
  // make sure we have the coeff
  // 
  load_();
  //
  // create a subspace object
  //
  Subspace<complex<double> > *sub = new Subspace<complex<double> >( global_range_indices_, context_ );
  //
  // set size of this subspace
  //
  sub->setSize(subspace.nVectors());
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_domain_indices_;
  //
  vector<complex<double> > coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_range_,coeff);
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_range_indices_.count(my_proc_) )
  {
    //
    // find local indices of the intersection
    //  
    vector<int> local_indices = domain_indices <= global_domain_indices_;
    //
    // get local domain of new subspace
    //
    vector<int> sub_local_domain=sub->localDomain();
    int size_sub_local_domain=sub_local_domain.size();
    //
    // get number of vectors of new subspace
    //
    int nVectors=sub->nVectors();
    //
    // get pointer on local coeff of new subspace
    // 
    complex<double> *sub_coeff=sub->coeffPtr(0);
    //
    // get dimensions of matrix_pencil
    //
    int size_local_range=local_range_indices_[my_proc_].size();
    int size_domain=global_domain_indices_.size();
    //
    // compute coefficient of the projection
    //
    for ( int irange=0 ; irange<size_local_range ; irange++ )
    {
      //
      // get local subspace domain index of corresponding matrix_pencil range
      //
      int index_domain = local_range_indices_[my_proc_][irange] <= sub_local_domain;
      //
      // for each vector of the subspace
      //  
      if ( hasOverlaps_ )
      {
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<double> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*(coeff_[local_indices[icoeff]+irange*size_domain]+energy*overlaps_[local_indices[icoeff]+irange*size_domain]);
          }
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
      else
      {
        // find local index for the diagonal element
        int i_diag = local_range_indices_[my_proc_][irange] <= domain_indices;
        for ( int ivect=0 ; ivect<nVectors ; ivect++ )
        {
          complex<double> c=0.0;
          for ( int icoeff=0 , stop=local_indices.size() ; icoeff<stop ; icoeff++ )
          {
            c+=coeff[icoeff*nVectors+ivect]*coeff_[local_indices[icoeff]+irange*size_domain];
          }
          // add product with diagonal of overlaps
          c+=coeff[i_diag*nVectors+ivect]*energy;
          //
          // store coeff
          //
          sub_coeff[index_domain+ivect*size_sub_local_domain]=c;
        }
      }
    }    
  }
  //
  // release memory for coefficients if necessary
  //
  release_();       
  //
  // return subspace object adress
  //
  return sub;
}

#endif 


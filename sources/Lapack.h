#ifndef LAPACK_H
#define LAPACK_H
//
// optimal block size
//
#define NB_LAPACK 32
//
// for lapack compatibilities
//
#ifdef LOWER_CASE

  #define SGERQF sgerqf
  #define SORMRQ sormrq
  #define SORMQR sormqr
  #define SORMLQ sormlq
  #define SGELQF sgelqf
  #define SORGLQ sorglq
  #define SGEQRF sgeqrf
  #define SORGQR sorgqr
  #define SGGEV  sggev
  #define SGETRF sgetrf
  #define SGETRI sgetri
  #define SGEMM  sgemm
  #define SGEMV  sgemv
  #define SAXPY  saxpy
  #define SDOT   sdot
  #define SSCAL  sscal
  
  #define CUNMLQ cunmlq
  #define CUNMQR cunmqr
  #define CGELQF cgelqf
  #define CUNGLQ cunglq
  #define CGEQRF cgeqrf
  #define CUNGQR cungqr
  #define CGGEV  cggev
  #define CGETRF cgetrf
  #define CGETRI cgetri
  #define CGEMM  cgemm
  #define CGEMV  cgemv
  #define CAXPY  caxpy
  #define CDOTC  cdotc
  #define CSCAL  cscal
  
  #define DGERQF dgerqf
  #define DORMRQ dormrq
  #define DORMQR dormqr
  #define DORMLQ dormlq
  #define DGELQF dgelqf
  #define DORGLQ dorglq
  #define DGEQRF dgeqrf
  #define DORGQR dorgqr
  #define DGGEV  dggev
  #define DGETRF dgetrf
  #define DGETRI dgetri
  #define DGEMM  dgemm
  #define DGEMV  dgemv
  #define DAXPY  daxpy
  #define DDOT   ddot
  #define DSCAL  dscal
    
  #define ZUNMQR zunmqr
  #define ZGELQF zgelqf
  #define ZUNGLQ zunglq
  #define ZGEQRF zgeqrf
  #define ZUNGQR zungqr
  #define ZGGEV  zggev
  #define ZGETRF zgetrf
  #define ZGETRI zgetri
  #define ZGEMM  zgemm
  #define ZGEMV  zgemv
  #define ZAXPY  zaxpy
  #define ZDOTC  zdotc
  #define ZSCAL  zscal
  
#endif

#ifdef LOWER_CASE_

  #define SGERQF sgerqf_
  #define SORMRQ sormrq_
  #define SORMQR sormqr_
  #define SORMLQ sormlq_
  #define SGELQF sgelqf_
  #define SORGLQ sorglq_
  #define SGEQRF sgeqrf_
  #define SORGQR sorgqr_
  #define SGGEV  sggev_
  #define SGETRF sgetrf_
  #define SGETRI sgetri_
  #define SGEMM  sgemm_
  #define SGEMV  sgemv_
  #define SAXPY  saxpy_
  #define SDOT   sdot_
  #define SSCAL  sscal_
  
  #define CUNMLQ cunmlq_
  #define CUNMQR cunmqr_
  #define CGELQF cgelqf_
  #define CUNGLQ cunglq_
  #define CGEQRF cgeqrf_
  #define CUNGQR cungqr_
  #define CGGEV  cggev_
  #define CGETRF cgetrf_
  #define CGETRI cgetri_
  #define CGEMM  cgemm_
  #define CGEMV  cgemv_
  #define CAXPY  caxpy_
  #define CDOTC  cdotc_
  #define CSCAL  cscal_
  
  #define DGERQF dgerqf_
  #define DORMRQ dormrq_
  #define DORMQR dormqr_
  #define DORMLQ dormlq_
  #define DGELQF dgelqf_
  #define DORGLQ dorglq_
  #define DGEQRF dgeqrf_
  #define DORGQR dorgqr_
  #define DGGEV  dggev_
  #define DGETRF dgetrf_
  #define DGETRI dgetri_
  #define DGEMM  dgemm_
  #define DGEMV  dgemv_
  #define DAXPY  daxpy_
  #define DDOT   ddot_
  #define DSCAL  dscal_
    
  #define ZUNMQR zunmqr_
  #define ZGELQF zgelqf_
  #define ZUNGLQ zunglq_
  #define ZGEQRF zgeqrf_
  #define ZUNGQR zungqr_
  #define ZGGEV  zggev_
  #define ZGETRF zgetrf_
  #define ZGETRI zgetri_
  #define ZGEMM  zgemm_
  #define ZGEMV  zgemv_
  #define ZAXPY  zaxpy_
  #define ZDOTC  zdotc_
  #define ZSCAL  zscal_
  
#endif

//
// complex definition
//
#include <complex>
using namespace std;
//
// lapack functions declaration
// 
extern "C" 
{
  extern void SORMRQ(char *SIDE, char *TRANS, int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *C, int *LDC, float *WORK, int *LWORK, int *INFO );
  extern void SORMQR(char *SIDE, char *TRANS, int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *C, int *LDC, float *WORK, int *LWORK, int *INFO );
  extern void SORMLQ(char *SIDE, char *TRANS, int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *C, int *LDC, float *WORK, int *LWORK, int *INFO );
  extern void SGELQF(int *M, int *N, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO );
  extern void SORGLQ(int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO);
  extern void SGEQRF(int *M, int *N, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO );
  extern void SGERQF(int *M, int *N, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO );
  extern void SORGQR(int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO);
  extern void SGGEV(char *JOBVL, char *JOBVR, int *N, float *A, int *LDA, float *B, int *LDB, float *ALPHAR, float *ALPHAI, float *BETA, float *VL, int *LDVL, float *VR, int *LDVR, float *WORK, int *LWORK, int *INFO );
  extern void SGETRF(int *M, int *N, float *A, int *LDA, int *IPIV, int *INFO );
  extern void SGETRI(int *N, float *A, int *LDA, int *IPIV, float *WORK, int *LWORK, int *INFO );
  extern void SGEMM(char *TRANSA, char *TRANSB, int *M, int *N, int *K, float *ALPHA, float *A, int *LDA, float *B, int *LDB, float *BETA, float *C, int *LDC);
  extern void SGEMV(char *TRANSA, int *M, int *N, float *ALPHA, float *A, int *LDA, float *B, int *LDB, float *BETA, float *C, int *LDC);  
  extern void SSCAL(int *N, float *ALPHA, float *A, int *INC);  
  extern void SAXPY(int *N, float *ALPHA, float *A, int *INCA, float *B, int *INCB);  
  extern void SDOT(float *result, int *N, float *A, int *INCA, float *B, int *INCB);  
  
  
  extern void DORMRQ(char *SIDE, char *TRANS, int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *C, int *LDC, double *WORK, int *LWORK, int *INFO );
  extern void DORMQR(char *SIDE, char *TRANS, int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *C, int *LDC, double *WORK, int *LWORK, int *INFO );
  extern void DORMLQ(char *SIDE, char *TRANS, int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *C, int *LDC, double *WORK, int *LWORK, int *INFO );
  extern void DGELQF(int *M, int *N, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO );
  extern void DORGLQ(int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO);
  extern void DGEQRF(int *M, int *N, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO );
  extern void DGERQF(int *M, int *N, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO );
  extern void DORGQR(int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO);
  extern void DGGEV(char *JOBVL, char *JOBVR, int *N, double *A, int *LDA, double *B, int *LDB, double *ALPHAR, double *ALPHAI, double *BETA, double *VL, int *LDVL, double *VR, int *LDVR, double *WORK, int *LWORK, int *INFO );
  extern void DGETRF(int *M, int *N, double *A, int *LDA, int *IPIV, int *INFO );
  extern void DGETRI(int *N, double *A, int *LDA, int *IPIV, double *WORK, int *LWORK, int *INFO );
  extern void DGEMM(char *TRANSA, char *TRANSB, int *M, int *N, int *K, double *ALPHA, double *A, int *LDA, double *B, int *LDB, double *BETA, double *C, int *LDC);
  extern void DGEMV(char *TRANSA, int *M, int *N, double *ALPHA, double *A, int *LDA, double *B, int *LDB, double *BETA, double *C, int *LDC);  
  extern void DSCAL(int *N, double *ALPHA, double *A, int *INC);  
  extern void DAXPY(int *N, double *ALPHA, double *A, int *INCA, double *B, int *INCB);  
  extern void DDOT(double *result, int *N, double *A, int *INCA, double *B, int *INCB);  
  
  
  extern void CUNMLQ(char *SIDE, char *TRANS, int *M, int *N, int *K, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *C, int *LDC, complex<float> *WORK, int *LWORK, int *INFO );
  extern void CUNMQR(char *SIDE, char *TRANS, int *M, int *N, int *K, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *C, int *LDC, complex<float> *WORK, int *LWORK, int *INFO );
  extern void CGELQF(int *M, int *N, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *WORK, int *LWORK, int *INFO );
  extern void CUNGLQ(int *M, int *N, int *K, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *WORK, int *LWORK, int *INFO);
  extern void CGEQRF(int *M, int *N, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *WORK, int *LWORK, int *INFO );
  extern void CUNGQR(int *M, int *N, int *K, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *WORK, int *LWORK, int *INFO);
  extern void CGGEV(char *JOBVL, char *JOBVR, int *N, complex<float> *A, int *LDA, complex<float> *B, int *LDB, complex<float> *ALPHA, complex<float> *BETA, complex<float> *VL, int *LDVL, complex<float> *VR, int *LDVR, complex<float> *WORK, int *LWORK, float *RWORK, int *INFO );
  extern void CGETRF(int *M, int *N, complex<float> *A, int *LDA, int *IPIV, int *INFO );
  extern void CGETRI(int *N, complex<float> *A, int *LDA, int *IPIV, complex<float> *WORK, int *LWORK, int *INFO );
  extern void CGEMM(char *TRANSA, char *TRANSB, int *M, int *N, int *K, complex<float> *ALPHA, complex<float> *A, int *LDA, complex<float> *B, int *LDB, complex<float> *BETA, complex<float> *C, int *LDC);
  extern void CGEMV(char *TRANSA, int *M, int *N, complex<float> *ALPHA, complex<float> *A, int *LDA, complex<float> *B, int *LDB, complex<float> *BETA, complex<float> *C, int *LDC);
  extern void CSCAL(int *N, complex<float> *ALPHA, complex<float> *A, int *INC);  
  extern void CAXPY(int *N, complex<float> *ALPHA, complex<float> *A, int *INCA, complex<float> *B, int *INCB);  
  extern void CDOTC(complex<float> *result, int *N, complex<float> *A, int *INCA, complex<float> *B, int *INCB);  
  

  extern void ZUNMLQ(char *SIDE, char *TRANS, int *M, int *N, int *K, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *C, int *LDC, complex<double> *WORK, int *LWORK, int *INFO );
  extern void ZUNMQR(char *SIDE, char *TRANS, int *M, int *N, int *K, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *C, int *LDC, complex<double> *WORK, int *LWORK, int *INFO );
  extern void ZGELQF(int *M, int *N, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *WORK, int *LWORK, int *INFO );
  extern void ZUNGLQ(int *M, int *N, int *K, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *WORK, int *LWORK, int *INFO);
  extern void ZGEQRF(int *M, int *N, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *WORK, int *LWORK, int *INFO );
  extern void ZUNGQR(int *M, int *N, int *K, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *WORK, int *LWORK, int *INFO);
  extern void ZGGEV(char *JOBVL, char *JOBVR, int *N, complex<double> *A, int *LDA, complex<double> *B, int *LDB, complex<double> *ALPHA, complex<double> *BETA, complex<double> *VL, int *LDVL, complex<double> *VR, int *LDVR, complex<double> *WORK, int *LWORK, double *RWORK, int *INFO );
  extern void ZGETRF(int *M, int *N, complex<double> *A, int *LDA, int *IPIV, int *INFO );
  extern void ZGETRI(int *N, complex<double> *A, int *LDA, int *IPIV, complex<double> *WORK, int *LWORK, int *INFO );
  extern void ZGEMM(char *TRANSA, char *TRANSB, int *M, int *N, int *K, complex<double> *ALPHA, complex<double> *A, int *LDA, complex<double> *B, int *LDB, complex<double> *BETA, complex<double> *C, int *LDC);
  extern void ZGEMV(char *TRANSA, int *M, int *N, complex<double> *ALPHA, complex<double> *A, int *LDA, complex<double> *B, int *LDB, complex<double> *BETA, complex<double> *C, int *LDC);
  extern void ZSCAL(int *N, complex<double> *ALPHA, complex<double> *A, int *INC);  
  extern void ZAXPY(int *N, complex<double> *ALPHA, complex<double> *A, int *INCA, complex<double> *B, int *INCB);  
  extern void ZDOTC(complex<double> *result, int *N, complex<double> *A, int *INCA, complex<double> *B, int *INCB);  
  
};

namespace lapack {

// ********************************************
// GEMM Template
template <class Type>
inline void gemm(char *TRANSA, char *TRANSB, int *M, int *N, int *K, Type *ALPHA, Type *A, int *LDA, Type *B, int *LDB, Type *BETA, Type *C, int *LDC)
{
  cout << "Error: unknown type in gemm template" << endl;
  exit(0);
}

template <>
inline void gemm<float>(char *TRANSA, char *TRANSB, int *M, int *N, int *K, float *ALPHA, float *A, int *LDA, float *B, int *LDB, float *BETA, float *C, int *LDC)
{
  SGEMM(TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}

template <>
inline void gemm<double>(char *TRANSA, char *TRANSB, int *M, int *N, int *K, double *ALPHA, double *A, int *LDA, double *B, int *LDB, double *BETA, double *C, int *LDC)
{
  DGEMM(TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}

template <>
inline void gemm<complex<float> >(char *TRANSA, char *TRANSB, int *M, int *N, int *K, complex<float> *ALPHA, complex<float> *A, int *LDA, complex<float> *B, int *LDB, complex<float> *BETA, complex<float> *C, int *LDC)
{
  CGEMM(TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}  

template <>
inline void gemm<complex<double> >(char *TRANSA, char *TRANSB, int *M, int *N, int *K, complex<double> *ALPHA, complex<double> *A, int *LDA, complex<double> *B, int *LDB, complex<double> *BETA, complex<double> *C, int *LDC)
{
  ZGEMM(TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}  

// ********************************************
// GEMV Template
template <class Type>
inline void gemv(char *TRANSA, int *M, int *N, Type *ALPHA, Type *A, int *LDA, Type *B, int *LDB, Type *BETA, Type *C, int *LDC)
{
  cout << "Error: unknown type in gemv template" << endl;
  exit(0);
}

template <>
inline void gemv<float>(char *TRANSA, int *M, int *N, float *ALPHA, float *A, int *LDA, float *B, int *LDB, float *BETA, float *C, int *LDC)
{
  SGEMV(TRANSA, M, N, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}

template <>
inline void gemv<double>(char *TRANSA, int *M, int *N, double *ALPHA, double *A, int *LDA, double *B, int *LDB, double *BETA, double *C, int *LDC)
{
  DGEMV(TRANSA, M, N, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}

template <>
inline void gemv<complex<float> >(char *TRANSA, int *M, int *N, complex<float> *ALPHA, complex<float> *A, int *LDA, complex<float> *B, int *LDB, complex<float> *BETA, complex<float> *C, int *LDC)
{
  CGEMV(TRANSA,M, N, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}

template <>
inline void gemv<complex<double> >(char *TRANSA, int *M, int *N, complex<double> *ALPHA, complex<double> *A, int *LDA, complex<double> *B, int *LDB, complex<double> *BETA, complex<double> *C, int *LDC)
{
  ZGEMV(TRANSA, M, N, ALPHA, A, LDA, B, LDB, BETA, C, LDC);
}

// ********************************************
// SCAL Template
template <class Type>
inline void scal(int *N, Type *ALPHA, Type *A, int *INC)
{
  cout << "Error: unknown type in scal template" << endl;
  exit(0);
}

template <>
inline void scal<float>(int *N, float *ALPHA, float *A, int *INC)
{
  SSCAL(N, ALPHA, A, INC);
}

template <>
inline void scal<double>(int *N, double *ALPHA, double *A, int *INC)
{
  DSCAL(N, ALPHA, A, INC);
}

template <>
inline void scal<complex<float> >(int *N, complex<float> *ALPHA, complex<float> *A, int *INC)
{
  CSCAL(N, ALPHA, A, INC);
}

template <>
inline void scal<complex<double> >(int *N, complex<double> *ALPHA, complex<double> *A, int *INC)
{
  ZSCAL(N, ALPHA, A, INC);
}

// ********************************************
// AXPY Template
template <class Type>
inline void axpy(int *N, Type *ALPHA, Type *A, int *INCA, Type *B, int *INCB)
{
  cout << "Error: unknown type in axpy template" << endl;
  exit(0);
}

template <>
inline void axpy<float>(int *N, float *ALPHA, float *A, int *INCA, float *B, int *INCB)
{
  SAXPY(N,ALPHA,A,INCA,B,INCB);
}

template <>
inline void axpy<double>(int *N, double *ALPHA, double *A, int *INCA, double *B, int *INCB)
{
  DAXPY(N,ALPHA,A,INCA,B,INCB);
}

template <>
inline void axpy<complex<float> >(int *N, complex<float> *ALPHA, complex<float> *A, int *INCA, complex<float> *B, int *INCB)
{
  CAXPY(N,ALPHA,A,INCA,B,INCB);
}

template <>
inline void axpy<complex<double> >(int *N, complex<double> *ALPHA, complex<double> *A, int *INCA, complex<double> *B, int *INCB)
{
  ZAXPY(N,ALPHA,A,INCA,B,INCB);
}

// ********************************************
// DOT Template
template <class Type>
inline Type dot(int *N, Type *A, int *INCA, Type *B, int *INCB)
{
  cout << "Error: unknown type in dot template" << endl;
  exit(0);
}

template <>
inline float dot<float>(int *N, float *A, int *INCA, float *B, int *INCB)
{
  float result;
  SDOT(&result,N,A,INCA,B,INCB);
  return result;
}

template <>
inline double dot<double>(int *N, double *A, int *INCA, double *B, int *INCB)
{
  double result;
  DDOT(&result,N,A,INCA,B,INCB);
  return result;
}

template <>
inline complex<float> dot<complex<float> >(int *N, complex<float> *A, int *INCA, complex<float> *B, int *INCB)
{
  complex<float> result;
  CDOTC(&result,N,A,INCA,B,INCB);
  return result;
}

template <>
inline complex<double> dot<complex<double> >(int *N, complex<double> *A, int *INCA, complex<double> *B, int *INCB)
{
  complex<double> result;
  ZDOTC(&result,N,A,INCA,B,INCB);
  return result;
}

// ********************************************
// GEQRF Template
template <class Type>
inline void geqrf(int *M, int *N, Type *A, int *LDA, Type *TAU, Type *WORK, int *LWORK, int *INFO )
{
  cout << "Error: unknown type in geqrf template" << endl;
  exit(0);
}

template <>
inline void geqrf<float>(int *M, int *N, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO )
{
  SGEQRF(M, N, A, LDA, TAU, WORK, LWORK, INFO );
}

template <>
inline void geqrf<double>(int *M, int *N, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO )
{
  DGEQRF(M, N, A, LDA, TAU, WORK, LWORK, INFO );
}

template <>
inline void geqrf<complex<float> >(int *M, int *N, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *WORK, int *LWORK, int *INFO )
{
  CGEQRF(M, N, A, LDA, TAU, WORK, LWORK, INFO );
}

template <>
inline void geqrf<complex<double> >(int *M, int *N, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *WORK, int *LWORK, int *INFO )
{
  ZGEQRF(M, N, A, LDA, TAU, WORK, LWORK, INFO );
}

// ********************************************
// GQR Template
template <class Type>
inline void gqr(int *M, int *N, int *K, Type *A, int *LDA, Type *TAU, Type *WORK, int *LWORK, int *INFO)
{
  cout << "Error: unknown type in mqr template" << endl;
  exit(0);
}

template <>
inline void gqr<float>(int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *WORK, int *LWORK, int *INFO)
{
  SORGQR( M, N, K, A, LDA, TAU, WORK, LWORK, INFO); 
}

template <>
inline void gqr<double>(int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO)
{
  DORGQR( M, N, K, A, LDA, TAU, WORK, LWORK, INFO); 
}

template <>
inline void gqr<complex<float> >(int *M, int *N, int *K, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *WORK, int *LWORK, int *INFO)
{
  CUNGQR( M, N, K, A, LDA, TAU, WORK, LWORK, INFO); 
}

template <>
inline void gqr<complex<double> >(int *M, int *N, int *K, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *WORK, int *LWORK, int *INFO)
{
  ZUNGQR( M, N, K, A, LDA, TAU, WORK, LWORK, INFO); 
}


// ********************************************
// MQR Template
template <class Type>
inline void mqr(char *SIDE, char *TRANS, int *M, int *N, int *K, Type *A, int *LDA, Type *TAU, Type *C, int *LDC, Type *WORK, int *LWORK, int *INFO )
{
  cout << "Error: unknown type in mqr template" << endl;
  exit(0);
}

template <>
inline void mqr<float>(char *SIDE, char *TRANS, int *M, int *N, int *K, float *A, int *LDA, float *TAU, float *C, int *LDC, float *WORK, int *LWORK, int *INFO )
{
  SORMQR(SIDE, TRANS, M, N, K, A, LDA, TAU, C, LDC, WORK, LWORK, INFO );
}

template <>
inline void mqr<double>(char *SIDE, char *TRANS, int *M, int *N, int *K, double *A, int *LDA, double *TAU, double *C, int *LDC, double *WORK, int *LWORK, int *INFO )
{
  DORMQR(SIDE, TRANS, M, N, K, A, LDA, TAU, C, LDC, WORK, LWORK, INFO );
}

template <>
inline void mqr<complex<float> >(char *SIDE, char *TRANS, int *M, int *N, int *K, complex<float> *A, int *LDA, complex<float> *TAU, complex<float> *C, int *LDC, complex<float> *WORK, int *LWORK, int *INFO )
{
  CUNMQR(SIDE, TRANS, M, N, K, A, LDA, TAU, C, LDC, WORK, LWORK, INFO );
}

template <>
inline void mqr<complex<double> >(char *SIDE, char *TRANS, int *M, int *N, int *K, complex<double> *A, int *LDA, complex<double> *TAU, complex<double> *C, int *LDC, complex<double> *WORK, int *LWORK, int *INFO )
{
  ZUNMQR(SIDE, TRANS, M, N, K, A, LDA, TAU, C, LDC, WORK, LWORK, INFO );
}

// ********************************************
// GETRI Template
template <class Type>
inline void getri(int *N, Type *A, int *LDA, int *IPIV, Type *WORK, int *LWORK, int *INFO )
{
  cout << "Error: unknown type in getri template" << endl;
  exit(0);
}

template <>
inline void getri<float>(int *N, float *A, int *LDA, int *IPIV, float *WORK, int *LWORK, int *INFO )
{
  SGETRI(N, A, LDA, IPIV, WORK, LWORK, INFO );
}

template <>
inline void getri<double>(int *N, double *A, int *LDA, int *IPIV, double *WORK, int *LWORK, int *INFO )
{
  DGETRI(N, A, LDA, IPIV, WORK, LWORK, INFO );
}

template <>
inline void getri<complex<float> >(int *N, complex<float> *A, int *LDA, int *IPIV, complex<float> *WORK, int *LWORK, int *INFO )
{
  CGETRI(N, A, LDA, IPIV, WORK, LWORK, INFO );
}

template <>
inline void getri<complex<double> >(int *N, complex<double> *A, int *LDA, int *IPIV, complex<double> *WORK, int *LWORK, int *INFO )
{
  ZGETRI(N, A, LDA, IPIV, WORK, LWORK, INFO );
}

// ********************************************
// GETRI Template
template <class Type>
inline void getrf(int *M, int *N, Type *A, int *LDA, int *IPIV, int *INFO )
{
  cout << "Error: unknown type in getrf template" << endl;
  exit(0);
}

template <>
inline void getrf<float>(int *M, int *N, float *A, int *LDA, int *IPIV, int *INFO )
{
  SGETRF(M, N, A, LDA, IPIV, INFO );
}

template <>
inline void getrf<double>(int *M, int *N, double *A, int *LDA, int *IPIV, int *INFO )
{
  DGETRF(M, N, A, LDA, IPIV, INFO );
}

template <>
inline void getrf<complex<float> >(int *M, int *N, complex<float> *A, int *LDA, int *IPIV, int *INFO )
{
  CGETRF(M, N, A, LDA, IPIV, INFO );
}

template <>
inline void getrf<complex<double> >(int *M, int *N, complex<double> *A, int *LDA, int *IPIV, int *INFO )
{
  ZGETRF(M, N, A, LDA, IPIV, INFO );
}

// ********************************************
// GETRI Template
template <class Type>
inline void ggev_real(char *JOBVL, char *JOBVR, int *N, Type *A, int *LDA, Type *B, int *LDB, Type *ALPHAR, Type *ALPHAI, Type *BETA, Type *VL, int *LDVL, Type *VR, int *LDVR, Type *WORK, int *LWORK, int *INFO )
{
  cout << "Error: unknown type in getrf template" << endl;
  exit(0);
}

template <class Type>
inline void ggev_complex(char *JOBVL, char *JOBVR, int *N, complex<Type> *A, int *LDA, complex<Type> *B, int *LDB, complex<Type> *ALPHA, complex<Type> *BETA, complex<Type> *VL, int *LDVL, complex<Type> *VR, int *LDVR, complex<Type> *WORK, int *LWORK, Type *RWORK, int *INFO )
{
  cout << "Error: unknown type in getrf template" << endl;
  exit(0);
}

template <>
inline void ggev_real<float>(char *JOBVL, char *JOBVR, int *N, float *A, int *LDA, float *B, int *LDB, float *ALPHAR, float *ALPHAI, float *BETA, float *VL, int *LDVL, float *VR, int *LDVR, float *WORK, int *LWORK, int *INFO )
{
  SGGEV( JOBVL, JOBVR, N, A, LDA, B, LDB, ALPHAR, ALPHAI, BETA, VL, LDVL, VR, LDVR, WORK, LWORK, INFO ); 
}

template <>
inline void ggev_real<double>(char *JOBVL, char *JOBVR, int *N, double *A, int *LDA, double *B, int *LDB, double *ALPHAR, double *ALPHAI, double *BETA, double *VL, int *LDVL, double *VR, int *LDVR, double *WORK, int *LWORK, int *INFO )
{
  DGGEV( JOBVL, JOBVR, N, A, LDA, B, LDB, ALPHAR, ALPHAI, BETA, VL, LDVL, VR, LDVR, WORK, LWORK, INFO ); 
}

template <>
inline void ggev_complex<float>(char *JOBVL, char *JOBVR, int *N, complex<float> *A, int *LDA, complex<float> *B, int *LDB, complex<float> *ALPHA, complex<float> *BETA, complex<float> *VL, int *LDVL, complex<float> *VR, int *LDVR, complex<float> *WORK, int *LWORK, float *RWORK, int *INFO )
{
  CGGEV( JOBVL, JOBVR, N, A, LDA, B, LDB, ALPHA, BETA, VL, LDVL, VR, LDVR, WORK, LWORK, RWORK, INFO );
}

template <>
inline void ggev_complex<double>(char *JOBVL, char *JOBVR, int *N, complex<double> *A, int *LDA, complex<double> *B, int *LDB, complex<double> *ALPHA, complex<double> *BETA, complex<double> *VL, int *LDVL, complex<double> *VR, int *LDVR, complex<double> *WORK, int *LWORK, double *RWORK, int *INFO )
{
  ZGGEV( JOBVL, JOBVR, N, A, LDA, B, LDB, ALPHA, BETA, VL, LDVL, VR, LDVR, WORK, LWORK, RWORK, INFO );
}

}
#endif

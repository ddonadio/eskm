/*! \file GraphClustering.h
    \brief Spectral Graph Clustering and kMeans function definitions
*/

#ifndef GRAPHCLUSTERING_H
#define GRAPHCLUSTERING_H

// std lib includes
#include <map>
#include <vector>
#include <iostream>

// specific includes
#include "TPChart.h"
#include "Lapack.h"

// using std lib namespace
using namespace std;

template <class GraphPrecision>
class GraphClustering
{
  private:
   
    // private attributes
    int                             size_;
    int                             dimk_;
    int                             size_cluster_max_;
    int                             size_cluster_min_;
    vector<GraphPrecision>          graph_;
    vector<vector<int> >            groups_;
    vector<vector<GraphPrecision> > centers_;
    
    // private methods
    bool                            iterateCenters_(GraphPrecision *A, bool fixedClusterSize);
    vector<vector<int> >            generateGroups_(GraphPrecision *A, bool fixedClusterSize);
    vector<vector<int> >            kMeans_(GraphPrecision *A, int nClusters, bool fixedClusterSize, int nStepMax);
    vector<vector<int> >            refineClusters_(vector<vector<int> > clusters);
    vector<vector<GraphPrecision> > initCenters_(GraphPrecision *A, int nClusters);
    void                            printVectors_(GraphPrecision *A);
    void                            orthogonalize_(GraphPrecision *p1, GraphPrecision *p2);
    void                            mix_(GraphPrecision *p1, GraphPrecision *p2, GraphPrecision alpha);
    
  public:
    
    // initialisation
    void setGraph(vector<GraphPrecision> &graph_coeff, int size_graph);
    
    // clustering method
    vector<vector<int> > clusterGraph(int nCLusters, int slack=0, bool fixedClusterSize=true, bool normalized=true, bool refine=true, int nStepMax=100, int dim=0, int dimOptMaxSteps=0);
    
    // display
    void printClusters(void);
    void printCenters(void);
    void printGraph(void);
    
    // constructor/destructor
    GraphClustering(){}
   ~GraphClustering(){}
};

template <class GraphPrecision>
void GraphClustering<GraphPrecision>::orthogonalize_(GraphPrecision *p1, GraphPrecision *p2)
{
  GraphPrecision sum=0.0;
  // compute overlap
  for ( int i=0 ; i<size_ ; i++ ) sum+=p1[i]*p2[i];
  // orthogonalize
  for ( int i=0 ; i<size_ ; i++ ) p2[i]-=sum*p1[i];
  // normalize p2
  sum=0.0;
  for ( int i=0 ; i<size_ ; i++ ) sum+=p2[i]*p2[i];
  GraphPrecision norm=sqrt(sum);
  for ( int i=0 ; i<size_ ; i++ ) p2[i]/=norm;
}


template <class GraphPrecision>
void GraphClustering<GraphPrecision>::mix_(GraphPrecision *p1, GraphPrecision *p2, GraphPrecision alpha)
{
  GraphPrecision beta=sqrt(1.0-alpha);
  GraphPrecision tmp1;
  GraphPrecision tmp2;
  // compute overlap
  for ( int i=0 ; i<size_ ; i++ ) 
  {
    tmp1=p1[i]*alpha+p2[i]*beta;
    tmp2=p1[i]*beta-p2[i]*alpha;
    p1[i]=tmp1;
    p2[i]=tmp2;
  }
}

template <class GraphPrecision>
void GraphClustering<GraphPrecision>::setGraph(vector<GraphPrecision> &graph_coeff, int size_graph)
{
  // check that the number of coeff passed is correct
  if ( graph_coeff.size()!=size_graph*size_graph )
  {
    cout << "Error: incorect size for graph_coeff argument in - GraphClustering::setGraph -" << endl;
    exit(0);
  } 
  // copy the graph
  size_=size_graph;
  graph_=graph_coeff;
}

template <class GraphPrecision>
vector<vector<int> > GraphClustering<GraphPrecision>::clusterGraph(int nClusters, int slack, bool fixedClusterSize, bool normalized, bool refine, int nStepMax, int dim, int dimOptMaxSteps)
{
  // compute P matrix
  vector<GraphPrecision> P(size_*size_);
  for ( int i=0 ; i<size_ ; i++ )
  {
    GraphPrecision *s=&graph_[i*size_];
    GraphPrecision p=s[0];
    //
    for ( int j=1 ; j<size_ ; j++ ) p+=s[j];
    //
    P[i*(size_+1)]=p;
  }
  // construct graph laplacian
  for ( int i=0 ; i<size_ ; i++ )
  {
    graph_[i*(size_+1)]-=P[i*(size_+1)];
    // determine between normalized and unnormalized
    if ( !normalized ) P[i*(size_+1)]=1.0;
  }
  // solve generalized eigen problem
  cout << "    -> get graph spectral decomposition" << endl;     
  vector<GraphPrecision> sortedVR(size_*size_);
  {
    // parameters for lapack call
    int  INFO;
    int  LWORK;
    int  LDB=size_;
    int  LDVL=size_;
    int  LDVR=size_;
    char JOBVL='N';
    char JOBVR='V';
    // copy of the graph
    vector<GraphPrecision> graph_tmp=graph_;
    // arrays for lapack
    vector<GraphPrecision> WORK(1); 
    vector<GraphPrecision> BETA(size_);
    vector<GraphPrecision> ALPHAR(size_);
    vector<GraphPrecision> ALPHAI(size_);
    vector<GraphPrecision> VR(size_*size_);
    // querry for the optimal work array size
    LWORK=-1;  
    lapack::ggev_real<GraphPrecision>( &JOBVL, &JOBVR, &size_, &graph_tmp[0], &size_, &P[0], &LDB, &ALPHAR[0], &ALPHAI[0], &BETA[0], NULL, &LDVL, &VR[0], &LDVR, &WORK[0], &LWORK, &INFO );		
    // reallocate if more work space is needed
    if ( (int)WORK[0] > WORK.size() ) WORK.resize((int)WORK[0]);
    LWORK=WORK.size();
    // compute transform to periodic solutions
    lapack::ggev_real<GraphPrecision>( &JOBVL, &JOBVR, &size_, &graph_tmp[0], &size_, &P[0], &LDB, &ALPHAR[0], &ALPHAI[0], &BETA[0], NULL, &LDVL, &VR[0], &LDVR, &WORK[0], &LWORK, &INFO );		
    // check status
    if ( INFO!=0 )
    {
      cout << "Error:: problem during generalized eigen problem solving with xGGEV in - GraphClustering::clusterGraph -, INFO=" << INFO << endl;
      exit(0); 
    }
    // sort solutions according to their energy
    vector<int> order(size_);
    vector<GraphPrecision> energy(size_);
    for ( int i=0 ; i<size_ ; i++ )
    {
      order[i]=i;
      energy[i]=ALPHAR[i]/BETA[i];
    }
    sort<GraphPrecision>(&energy[0],energy.size(),&order[0]);
    // reorder solutions
    for ( int i=0 ; i<size_ ; i++ )
    {
      GraphPrecision *p1=&VR[order[i]*size_];
      GraphPrecision *p2=&sortedVR[i*size_];
      GraphPrecision sum=0.0;
      for ( int j=0 ; j<size_ ; j++ ) 
      {
        p2[j]=p1[j];
        sum += p2[j]*p2[j];
      }
      // norm vectors
      GraphPrecision norm = sqrt(sum);
      for ( int j=0 ; j<size_ ; j++ ) p2[j]/=norm;
    }
    // orthogonalize
    for ( int i=0 ; i<size_-1 ; i++ )
    for ( int j=i+1 ; j<size_ ; j++ )
    {
      orthogonalize_(&sortedVR[i*size_],&sortedVR[j*size_]);
    }
  }
  // compute maximum size for the clusters
  size_cluster_max_=(size_+nClusters-1)/nClusters+slack;
  size_cluster_min_=(size_+nClusters-1)/nClusters-slack;
  // determine wich dimk_ we should use
  if ( dim==0 ) dimk_=max((nClusters+1)/2,2);
  else dimk_=dim;
  // find incremant for dim optimization
  int increment = ( (dimOptMaxSteps==0) ? 1 : max(1,nClusters/dimOptMaxSteps) );
  // start with first assumption on the dimension
  cout << "    -> start kmeans with dim=" << dimk_ << endl; 
  int best_dimk=dimk_;
  // compute clusters for this dimension
  vector<vector<int> > best_cluster=kMeans_(&sortedVR[size_*(size_-dimk_)], nClusters, fixedClusterSize, nStepMax);
  // compute ncut for this dimension
  GraphPrecision best_ncut=0.0;
  for ( int igroup=0 ; igroup<best_cluster.size() ; igroup++ )
  for ( int jgroup=0 ; jgroup<best_cluster.size() ; jgroup++ )
  { 
    if ( igroup!=jgroup )
    {
      for ( int iel=0 ; iel<best_cluster[igroup].size() ; iel++ )
      for ( int jel=0 ; jel<best_cluster[jgroup].size() ; jel++ )
      {
        best_ncut += graph_[best_cluster[igroup][iel]+best_cluster[jgroup][jel]*size_];
      }
    }
  }
  // try to optimize dimension if no dimension has been imposed
  while ( dimk_<nClusters && dim<=0 )
  {
    // increase dimension
    dimk_+=increment;
    // inform
    cout << "    -> try kmeans with dim=" << dimk_ << endl; 
    // compute new clusters
    vector<vector<int> > cluster=kMeans_(&sortedVR[size_*(size_-dimk_)], nClusters, fixedClusterSize, nStepMax);
    // compute ncut for this dimension
    GraphPrecision ncut=0.0;
    for ( int igroup=0 ; igroup<cluster.size() ; igroup++ )
    for ( int jgroup=0 ; jgroup<cluster.size() ; jgroup++ )
    {
      if ( igroup!=jgroup )
      {
        for ( int iel=0 ; iel<cluster[igroup].size() ; iel++ )
        for ( int jel=0 ; jel<cluster[jgroup].size() ; jel++ )
        {
          ncut += graph_[cluster[igroup][iel]+cluster[jgroup][jel]*size_];
        }
      }
    }
    // keep best case
    if ( ncut < best_ncut ) 
    {
      best_ncut=ncut;
      best_dimk=dimk_;
      best_cluster=cluster;
    }
  }
  // try to refine the clusters if requested
  if ( refine ) 
  {
    cout << "    -> try refining clusters" << endl; 
    best_cluster=refineClusters_(best_cluster);
  }
  // compute best ncut found
  GraphPrecision ncut=0.0;
  for ( int igroup=0 ; igroup<best_cluster.size() ; igroup++ )
  for ( int jgroup=0 ; jgroup<best_cluster.size() ; jgroup++ )
  {
    if ( igroup!=jgroup )
    {
      for ( int iel=0 ; iel<best_cluster[igroup].size() ; iel++ )
      for ( int jel=0 ; jel<best_cluster[jgroup].size() ; jel++ )
      {
        ncut += graph_[best_cluster[igroup][iel]+best_cluster[jgroup][jel]*size_];
      }
    }
  }
  cout << "    -> best ncut=" << ncut << " found for kMeansDim=" << best_dimk << endl; 
  // return the best groups we found
  return best_cluster;
}


#define N_CHANGE_REFINE_MAX 200
#define N_SWAP_REFINE_MAX 100

template <class GraphPrecision>
vector<vector<int> > GraphClustering<GraphPrecision>::refineClusters_(vector<vector<int> > clusters)
{
  // start by constructing the detailed ncut map
  vector<vector<GraphPrecision> > ncut_map(size_,vector<GraphPrecision>(clusters.size(),0.0));
  for ( int igroup=0 ; igroup<clusters.size() ; igroup++ ) 
  for ( int jgroup=0 ; jgroup<clusters.size() ; jgroup++ ) 
  {
    for ( int iel=0 ; iel<clusters[igroup].size() ; iel++ )
    for ( int jel=0 ; jel<clusters[jgroup].size() ; jel++ )
    {
      ncut_map[clusters[igroup][iel]][jgroup]+=graph_[clusters[igroup][iel]+clusters[jgroup][jel]*size_];
    }
  }
  // remove the diagonal trash components
  for ( int igroup=0 ; igroup<clusters.size() ; igroup++ ) 
  for ( int iel=0 ; iel<clusters[igroup].size() ; iel++ )
    ncut_map[clusters[igroup][iel]][igroup]-=graph_[clusters[igroup][iel]*(1+size_)];
  // try optimizing solution
  bool changed=true;
  int  iteration=0;
  while ( changed && iteration<N_CHANGE_REFINE_MAX )
  {
    iteration++;
    // reset changed flag
    changed=false;
    // try optimizing slacks:
    for ( int igroup=0 ; igroup<clusters.size() ; igroup++ ) 
    {
      // if this group have room for some elements
      if ( clusters[igroup].size()<size_cluster_max_ )
      {
        // look for an element that has more connection with this group than its own
        int            best_jgroup=-1;
        int            best_jel=-1;
        GraphPrecision best_diff=0;
        for ( int jgroup=0 ; jgroup<clusters.size() ; jgroup++ )
        {
          //
          // if this group can loose an element
          //
          if ( clusters[jgroup].size()>size_cluster_min_ )
          {
            //
            // seek for an element to transfert
            //
            for ( int jel=0 ; jel<clusters[jgroup].size() ; jel++ )
            {
              if ( best_diff < ncut_map[clusters[jgroup][jel]][igroup]-ncut_map[clusters[jgroup][jel]][jgroup] )
              {
                best_jgroup=jgroup;
                best_jel=jel;
                best_diff=ncut_map[clusters[jgroup][jel]][igroup]-ncut_map[clusters[jgroup][jel]][jgroup];
              }
            }
          }
        }
        // if we found an element, change its group
        if ( best_diff )
        {
          // add this element to te new group
          int best_index=clusters[best_jgroup][best_jel];
          clusters[igroup].push_back(best_index);
          // remove it from th old group
          clusters[best_jgroup].erase(clusters[best_jgroup].begin()+best_jel);
          // correct ncut map
          for ( int jgroup=0 ; jgroup<clusters.size() ; jgroup++ )
          for ( int jel=0 ; jel<clusters[jgroup].size() ; jel++ )
          {
            int jel_index=clusters[jgroup][jel];
            ncut_map[jel_index][igroup]+=graph_[jel_index+best_index*size_];
            ncut_map[jel_index][jgroup]-=graph_[jel_index+best_index*size_];
          }
          // set the flag to indicate that some changes has been done
          changed=true;
        }
      }
    } // end of slack optimization
    // try to swap pairs
    // start by constructing deficit map
    vector<vector<GraphPrecision> > deficit_map=ncut_map;
    for ( int igroup=0 ; igroup<clusters.size() ; igroup++ ) 
    for ( int jgroup=0 ; jgroup<clusters.size() ; jgroup++ ) 
    for ( int iel=0 ; iel<clusters[igroup].size() ; iel++ )
    {
      int iel_index=clusters[igroup][iel];
      deficit_map[iel_index][jgroup]-=ncut_map[iel_index][igroup];
    }
    // start swapping  
    bool swaped=true;
    int  nswaps=0;
    while ( swaped && nswaps<N_SWAP_REFINE_MAX )
    {
      nswaps++;
      // reset flag
      swaped=false;
      // construct possible swapping list
      int            best_igroup=-1;
      int            best_jgroup=-1;
      int            best_iel=-1;
      int            best_jel=-1;
      GraphPrecision best_gain=0.0;
      for ( int igroup=0 ; igroup<clusters.size() ; igroup++ )
      for ( int jgroup=0 ; jgroup<clusters.size() ; jgroup++ )
      {
        // swap only between different groups
        if ( igroup!=jgroup )
        {
          for ( int iel=0 ; iel<clusters[igroup].size() ; iel++ ) 
          {
            // get global index of this element
            int iel_index=clusters[igroup][iel];
            // if there is a defecit for this element
            if ( deficit_map[iel_index][jgroup]>0 )
            {
              // search best element in the second group to swap with
              for ( int jel=0 ; jel<clusters[jgroup].size() ; jel++ ) 
              {
                // get global index of this element
                int jel_index=clusters[jgroup][jel];
                // test gain for this swap
                if ( deficit_map[iel_index][jgroup]+deficit_map[jel_index][igroup]>best_gain )
                {
                  // keep this pair
                  best_iel=iel;
                  best_jel=jel;
                  best_igroup=igroup;
                  best_jgroup=jgroup;
                  best_gain=deficit_map[iel_index][jgroup]+deficit_map[jel_index][igroup];
                }
              }
            }
          }
        }
      }
      // if we found a pair to swap
      if ( best_gain>0.0 )
      {
        // swap pair
        int best_iel_index=clusters[best_igroup][best_iel];
        int best_jel_index=clusters[best_jgroup][best_jel];
        clusters[best_igroup][best_iel]=best_jel_index;
        clusters[best_jgroup][best_jel]=best_iel_index;
        // correct ncut_map
        for ( int igroup=0 ; igroup<clusters.size() ; igroup++ )
        for ( int iel=0 ; iel<clusters[igroup].size() ; iel++ )
        {
          int iel_index=clusters[igroup][iel];
          ncut_map[iel_index][best_igroup]+=graph_[iel_index+best_jel_index*size_];
          ncut_map[iel_index][best_jgroup]-=graph_[iel_index+best_jel_index*size_];
          ncut_map[iel_index][best_igroup]-=graph_[iel_index+best_iel_index*size_];
          ncut_map[iel_index][best_jgroup]+=graph_[iel_index+best_iel_index*size_];
        }
        // remove added trash from diagonal coeff of the graph
        ncut_map[best_iel_index][best_igroup]-=graph_[best_iel_index+best_jel_index*size_];
        ncut_map[best_iel_index][best_jgroup]+=graph_[best_iel_index+best_jel_index*size_];
        ncut_map[best_iel_index][best_igroup]+=graph_[best_iel_index+best_iel_index*size_];
        ncut_map[best_iel_index][best_jgroup]-=graph_[best_iel_index+best_iel_index*size_];
        ncut_map[best_jel_index][best_igroup]-=graph_[best_jel_index+best_jel_index*size_];
        ncut_map[best_jel_index][best_jgroup]+=graph_[best_jel_index+best_jel_index*size_];
        ncut_map[best_jel_index][best_igroup]+=graph_[best_jel_index+best_iel_index*size_];
        ncut_map[best_jel_index][best_jgroup]-=graph_[best_jel_index+best_iel_index*size_];
        // recompute deficit_map
        deficit_map=ncut_map;
        for ( int igroup=0 ; igroup<clusters.size() ; igroup++ ) 
        for ( int jgroup=0 ; jgroup<clusters.size() ; jgroup++ ) 
        for ( int iel=0 ; iel<clusters[igroup].size() ; iel++ )
        {
          int iel_index=clusters[igroup][iel];
          deficit_map[iel_index][jgroup]-=ncut_map[iel_index][igroup];
        }
        // set flag as we swaped something
        swaped=true; 
      }
    } 
  }
  // return the result of the optimization
  return clusters;
}

template <class GraphPrecision>
bool GraphClustering<GraphPrecision>::iterateCenters_(GraphPrecision *A, bool fixedClusterSize)
{
  // start by forming the groups associated to those centers
  groups_=generateGroups_(A, fixedClusterSize);
  // compute the new centers
  vector<vector<GraphPrecision> > new_centers(centers_.size());
  for ( int icenter=0 ; icenter<centers_.size() ; icenter++ )
  {
    // init center coeff
    new_centers[icenter]=vector<GraphPrecision>(dimk_,0.0);
    // points current center
    GraphPrecision *p2=&new_centers[icenter][0];
    // average new center over member of this group
    for ( int ivect=0 ; ivect<groups_[icenter].size() ; ivect++ )
    {
      // points current vector
      GraphPrecision *p1=&A[groups_[icenter][ivect]];
      // add vector coordinates to this center
      for ( int icoord=0 ; icoord<dimk_ ; icoord++ ) p2[icoord]+=p1[icoord*size_];
    }
    // rescale center coordinates
    GraphPrecision scale=(GraphPrecision)groups_[icenter].size();
    for ( int icoord=0 ; icoord<dimk_ ; icoord++ ) p2[icoord]/=scale;
  }  
  // check if something has changed
  for ( int icenter=0 ; icenter<centers_.size() ; icenter++ )
  {
    // search for correspondances between old and new centers.
    // We do assume that the order is respected between the
    // two lists as the groups are sorted.
    // get distance between those two centers
    GraphPrecision *p1=&centers_[icenter][0];
    GraphPrecision *p2=&new_centers[icenter][0];
    GraphPrecision tmp;
    GraphPrecision sum=0.0;
    //
    for ( int i=0 ; i<dimk_ ; i++ ) 
    {
      tmp=p1[i]-p2[i];
      sum+=tmp*tmp;
    }
    // check if we found a correspondance
    if ( sum>1E-8 ) 
    {
      // set new centers
      centers_=new_centers;
      // indicate that the solution is not still optimal
      return false;
    }
  }
  //if nothing has changed, return 1
  return true;
}



template <class GraphPrecision>
vector<vector<int> > GraphClustering<GraphPrecision>::generateGroups_(GraphPrecision *A, bool fixedClusterSize)
{
  // verify that A is correct
  // even if useless, this statment seems also
  // to stabilize the code regarding to optimizations...
  if ( A==NULL )
  {
    cout << "Error: incorrect pointer A in - GraphClustering::generateGroups_ -" << endl;
  }
  // init the clusters
  vector<vector<int> > groups(centers_.size());
  // form the groups according to choosen method:
  // if the group size is fixed
  if ( fixedClusterSize )
  {
    // init the cost matrix
    vector<GraphPrecision> costs(centers_.size()*size_);
    // fill the cost matrix with distances to the centers
    for ( int ivect=0 ; ivect<size_ ; ivect++ )
    {
      // compute distance with each center 
      for ( int ic=0 , stop=centers_.size() ; ic<stop ; ic++ )
      {
        GraphPrecision *p1=&centers_[ic][0];
        GraphPrecision *p2=&A[ivect];
        GraphPrecision tmp;
        GraphPrecision sum=0.0;
        //
        for ( int i=0 ; i<dimk_ ; i++ ) 
        {
          tmp = p1[i]-p2[i*size_];
          sum += tmp*tmp;
        }
        // set cost
        costs[ivect*centers_.size()+ic]=sqrt(sum);
      }
    }
    // init the requirements
    vector<int> requirements(size_,1);
    // init the stocks
    vector<int> stocks(centers_.size(),size_cluster_max_);
    // create Transportation chart
    TPChart<int,GraphPrecision> tpchart(costs,requirements,stocks);
    // find BFS
    tpchart.VogelApproximantion();
    // optimize BFS
    tpchart.optimizeBFS();
    // get the flows
    vector<TPFlow<int> > flows=tpchart.getFlows();
    // translate flows in terms of groups
    for ( int iflow=0 ; iflow<flows.size() ; iflow++ )
    {
      groups[flows[iflow].istock].push_back(flows[iflow].irequirement); 
    }
  }
  // if the group size is free
  else
  {
    // associate every element to a center
    for ( int ivect=0 ; ivect<size_ ; ivect++ )
    {
      // compute distance with each center 
      int            best_index;
      GraphPrecision dist_min=1E10;
      //
      for ( int ic=0 , stop=centers_.size() ; ic<stop ; ic++ )
      {
        GraphPrecision *p1=&centers_[ic][0];
        GraphPrecision *p2=&A[ivect];
        GraphPrecision tmp;
        GraphPrecision sum=0.0;
        //
        for ( int i=0 ; i<dimk_ ; i++ ) 
        {
          tmp = p1[i]-p2[i*size_];
          sum += tmp*tmp;
        }
        // keep best index
        if ( sum<dist_min )
        {
          dist_min=sum;
          best_index=ic;
        }
      }
      // add this element to the closest group
      groups[best_index].push_back(ivect); 
    }
  }
  // sort the indices within the groups
  // keep the smallest index of each group
  vector<int> indices;
  vector<int> order;
  for ( int igroup=0 ; igroup<groups.size() ; igroup++ )
  {
    if ( groups[igroup].size()>0 )
    {
      sort<int>(&groups[igroup][0], groups[igroup].size());
      indices.push_back(groups[igroup][0]);
      order.push_back(igroup);
    }
    else
    {
      cout << "Error: empty cluter should not be found in - GraphClustering::generateGroups_ -" << endl;
    }
  }
  // find the order of the groups according to their smallest index
  sort<int>(&indices[0], indices.size(), &order[0]);
  // apply global order to the groups
  vector<vector<int> > old_groups=groups;
  for ( int igroup=0 ; igroup<groups.size() ; igroup++ )
  {
    groups[igroup]=old_groups[order[igroup]];
  }
  // return sorted groups
  return groups;
}


template <class GraphPrecision>
vector<vector<GraphPrecision> > GraphClustering<GraphPrecision>::initCenters_(GraphPrecision *A, int nClusters)
{
  // initialize center positions
  vector<vector<GraphPrecision> > centers;
  // choose the first vector as first center
  {
    vector<GraphPrecision> first_center(dimk_);
    for ( int i=0 ; i<dimk_ ; i++ ) first_center[i]=A[i*size_];
    centers.push_back(first_center);
  }
  // determine next center regarding to distance 
  for ( int icenter=1 ; icenter<nClusters ; icenter++ )
  {
    // look for distant center
    GraphPrecision         dist_max=0.0;
    vector<GraphPrecision> best_center(dimk_);
    //  
    for ( int ivect=0 ; ivect<size_ ; ivect++ )
    {
      // compute minimal distance with other centers
      GraphPrecision dist_min=1E10;
      //
      for ( int ic=0 ; ic<centers.size() ; ic++ )
      {
        GraphPrecision *p1=&centers[ic][0];
        GraphPrecision *p2=&A[ivect];
        GraphPrecision tmp;
        GraphPrecision sum=0.0;
        //
        for ( int i=0 ; i<dimk_ ; i++ ) 
        {
          tmp = p1[i]-p2[i*size_];
          sum += tmp*tmp;
        }        
        // keep minimu distance
        if ( sum<dist_min ) dist_min=sum;
      }
      // keep best candidate
      if ( dist_min>dist_max )
      {
        dist_max=dist_min;
        for ( int i=0 ; i<dimk_ ; i++ ) best_center[i]=A[ivect+size_*i];
      }
    }
    // add best candidate to the center list
    centers.push_back(best_center);
  }
  // return result of the search
  return centers;
}

template <class GraphPrecision>
vector<vector<int> > GraphClustering<GraphPrecision>::kMeans_(GraphPrecision *A, int nClusters, bool fixedClusterSize, int nStepMax)
{
  // verify that nClusters<=size_
  if ( nClusters>size_ )
  {
    cout << "Error: number of cluster requested superior to number of object in - GraphClustering::kMeans -" << endl;
    exit(0);
  }
  // initialize center positions
  centers_=initCenters_(A, nClusters);
  // optimize the centers
  int nsteps=0;
  //
  while ( nsteps<nStepMax && !iterateCenters_(A, fixedClusterSize) ) nsteps++;
  // check if the optimal solution have been found
  if ( nsteps>=nStepMax )
  {
    cout << "Warning: exiting graph clustering algorithm without having found a locally optimal solution" << endl;
  }
  // return the groups fro those centers
  return groups_;  
}


template <class GraphPrecision>
void GraphClustering<GraphPrecision>::printClusters(void)
{
  cout << "clusters:" << endl;
  for ( int i=0 ; i<groups_.size() ; i++ )
  {
    cout << "  cluster " << i << ": ";
    for ( int j=0 ; j<groups_[i].size() ; j++ ) { cout.width(3); cout << groups_[i][j] << " "; }
    cout << endl;
  }
  cout << endl;
}

template <class GraphPrecision>
void GraphClustering<GraphPrecision>::printCenters(void)
{
  cout << "centers:" << endl;
  for ( int i=0 ; i<centers_.size() ; i++ )
  {
    cout << "  center " << i << ": ";
    for ( int j=0 ; j<centers_[i].size() ; j++ ) { cout.width(10); cout << centers_[i][j] << " "; }
    cout << endl;
  }
  cout << endl;
}

template <class GraphPrecision>
void GraphClustering<GraphPrecision>::printGraph(void)
{
  // save current formatting info
  ios_base::fmtflags ff=cout.flags();
  // set precision format 
  cout.precision(3);
  // print graph
  cout << "graph:" << endl;
  for ( int i=0 ; i<size_ ; i++ )
  {
    cout << "  " << i << ": ";
    for ( int j=0 ; j<size_ ; j++ ) cout << graph_[i+j*size_] << " ";
    cout << endl;
  }
  cout << endl;
  // restore output format
  cout.flags(ff);
}

template <class GraphPrecision>
void GraphClustering<GraphPrecision>::printVectors_(GraphPrecision *A)
{
  cout << "vectors:" << endl;
  for ( int i=0 ; i<size_ ; i++ )
  {
    cout << "  vector " << i << ": ";
    for ( int j=0 ; j<dimk_ ; j++ ) { cout.width(15); cout << scientific << A[i+j*size_] << " "; }
    cout << endl;
  }
  cout << endl;
}

#endif

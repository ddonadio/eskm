#ifndef SORT_H
#define SORT_H

#define SIZE_MERGE_MIN 64

#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>

using namespace std; 

template <class Type>
static void mergeSort_mixUtility_(Type* coeff1, Type* coeff2, int count1, int count2, int* trans1, int* trans2, Type* temp_coeff, int* temp_trans)
{
  //
  // keep track of pointer
  //
  Type *__restrict__ tmp_coeff=temp_coeff;
  Type *__restrict__ coef1=coeff1;
  Type *__restrict__ coef2=coeff2;
  int  *__restrict__ tmp_trans=temp_trans;
  int  *__restrict__ tran1=trans1;
  int  *__restrict__ tran2=trans2;
  //
  // init indices
  //
  Type *borne1=coeff1+count1;
  Type *borne2=coeff2+count2;
  //
  // check what we should do with trans
  //
  bool comp_trans = ( temp_trans!=NULL );
  //
  // sort according to the coeffs
  // 
  if ( comp_trans )
  {
    while ( (coef1<borne1) && (coef2<borne2) )
    {
      while ( (coef1<borne1) && ( *(coef1)<=*(coef2) ) )
      {
        *(tmp_coeff++) = *(coef1++);
        *(tmp_trans++) = *(tran1++);
      }
      if (coef1<borne1)
      {
        while ( (coef2<borne2) && (*(coef2)<=*(coef1) ) )
        {
          *(tmp_coeff++) = *(coef2++);
          *(tmp_trans++) = *(tran2++); 
        }
      }
    }
    //
    // copy last values in temp arrays
    //
    while (coef1<borne1)
    {
      *(tmp_coeff++) = *(coef1++);
      *(tmp_trans++) = *(tran1++);
    }
    while (coef2<borne2)
    {
      *(tmp_coeff++) = *(coef2++);
      *(tmp_trans++) = *(tran2++);
    }
  }
  else
  {
    while ( (coef1<borne1) && (coef2<borne2) )
    {
      while ( (coef1<borne1) && ( *(coef1)<=*(coef2) ) )
      {
        *(tmp_coeff++) = *(coef1++);
      }
      if (coef1<borne1)
      {
        while ( (coef2<borne2) && (*(coef2)<=*(coef1) ) )
        {
          *(tmp_coeff++) = *(coef2++);
        }
      }
    }
    //
    // copy last values in temp arrays
    //
    while (coef1<borne1)
    {
      *(tmp_coeff++) = *(coef1++);
    }
    while (coef2<borne2)
    {
      *(tmp_coeff++) = *(coef2++);
    }
  }
  //
  // reset pointers 
  //
  tmp_coeff=temp_coeff;
  coef1=coeff1;
  coef2=coeff2;
  tmp_trans=temp_trans;
  tran1=trans1;
  tran2=trans2;
  //
  // copy data
  //  
  if ( comp_trans )
  {
    while (coef1<borne1)
    {
      *(coef1++) = *(tmp_coeff++);
      *(tran1++) = *(tmp_trans++);
    }
    while (coef2<borne2)
    {
      *(coef2++) = *(tmp_coeff++);
      *(tran2++) = *(tmp_trans++);
    }
  }
  else
  {
    while (coef1<borne1)
    {
      *(coef1++) = *(tmp_coeff++);
    }
    while (coef2<borne2)
    {
      *(coef2++) = *(tmp_coeff++);
    }
  }
}

template <class Type>
static void insertion_sort_(Type* coeff, int count, int* trans)
{
  if ( trans!=NULL )
  {
    Type *end_coeff=coeff+count;
    //
    // bring minimum value to the begining (avoid condition j>=0 later) 
    //
    Type min=(*coeff);
    Type *imin=coeff;
    //
    for ( Type *i=coeff ; i<end_coeff ; i++ )
    {
      if ( (*i)<min )
      {
        min=(*i);
        imin=i;
      }
    }
    //
    (*imin)=(*coeff);
    (*coeff)=min;
    //
    int *ptrans_min=trans+(int)(imin-coeff);
    int tmp=(*ptrans_min);
    (*ptrans_min)=(*trans);
    (*trans)=tmp;
    //
    // register variable
    //
    register Type tmp1;
    register int  itmp1;
    //
    // start insertion algorithm
    //
    for ( Type *i=coeff+2 ; i<end_coeff ; i++ )
    {
      //
      // keep values at this place
      //
      Type value=(*i);
      int  vtrans=(*(trans+(int)(i-coeff)));
      //
      // get pointers on arrays
      //
      Type *j=i-1;
      int  *t=trans+(int)(i-coeff)-1;
      //
      // shift values
      //
      while ( (*j)>value )
      {
        tmp1=*j; 
        itmp1=*t; 
        *(j+1)=tmp1;
        *(t+1)=itmp1;
        j--;
        t--;
      }
      //
      // insert
      //
      (*(j+1))=value;
      (*(t+1))=vtrans;
    }
  }
  else
  {
    Type *end_coeff=coeff+count;
    {
      register Type min=(*coeff);
      register Type *imin=coeff;
      //
      for ( Type *i=coeff ; i<end_coeff ; i++ )
      {
        if ( (*i)<min )
        {
          min=(*i);
          imin=i;
        }
      }
      //
      (*imin)=(*coeff);
      (*coeff)=min;
    }
    //
    // register variable
    //
    register Type tmp1;
    //
    // start insertion algorithm
    //
    for ( Type *i=coeff+2 ; i<end_coeff ; i++ )
    {
      //
      // keep values at this place
      //
      Type value=(*i);
      //
      // get pointers on arrays
      //
      Type *j=i-1;
      int  *t=trans+(int)(i-coeff)-1;
      //
      // shift values
      //
      while ( (*j)>value )
      {
        tmp1=*j; 
        *(j+1)=tmp1;
        j--;
      }
      //
      // insert
      //
      (*(j+1))=value;
    }
  }
}

// ****************************************************
//
// sort<Type>(Type* coeff, int count, int* trans)
//
//   sort a list of coefficients, Also apply te same 
//   transfo to the arrays trans.
// 
// ****************************************************

template <class Type>
static void merge_sort_(Type* coeff, int count, int* trans, Type* temp_coeff, int* temp_trans)
{
  //
  // if the list contains only one element, return
  //
  if (count==1) return;
  //
  // if we should apply the transform to trans
  //
  if ( trans!=NULL )
  {
    if ( count>SIZE_MERGE_MIN )
    {
      //
      // split the list in two an sort recursively
      //
      merge_sort_<Type>(coeff,count/2,trans,temp_coeff,temp_trans);
      merge_sort_<Type>(coeff+count/2,(count+1)/2,trans+count/2,temp_coeff+count/2,temp_trans+count/2);
      //
      // mix the two sorted lists
      //
      mergeSort_mixUtility_<Type>(coeff,coeff+count/2,count/2,(count+1)/2,trans,trans+count/2,temp_coeff,temp_trans);
    }
    else
    {
      insertion_sort_<Type>(coeff, count, trans);
    }
  }
  else
  {
    if ( count>SIZE_MERGE_MIN )
    {
      //
      // split the list in two an sort recursively
      //
      merge_sort_<Type>(coeff,count/2,NULL,temp_coeff,NULL);
      merge_sort_<Type>(coeff+count/2,(count+1)/2,NULL,temp_coeff+count/2,NULL);
      //
      // mix the two sorted lists
      //
      mergeSort_mixUtility_<Type>(coeff,coeff+count/2,count/2,(count+1)/2,NULL,NULL,temp_coeff,NULL);
    }
    else
    {
      insertion_sort_<Type>(coeff, count, NULL);
    }
  }
}

template <class Type>
static void sort(Type* coeff, int count, int* trans=NULL)
{
  //
  // fast return if possible
  //
  if ( count==0 ) return;
  //
  // sort according to size
  // 
  if ( count>SIZE_MERGE_MIN )
  {
    //
    // allocate memory for work 
    //
    std::vector<Type> temp_coeff(count);
    //
    // allocate trans only if necessary
    // 
    std::vector<int>  temp_trans;
    if ( trans!=NULL ) temp_trans.resize(count);
    //
    // call merge_sort_
    //
    if ( trans!=NULL ) 
    {
      merge_sort_<Type>(coeff, count, trans, &temp_coeff[0], &temp_trans[0]);
    }
    else
    {
      merge_sort_<Type>(coeff, count, trans, &temp_coeff[0], NULL);
    }
  }
  else
  {
    //
    // use directly an insertion sort
    //
    insertion_sort_<Type>(coeff, count, trans);
  }
}



//*********************************************************************
//
// sort using size_t for indexing instead of int
//
//*********************************************************************

template <class Type>
static void mergeSort_mixUtility_(Type* coeff1, Type* coeff2, size_t count1, size_t count2, size_t* trans1, size_t* trans2, Type* temp_coeff, size_t* temp_trans)
{
  //
  // keep track of pointer
  //
  Type *__restrict__ tmp_coeff=temp_coeff;
  Type *__restrict__ coef1=coeff1;
  Type *__restrict__ coef2=coeff2;
  size_t  *__restrict__ tmp_trans=temp_trans;
  size_t  *__restrict__ tran1=trans1;
  size_t  *__restrict__ tran2=trans2;
  //
  // init indices
  //
  Type *borne1=coeff1+count1;
  Type *borne2=coeff2+count2;
  //
  // check what we should do with trans
  //
  bool comp_trans = ( temp_trans!=NULL );
  //
  // sort according to the coeffs
  // 
  if ( comp_trans )
  {
    while ( (coef1<borne1) && (coef2<borne2) )
    {
      while ( (coef1<borne1) && ( *(coef1)<=*(coef2) ) )
      {
        *(tmp_coeff++) = *(coef1++);
        *(tmp_trans++) = *(tran1++);
      }
      if (coef1<borne1)
      {
        while ( (coef2<borne2) && (*(coef2)<=*(coef1) ) )
        {
          *(tmp_coeff++) = *(coef2++);
          *(tmp_trans++) = *(tran2++); 
        }
      }
    }
    //
    // copy last values in temp arrays
    //
    while (coef1<borne1)
    {
      *(tmp_coeff++) = *(coef1++);
      *(tmp_trans++) = *(tran1++);
    }
    while (coef2<borne2)
    {
      *(tmp_coeff++) = *(coef2++);
      *(tmp_trans++) = *(tran2++);
    }
  }
  else
  {
    while ( (coef1<borne1) && (coef2<borne2) )
    {
      while ( (coef1<borne1) && ( *(coef1)<=*(coef2) ) )
      {
        *(tmp_coeff++) = *(coef1++);
      }
      if (coef1<borne1)
      {
        while ( (coef2<borne2) && (*(coef2)<=*(coef1) ) )
        {
          *(tmp_coeff++) = *(coef2++);
        }
      }
    }
    //
    // copy last values in temp arrays
    //
    while (coef1<borne1)
    {
      *(tmp_coeff++) = *(coef1++);
    }
    while (coef2<borne2)
    {
      *(tmp_coeff++) = *(coef2++);
    }
  }
  //
  // reset pointers 
  //
  tmp_coeff=temp_coeff;
  coef1=coeff1;
  coef2=coeff2;
  tmp_trans=temp_trans;
  tran1=trans1;
  tran2=trans2;
  //
  // copy data
  //  
  if ( comp_trans )
  {
    while (coef1<borne1)
    {
      *(coef1++) = *(tmp_coeff++);
      *(tran1++) = *(tmp_trans++);
    }
    while (coef2<borne2)
    {
      *(coef2++) = *(tmp_coeff++);
      *(tran2++) = *(tmp_trans++);
    }
  }
  else
  {
    while (coef1<borne1)
    {
      *(coef1++) = *(tmp_coeff++);
    }
    while (coef2<borne2)
    {
      *(coef2++) = *(tmp_coeff++);
    }
  }
}

template <class Type>
static void insertion_sort_(Type* coeff, size_t count, size_t* trans)
{
  if ( trans!=NULL )
  {
    Type *end_coeff=coeff+count;
    //
    // bring minimum value to the begining (avoid condition j>=0 later) 
    //
    Type min=(*coeff);
    Type *imin=coeff;
    //
    for ( Type *i=coeff ; i<end_coeff ; i++ )
    {
      if ( (*i)<min )
      {
        min=(*i);
        imin=i;
      }
    }
    //
    (*imin)=(*coeff);
    (*coeff)=min;
    //
    size_t *ptrans_min=trans+(size_t)(imin-coeff);
    size_t tmp=(*ptrans_min);
    (*ptrans_min)=(*trans);
    (*trans)=tmp;
    //
    // register variable
    //
    register Type tmp1;
    register size_t  itmp1;
    //
    // start insertion algorithm
    //
    for ( Type *i=coeff+2 ; i<end_coeff ; i++ )
    {
      //
      // keep values at this place
      //
      Type value=(*i);
      size_t  vtrans=(*(trans+(size_t)(i-coeff)));
      //
      // get pointers on arrays
      //
      Type *j=i-1;
      size_t  *t=trans+(size_t)(i-coeff)-1;
      //
      // shift values
      //
      while ( (*j)>value )
      {
        tmp1=*j; 
        itmp1=*t; 
        *(j+1)=tmp1;
        *(t+1)=itmp1;
        j--;
        t--;
      }
      //
      // insert
      //
      (*(j+1))=value;
      (*(t+1))=vtrans;
    }
  }
  else
  {
    Type *end_coeff=coeff+count;
    {
      register Type min=(*coeff);
      register Type *imin=coeff;
      //
      for ( Type *i=coeff ; i<end_coeff ; i++ )
      {
        if ( (*i)<min )
        {
          min=(*i);
          imin=i;
        }
      }
      //
      (*imin)=(*coeff);
      (*coeff)=min;
    }
    //
    // register variable
    //
    register Type tmp1;
    //
    // start insertion algorithm
    //
    for ( Type *i=coeff+2 ; i<end_coeff ; i++ )
    {
      //
      // keep values at this place
      //
      Type value=(*i);
      //
      // get pointers on arrays
      //
      Type *j=i-1;
      size_t  *t=trans+(size_t)(i-coeff)-1;
      //
      // shift values
      //
      while ( (*j)>value )
      {
        tmp1=*j; 
        *(j+1)=tmp1;
        j--;
      }
      //
      // insert
      //
      (*(j+1))=value;
    }
  }
}

// ****************************************************
//
// sort<Type>(Type* coeff, size_t count, size_t* trans)
//
//   sort a list of coefficients, Also apply te same 
//   transfo to the arrays trans.
// 
// ****************************************************

template <class Type>
static void merge_sort_(Type* coeff, size_t count, size_t* trans, Type* temp_coeff, size_t* temp_trans)
{
  //
  // if the list contains only one element, return
  //
  if (count==1) return;
  //
  // if we should apply the transform to trans
  //
  if ( trans!=NULL )
  {
    if ( count>SIZE_MERGE_MIN )
    {
      //
      // split the list in two an sort recursively
      //
      merge_sort_<Type>(coeff,count/2,trans,temp_coeff,temp_trans);
      merge_sort_<Type>(coeff+count/2,(count+1)/2,trans+count/2,temp_coeff+count/2,temp_trans+count/2);
      //
      // mix the two sorted lists
      //
      mergeSort_mixUtility_<Type>(coeff,coeff+count/2,count/2,(count+1)/2,trans,trans+count/2,temp_coeff,temp_trans);
    }
    else
    {
      insertion_sort_<Type>(coeff, count, trans);
    }
  }
  else
  {
    if ( count>SIZE_MERGE_MIN )
    {
      //
      // split the list in two an sort recursively
      //
      merge_sort_<Type>(coeff,count/2,NULL,temp_coeff,NULL);
      merge_sort_<Type>(coeff+count/2,(count+1)/2,NULL,temp_coeff+count/2,NULL);
      //
      // mix the two sorted lists
      //
      mergeSort_mixUtility_<Type>(coeff,coeff+count/2,count/2,(count+1)/2,NULL,NULL,temp_coeff,NULL);
    }
    else
    {
      insertion_sort_<Type>(coeff, count, NULL);
    }
  }
}

template <class Type>
static void sort(Type* coeff, size_t count, size_t* trans=NULL)
{
  //
  // fast return if possible
  //
  if ( count==0 ) return;
  //
  // sort according to size
  // 
  if ( count>SIZE_MERGE_MIN )
  {
    //
    // allocate memory for work 
    //
    std::vector<Type> temp_coeff(count);
    //
    // allocate trans only if necessary
    // 
    std::vector<size_t>  temp_trans;
    if ( trans!=NULL ) temp_trans.resize(count);
    //
    // call merge_sort_
    //
    if ( trans!=NULL ) 
    {
      merge_sort_<Type>(coeff, count, trans, &temp_coeff[0], &temp_trans[0]);
    }
    else
    {
      merge_sort_<Type>(coeff, count, trans, &temp_coeff[0], NULL);
    }
  }
  else
  {
    //
    // use directly an insertion sort
    //
    insertion_sort_<Type>(coeff, count, trans);
  }
}

#endif

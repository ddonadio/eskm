#include <iostream>
#include "Context.h"

// constructor
Context::Context(vector<int> list_proc, MPI_Comm comm, bool fake)
{
  //
  // init parent and child rank
  //
  parent_=NULL;
  child_rank_=-1;
  //
  // set flag
  //
  isFake_=fake;
  //
#ifdef WITH_MPI
  //
  // get the size of the current communicator
  //
  int size;
  MPI_Comm_size(comm, &size);
  //
  // get the rank of the process in the current communicator
  //
  int rank;
  MPI_Comm_rank(comm, &rank);
  //
  // check that the indices passed are valid and that this process is concerned
  //
  bool participate=false;
  //
  for ( int i=0 ; i<list_proc.size() ; i++ )
  {
    if ( list_proc[i]>=size && !fake )
    {
      cout << "Error: Context::Context - invalid processor index " << list_proc[i] << ", communicator size is " << size << endl;
      exit(0);
    }
    if ( list_proc[i]==rank ) participate=true;
  }
  //
  // if this is a real context
  //
  if ( !fake )
  {
    //
    // create the group defined by the proc list
    //
    {
      MPI_Group group;
      MPI_Comm_group( comm, &group );
      MPI_Group_incl( group, list_proc.size(), &list_proc[0], &group_handle_ );
      MPI_Group_free( &group );
    }
    //
    // create the associated communicator
    //
    MPI_Comm_create( comm, group_handle_, &comm_handle_);
    //
    // set the rank of this processor
    //
    if ( participate )
    {
      MPI_Comm_rank( comm_handle_, &my_proc_);
    }
    else
    {
      my_proc_=-1;
    }
    //
    // set number of processors in this context
    //
    if ( participate )
    {
      MPI_Comm_size( comm_handle_, &n_procs_);
    }
    else
    {
      n_procs_=list_proc.size();
    }
  }
  //
  // otherwise, if this is a fake context
  //
  else
  {
    //
    // place this proc in the global comm
    //
    MPI_Comm_rank( comm, &my_proc_);
    //
    if ( my_proc_>=list_proc.size() ) my_proc_=-1;
    //
    // set number of procs
    // 
    n_procs_=list_proc.size();
    //
    // set communicator
    //
    comm_handle_=comm;
  }
#else
  //
  // check that there is only proc index 0 in the list
  //
  if ( list_proc.size()>1 || list_proc[0]!=0 )
  {
    cout << "Error: Context::Context - invalid processor index " << list_proc[0] << "in serial build" << endl;
    exit(0);
  }
  //
  // set rank of this proc
  //
  my_proc_=0;
  //
  // set number of procs
  //
  n_procs_=1;
#endif  
  //
  // create a map entry for each processor index
  //
  for  ( int i=0 ; i<list_proc.size() ; i++ ) global_indices_on_proc_[i];
}

// destructor
Context::~Context(void)
{
#ifdef WITH_MPI
  //
  // free group
  //
  MPI_Group_free(&group_handle_);
  //
  // free comm if necessary
  //
  if ( comm_handle_!=MPI_COMM_NULL ) MPI_Comm_free(&comm_handle_);
#endif
  //
  // delete all the child context
  //
  for ( int icontext=0 ; icontext<children_.size() ; icontext++ ) delete children_[icontext];
}

// division
Context* Context::divide(int nContexts)
{
  //
  // get size of the groups
  //
  int group_size=n_procs_/nContexts;
  //
  // warn if any proc is let asside
  //
  if ( group_size*nContexts<n_procs_ && my_proc_==0 )
  {
    cout << endl;
    cout << "*******************************************************************" << endl;
    cout << "* Warning !!! " << n_procs_-group_size*nContexts << " proc(s) is/are unsed for context division in - Context::divide -" << endl;
    cout << "*******************************************************************" << endl;
    cout << endl;
  }
  //
  // init pointer on new context to which this proc belongs
  //
  Context *context=NULL;
  //
  // create the new contexts
  //
  for ( int icontext=0 ; icontext<nContexts ; icontext++ )
  {
    //
    // create index list for this context
    //
    vector<int> proc_indices;
    for ( int i=0 , iproc=icontext*group_size ; i<group_size ; i++ , iproc++ ) proc_indices.push_back(iproc);
    //
    // create the new context
    //
    children_.push_back( new Context(proc_indices,comm_handle_,isFake_) );
    //
    // set parents attribute
    //
    children_.back()->parent_=this;
    //
    // set child position within parent context
    //
    children_.back()->child_rank_=icontext;
    //
    // check if proc belongs to this context
    //
    for ( int i=0 , iproc=icontext*group_size ; i<group_size ; i++ , iproc++ ) if ( iproc==my_proc_ ) context=children_.back();
  }
  //
  // return the context list
  //
  return context;
}

// indices access method
void Context::setIndicesOnProc(int i_proc, vector<int> indices)
{
  //
  // check if proc i is in the map
  //
  if ( !global_indices_on_proc_.count(i_proc) ) 
  {
    cout << "Error: Context::setIndicesOnProc - processor index " << i_proc << " is not part of the context" << endl;
    exit(0);
  }
  //
  // set the indices for the proc
  //
  global_indices_on_proc_[i_proc]=indices;
} 

void Context::addIndicesOnProc(int i_proc, vector<int> indices)
{
  //
  // check if proc i is in the map
  //
  if ( !global_indices_on_proc_.count(i_proc) ) 
  {
    cout << "Error: Context::setIndicesOnProc - processor index " << i_proc << " is not part of the context" << endl;
    exit(0);
  }
  //
  // set the indices for the proc
  //
  global_indices_on_proc_[i_proc].insert(global_indices_on_proc_[i_proc].end(), indices.begin(), indices.end());
} 

vector<int> Context::getIndicesOnProc(int i_proc)
{
  //
  // check if proc i is in the map
  //
  if ( !global_indices_on_proc_.count(i_proc) ) 
  {
    cout << "Error: Context::getIndicesOnProc - processor index " << i_proc << " is not part of the context" << endl;
    exit(0);
  }
  //
  // return the indices for the proc
  //
  return global_indices_on_proc_[i_proc];
} 

vector<int> Context::getIndices(void)
{
  //
  // return the indices for my_proc
  //
  return global_indices_on_proc_[my_proc_];
} 


    


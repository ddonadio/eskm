#ifndef CENTERLIST_H
#define CENTERLIST_H

#include "Basis3D.h"
#include "Lattice.h"

// ****************************************************
//
// center list handling 
// 
// ****************************************************

template <class CoordinatePrecision>
bool areNeighbor( vector<Vector3D<CoordinatePrecision> > &center_list1 , vector<Vector3D<CoordinatePrecision> > &center_list2 , CoordinatePrecision cutoff, Vector3D<CoordinatePrecision> &T1, Vector3D<CoordinatePrecision> &T2 )
{
  //
  // get translation norms 
  // 
  CoordinatePrecision norm0=T1*T1;
  CoordinatePrecision norm1=T2*T2;
  //
  // get square cutoff 
  //
  CoordinatePrecision cutoff2 = cutoff*cutoff;
  //
  // case 1: if there is no translation symmetry
  //
  if ( norm0==0.0 && norm1==0.0 )
  {
    //
    // get bounding box of list 1
    //
    CoordinatePrecision xMin,yMin,zMin;
    CoordinatePrecision xMax,yMax,zMax;
    {
      //
      // init bounding box
      //
      xMin=center_list1[0][0];
      xMax=center_list1[0][0];
      yMin=center_list1[0][1];
      yMax=center_list1[0][1];
      zMin=center_list1[0][2];
      zMax=center_list1[0][2];
      //
      // loop on remaining centers
      //
      for ( int i_center=1 ; i_center<center_list1.size() ; i_center++ )
      {
        if ( xMin > center_list1[i_center][0] ) xMin = center_list1[i_center][0];
        if ( xMax < center_list1[i_center][0] ) xMax = center_list1[i_center][0];
        if ( yMin > center_list1[i_center][1] ) yMin = center_list1[i_center][1];
        if ( yMax < center_list1[i_center][1] ) yMax = center_list1[i_center][1];
        if ( zMin > center_list1[i_center][2] ) zMin = center_list1[i_center][2];
        if ( zMax < center_list1[i_center][2] ) zMax = center_list1[i_center][2];
      }
      //
      // extend boudary of this bounding box by cutoff distance 
      //
      xMax+=cutoff;
      xMin-=cutoff;
      yMax+=cutoff;
      yMin-=cutoff;
      zMax+=cutoff;
      zMin-=cutoff;
    }
    //
    // loop on second center list
    //
    for ( int icenter2=0 , stop2=center_list2.size() ; icenter2<stop2 ; icenter2++ )
    {
      //
      // get coordinates of center 1
      //
      CoordinatePrecision x2=center_list2[icenter2][0];
      CoordinatePrecision y2=center_list2[icenter2][1];
      CoordinatePrecision z2=center_list2[icenter2][2];      //
      //check if this center is in the bounding box of list 1
      // 
      bool in_x = ( xMax >= x2 ) && ( x2 >= xMin );
      bool in_y = ( yMax >= y2 ) && ( y2 >= yMin );
      bool in_z = ( zMax >= z2 ) && ( z2 >= zMin );
      //
      if ( in_x && in_y && in_z )
      {
        //
        // loop on first center list
        //
        for ( int icenter1=0 , stop1=center_list1.size() ; icenter1<stop1 ; icenter1++ )
        { 
          //
          // get distance between centers
          //
          CoordinatePrecision dx=x2-center_list1[icenter1][0];
          CoordinatePrecision dy=y2-center_list1[icenter1][1];
          CoordinatePrecision dz=z2-center_list1[icenter1][2];
          CoordinatePrecision d=dx*dx+dy*dy+dz*dz;
          //
          // return true if distance is shorter than cutoff
          //
          if ( d<cutoff2 ) return true;
        }
      }
    }
  }
  //
  // case 2: there is at least one translation symmetry
  //
  else
  {
    //
    // generate a list of the non zero translation
    //
    vector<Vector3D<CoordinatePrecision> > vector_list;
    if ( norm0!=0.0 ) vector_list.push_back(T1);
    if ( norm1!=0.0 ) vector_list.push_back(T2);
    int nTranslations=vector_list.size();
    //
    // generate a basis from the translation vector(s)
    //
    Basis3D<CoordinatePrecision> basis(vector_list);
    //
    // generate a Lattice from the translation vector(s)
    //
    Lattice<CoordinatePrecision> lattice(vector_list);
    //
    // compute the bounding box of the first list, expressed in basis coordinates
    //
    CoordinatePrecision yMin,zMin;
    CoordinatePrecision yMax,zMax;
    {
      //
      // init bounding box
      //
      Vector3D<CoordinatePrecision> coordinates=basis.getCoordinatesOf( center_list1[0] );
      yMin=coordinates[1];
      yMax=coordinates[1];
      zMin=coordinates[2];
      zMax=coordinates[2];
      //
      // loop on remaining centers
      //
      for ( int i_center=1 ; i_center<center_list1.size() ; i_center++ )
      {
        coordinates=basis.getCoordinatesOf( center_list1[i_center] );
        if ( yMin>coordinates[1] ) yMin=coordinates[1];
        if ( yMax<coordinates[1] ) yMax=coordinates[1];
        if ( zMin>coordinates[2] ) zMin=coordinates[2];
        if ( zMax<coordinates[2] ) zMax=coordinates[2]; 
      }
      //
      // widen the bounding box with cutoff
      //
      yMin-=cutoff;
      yMax+=cutoff;
      zMin-=cutoff;
      zMax+=cutoff;
    }
    //
    // loop on second center list
    //
    for ( int icenter2=0 , stop2=center_list2.size() ; icenter2<stop2 ; icenter2++ )
    {
      //
      // get coordinates of center in basis coordinates
      //
      Vector3D<CoordinatePrecision> coordinates=basis.getCoordinatesOf( center_list2[icenter2] );
      //
      // see if this center fit in the bounding box
      //
      bool in;
      //
      switch ( nTranslations )
      {
        case 1:
          //
          // take both y and z direction into account
          //
          in  = ( yMax >= coordinates[1] ) && ( coordinates[1] >= yMin );
          in &= ( zMax >= coordinates[2] ) && ( coordinates[2] >= zMin );
          break;
          
        case 2:
          //
          // only z direction is relevant
          //
          in  = ( zMax >= coordinates[2] ) && ( coordinates[2] >= zMin );
          break;
          
        default:
          //
          // should never get here
          //
          cout << "Error: reached a point not supposed to in - Interact -" << endl;
          exit(0); 
      }
      //
      // if this center is in bounding box of list 1
      //
      if ( in )
      {
        //
        // loop on centers of list 1
        //
        for ( int icenter1=0 , stop1=center_list1.size() ; icenter1<stop1 ; icenter1++ )
        { 
          //
          // get difference vector between centeric posistion
          //
          Vector3D<CoordinatePrecision> V;
          V[0]=center_list2[icenter2][0]-center_list1[icenter1][0];
          V[1]=center_list2[icenter2][1]-center_list1[icenter1][1];
          V[2]=center_list2[icenter2][2]-center_list1[icenter1][2];
          //
          // fold the difference vector according to lattice
          //
          lattice.fold(V);
          //
          // compute minimal distance between centers
          //
          CoordinatePrecision d=V[0]*V[0]+V[1]*V[1]+V[2]*V[2];
          //
          // return true if distance is shorter than cutoff
          //
          if ( d<cutoff2 ) return true;
        }
      }
    }
  }
  //
  // if none interact, return 0
  //
  return false;
}

#endif

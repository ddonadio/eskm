/*! \file Atom.h
    \brief Atom class and related function declarations
*/

#ifndef ATOM_H
#define ATOM_H

#include <string>
#include <iostream>
#include "Sort.h"
#include "Vector3D.h"
#include "Basis3D.h"
#include "Lattice.h"
#include "Round.h" // put this in last position to override all previous definitions.
#include "FileHandler.h"

using namespace std;

#define EPSILON 1.0E-3
#define round   localRound

//! Atom Class.
/// This class template is intended to help manipulate atoms. 
/// It implements mainly operations on atom coordinates.
/// Some list operations are also provided for vector<Atom> objects (not listed in the help).
 
template <class CoordinatePrecision>
class Atom
{
  private:
    
  public:
  
    // atribute
    string name;                 ///< contains the name of the atom, no size limit
    CoordinatePrecision x_pos;   ///< x coordinate of the atom
    CoordinatePrecision y_pos;   ///< y coordinate of the atom
    CoordinatePrecision z_pos;   ///< z coordinate of the atom
    
    // method
    bool isNeighbor( Atom &atom, CoordinatePrecision cuttoff); ///< neighboring test. @return true if atom is within cutoff distance.
    Atom<CoordinatePrecision>& changeBasis(CoordinatePrecision V1[3], CoordinatePrecision V2[3], CoordinatePrecision V3[3]); ///< onsight coordinates basis change. \param V1, V2 and V3 should be the duals of the new basis vectors. @return reference on this atom
    Atom<CoordinatePrecision>& changeBasis(Vector3D<CoordinatePrecision> &V1, Vector3D<CoordinatePrecision> &V2, Vector3D<CoordinatePrecision> &V3); ///< onsight coordinates basis change. \param V1, V2 and V3 should be the duals of the new basis vectors. @return reference on this atom
    Vector3D<CoordinatePrecision> coordinates(void);  ///< coordinates access. @return Vector3D<precision> containing coordinates of the atom.
    
    // assignement operator
    Atom<CoordinatePrecision>& operator = (const Atom<CoordinatePrecision> &other); ///< assignement operator
    
    // copy constructor
    Atom<CoordinatePrecision>(const Atom<CoordinatePrecision> &other); ///< Atom copy constructor

    // sorting operators
    bool  operator > (const Atom &other);  ///< > comparision operator. @return \f$ (z_1>z_2) \,|\, ( (z_1=z_2) \& (y_1>y_2) ) \,|\, ( (z_1=z_2) \& (y_1=y_2) \& (x_1>x_2) ) \f$
    bool  operator < (const Atom &other);  ///< < comparision operator. @return \f$ (z_1<z_2) \,|\, ( (z_1=z_2) \& (y_1<y_2) ) \,|\, ( (z_1=z_2) \& (y_1=y_2) \& (x_1<x_2) ) \f$
    bool  operator ==(const Atom &other);  ///< == comparision operator. @return \f$ (z_1=z_2) \& (y_1=y_2) \& (x_1=x_2) \f$
    bool  operator !=(const Atom &other);  ///< != comparision operator. @return \f$ (z_1\neq z_2) \,|\, (y_1\neq y_2) \,|\, (x_1\neq x_2) \f$
    bool  operator >=(const Atom &other);  ///< >= comparision operator. @return \f$ (Atom_1>Atom_2) \,|\, (Atom_1=Atom_2) \f$
    bool  operator <=(const Atom &other);  ///< <= comparision operator. @return \f$ (Atom_1<Atom_2) \,|\, (Atom_1=Atom_2) \f$
    
    // translation operators
    Atom<CoordinatePrecision>  operator + (CoordinatePrecision T[3]); ///< translation operator. @return Atom<CoordinatePrecision>(name,(x+t[1],y+t[2],z+t[3])). 
    Atom<CoordinatePrecision>  operator - (CoordinatePrecision T[3]); ///< translation operator. @return Atom<CoordinatePrecision>(name,(x-t[1],y-t[2],z-t[3])).
    Atom<CoordinatePrecision>  operator + (Vector3D<CoordinatePrecision> &T); ///< translation operator. @return Atom<CoordinatePrecision>(name,(x+t[1],y+t[2],z+t[3])).
    Atom<CoordinatePrecision>  operator - (Vector3D<CoordinatePrecision> &T); ///< translation operator. @return Atom<CoordinatePrecision>(name,(x-t[1],y-t[2],z-t[3])).
    
    // translation operators
    Atom<CoordinatePrecision>& operator +=(CoordinatePrecision T[3]); ///< onsight translation operator. @return &Atom<CoordinatePrecision>(name,(x+t[1],y+t[2],z+t[3])).
    Atom<CoordinatePrecision>& operator -=(CoordinatePrecision T[3]); ///< onsight translation operator. @return &Atom<CoordinatePrecision>(name,(x-t[1],y-t[2],z-t[3])).
    Atom<CoordinatePrecision>& operator +=(Vector3D<CoordinatePrecision> &T); ///< onsight translation operator. @return &Atom<CoordinatePrecision>(name,(x+t[1],y+t[2],z+t[3])).
    Atom<CoordinatePrecision>& operator -=(Vector3D<CoordinatePrecision> &T); ///< onsight translation operator. @return &Atom<CoordinatePrecision>(name,(x-t[1],y-t[2],z-t[3])).
        
    // construtor
    Atom<CoordinatePrecision>(void){ x_pos=0.0; y_pos=0.0 ; z_pos=0.0; } ///< standard Atom constructor
    
    // destructor
    ~Atom<CoordinatePrecision>(void){ name.clear(); x_pos=0.0; y_pos=0.0 ; z_pos=0.0; } ///< Atom destructor
};

// ****************************************************
//
// display function
// 
// ****************************************************

//! 
//! << stream output of an atom object

template <class CoordinatePrecision>
ostream& operator << ( ostream &str , Atom<CoordinatePrecision> atom )
{
  str.precision(6);
  str.width(3);
  str << atom.name; 
  str.width(15);
  str << fixed << atom.x_pos;
  str.width(15);
  str << fixed << atom.y_pos;
  str.width(15);
  str << fixed << atom.z_pos;
  //
  return str;
} 

// ****************************************************
//
// input from file handler
// 
// ****************************************************

//! 
//! << input of an atom object from FileHandler

template <class CoordinatePrecision>
Atom<CoordinatePrecision>& operator << (Atom<CoordinatePrecision> &atom, FileHandler &file)
{
  char *buff;
  //
  atom.name=file.readNextWord();
  if (file.eof() || atom.name[0]==0 ) 
  {    
    cout << "error while reading atome name in - Atom << FileHandler - " << endl;
    exit(0);
  }
  
  buff=file.readNextWord();
  if (file.eof() || buff==NULL ) 
  {    
    cout << "error while reading atome x coordinate in - Atom << FileHandler - " << endl;
    exit(0);
  }
  atom.x_pos=atof(buff);
  
  buff=file.readNextWord();
  if (file.eof() || buff==NULL ) 
  {    
    cout << "error while reading atome y coordinate in - Atom << FileHandler - " << endl;
    exit(0);
  }
  atom.y_pos=atof(buff);
  
  buff=file.readNextWord();
  if ( buff==NULL ) 
  {    
    cout << "error while reading atome z coordinate in - Atom << FileHandler - " << endl;
    exit(0);
  }
  atom.z_pos=atof(buff);
  
  //
  return atom;
}

// ****************************************************
//
// Atom neigborhood method
// 
// ****************************************************

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::isNeighbor( Atom<CoordinatePrecision> &atom, CoordinatePrecision cutoff)
{
  CoordinatePrecision x=x_pos-atom.x_pos;
  CoordinatePrecision y=y_pos-atom.y_pos;
  CoordinatePrecision z=z_pos-atom.z_pos;
  CoordinatePrecision dist= x*x + y*y + z*z;
  return ( dist<cutoff*cutoff && dist>EPSILON );
}

// ****************************************************
//
// Basis change method
// 
// ****************************************************

template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::changeBasis(CoordinatePrecision V1[3], CoordinatePrecision V2[3], CoordinatePrecision V3[3])
{
  CoordinatePrecision x=V1[0]*x_pos+V1[1]*y_pos+V1[2]*z_pos;
  CoordinatePrecision y=V2[0]*x_pos+V2[1]*y_pos+V2[2]*z_pos;
  CoordinatePrecision z=V3[0]*x_pos+V3[1]*y_pos+V3[2]*z_pos;
  x_pos=x;
  y_pos=y;
  z_pos=z;
  return *this;
}

template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::changeBasis(Vector3D<CoordinatePrecision> &V1, Vector3D<CoordinatePrecision> &V2, Vector3D<CoordinatePrecision> &V3)
{
  CoordinatePrecision x=V1[0]*x_pos+V1[1]*y_pos+V1[2]*z_pos;
  CoordinatePrecision y=V2[0]*x_pos+V2[1]*y_pos+V2[2]*z_pos;
  CoordinatePrecision z=V3[0]*x_pos+V3[1]*y_pos+V3[2]*z_pos;
  x_pos=x;
  y_pos=y;
  z_pos=z;
  return *this;
}

// ****************************************************
//
// Coordinates access
// 
// ****************************************************

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Atom<CoordinatePrecision>::coordinates(void)
{
  Vector3D<CoordinatePrecision> V;
  V[0]=x_pos;
  V[1]=y_pos;
  V[2]=z_pos;
  return V;
}

// ****************************************************
//
// Atom copy operator
// 
// ****************************************************

template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::operator = (const Atom<CoordinatePrecision> &other)
{ 
  //
  // copy attributes
  // 
  name=other.name;
  x_pos=other.x_pos;
  y_pos=other.y_pos;
  z_pos=other.z_pos;
  //
  // return reference
  //
  return *this;
}


// ****************************************************
//
// Atom copy constructor 
// 
// ****************************************************

template <class CoordinatePrecision>
Atom<CoordinatePrecision>::Atom(const Atom<CoordinatePrecision> &other)
{
  //
  // copy attributes
  //
  name.assign(other.name);
  x_pos=other.x_pos;
  y_pos=other.y_pos;
  z_pos=other.z_pos;
}

// ****************************************************
//
// Atom comparison operator (usefull for sorting)
// 
// ****************************************************

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::operator > (const Atom<CoordinatePrecision> &other)
{ 
  int x1=(int)round(x_pos/EPSILON);
  int y1=(int)round(y_pos/EPSILON);
  int z1=(int)round(z_pos/EPSILON);
  
  int x2=(int)round(other.x_pos/EPSILON);
  int y2=(int)round(other.y_pos/EPSILON);
  int z2=(int)round(other.z_pos/EPSILON);
  
  bool answer=false;
  answer = ( answer || ( z1>z2 ) );
  answer = ( answer || ( z1==z2 && y1>y2 ) );
  answer = ( answer || ( z1==z2 && y1==y2 && x1>x2 ) );
  return answer;
}

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::operator < (const Atom<CoordinatePrecision> &other)
{
  int x1=(int)round(x_pos/EPSILON);
  int y1=(int)round(y_pos/EPSILON);
  int z1=(int)round(z_pos/EPSILON);
  
  int x2=(int)round(other.x_pos/EPSILON);
  int y2=(int)round(other.y_pos/EPSILON);
  int z2=(int)round(other.z_pos/EPSILON);
   
  bool answer=false;
  answer = ( answer || ( z1<z2 ) );
  answer = ( answer || ( z1==z2 && y1<y2 ) );
  answer = ( answer || ( z1==z2 && y1==y2 && x1<x2 ) );
  return answer;
}    

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::operator == (const Atom<CoordinatePrecision> &other)
{ 
  int x1=(int)round(x_pos/EPSILON);
  int y1=(int)round(y_pos/EPSILON);
  int z1=(int)round(z_pos/EPSILON);
  
  int x2=(int)round(other.x_pos/EPSILON);
  int y2=(int)round(other.y_pos/EPSILON);
  int z2=(int)round(other.z_pos/EPSILON);
  
  bool answer = name==other.name;
  answer = ( answer && ( x1==x2 ) );
  answer = ( answer && ( y1==y2 ) );
  answer = ( answer && ( z1==z2 ) );
  return answer;
}

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::operator != (const Atom<CoordinatePrecision> &other)
{ 
  int x1=(int)round(x_pos/EPSILON);
  int y1=(int)round(y_pos/EPSILON);
  int z1=(int)round(z_pos/EPSILON);
  
  int x2=(int)round(other.x_pos/EPSILON);
  int y2=(int)round(other.y_pos/EPSILON);
  int z2=(int)round(other.z_pos/EPSILON);

  bool answer = name!=other.name;
  answer = ( answer || ( x1!=x2 ) );
  answer = ( answer || ( y1!=y2 ) );
  answer = ( answer || ( z1!=z2 ) );
  return answer;
}

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::operator <= (const Atom<CoordinatePrecision> &other)
{ 
  int x1=(int)round(x_pos/EPSILON);
  int y1=(int)round(y_pos/EPSILON);
  int z1=(int)round(z_pos/EPSILON);
  
  int x2=(int)round(other.x_pos/EPSILON);
  int y2=(int)round(other.y_pos/EPSILON);
  int z2=(int)round(other.z_pos/EPSILON);
  
  bool answer=true;
  answer = ( answer && ( x1==x2 ) );
  answer = ( answer && ( y1==y2 ) );
  answer = ( answer && ( z1==z2 ) );
  answer = ( answer || ( z1<z2 ) );
  answer = ( answer || ( z1==z2 && y1<y2 ) );
  answer = ( answer || ( z1==z2 && y1==y2 && x1<x2 ) );
  return answer;
}

template <class CoordinatePrecision>
bool Atom<CoordinatePrecision>::operator >= (const Atom<CoordinatePrecision> &other)
{ 
  int x1=(int)round(x_pos/EPSILON);
  int y1=(int)round(y_pos/EPSILON);
  int z1=(int)round(z_pos/EPSILON);
  
  int x2=(int)round(other.x_pos/EPSILON);
  int y2=(int)round(other.y_pos/EPSILON);
  int z2=(int)round(other.z_pos/EPSILON);
  
  bool answer=true;
  answer = ( answer && ( x1==x2 ) );
  answer = ( answer && ( y1==y2 ) );
  answer = ( answer && ( z1==z2 ) );
  answer = ( answer || ( z1>z2 ) );
  answer = ( answer || ( z1==z2 && y1>y2 ) );
  answer = ( answer || ( z1==z2 && y1==y2 && x1>x2 ) );
  return answer;
}

// ****************************************************
//
// Atom translation operators 
// 
// ****************************************************

template <class CoordinatePrecision>
Atom<CoordinatePrecision> Atom<CoordinatePrecision>::operator + (CoordinatePrecision T[3])
{
  //
  // copy atom object
  // 
  Atom<CoordinatePrecision> atom;
  atom.name=name;
  atom.x_pos=x_pos+T[0];
  atom.y_pos=y_pos+T[1];
  atom.z_pos=z_pos+T[2];
  //
  // return translated atom
  //
  return atom;  
}  
  
template <class CoordinatePrecision>
Atom<CoordinatePrecision> Atom<CoordinatePrecision>::operator - (CoordinatePrecision T[3])
{
  //
  // copy atom object
  // 
  Atom<CoordinatePrecision> atom;
  atom.name=name;
  atom.x_pos=x_pos-T[0];
  atom.y_pos=y_pos-T[1];
  atom.z_pos=z_pos-T[2];
  //
  // return translated atom
  //
  return atom;  
} 

template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::operator += (CoordinatePrecision T[3])
{
  //
  // translate coordinates
  // 
  x_pos+=T[0];
  y_pos+=T[1];
  z_pos+=T[2];
  return *this;
}  
  
template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::operator -= (CoordinatePrecision T[3])
{
  //
  // translate coordinates
  // 
  x_pos-=T[0];
  y_pos-=T[1];
  z_pos-=T[2];
  return *this;
}  

template <class CoordinatePrecision>
Atom<CoordinatePrecision> Atom<CoordinatePrecision>::operator + (Vector3D<CoordinatePrecision> &T)
{
  //
  // copy atom object
  // 
  Atom<CoordinatePrecision> atom;
  atom.name=name;
  atom.x_pos=x_pos+T[0];
  atom.y_pos=y_pos+T[1];
  atom.z_pos=z_pos+T[2];
  //
  // return translated atom
  //
  return atom;  
}  
  
template <class CoordinatePrecision>
Atom<CoordinatePrecision> Atom<CoordinatePrecision>::operator - (Vector3D<CoordinatePrecision> &T)
{
  //
  // copy atom object
  // 
  Atom<CoordinatePrecision> atom;
  atom.name=name;
  atom.x_pos=x_pos-T[0];
  atom.y_pos=y_pos-T[1];
  atom.z_pos=z_pos-T[2];
  //
  // return translated atom
  //
  return atom;  
} 

template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::operator += (Vector3D<CoordinatePrecision> &T)
{
  //
  // translate coordinates
  // 
  x_pos+=T[0];
  y_pos+=T[1];
  z_pos+=T[2];
  return *this;
}  
  
template <class CoordinatePrecision>
Atom<CoordinatePrecision>& Atom<CoordinatePrecision>::operator -= (Vector3D<CoordinatePrecision> &T)
{
  //
  // translate coordinates
  // 
  x_pos-=T[0];
  y_pos-=T[1];
  z_pos-=T[2];
  return *this;
}  

// ****************************************************
//
// Atom list handling 
// 
// ****************************************************

//! areNeighbor:
//! atom list neighboring test
//! return true if two atoms, one from each list, are within cutoff distance given two optional translations T1 and T2 

template <class CoordinatePrecision>
bool areNeighbor( vector<Atom<CoordinatePrecision> > &atom_list1 , vector<Atom<CoordinatePrecision> > &atom_list2 , CoordinatePrecision cutoff, Vector3D<CoordinatePrecision> &T1, Vector3D<CoordinatePrecision> &T2 )
{
  //
  // get translation norms 
  // 
  CoordinatePrecision norm0=T1*T1;
  CoordinatePrecision norm1=T2*T2;
  //
  // get square cutoff 
  //
  CoordinatePrecision cutoff2 = cutoff*cutoff;
  //
  // case 1: if there is no translation symmetry
  //
  if ( norm0==0.0 && norm1==0.0 )
  {
    //
    // get bounding box of list 1
    //
    CoordinatePrecision xMin,yMin,zMin;
    CoordinatePrecision xMax,yMax,zMax;
    {
      //
      // init bounding box
      //
      xMin=atom_list1[0].x_pos;
      xMax=atom_list1[0].x_pos;
      yMin=atom_list1[0].y_pos;
      yMax=atom_list1[0].y_pos;
      zMin=atom_list1[0].z_pos;
      zMax=atom_list1[0].z_pos;
      //
      // loop on remaining atoms
      //
      for ( int i_atom=1 ; i_atom<atom_list1.size() ; i_atom++ )
      {
        if ( xMin > atom_list1[i_atom].x_pos ) xMin = atom_list1[i_atom].x_pos;
        if ( xMax < atom_list1[i_atom].x_pos ) xMax = atom_list1[i_atom].x_pos;
        if ( yMin > atom_list1[i_atom].y_pos ) yMin = atom_list1[i_atom].y_pos;
        if ( yMax < atom_list1[i_atom].y_pos ) yMax = atom_list1[i_atom].y_pos;
        if ( zMin > atom_list1[i_atom].z_pos ) zMin = atom_list1[i_atom].z_pos;
        if ( zMax < atom_list1[i_atom].z_pos ) zMax = atom_list1[i_atom].z_pos;
      }
      //
      // extend boudary of this bounding box by cutoff distance 
      //
      xMax+=cutoff;
      xMin-=cutoff;
      yMax+=cutoff;
      yMin-=cutoff;
      zMax+=cutoff;
      zMin-=cutoff;
    }
    //
    // loop on second atom list
    //
    for ( int iatom2=0 , stop2=atom_list2.size() ; iatom2<stop2 ; iatom2++ )
    {
      //
      // get coordinates of atom 1
      //
      CoordinatePrecision x2=atom_list2[iatom2].x_pos;
      CoordinatePrecision y2=atom_list2[iatom2].y_pos;
      CoordinatePrecision z2=atom_list2[iatom2].z_pos;      //
      //check if this atom is in the bounding box of list 1
      // 
      bool in_x = ( xMax >= x2 ) && ( x2 >= xMin );
      bool in_y = ( yMax >= y2 ) && ( y2 >= yMin );
      bool in_z = ( zMax >= z2 ) && ( z2 >= zMin );
      //
      if ( in_x && in_y && in_z )
      {
        //
        // loop on first atom list
        //
        for ( int iatom1=0 , stop1=atom_list1.size() ; iatom1<stop1 ; iatom1++ )
        { 
          //
          // get distance between atoms
          //
          CoordinatePrecision dx=x2-atom_list1[iatom1].x_pos;
          CoordinatePrecision dy=y2-atom_list1[iatom1].y_pos;
          CoordinatePrecision dz=z2-atom_list1[iatom1].z_pos;
          CoordinatePrecision d=dx*dx+dy*dy+dz*dz;
          //
          // return true if distance is shorter than cutoff
          //
          if ( d<cutoff2 ) return true;
        }
      }
    }
  }
  //
  // case 2: there is at least one translation symmetry
  //
  else
  {
    //
    // generate a list of the non zero translation
    //
    vector<Vector3D<CoordinatePrecision> > vector_list;
    if ( norm0!=0.0 ) vector_list.push_back(T1);
    if ( norm1!=0.0 ) vector_list.push_back(T2);
    int nTranslations=vector_list.size();
    //
    // generate a basis from the translation vector(s)
    //
    Basis3D<CoordinatePrecision> basis(vector_list);
    //
    // generate a Lattice from the translation vector(s)
    //
    Lattice<CoordinatePrecision> lattice(vector_list);
    //
    // compute the bounding box of the first list, expressed in basis coordinates
    //
    CoordinatePrecision yMin,zMin;
    CoordinatePrecision yMax,zMax;
    {
      //
      // init bounding box
      //
      Vector3D<CoordinatePrecision> coordinates=basis.getCoordinatesOf( atom_list1[0].coordinates() );
      yMin=coordinates[1];
      yMax=coordinates[1];
      zMin=coordinates[2];
      zMax=coordinates[2];
      //
      // loop on remaining atoms
      //
      for ( int i_atom=1 ; i_atom<atom_list1.size() ; i_atom++ )
      {
        coordinates=basis.getCoordinatesOf( atom_list1[i_atom].coordinates() );
        if ( yMin>coordinates[1] ) yMin=coordinates[1];
        if ( yMax<coordinates[1] ) yMax=coordinates[1];
        if ( zMin>coordinates[2] ) zMin=coordinates[2];
        if ( zMax<coordinates[2] ) zMax=coordinates[2]; 
      }
      //
      // widen the bounding box with cutoff
      //
      yMin-=cutoff;
      yMax+=cutoff;
      zMin-=cutoff;
      zMax+=cutoff;
    }
    //
    // loop on second atom list
    //
    for ( int iatom2=0 , stop2=atom_list2.size() ; iatom2<stop2 ; iatom2++ )
    {
      //
      // get coordinates of atom in basis coordinates
      //
      Vector3D<CoordinatePrecision> coordinates=basis.getCoordinatesOf( atom_list2[iatom2].coordinates() );
      //
      // see if this atom fit in the bounding box
      //
      bool in;
      //
      switch ( nTranslations )
      {
        case 1:
          //
          // take both y and z direction into account
          //
          in  = ( yMax >= coordinates[1] ) && ( coordinates[1] >= yMin );
          in &= ( zMax >= coordinates[2] ) && ( coordinates[2] >= zMin );
          break;
          
        case 2:
          //
          // only z direction is relevant
          //
          in  = ( zMax >= coordinates[2] ) && ( coordinates[2] >= zMin );
          break;
          
        default:
          //
          // should never get here
          //
          cout << "Error: reached a point not supposed to in - Interact -" << endl;
          exit(0); 
      }
      //
      // if this atom is in bounding box of list 1
      //
      if ( in )
      {
        //
        // loop on atoms of list 1
        //
        for ( int iatom1=0 , stop1=atom_list1.size() ; iatom1<stop1 ; iatom1++ )
        { 
          //
          // get difference vector between atomic posistion
          //
          Vector3D<CoordinatePrecision> V;
          V[0]=atom_list2[iatom2].x_pos-atom_list1[iatom1].x_pos;
          V[1]=atom_list2[iatom2].y_pos-atom_list1[iatom1].y_pos;
          V[2]=atom_list2[iatom2].z_pos-atom_list1[iatom1].z_pos;
          //
          // fold the difference vector according to lattice
          //
          lattice.fold(V);
          //
          // compute minimal distance between atoms
          //
          CoordinatePrecision d=V[0]*V[0]+V[1]*V[1]+V[2]*V[2];
          //
          // return true if distance is shorter than cutoff
          //
          if ( d<cutoff2 ) return true;
        }
      }
    }
  }
  //
  // if none interact, return 0
  //
  return false;
}

#define SMALL_ATOM_ARRAY_THRESHOLD 32

//! getAtomIndices:
//! atom list index extraction
//! left atom list should only contain atom present in the right list.
//! return the indices in the right list of atoms given in the left list.

template <class CoordinatePrecision>
vector<int> getAtomIndices( vector<Atom<CoordinatePrecision> > &left, vector<Atom<CoordinatePrecision> > &right )
{
  //
  // create the list
  //
  vector<int> result(left.size());
  //
  // fast return if possible
  //
  if ( left.size()==0 )
  {
    return result;
  }
  //
  // case 1: two small arrays => extract without sorting  
  //
  if ( left.size()<SMALL_ATOM_ARRAY_THRESHOLD && right.size()<SMALL_ATOM_ARRAY_THRESHOLD )
  {
    int size_right=right.size();
    int *__restrict__ p_result=&result[0];
    Atom<CoordinatePrecision> *start_left=&left[0];
    Atom<CoordinatePrecision> *start_right=&right[0];
    const Atom<CoordinatePrecision> *end_left=&left[0]+left.size();
    const Atom<CoordinatePrecision> *end_right=&right[0]+right.size();
    //
    // loop on the lists
    //
    for ( Atom<CoordinatePrecision> *__restrict__ i=start_left ; i!=end_left ; i++ )
    {
      register int n=0;
      //
      for ( Atom<CoordinatePrecision> *__restrict__ j=start_right ; j!=end_right ; j++ , n++ )
      {
        if ( (*i)==(*j) ) { (*p_result)=n; p_result++;  break; }
      }
      //
      if ( n>=size_right )
      {
        cout << "Error: cannot find atom " << (*i) << " during left_atom_list<=right_atom_list indices extraction" << endl;
        exit(0);
      }
    }
  }
  //
  // case 2: at least one large array => sort and intersect 
  //
  else
  {
    int size_left=left.size();
    int size_right=right.size();
    //
    // get a copy of the vectors
    //
    vector<Atom<CoordinatePrecision> > left_=left;
    vector<Atom<CoordinatePrecision> > right_=right;
    vector<int> trans_l(left.size());
    vector<int> trans_r(right.size());
    for ( int i=0 ; i<size_left  ; i++ ) trans_l[i]=i;
    for ( int i=0 ; i<size_right ; i++ ) trans_r[i]=i;
    //
    // sort the indices by cressent order
    //
    if ( left_.size()>0 )  sort<Atom<CoordinatePrecision> >( &left_[0], (int)left_.size(), &trans_l[0] );
    if ( right_.size()>0 ) sort<Atom<CoordinatePrecision> >( &right_[0], (int)right_.size(), &trans_r[0] );
    //
    // intersect
    //
    int *__restrict__ p_result=&result[0];
    Atom<CoordinatePrecision> *il=&left_[0];
    Atom<CoordinatePrecision> *ir=&right_[0];
    int *tl=&trans_l[0];
    int *tr=&trans_r[0];
    const Atom<CoordinatePrecision> *end_left=&left_[0]+left_.size();
    const Atom<CoordinatePrecision> *end_right=&right_[0]+right_.size();
    //
    while ( il<end_left && ir<end_right )
    {
      
      while ( ir<end_right && !((*ir)==(*il)) ) { ir++; tr++; }
      //
      if ( ir<end_right && (*il)==(*ir) ) 
      {
        (*(p_result+(*tl)))=(*tr);
        il++;
        ir++;
        tl++;
        tr++;
      }
      else
      {
        cout << "Error: cannot find atom " << (*il) << " during left_atom_list<=right_atom_list indices extraction" << endl;
        exit(0);
      }
    }
  }
  //
  // return the list
  //
  return result;
}

#endif

/*! \file Export2Povray.h
    \brief Defines export funtion templates for Povray rendering
*/

#ifndef EXPORT2POVRAY_H
#define EXPORT2POVRAY_H

#include "Box.h"
#include "Atom.h"

//! \brief export atoms to a povray file
/// \param fileName file for output
/// \param atom_list list of atoms to be exported
/// \param bond_cutoff max bond length for bond representation 
/// \param box box containing the atoms to be printed (all atoms are printed if not initialized)
/// \param clip_box list of clipping boxes applied to the representation
 
template <class Precision>
void atom2POV(string fileName, vector<Atom<Precision> > atom_list, Precision bond_cutoff, Box<Precision> box, vector<Box<Precision> > clip_box)
{
  //
  // open the file
  //
  ofstream filestr;
  filestr.open (fileName.data());
  //
  // write some info
  //
  filestr << "// atom definition generated by atom2POV" << endl;
  
  
  //
  // init material and size lists with the different species in the list
  //
  map<string,string> atom_materials;
  map<string,string> atom_sizes;
  //
  for ( int iat=0 ; iat<atom_list.size() ; iat++ ) 
  {
    atom_materials[atom_list[iat].name]="";
    atom_sizes[atom_list[iat].name]="";
  }
  
  
  //
  // create the list of materials
  //
  for ( map<string,string>::iterator it=atom_materials.begin() ; it!=atom_materials.end() ; it++ )
  {
    //
    // create material name
    //
    char buff[256];
    sprintf(buff,"texture_%s",it->first.data());
    //
    // keep material name
    //
    it->second=buff; 
    //
    // create material
    //
    filestr << endl << "// create texture for atom" << it->first << endl;
    filestr << "#declare " << buff 
            << " = texture { finish { ambient 1.0 diffuse 0.0 phong 0.0 specular 0.0 } pigment { rgbt<0.7,0.7,0.7> } }" << endl;            
  }
  filestr << endl;
  filestr << endl;
  
  
  //
  // create the list of sizes
  //
  for ( map<string,string>::iterator it=atom_sizes.begin() ; it!=atom_sizes.end() ; it++ )
  {
    //
    // create material name
    //
    char buff[256];
    sprintf(buff,"SIZE_ATOM_%s",it->first.data());
    //
    // keep material name
    //
    it->second=buff; 
    //
    // create size
    //
    filestr << endl << "// define size for atom" << it->first << endl;
    filestr << "#declare " << buff << " = 0.3" << endl;            
  }
  filestr << endl;
  filestr << endl;
  
  
  //
  // define size for the bonds
  //
  filestr << endl << "// define size for the bonds" << endl;
  filestr << "#declare SIZE_BONDS = 0.2" << endl;  
  filestr << endl;
  filestr << endl;
  
  
  //
  // test which atom should be displayed 
  //
  vector<bool> print_atom;
  print_atom.reserve(atom_list.size());
  //
  // if box is not initialized, display all
  //
  if ( box.lx()==0 || box.ly()==0 || box.lz()==0 )
  {
    print_atom.resize(atom_list.size(),true);
  }
  //
  // otherwise test each atom
  //
  else
  {
    for ( int iatom=0 , natoms=atom_list.size() , nclip=clip_box.size() ; iatom<natoms ; iatom++ )
    {
      //
      // test first that the atom is within the box
      //
      bool print_this_atom = box.hasPoint( atom_list[iatom].coordinates() );
      //
      // apply clipping if any
      //
      for ( int iclip=0 ; iclip<nclip ; iclip++ )
      { 
        print_this_atom = ( print_this_atom && !clip_box[iclip].hasPoint( atom_list[iatom].coordinates() ) );
      }
      //
      // keep printing value
      //
      print_atom.push_back(print_this_atom);
    }
  }
  
  
  //
  // print the atoms
  //
  filestr << endl << "// print the atoms" << endl;
  for ( int iat=0 ; iat<atom_list.size() ; iat++ )
  {
    if ( print_atom[iat] )
    {
      filestr << "sphere { <" << atom_list[iat].x_pos << "," << atom_list[iat].y_pos << "," << atom_list[iat].z_pos 
              << "> , " << atom_sizes[atom_list[iat].name] << " texture { " << atom_materials[atom_list[iat].name] 
              << " } no_shadow }" << endl; 
    }
  }  
  filestr << endl;
  filestr << endl;  
  
  
  //
  // print the bonds
  //
  filestr << endl << "// print the bonds" << endl;
  if ( bond_cutoff>0.0 )
  {
    Precision bond_cutoff_sqr=bond_cutoff*bond_cutoff;
    //
    // determine the box if nothing has been specified
    //
    Precision xmin= 1E10;
    Precision xmax=-1E10;
    Precision ymin= 1E10;
    Precision ymax=-1E10;
    Precision zmin= 1E10;
    Precision zmax=-1E10;
    //
    if ( box.xmin() || box.xmax() || box.ymin() || box.ymax() || box.zmin() || box.zmax() )
    {
      xmin=box.xmin();
      xmax=box.xmax();
      ymin=box.ymin();
      ymax=box.ymax();
      zmin=box.zmin();
      zmax=box.zmax();
    }
    else
    {
      for ( int iat=0 ; iat<atom_list.size() ; iat++ )
      {
        Atom<Precision> &atom=atom_list[iat];
        //
        if ( atom.x_pos>xmax ) xmax=atom.x_pos;
        if ( atom.x_pos<xmin ) xmin=atom.x_pos;
        if ( atom.y_pos>ymax ) ymax=atom.y_pos;
        if ( atom.y_pos<ymin ) ymin=atom.y_pos;
        if ( atom.z_pos>zmax ) zmax=atom.z_pos;
        if ( atom.z_pos<zmin ) zmin=atom.z_pos;
      }
    }
    //
    // divide into subdomains of size >=cutoff
    //
    int Nx=max( 1 , (int) ((xmax-xmin)/(bond_cutoff)) );
    int Ny=max( 1 , (int) ((ymax-ymin)/(bond_cutoff)) );
    int Nz=max( 1 , (int) ((zmax-zmin)/(bond_cutoff)) );
    //
    Precision lx=(xmax-xmin)/Nx;
    Precision ly=(ymax-ymin)/Ny;
    Precision lz=(zmax-zmin)/Nz;
    
    
    //
    // loop on x coordinates
    //
    for ( int ix=0 ; ix<Nx ; ix++ )
    {
      Precision x_min_at1=xmin+ix*lx;
      Precision x_max_at1=xmin+ix*lx+lx;
      Precision x_min_at2=xmin+ix*lx-bond_cutoff;
      Precision x_max_at2=xmin+ix*lx+bond_cutoff+lx;
      //
      vector<Atom<Precision> > local_atom1_x;
      vector<Atom<Precision> > local_atom2_x;
      //
      // construct first the list of atoms potentially bonding with this region
      //
      local_atom2_x.reserve(atom_list.size());
      for ( int iat=0 , stop=atom_list.size() ; iat<stop ; iat++ )
      {
        Atom<Precision> &atom=atom_list[iat];
        if ( print_atom[iat] && x_min_at2 <= atom.x_pos && atom.x_pos <= x_max_at2 ) local_atom2_x.push_back(atom);
      }
      //
      // construct then the list of atoms in the region
      //
      local_atom1_x.reserve(local_atom2_x.size());
      for ( int iat=0 , stop=local_atom2_x.size() ; iat<stop ; iat++ )
      {
        Atom<Precision> &atom=local_atom2_x[iat];
        if ( x_min_at1 <= atom.x_pos && atom.x_pos <= x_max_at1 ) local_atom1_x.push_back(atom);
      }
      
      
      //
      // loop on y coordinate
      //
      for ( int iy=0 ; iy<Ny ; iy++ )
      {
        Precision y_min_at1=ymin+iy*ly;
        Precision y_max_at1=ymin+iy*ly+ly;
        Precision y_min_at2=ymin+iy*ly-bond_cutoff;
        Precision y_max_at2=ymin+iy*ly+bond_cutoff+ly;
        //
        vector<Atom<Precision> > local_atom1_y;
        vector<Atom<Precision> > local_atom2_y;
        //
        // construct first the list of atoms potentially bonding with this region
        //
        local_atom2_y.reserve(local_atom2_x.size());
        for ( int iat=0 , stop=local_atom2_x.size() ; iat<stop ; iat++ )
        {
          Atom<Precision> &atom=local_atom2_x[iat];
          if ( y_min_at2 <= atom.y_pos && atom.y_pos <= y_max_at2 ) local_atom2_y.push_back(atom);
        }
        //
        // construct then the list of atoms in the region
        //
        local_atom1_y.reserve(local_atom1_x.size());
        for ( int iat=0 , stop=local_atom1_x.size() ; iat<stop ; iat++ )
        {
          Atom<Precision> &atom=local_atom1_x[iat];
          if ( y_min_at1 <= atom.y_pos && atom.y_pos <= y_max_at1 ) local_atom1_y.push_back(atom);
        }
        
        
        //
        // loop on z coordinate
        //
        for ( int iz=0 ; iz<Nz ; iz++ )
        {
          Precision z_min_at1=zmin+iz*lz;
          Precision z_max_at1=zmin+iz*lz+lz;
          Precision z_min_at2=zmin+iz*lz-bond_cutoff;
          Precision z_max_at2=zmin+iz*lz+bond_cutoff+lz;
          //
          vector<Atom<Precision> > local_atom1_z;
          vector<Atom<Precision> > local_atom2_z;
          //
          // construct first the list of atoms potentially bonding with this region
          //
          local_atom2_z.reserve(local_atom2_y.size());
          for ( int iat=0 , stop=local_atom2_y.size() ; iat<stop ; iat++ )
          {
            Atom<Precision> &atom=local_atom2_y[iat];
            if ( z_min_at2 <= atom.z_pos && atom.z_pos <= z_max_at2 ) local_atom2_z.push_back(atom);
          }
          //
          // construct then the list of atoms in the region
          //
          local_atom1_z.reserve(local_atom1_y.size());
          for ( int iat=0 , stop=local_atom1_y.size() ; iat<stop ; iat++ )
          {
            Atom<Precision> &atom=local_atom1_y[iat];
            if ( z_min_at1 <= atom.z_pos && atom.z_pos <= z_max_at1 ) local_atom1_z.push_back(atom);
          }
          //
          // we have now the list of local atoms and their potential neighbors
          //
          for ( int iat1=0 , stop1=local_atom1_z.size() ; iat1<stop1 ; iat1++ )
          for ( int iat2=0 , stop2=local_atom2_z.size() ; iat2<stop2 ; iat2++ )
          {
            //
            // get reference on the two atoms
            //
            Atom<Precision> &atom1=local_atom1_z[iat1];
            Atom<Precision> &atom2=local_atom2_z[iat2];
            //
            // compute bond 
            //
            Vector3D<Precision> coord1=atom1.coordinates();
            Vector3D<Precision> coord2=atom2.coordinates();
            Vector3D<Precision> bond=coord2-coord1;
            //
            // if the two atoms are neighbor
            //
            Precision dist=bond*bond;
            //
            if ( dist<=bond_cutoff_sqr && dist>0.0 )
            {
              Vector3D<Precision> center=(coord2+coord1)/2.0;
              //
              filestr << "cylinder { <" << coord1[0] << "," << coord1[1] << "," << coord1[2] << "> , <" 
                      << center[0] << "," << center[1] << "," << center[2] << "> , " << "SIZE_BONDS" 
                      << " open texture {" << atom_materials[atom1.name] << "} no_shadow }" << endl;
              filestr << "cylinder { <" << coord2[0] << "," << coord2[1] << "," << coord2[2] << "> , <" 
                      << center[0] << "," << center[1] << "," << center[2] << "> , " << "SIZE_BONDS"
                      << " open texture {" << atom_materials[atom2.name] << "} no_shadow }" << endl;        
            }
          }
        }
      }
    }
  }
  //
  // close the file
  //
  filestr.close(); 
}



//! \brief export flux to a povray df3 density file
/// \param fileName file for output
/// \param flux list of flux to be exported
/// \param box box containing the flux to be exported (full range if not initialized)
/// \param clip_box list of clipping boxes applied to the representation
/// \param nx number of points for x direction
/// \param ny number of points for y direction
/// \param nz number of points for z direction
/// \param cutoff cutoff distance for flux computation
/// \param alpha width of gaussian for flux density representation
/// \param projection direction of projection for flux representation

template <class Precision>
void printFluxDensityGrid(string fileName, vector<Flux<Atom<Precision> > > &flux, Box<Precision> box, vector<Box<Precision> > clip_box, int nx, int ny, int nz, Precision cutoff, Precision alpha, Vector3D<Precision> projection=Vector3D<Precision>(0.0,0.0,0.0))
{
 
#define BLOC_SIZE 16        
#define INTERNAL_PRECISION float
#define INTERNAL_EPSILON 1E-4

  //
  // determine the box
  //
  INTERNAL_PRECISION xmin= 1E10;
  INTERNAL_PRECISION xmax=-1E10;
  INTERNAL_PRECISION ymin= 1E10;
  INTERNAL_PRECISION ymax=-1E10;
  INTERNAL_PRECISION zmin= 1E10;
  INTERNAL_PRECISION zmax=-1E10;
  //
  if ( box.xmin() || box.xmax() || box.ymin() || box.ymax() || box.zmin() || box.zmax() )
  {
    xmin=box.xmin();
    xmax=box.xmax();
    ymin=box.ymin();
    ymax=box.ymax();
    zmin=box.zmin();
    zmax=box.zmax();
  }
  else
  {
    for ( int iflux=0 ; iflux<flux.size() ; iflux++ )
    {
      Atom<double> &atom_i1=flux[iflux].fromObject;
      Atom<double> &atom_i2=flux[iflux].toObject;
      //
      if ( atom_i1.x_pos>xmax ) xmax=atom_i1.x_pos;
      if ( atom_i1.x_pos<xmin ) xmin=atom_i1.x_pos;
      if ( atom_i1.y_pos>ymax ) ymax=atom_i1.y_pos;
      if ( atom_i1.y_pos<ymin ) ymin=atom_i1.y_pos;
      if ( atom_i1.z_pos>zmax ) zmax=atom_i1.z_pos;
      if ( atom_i1.z_pos<zmin ) zmin=atom_i1.z_pos;
      //
      if ( atom_i2.x_pos>xmax ) xmax=atom_i2.x_pos;
      if ( atom_i2.x_pos<xmin ) xmin=atom_i2.x_pos;
      if ( atom_i2.y_pos>ymax ) ymax=atom_i2.y_pos;
      if ( atom_i2.y_pos<ymin ) ymin=atom_i2.y_pos;
      if ( atom_i2.z_pos>zmax ) zmax=atom_i2.z_pos;
      if ( atom_i2.z_pos<zmin ) zmin=atom_i2.z_pos;
    }
  }
  
  //
  // define grid spacing
  //
  INTERNAL_PRECISION dx=(xmax-xmin)/(nx-1);
  INTERNAL_PRECISION dy=(ymax-ymin)/(ny-1);
  INTERNAL_PRECISION dz=(zmax-zmin)/(nz-1);  
  //
  // divide into subdomains of size >=cutoff
  //
  int Nx=max( 1 , (int) ((xmax-xmin)/(cutoff)) );
  int Ny=max( 1 , (int) ((ymax-ymin)/(cutoff)) );
  int Nz=max( 1 , (int) ((zmax-zmin)/(cutoff)) );
  //
  // get size of subdomains
  //
  INTERNAL_PRECISION lx=(xmax-xmin+dx)/Nx;
  INTERNAL_PRECISION ly=(ymax-ymin+dy)/Ny;
  INTERNAL_PRECISION lz=(zmax-zmin+dz)/Nz;

cout << "Nx  =" << Nx << endl; 
cout << "Ny  =" << Ny << endl; 
cout << "Nz  =" << Nz << endl; 
cout << "lx  =" << lx << endl; 
cout << "ly  =" << ly << endl; 
cout << "lz  =" << lz << endl; 
cout << "dx  =" << dx << endl;
cout << "dy  =" << dy << endl;
cout << "dz  =" << dz << endl;
cout << "xmin=" << xmin << endl;
cout << "ymin=" << ymin << endl;
cout << "zmin=" << zmin << endl;
cout << "xmax=" << xmax << endl;
cout << "ymax=" << ymax << endl;
cout << "zmax=" << zmax << endl;

  //
  // precompute integral
  //
  int  n_step_x=2048;
  INTERNAL_PRECISION srq_cutoff=cutoff*cutoff;
  INTERNAL_PRECISION one_over_sqrt_pi=0.564189583547756;
  INTERNAL_PRECISION sqrt_alpha=sqrt(alpha);
  INTERNAL_PRECISION step_x=2.0*cutoff/n_step_x*alpha;
  INTERNAL_PRECISION step_x_times_alpha=step_x*alpha;
  INTERNAL_PRECISION one_over_step_x_times_alpha=1.0/step_x_times_alpha;
  INTERNAL_PRECISION minus_one_over_step_x_times_alpha=-1.0/step_x_times_alpha;
  INTERNAL_PRECISION x_max_in_precomp_integral=2.0*cutoff*alpha;
  INTERNAL_PRECISION minus_x_max_in_precomp_integral=-2.0*cutoff*alpha;
  
  INTERNAL_PRECISION one_over_alpha_sqr=1.0/(alpha*alpha);
  vector<INTERNAL_PRECISION> precomp_integral(n_step_x+1,0.0);
  vector<INTERNAL_PRECISION> precomp_value(n_step_x+1);
  for ( int i=0 ; i<n_step_x+1 ; i++ ) precomp_value[i]=one_over_sqrt_pi*exp(-i*i*step_x*step_x);
  for ( int i=1 ; i<n_step_x+1 ; i++ ) precomp_integral[i]=precomp_integral[i-1]+precomp_value[i]*step_x;
  
  int n_step_h_sqr=2048;
  INTERNAL_PRECISION step_h_sqr=srq_cutoff/n_step_h_sqr;
  INTERNAL_PRECISION one_over_step_h_sqr=1.0/step_h_sqr;
  vector<INTERNAL_PRECISION> precomp_exponential(n_step_h_sqr+1);
  for ( int i=0 ; i<n_step_h_sqr+1 ; i++ ) precomp_exponential[i]=exp(-i*step_h_sqr*one_over_alpha_sqr);
   
  
  
  
  //
  // inform
  //
  cout << "precomp of gaussian integrals ok" << endl;
  
  
  //
  // loop on the subdomains
  //
  vector<Vector3D<INTERNAL_PRECISION> > density(nx*ny*nz);
  //
  for ( int ix=0 , it=0 ; ix<Nx ; ix++ )
  {
    INTERNAL_PRECISION x_min_at=xmin+ix*lx-cutoff;
    INTERNAL_PRECISION x_max_at=xmin+ix*lx+cutoff+lx;
    //
    vector<Flux<Atom<double> > > local_flux_x;
    local_flux_x.reserve(flux.size());
    for ( int iflux=0 ; iflux<flux.size() ; iflux++ ) 
    {
      Atom<double> &atom_i1=flux[iflux].fromObject;
      Atom<double> &atom_i2=flux[iflux].toObject;
      //
      // keep flux if one of the atoms is in the box 
      // or is the atom surround the box
      //
      if      ( x_min_at <= atom_i1.x_pos && atom_i1.x_pos <= x_max_at ) local_flux_x.push_back(flux[iflux]);
      else if ( x_min_at <= atom_i2.x_pos && atom_i2.x_pos <= x_max_at ) local_flux_x.push_back(flux[iflux]);
      else if ( x_min_at >= atom_i1.x_pos && atom_i2.x_pos >= x_max_at ) local_flux_x.push_back(flux[iflux]);
      else if ( x_min_at >= atom_i2.x_pos && atom_i1.x_pos >= x_max_at ) local_flux_x.push_back(flux[iflux]);
    }
    
    
    //
    // loop on y coordinate
    //
    for ( int iy=0 ; iy<Ny ; iy++ )
    {
      INTERNAL_PRECISION y_min_at=ymin+iy*ly-cutoff;
      INTERNAL_PRECISION y_max_at=ymin+iy*ly+cutoff+ly;
      //
      vector<Flux<Atom<double> > > local_flux_y;
      local_flux_y.reserve(local_flux_x.size());
      for ( int iflux=0 ; iflux<local_flux_x.size() ; iflux++ ) 
      {
        Atom<double> &atom_i1=local_flux_x[iflux].fromObject;
        Atom<double> &atom_i2=local_flux_x[iflux].toObject;
        //
        // keep flux if one of the atoms is in the box 
        // or is the atom surround the box
        //
        if      ( y_min_at <= atom_i1.y_pos && atom_i1.y_pos <= y_max_at ) local_flux_y.push_back(local_flux_x[iflux]);
        else if ( y_min_at <= atom_i2.y_pos && atom_i2.y_pos <= y_max_at ) local_flux_y.push_back(local_flux_x[iflux]);
        else if ( y_min_at >= atom_i1.y_pos && atom_i2.y_pos >= y_max_at ) local_flux_y.push_back(local_flux_x[iflux]);
        else if ( y_min_at >= atom_i2.y_pos && atom_i1.y_pos >= y_max_at ) local_flux_y.push_back(local_flux_x[iflux]);
      }
      
      
      //
      // loop on z coordinate
      //
      for ( int iz=0 ; iz<Nz ; iz++ )
      {
        INTERNAL_PRECISION z_min_at=zmin+iz*lz-cutoff;
        INTERNAL_PRECISION z_max_at=zmin+iz*lz+cutoff+lz;
        //
        vector<Flux<Atom<double> > > local_flux_z;
        local_flux_z.reserve(local_flux_y.size());
        for ( int iflux=0 ; iflux<local_flux_y.size() ; iflux++ ) 
        {
          Atom<double> &atom_i1=local_flux_y[iflux].fromObject;
          Atom<double> &atom_i2=local_flux_y[iflux].toObject;
          //
          // keep flux if one of the atoms is in the box 
          // or if the atoms surround the box
          //
          if      ( z_min_at <= atom_i1.z_pos && atom_i1.z_pos <= z_max_at ) local_flux_z.push_back(local_flux_y[iflux]);
          else if ( z_min_at <= atom_i2.z_pos && atom_i2.z_pos <= z_max_at ) local_flux_z.push_back(local_flux_y[iflux]);
          else if ( z_min_at >= atom_i1.z_pos && atom_i2.z_pos >= z_max_at ) local_flux_z.push_back(local_flux_y[iflux]);
          else if ( z_min_at >= atom_i2.z_pos && atom_i1.z_pos >= z_max_at ) local_flux_z.push_back(local_flux_y[iflux]);
        }
        //
        // now we have a list of restricted flux => loop on the local grid points 
        //
        INTERNAL_PRECISION local_minx=xmin+ix*lx-INTERNAL_EPSILON;
        INTERNAL_PRECISION local_maxx=xmin+ix*lx+lx-INTERNAL_EPSILON;
        INTERNAL_PRECISION x_start=xmin; int ix_start=0; while ( x_start<local_minx ) { ix_start++; x_start+=dx; }
        //
        INTERNAL_PRECISION local_miny=ymin+iy*ly-INTERNAL_EPSILON;
        INTERNAL_PRECISION local_maxy=ymin+iy*ly+ly-INTERNAL_EPSILON;
        INTERNAL_PRECISION y_start=ymin; int iy_start=0; while ( y_start<local_miny ) { iy_start++; y_start+=dy; }
        //
        INTERNAL_PRECISION local_minz=zmin+iz*lz-INTERNAL_EPSILON;
        INTERNAL_PRECISION local_maxz=zmin+iz*lz+lz-INTERNAL_EPSILON;
        INTERNAL_PRECISION z_start=zmin; int iz_start=0; while ( z_start<local_minz ) { iz_start++; z_start+=dz; }
        //
        // increase upper boundaries if we are on the last chunk to avoid missing last point
        //
        if ( ix==Nx-1 ) local_maxx+=dx;
        if ( iy==Ny-1 ) local_maxy+=dy; 
        if ( iz==Nz-1 ) local_maxz+=dz; 
        //
        INTERNAL_PRECISION x,y,z;
        int                i,j,k;
        
        
        //
        // print info
        //

        if ( it==100 )
        {
          cout << " stage ";
          cout.width(4); cout << ix+1 << " ";
          cout.width(4); cout << iy+1 << " ";
          cout.width(4); cout << iz+1 << "    of ";
          cout.width(4); cout << Nx << " ";
          cout.width(4); cout << Ny << " ";
          cout.width(4); cout << Nz << "         nflux=" << local_flux_z.size() << endl;
          it=0;
        }
        it++;
        //
        // bloc the algo to avoid cache miss on density[index]
        // => work on part of density of dim BLOC_SIZE³
        //
        int ibloc=1;
        int ix_start_;
        int iy_start_;
        int iz_start_;
        INTERNAL_PRECISION x_start_;
        INTERNAL_PRECISION y_start_;
        INTERNAL_PRECISION z_start_;    
        //    
        for ( ix_start_=ix_start , x_start_=x_start ; x_start_<local_maxx ; ix_start_+=BLOC_SIZE , x_start_+=BLOC_SIZE*dx )
        for ( iy_start_=iy_start , y_start_=y_start ; y_start_<local_maxy ; iy_start_+=BLOC_SIZE , y_start_+=BLOC_SIZE*dy )
        for ( iz_start_=iz_start , z_start_=z_start ; z_start_<local_maxz ; iz_start_+=BLOC_SIZE , z_start_+=BLOC_SIZE*dz )
        {  
          //
          // compute this bloc stops (substract a small epsilon to avoid floating point comparison problem) 
          //
          INTERNAL_PRECISION local_maxx_=min(local_maxx,x_start_+BLOC_SIZE*dx-(INTERNAL_PRECISION)(INTERNAL_EPSILON));
          INTERNAL_PRECISION local_maxy_=min(local_maxy,y_start_+BLOC_SIZE*dy-(INTERNAL_PRECISION)(INTERNAL_EPSILON));
          INTERNAL_PRECISION local_maxz_=min(local_maxz,z_start_+BLOC_SIZE*dz-(INTERNAL_PRECISION)(INTERNAL_EPSILON));
          //
          // check that the stops are ok
          //
          for ( i=ix_start_ , x=x_start_ ; x<local_maxx_ ; i++ , x+=dx );
          while  ( i>nx ) { local_maxx_-=dx; i--; }
          //   
          for ( j=iy_start_ , y=y_start_ ; y<local_maxy_ ; j++ , y+=dy );
          while  ( j>ny ) { local_maxy_-=dy; j--; }
          //
          for ( k=iz_start_ , z=z_start_ ; z<local_maxz_ ; k++ , z+=dz );
          while  ( k>nz ) { local_maxz_-=dz; k--; }
          
          //
          // loop on the flux close to this area  
          //
          for ( int iflux=0 ; iflux<local_flux_z.size() ; iflux++ ) 
          {        
            Vector3D<INTERNAL_PRECISION> atom1_pos(local_flux_z[iflux].fromObject.x_pos,
                                                   local_flux_z[iflux].fromObject.y_pos,
                                                   local_flux_z[iflux].fromObject.z_pos);
            Vector3D<INTERNAL_PRECISION> atom2_pos(local_flux_z[iflux].toObject.x_pos,
                                                   local_flux_z[iflux].toObject.y_pos,
                                                   local_flux_z[iflux].toObject.z_pos);
            //
            // get the vector difference and direction
            //
            Vector3D<INTERNAL_PRECISION> base=atom2_pos-atom1_pos;
            INTERNAL_PRECISION           distance=sqrt(base*base);
            Vector3D<INTERNAL_PRECISION> normed_base=base/distance;
            //
            // get value for this flux
            //
            Vector3D<INTERNAL_PRECISION> value=normed_base * local_flux_z[iflux].value;
          
            
            INTERNAL_PRECISION &nbx=normed_base[0];
            INTERNAL_PRECISION &nby=normed_base[1];
            INTERNAL_PRECISION &nbz=normed_base[2];
            
            INTERNAL_PRECISION &at1x=atom1_pos[0];
            INTERNAL_PRECISION &at1y=atom1_pos[1];
            INTERNAL_PRECISION &at1z=atom1_pos[2];
            
            INTERNAL_PRECISION &at2x=atom2_pos[0];
            INTERNAL_PRECISION &at2y=atom2_pos[1];
            INTERNAL_PRECISION &at2z=atom2_pos[2];
            
            
            //
            // loop on the points in this subdomain
            //          
            for ( i=ix_start_ , x=x_start_ ; x<local_maxx_ ; i++ , x+=dx )
            for ( j=iy_start_ , y=y_start_ ; y<local_maxy_ ; j++ , y+=dy )
            for ( k=iz_start_ , z=z_start_ ; z<local_maxz_ ; k++ , z+=dz )
            {
              //
              // compute first the height
              //
              /*
              Vector3D<double> center(x,y,z);         
              Vector3D<double> dir1   = center-atom1_pos;
              double           x1     = normed_base*dir1;
              Vector3D<double> hvec   = dir1-normed_base*x1;
              double           h_sqr  = hvec*hvec;
              */
              INTERNAL_PRECISION dirx=(x-at1x);
              INTERNAL_PRECISION diry=(y-at1y);
              INTERNAL_PRECISION dirz=(z-at1z);
              INTERNAL_PRECISION x1=nbx*dirx+nby*diry+nbz*dirz;
              INTERNAL_PRECISION hvecx=dirx-nbx*x1;
              INTERNAL_PRECISION hvecy=diry-nby*x1;
              INTERNAL_PRECISION hvecz=dirz-nbz*x1;
              INTERNAL_PRECISION h_sqr=hvecx*hvecx+hvecy*hvecy+hvecz*hvecz;
              //
              // if the height is small enough
              //
              if ( h_sqr<srq_cutoff )
              {
                //
                // get the corresponding index 
                //
                int index=i+j*nx+k*nx*ny;
                //
                // compute h, l1 and l2
                // 
                /*
                Vector3D<double> dir2 = center-atom2_pos;
                double           x2   = normed_base*dir2;
                */  
                dirx=(x-at2x);
                diry=(y-at2y);
                dirz=(z-at2z);
                INTERNAL_PRECISION x2=nbx*dirx+nby*diry+nbz*dirz;
                
                // 
                // estimate the integral
                // those have been calculated in advance to gain some time
                //
                /*
                INTERNAL_PRECISION prefactor = exp(-h_sqr*one_over_alpha_sqr);
                */
                INTERNAL_PRECISION prefactor = precomp_exponential[h_sqr*one_over_step_h_sqr];
                
                //
                // the portion below is an optimized version of the following
                // keep the old code for comprehension...
                //
                /*
                int                ix1       = min((int)(fabs(x1)*one_over_step_x_times_alpha),n_step_x);
                int                ix2       = min((int)(fabs(x2)*one_over_step_x_times_alpha),n_step_x);
                INTERNAL_PRECISION sign1     = (x1<0.0) ? -1.0 : 1.0;
                INTERNAL_PRECISION sign2     = (x2<0.0) ? -1.0 : 1.0;
                INTERNAL_PRECISION integral  = sign1*precomp_integral[ix1]-sign2*precomp_integral[ix2];
                */
                INTERNAL_PRECISION integral;
                if ( x1<0 )
                {
                  if ( x1<minus_x_max_in_precomp_integral )
                  {
                    integral=-precomp_integral[n_step_x];
                  }
                  else
                  {
                    integral=-precomp_integral[ (int)(x1*minus_one_over_step_x_times_alpha) ];
                  }
                }
                else
                {
                  if ( x1>x_max_in_precomp_integral )
                  {
                    integral=precomp_integral[n_step_x];
                  }
                  else
                  {
                    integral=precomp_integral[ (int)(x1*one_over_step_x_times_alpha) ];
                  }
                }
                if ( x2<0 )
                {
                  if ( x2<minus_x_max_in_precomp_integral )
                  {
                    integral+=precomp_integral[n_step_x];
                  }
                  else
                  {
                    integral+=precomp_integral[ (int)(x2*minus_one_over_step_x_times_alpha) ];
                  }
                }
                else
                {
                  if ( x2>x_max_in_precomp_integral )
                  {
                    integral-=precomp_integral[n_step_x];
                  }
                  else
                  {
                    integral-=precomp_integral[ (int)(x2*one_over_step_x_times_alpha) ];
                  }
                }
                
                // 
                // add contribution to the density
                //
                density[index]+=value*(prefactor*integral);
              }
            }
          }
        }
      } 
    }
  }
  //
  // gat max and min value of the density
  //
  INTERNAL_PRECISION dmax=0.0;
  INTERNAL_PRECISION dmin=1E+10;
  for ( int i=0 ; i<density.size() ; i++ )
  {
    INTERNAL_PRECISION value=density[i]*density[i];
    if ( dmax<value ) dmax=value;
    if ( dmin>value ) dmin=value;
  }
  dmax=sqrt(dmax);
  dmin=sqrt(dmin);
  //
  // inform 
  //
  cout << "density min = " << dmin << endl;
  cout << "density max = " << dmax << endl;
  //
  // print density slices on standard output
  //
  for ( int i=0 , j=0 , k=0 ; i<density.size() ; i++ , j++ )
  {
    if ( j==nx ) 
    {
      cout << endl;
      j=0;
      k++;
    }
    if ( k==ny ) 
    {
      cout << endl;
      k=0;
    }
    INTERNAL_PRECISION value=sqrt(density[i]*density[i]);
    cout << (int)(9*value/dmax);
  }
  
  char buff[256];
  ofstream filestr;
  //
  // density map for povray
  //
  sprintf(buff,"%s.df3",fileName.data());
  filestr.open (buff);
  filestr << (unsigned char)(nx >> 8);
  filestr << (unsigned char)(nx & 0xff);
  filestr << (unsigned char)(ny >> 8);
  filestr << (unsigned char)(ny & 0xff);
  filestr << (unsigned char)(nz >> 8);
  filestr << (unsigned char)(nz & 0xff);
  for ( int i=0 ; i<density.size() ; i++ )
  {
    INTERNAL_PRECISION v=sqrt(density[i]*density[i]);
    int value=min(255,max(0,(int)(255*(v-dmin)/dmax)));
    // red component
    filestr << (unsigned char)value;
  }
  filestr.close();
  
  //
  // density vector field
  //
  sprintf(buff,"%s.vec",fileName.data());
  filestr.open (buff);
  filestr << "ngrid = " << nx << " " << ny << " " << nz << endl;
  filestr << "xmin  = " << xmin        << " , ymin = " << ymin      << " , zmin = " << zmin << endl;
  filestr << "Lx    = " << xmax-xmin   << " , Ly   = " << ymax-ymin << " , Lz   = " << zmax-zmin << endl;
  
  for ( int i=0 ; i<nx ; i++ )
  for ( int j=0 ; j<ny ; j++ )
  for ( int k=0 ; k<nz ; k++ )
  {
    filestr << i << " " << j << " " << k << " " << density[i+j*nx+k*nx*ny] << endl;
  }
  filestr.close();
}

#endif

#ifndef BASIS3D_H
#define BASIS3D_H

#include <stdlib.h>
#include <string>
#include <cstring>
#include <vector>
#include <math.h>
#include <iostream>
#include "Vector3D.h"

using namespace std;

template <class CoordinatePrecision>
class Basis3D
{
  private:
    
    // vectors defining the basis
    Vector3D<CoordinatePrecision> v1_;
    Vector3D<CoordinatePrecision> v2_;
    Vector3D<CoordinatePrecision> v3_;
    
    // dual vectors associated to normal vectors
    Vector3D<CoordinatePrecision> d1_;
    Vector3D<CoordinatePrecision> d2_;
    Vector3D<CoordinatePrecision> d3_;
     
  public:
  
    // constructor
    Basis3D( vector<Vector3D<CoordinatePrecision> > vector_list );
    
    // vector access
    Vector3D<CoordinatePrecision>& v1(void) { return v1_; }
    Vector3D<CoordinatePrecision>& v2(void) { return v2_; }
    Vector3D<CoordinatePrecision>& v3(void) { return v3_; }
    Vector3D<CoordinatePrecision>& v(int i);
    
    // method
    Vector3D<CoordinatePrecision>          getCoordinatesOf( Vector3D<CoordinatePrecision> vector );
    vector<Vector3D<CoordinatePrecision> > getDual(void);
    
    // destructor
    ~Basis3D() {}
};

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Basis3D<CoordinatePrecision>::v(int i)
{
  //
  // check index is correct
  // 
  if ( i<0 || i>2 )
  {
    cout << "Error: invalid vector index " << i << " in - Basis3D::v" << endl;
    exit(0);
  }
  //
  // return reference to the required vector
  //
  switch (i)
  {
    case 0:
      
      return v1_;
      
    case 1:
     
      return v2_;
      
    case 2:
    
      return v3_;
      
    default:
    
      cout << "Error: should not arrice there in - Basis3D::v" << endl;
      exit(0);    
  }
  return v1_;
}

// construction
template <class CoordinatePrecision>
Basis3D<CoordinatePrecision>::Basis3D( vector<Vector3D<CoordinatePrecision> > vector_list )
{
  //
  // check that we have at least one vectorfor construction and at most three
  //
  if ( vector_list.size()<1 || vector_list.size()>3 )
  {
    cout << "Error: incorrect number (" << vector_list.size() << ") of Vector3D in vector_list argument for constructor - Basis3D::Basis3D -" << endl;
    exit(0);
  }
  //
  // check the vectors passed for construction
  //
  for ( int i=0 ; i<vector_list.size() ; i++ )
  {
    //
    // check that this vector is different from 0
    //
    if ( vector_list[i][0]==0.0 && vector_list[i][1]==0.0 && vector_list[i][2]==0.0 )
    {
      cout << "Error: vector " << i << " is null in vector_list argument for constructor - Basis3D::Basis3D -" << endl;
      exit(0);
    }
    //
    // check that this vector is not colinear to the previous ones
    //
    for ( int j=0 ; j<i ; j++ )
    {
      CoordinatePrecision norme_i=sqrt(vector_list[i]*vector_list[i]);
      CoordinatePrecision norme_j=sqrt(vector_list[j]*vector_list[j]);
      CoordinatePrecision projection=vector_list[i]*vector_list[j];
      //
      if ( fabs(1.0-projection/norme_i/norme_j) < 1E-4 )
      {
        cout << "Error: vectors " << i << " = " << vector_list[i] << " and " << j << " = " << vector_list[j] << " are colinear or nearly colinear in vector_list argument for constructor - Basis3D::Basis3D -" << endl;
        exit(0);
      }
    }
  }
  //
  // construct basis in function of the number of vector given as argument
  //
  switch ( vector_list.size() )
  {
    case 1: 
      //
      // add 1 unit vector orthogonal with the first one
      //
      vector_list.resize(2);
      vector_list[1][0]= vector_list[0][1];
      vector_list[1][1]= vector_list[0][2];
      vector_list[1][2]=-vector_list[0][0];
      //
      vector_list[1] -= ((vector_list[1]*vector_list[0])/(vector_list[0]*vector_list[0]))*vector_list[0] ;
      vector_list[1] /= sqrt(vector_list[1]*vector_list[1]);
      
    case 2:
      //
      // add 1 unit vector orthogonal with the two others
      // 
      vector_list.resize(3);
      vector_list[2] = vector_list[0]^vector_list[1];
      vector_list[2] /= sqrt(vector_list[2]*vector_list[2]);
      
    default :
      //
      // nothing to do here
      //  
      break;
  }
  //
  // store the basis vectors
  //
  v1_=vector_list[0];
  v2_=vector_list[1];
  v3_=vector_list[2];
  //
  // compute the duals
  //
  d1_  = (v2_^v3_);
  d1_ /= (d1_*v1_);
  //
  d2_  = (v3_^v1_);
  d2_ /= (d2_*v2_);
  //
  d3_  = (v1_^v2_);
  d3_ /= (d3_*v3_);
}

template <class CoordinatePrecision>
vector<Vector3D<CoordinatePrecision> >  Basis3D<CoordinatePrecision>::getDual(void)
{
  //
  // create the list of dual vectors
  //
  vector<Vector3D<CoordinatePrecision> > vector_list(3);
  //
  vector_list[0]=d1_;
  vector_list[1]=d2_;
  vector_list[2]=d3_;
  //
  // return the list
  //
  return vector_list;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Basis3D<CoordinatePrecision>::getCoordinatesOf( Vector3D<CoordinatePrecision> vector )
{
  //
  // create the Vector3D of coordinates 
  //
  Vector3D<CoordinatePrecision> coordinates;
  //
  // compute coordinates
  //
  coordinates[0]=d1_*vector;
  coordinates[1]=d2_*vector;
  coordinates[2]=d3_*vector;
  //
  // return coordinates of vector in this basis
  //
  return coordinates;
}

#endif

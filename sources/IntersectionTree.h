#ifndef INTERSECTION_TREE_H
#define INTERSECTION_TREE_H

#include <ostream>
#include <string>
#include <vector>
#include "Part.h"
#include "Context.h"
#include "Subspace.h"

#define TIMING

using namespace std;

template <class Type>
class Leaf
{
  public:
    
    // constructor
    Leaf();

    // timers   
    TimerMap tmap_;
    
    // part / kernel atributes
    void   *part_ptr_;
    void   *subspace_ptr_;
    string leaf_type_;
    vector<int> domain_index_list_;
    vector<int> list_proc_domain_;
    
    // postition and connections in the tree
    int level;
    int child;
    int parent[2];    
    
    // dimension test
    int kernelDimension_;
    
    // attribute access
    vector<int> procDomain(void) { return list_proc_domain_; }
    
    // return the indices of leaves occupying the same processor
    vector<int> onSameProcs( vector<Leaf<Type> > &leaf_list, vector<int> &active_leaves);
    
    // return the leaf on free proc with the largest intersection
    int bestMatch( vector<Leaf<Type> > &leaf_list, vector<int> &active_leaves, vector<bool> &free_procs, int exept_index );
    
    // leaf execution
#ifndef TIMING
    void* getKernel( double energy );
    void* execute( vector<Leaf<Type> > &leaf_list_, double energy );
    int   getKernelDimension( double energy );
    int   testDimension( vector<Leaf<Type> > &leaf_list_, double energy );
#else
    void* getKernel( double energy , TimerMap &tmap);
    void* execute( vector<Leaf<Type> > &leaf_list_, double energy , TimerMap &tmap);
    int   getKernelDimension( double energy , TimerMap &tmap);
    int   testDimension( vector<Leaf<Type> > &leaf_list_, double energy , TimerMap &tmap);
#endif

    //destructor
    ~Leaf();
};

template <class Type>
class IntersectionTree
{
  private: 
    
    // timers   
    TimerMap tmap_;
    
    // context 
    Context& context_;
    int      my_proc_;
    int      n_procs_;
    
    // leaves and initial types of base leaves 
    vector<Leaf<Type> > leaf_list_;
    vector<string>      init_type_;
    
    // flag to indicate if the tree is branched
    bool branched_;
    
    // flag to indicate if the tree is reduced
    bool reduced_;
    
    // private methods
    void printLeaf_(int ileaf, string line_link, string line_empty);
    
  public:
    
    // constructor
    IntersectionTree(Context &context);
    
    // methods
    void  printRepartition(void);
    void  printTree(void);
    void  print(void);
    void  printDotGraph(ostream &str);
    void  printUtil_exploreTree(int itop_leaf, int icur_leaf, ostream &str, int level);
    void  branch(void);
    void  reset(void);
    void  initBotomLeaves(vector<void *> object);
    void  addLeaf( string leafType, vector<int> domain );
    void  addLeaf( int leaf1, int leaf2, vector<bool> &freeProc, int level);
    void  reduceDomains(vector<int> &finalDomain);
    void  setPartPtr( int ileaf , void *part ); 
    void* getPartPtr( int ileaf ); 
    void* execute( double energy );
    int   testDimension( double energy );
    bool  reduced(void) { return reduced_; }
    
    // access solution state indices
    vector<int> solutionStateIndices(void);
    
    // destructor
    ~IntersectionTree(void);
};


template <class Type>
void IntersectionTree<Type>::printUtil_exploreTree(int itop_leaf, int icur_leaf, ostream &str, int level)
{
  //
  // check if this leaf has parents
  //
  int parent1=leaf_list_[icur_leaf].parent[0];
  int parent2=leaf_list_[icur_leaf].parent[1];
  
  if ( parent1>=0 && parent2>=0 ) 
  {
    //
    // check if parents are disjoint groups of processors
    //
    vector<int> parent1_procs = leaf_list_[parent1].procDomain();
    vector<int> parent2_procs = leaf_list_[parent2].procDomain();
    vector<int> proc_union = parent1_procs || parent2_procs;
    //
    // if this is the case
    //
    if ( proc_union.size()>parent1_procs.size() && proc_union.size()>parent2_procs.size() && itop_leaf!=icur_leaf )
    {
      //
      // add a node
      //
      str << "node_" << itop_leaf << " -> node_" << icur_leaf << " [label=\"" << level << "\"]" << endl;
      str << "node_" << icur_leaf << " [label=\"node_" << icur_leaf << "\\n" << leaf_list_[icur_leaf].procDomain() << "\"]" << endl; 
      //
      // set current node as top node
      //
      level=0;
      itop_leaf=icur_leaf; 
    }
    //
    // continue exploring 
    // 
    level+=1;
    printUtil_exploreTree( itop_leaf, parent1, str, level);
    printUtil_exploreTree( itop_leaf, parent2, str, level);
  } 
  else
  {
    //
    // explore other parent genealogy
    //
    int ichild=leaf_list_[icur_leaf].child;
    //
    if ( ichild>=0 )
    {
      //
      // get other parent
      //
      int other_parent = leaf_list_[ichild].parent[0]==icur_leaf ? leaf_list_[ichild].parent[1] : leaf_list_[ichild].parent[0]; 
      //
      // get other parent procs
      //
      vector<int> curr_procs  = leaf_list_[icur_leaf].procDomain();
      vector<int> other_procs = leaf_list_[other_parent].procDomain();
      vector<int> proc_union  = curr_procs || other_procs; 
      // if parent come from different procs
      if ( proc_union.size()>curr_procs.size() && proc_union.size()>other_procs.size() )
      { 
        str << "node_" << itop_leaf << " -> node_" << icur_leaf << " [label=\"" << level << "\"]" << endl;
        str << "node_" << icur_leaf << " [label=\"node_" << icur_leaf << "\\n" << curr_procs << "\"]" << endl; 
      }
      //
      // otherwise, add a node only if both parents are base leaves
      //
      else 
      {
        int other_parent_parent1=leaf_list_[other_parent].parent[0];
        int other_parent_parent2=leaf_list_[other_parent].parent[1];
        //
        if ( other_parent_parent1<0 && other_parent_parent2<0 && icur_leaf<other_parent )
        {
          str << "node_" << itop_leaf << " -> node_" << icur_leaf << " [label=\"" << level << "\"]"  << endl;
          str << "node_" << icur_leaf << " [label=\"node_" << icur_leaf << "\\n" << curr_procs << "\"]" << endl; 
        }
      }
    }
  }
}

template <class Type>
void IntersectionTree<Type>::printDotGraph(ostream &str)
{
  if ( context_.myProc()==0 )
  {
    //
    // print header
    //
    str << "digraph G {" << endl;
    //
    // explore tree
    //  
    int itop_leaf=leaf_list_.size()-1;
    int icur_leaf=leaf_list_.size()-1;
    printUtil_exploreTree(itop_leaf, icur_leaf, str,0);
    //
    // print last stuffs
    //
    str << "}" << endl;
  }
}

template <class Type>
vector<int> IntersectionTree<Type>::solutionStateIndices(void)
{
  if ( !branched_ ) 
  {
    cout << "Error: trying to get solution state indices from at tree not branched yet" << endl;
    exit(0);
  }
  return leaf_list_.back().domain_index_list_;
}

// destructor
template <class Type>
IntersectionTree<Type>::~IntersectionTree(void)
{
#ifdef TIMING
  //
  // stop global timer
  //
  tmap_["total life time"].stop();
  //
  // print the different timers
  //
  tmap_.print(cout,"IntersectionTree:",this->context_);
#endif
}   


template <class Type>
#ifndef TIMING
void* Leaf<Type>::getKernel( double energy )
#else
void* Leaf<Type>::getKernel( double energy, TimerMap &tmap )
#endif
{
  //
  // start timer
  //
  tmap_["getKernel"].start();
  //
  // verify that this leaf is represented by a part
  //
  if ( leaf_type_ == "part" )
  {
    //
    // get pointer on the part object 
    //
    Part<Type> *part=(Part<Type> *)part_ptr_;
    //
    // find the corresponding kernel 
    //
#ifdef TIMING
    tmap["real part : getLocalSolution"].start();
#endif
    subspace_ptr_=(void *)part->getLocalSolution(domain_index_list_,energy);
#ifdef TIMING
    tmap["real part : getLocalSolution"].stop();
#endif
    //
    // set the type of the leaf
    //
    leaf_type_ = "kernel";
    //
    // stop timer
    //
    tmap_["getKernel"].stop();
    //
    // return pointer on kernel object 
    //
    return subspace_ptr_;
  }
  else if ( leaf_type_ == "complex part" )
  {
    //
    // get pointer on the part object 
    //
    Part<complex<Type> > *part=(Part<complex<Type> >*)part_ptr_;
    //
    // find the corresponding kernel 
    //
#ifdef TIMING
    tmap["complex part : getLocalSolution"].start();
#endif
    subspace_ptr_=(void *)part->getLocalSolution(domain_index_list_,energy);
#ifdef TIMING
    tmap["complex part : getLocalSolution"].stop();
#endif
    //
    // set the type of the leaf
    //
    leaf_type_ = "complex kernel";
    //
    // stop timer
    //
    tmap_["getKernel"].stop();
    //
    // return pointer on kernel object 
    //
    return subspace_ptr_;
  }
  else if ( leaf_type_ == "kernel" || leaf_type_ == "complex kernel" )
  {
    //
    // stop timer
    //
    tmap_["getKernel"].stop();
    //
    // return pointer on kernel object 
    //
    return subspace_ptr_;
  }
  else
  {
    cout << "Error: undefined leaf type " << leaf_type_ << " in - Leaf::getKernel" << endl;
    exit(0);
  }
  return NULL;
}

// ***************************************************
// Leaf::execute
//
//   compute the kernel subspace associated to this leaf
//   and return a pointer to the subspace object
//
// ***************************************************

template <class Type>
#ifndef TIMING
void* Leaf<Type>::execute( vector<Leaf<Type> > &leaf_list_, double energy )
#else
void* Leaf<Type>::execute( vector<Leaf<Type> > &leaf_list_, double energy , TimerMap &tmap)
#endif
{
  //
  // start timer
  //
  tmap_["execute"].start();
  //
  // if this leaf have no parents
  //
  if ( parent[0]<0 && parent[1]<0 )
  {
    //
    // stop timer
    //
    tmap_["execute"].stop();
    //
    // simply return pointer on part object
    //
#ifndef TIMING
    return getKernel( energy );
#else
    return getKernel( energy, tmap );
#endif
  }
  //
  // otherwise, if the kernel comes from an intersection
  //
  else if ( parent[0]>=0 && parent[1]>=0 )
  {
    //
    // get pointers on parent kernel object 
    //
#ifndef TIMING    
    void *kernel0=leaf_list_[parent[0]].getKernel( energy );
    void *kernel1=leaf_list_[parent[1]].getKernel( energy );
#else
    void *kernel0=leaf_list_[parent[0]].getKernel( energy, tmap );
    void *kernel1=leaf_list_[parent[1]].getKernel( energy, tmap );
#endif    
    //
    // intersect the kernels
    //
    if ( leaf_list_[parent[0]].leaf_type_ == "kernel" )
    {
      if ( leaf_list_[parent[1]].leaf_type_ == "kernel" )
      {
        //
        // get correct pointer on kernels
        //
        Subspace<Type> *ker0ptr=(Subspace<Type> *)kernel0;
        Subspace<Type> *ker1ptr=(Subspace<Type> *)kernel1;
        //
        // get intersection
        // 
#ifdef TIMING
    tmap["real/real kernels intersection"].start();
#endif
        ker0ptr->intersectWith(*ker1ptr, domain_index_list_);
#ifdef TIMING
    tmap["real/real kernels intersection"].stop();
#endif
        //
        // release kernel object
        //
        delete ker1ptr;
        ker1ptr=NULL;
      }
      else
      {
        cout << "Error: intersection <Type> with complex<Type> not implemented ( should be complex<Type> with <Type> instead ) in - Leaf::execute<Type> -" << endl;
        exit(0);
      }
    }
    else if ( leaf_list_[parent[0]].leaf_type_ == "complex kernel" )
    {
      if ( leaf_list_[parent[1]].leaf_type_ == "kernel" )
      {
        //
        // get correct pointer on kernels
        //
        Subspace<complex<Type> > *ker0ptr=(Subspace<complex<Type> > *)kernel0;
        Subspace<Type>           *ker1ptr=(Subspace<Type> *)kernel1;
        //
        // get intersection
        // 
#ifdef TIMING
    tmap["real/complex kernels intersection"].start();
#endif
        ker0ptr->intersectWith(*ker1ptr, domain_index_list_);
#ifdef TIMING
    tmap["real/complex kernels intersection"].stop();
#endif
        //
        // release kernel object
        //
        delete ker1ptr;
      }
      else if ( leaf_list_[parent[1]].leaf_type_ == "complex kernel" )
      {
        //
        // get correct pointer on kernels
        //
        Subspace<complex<Type> > *ker0ptr=(Subspace<complex<Type> > *)kernel0;
        Subspace<complex<Type> > *ker1ptr=(Subspace<complex<Type> > *)kernel1;
        //
        // get intersection
        // 
#ifdef TIMING
    tmap["complex/complex kernels intersection"].start();
#endif
        ker0ptr->intersectWith(*ker1ptr, domain_index_list_);
#ifdef TIMING
    tmap["complex/complex kernels intersection"].stop();
#endif
        //
        // release kernel object
        //
        delete ker1ptr;
      } 
      else
      {
        cout << "Error: unknown kernel type " << leaf_list_[parent[1]].leaf_type_ << " b in - Leaf::execute<Type> -" << endl;
        exit(0); 
      }
    }
    else
    {
      cout << "Error: unknown kernel type " << leaf_list_[parent[0]].leaf_type_ << " a in - Leaf::execute<Type> -" << endl;
      exit(0);
    }
    //
    // set subspace pointer
    //
    subspace_ptr_=kernel0;
  }
  //
  // stop timer
  //
  tmap_["execute"].stop();
  //
  // return subspacepointer
  //
  return subspace_ptr_;
}

template <class Type>
#ifndef TIMING
int Leaf<Type>::getKernelDimension( double energy )
#else
int Leaf<Type>::getKernelDimension( double energy, TimerMap &tmap )
#endif
{
  //
  // verify that this leaf is represented by a part
  //
  if ( leaf_type_ == "part" )
  {
    //
    // get pointer on the part object 
    //
    Part<Type> *part=(Part<Type> *)part_ptr_;
    //
    // find the corresponding kernel 
    //
#ifdef TIMING
    tmap["real part : getLocalSolution"].start();
#endif
    subspace_ptr_=(void *)part->getLocalSolution(domain_index_list_,energy);
#ifdef TIMING
    tmap["real part : getLocalSolution"].stop();
#endif
    //
    // set the type of the leaf
    //
    leaf_type_ = "kernel";
    //
    // check dimension of the subspace 
    //
    vector<int> domain = ((Subspace<Type>  *)subspace_ptr_)->domain();
    vector<int> diff1 = domain != domain_index_list_;
    vector<int> diff2 = domain_index_list_ != domain;
    //
    if ( diff1.size() > 0 || diff2.size() > 0 )
    {
      cout << "Error: inconsistent dimension for subspace solution attached to base leaf" << endl;
    }
    //
    // get dimension of the subspace
    //
    kernelDimension_ = ((Subspace<Type>  *)subspace_ptr_)->nVectors();
    //
    // delete subspace object
    //
    delete ((Subspace<Type>  *)subspace_ptr_);
    //
    // return dimension
    //
    return kernelDimension_;
  }
  else if ( leaf_type_ == "complex part" )
  {
    //
    // get pointer on the part object 
    //
    Part<complex<Type> > *part=(Part<complex<Type> >*)part_ptr_;
    //
    // find the corresponding kernel 
    //
#ifdef TIMING
    tmap["complex part : getLocalSolution"].start();
#endif
    subspace_ptr_=(void *)part->getLocalSolution(domain_index_list_,energy);
#ifdef TIMING
    tmap["complex part : getLocalSolution"].stop();
#endif
    //
    // set the type of the leaf
    //
    leaf_type_ = "complex kernel";
    //
    // check dimension of the subspace 
    //
    vector<int> domain = ((Subspace<complex<Type> >  *)subspace_ptr_)->domain();
    vector<int> diff1 = domain != domain_index_list_;
    vector<int> diff2 = domain_index_list_ != domain;
    //
    if ( diff1.size() > 0 || diff2.size() > 0 )
    {
      cout << "Error: inconsistent dimension for subspace solution attached to base leaf" << endl;
    }
    //
    // get dimension of the subspace
    //
    kernelDimension_ = ((Subspace<complex<Type> >  *)subspace_ptr_)->nVectors();
    //
    // delete subspace object
    //
    delete ((Subspace<complex<Type> >  *)subspace_ptr_);
    //
    // return dimension
    //
    return kernelDimension_;
  }
  else if ( leaf_type_ == "kernel" || leaf_type_ == "complex kernel" )
  {
    //
    // test that this leaf has been executed already
    //
    if ( kernelDimension_==-1 )
    {
      cout << "Error: trying to access a kernel dimension that has not been set yet" << endl;
      exit(0);
    }
    //
    // return dimension
    //
    return kernelDimension_;
  }
  else
  {
    cout << "Error: undefined leaf type " << leaf_type_ << " in - Leaf::getKernel" << endl;
    exit(0);
  }
  //
  // dummy return...
  //
  return 0;
}

// ***************************************************
// Leaf::test
//
//   compute the kernel subspace associated to this leaf
//   and return a pointer to the subspace object
//
// ***************************************************

template <class Type>
#ifndef TIMING
int Leaf<Type>::testDimension( vector<Leaf<Type> > &leaf_list_, double energy )
#else
int Leaf<Type>::testDimension( vector<Leaf<Type> > &leaf_list_, double energy , TimerMap &tmap)
#endif
{
  //
  // if this leaf have no parents
  //
  if ( parent[0]<0 && parent[1]<0 )
  {
    //
    // simply return pointer on part object
    //
#ifndef TIMING
    return getKernelDimension( energy );
#else
    return getKernelDimension( energy, tmap );
#endif
  }
  //
  // otherwise, if the kernel comes from an intersection
  //
  else if ( parent[0]>=0 && parent[1]>=0 )
  {
    //
    // get pointers on parent kernel object 
    //
#ifndef TIMING    
    int dim0=leaf_list_[parent[0]].getKernelDimension( energy );
    int dim1=leaf_list_[parent[1]].getKernelDimension( energy );
#else
    int dim0=leaf_list_[parent[0]].getKernelDimension( energy, tmap );
    int dim1=leaf_list_[parent[1]].getKernelDimension( energy, tmap );
#endif    
    //
    // intersect the kernels
    //
    if ( leaf_list_[parent[0]].leaf_type_ == "kernel" )
    {
      if ( leaf_list_[parent[1]].leaf_type_ == "kernel" )
      {
        //
        // get intersection size 
        //
        vector<int> intersection = leaf_list_[parent[0]].domain_index_list_ && leaf_list_[parent[1]].domain_index_list_;
        //
        // compute size of the resulting subspace
        //
        kernelDimension_ = dim0 + dim1 - intersection.size();
        //
        // test new dimension
        //
        if ( kernelDimension_ <= 0 ) 
        {
          cout << "Error: inconsistent kernel dimension " << kernelDimension_ << " found during intersection of leaves " << parent[0] << " and " << parent[1] << endl;
          exit(0);
        }
      } 
      else
      {
        cout << "Error: unknown kernel type " << leaf_list_[parent[1]].leaf_type_ << " b in - Leaf::execute<Type> -" << endl;
        exit(0); 
      }
    }
    else
    {
      cout << "Error: unknown kernel type " << leaf_list_[parent[0]].leaf_type_ << " a in - Leaf::execute<Type> -" << endl;
      exit(0);
    }
  }
  //
  // return dimension
  //
  return kernelDimension_;
}

template <class Type>
int IntersectionTree<Type>::testDimension( double energy )
{
  int kernel_dim;
  //
  // if there is more than on leaf 
  //
  if ( leaf_list_.size()>1 )
  {
    //
    // execute all non-base leaf 
    // 
    for ( int ileaf=0 ; ileaf<leaf_list_.size() ; ileaf++ )
    {
#ifndef TIMING
      if ( leaf_list_[ileaf].level>0 ) kernel_dim=leaf_list_[ileaf].testDimension( leaf_list_, energy );
#else
      if ( leaf_list_[ileaf].level>0 ) kernel_dim=leaf_list_[ileaf].testDimension( leaf_list_, energy, tmap_ );
#endif
    }
  }
  else
  {
    //
    // simply execute leaf containing part
    //
#ifndef TIMING
    kernel_dim=leaf_list_[0].testDimension( leaf_list_, energy );
#else
    kernel_dim=leaf_list_[0].testDimension( leaf_list_, energy, tmap_ );
#endif
  } 
  //
  // reset tree
  //
  reset(); 
  //
  // return subspace pointer
  //
  return kernel_dim;
}

template <class Type>
void* IntersectionTree<Type>::execute( double energy )
{
  void *subspace;
  //
  // launch leaves timers
  //
  for ( int ileaf=0 ; ileaf<leaf_list_.size() ; ileaf++ )
  {
    leaf_list_[ileaf].tmap_["leaf wait"].start();
    leaf_list_[ileaf].tmap_["leaf pass"].start();
  }    
  //
  // if there is more than on leaf 
  //
  if ( leaf_list_.size()>1 )
  {
    
    //
    // execute all non-base leaf 
    // 
    for ( int ileaf=0 ; ileaf<leaf_list_.size() ; ileaf++ )
    {
      //
      // stop leaf timer 
      //
      leaf_list_[ileaf].tmap_["leaf wait"].stop();
      //
      // execute leaf
      //
#ifndef TIMING
      if ( leaf_list_[ileaf].level>0 ) subspace=leaf_list_[ileaf].execute( leaf_list_, energy );
#else
      if ( leaf_list_[ileaf].level>0 ) subspace=leaf_list_[ileaf].execute( leaf_list_, energy, tmap_ );
#endif
      //
      // stop leaf timer 
      //
      leaf_list_[ileaf].tmap_["leaf pass"].stop();
    }
  }
  else
  {
    //
    // stop leaf timer 
    //
    leaf_list_[0].tmap_["leaf wait"].stop();
    //
    // simply execute leaf containing part
    //
#ifndef TIMING
    subspace=leaf_list_[0].execute( leaf_list_, energy );
#else
    subspace=leaf_list_[0].execute( leaf_list_, energy, tmap_ );
#endif
    //
    // stop leaf timer 
    //
    leaf_list_[0].tmap_["leaf pass"].stop();
  }  
  //
  // return subspace pointer
  //
  return subspace;
}

// ***************************************************
// Leaf::Leaf 
//
//   constructor
//
// ***************************************************

template <class Type>
Leaf<Type>::Leaf() 
{
  //
  // init pointers to subspace and part object to 0
  //
  part_ptr_=NULL;
  subspace_ptr_=NULL;
  kernelDimension_=-1;
} 

// ***************************************************
// Leaf::Leaf 
//
//   destructor
//
// ***************************************************

template <class Type>
Leaf<Type>::~Leaf<Type>() 
{
  //
  // delete subspace and part objects if needed
  //
//  if ( part_ptr_!=NULL ) delete part_ptr_;
//  if ( subspace_ptr_!=NULL ) delete subspace_ptr_;
} 

// ***************************************************
// Leaf::onSameProcs
//
//   return the indices in leaf_list_ of the actives_leaves
//   contained by the same group of procs as the calling leaf
//   and of index >= istart in active_leaves list
//
// ***************************************************

template <class Type>
vector<int> Leaf<Type>::onSameProcs( vector<Leaf<Type> > &leaf_list, vector<int> &active_leaves)
{
  vector<int> result;
  //
  // loop on input leaves
  //
  for ( int index_in_active_leaf=0 ; index_in_active_leaf<active_leaves.size() ; index_in_active_leaf++ )
  {
    //
    // get leaf index
    //
    int ileaf=active_leaves[index_in_active_leaf];
    //
    // intersect proc lists
    //
    vector<int> common_proc = list_proc_domain_ && leaf_list[ileaf].list_proc_domain_ ;
    //
    // if this leaf is contained in the same group of proc
    //
    if ( common_proc.size() == leaf_list[ileaf].list_proc_domain_.size() )
    {
      //
      // add index to the list
      // 
      result.push_back(ileaf);
    }
  }
  //
  // return result
  //
  return result;
}

// ***************************************************
// Leaf::bestMatch
//
//   return the index of the active leaf contained 
//   on free procs with the largest intersection with 
//   the calling leaf and index in active_leaf list
//   >= istart.
//
// ***************************************************

template <class Type>
int Leaf<Type>::bestMatch( vector<Leaf<Type> > &leaf_list, vector<int> &active_leaves, vector<bool> &free_procs, int except_index )
{
  //
  // look for the better matching leaf with free proc too
  //
  int ibestleaf_local=-1;
  int ibestleaf_total=-1;
  int best_intersection_size_local=0; // keep track of the maximum intersection with this leaf on the local procs
  int best_intersection_size_total=0; // keep track of the maximum intersection with this leaf on the total grid
  //
  for ( int index_in_active_leaf=0 ; index_in_active_leaf<active_leaves.size() ; index_in_active_leaf++ )
  {
    //
    // get leaf index
    //
    int ileaf=active_leaves[index_in_active_leaf];
    //
    // verify that the index is correct
    //
    if ( ileaf<0 || ileaf>=leaf_list.size() )
    {
      cout << "Error: Leaf::bestMatch - incorrect leaf index in active_leaf vector" << endl;
      exit(0);
    }
    //
    // if this is not the exception index
    //
    if ( ileaf!=except_index )
    {
      //
      // get the processor list for this leaf
      //
      vector<int> proc_list=leaf_list[ileaf].list_proc_domain_;
      //
      // test if those procs are free 
      //
      bool leaf_free=true;
      //
      for ( int i=0 , stop=proc_list.size() ; i<stop ; i++ ) leaf_free=( leaf_free && free_procs[proc_list[i]] );
      //
      // if this leaf is free
      //
      if ( leaf_free )
      {
        //
        // make sure that the list of domain proc for this leaves intersect 
        //
        vector<int> procs_inter = proc_list && list_proc_domain_;
        vector<int> procs_union = proc_list || list_proc_domain_;
        //
        // if this leaf occupy the same procs
        //
        if ( procs_union.size()==list_proc_domain_.size() )
        {
          //
          // compute intersection between the two leaves
          // 
          vector<int> intersection=leaf_list[ileaf].domain_index_list_ && domain_index_list_;
          //
          // keep best local intersection
          //
          if ( best_intersection_size_local < intersection.size() )
          {
            best_intersection_size_local = intersection.size();
            ibestleaf_local = ileaf;
          }
        }
        //
        // if the intersection is non nul, wich means the two leaves may intersect
        // and there is no local candidate yet
        //
        else if ( procs_inter.size()>0 && best_intersection_size_local==0 )
        {
          //
          // compute intersection between the two leaves
          // 
          vector<int> intersection=leaf_list[ileaf].domain_index_list_ && domain_index_list_;
          //
          // keep the largest intersection
          //
          if ( best_intersection_size_total < intersection.size() )
          {
            best_intersection_size_total = intersection.size();
            ibestleaf_total = ileaf;
          }
        }
      }
    }
  }
  //
  // return the best index found (-1 neans no leaf intersect the current one)
  //
  return ( ibestleaf_local>=0 ) ? ibestleaf_local : ibestleaf_total;
}

// ***************************************************
// nextFreeLeaf
//
//   return the index of the next active leaf contained 
//   on free procs with index in active_leaf list >= istart.
//
// ***************************************************

template <class Type>
static int nextFreeLeaf( vector<Leaf<Type> > &leaf_list, vector<int> &active_leaves, vector<bool> &free_procs, int &istart )
{
  int index_in_active_leaf;
  //
  // loop on active leaf
  //
  for ( index_in_active_leaf=istart ; index_in_active_leaf<active_leaves.size() ; index_in_active_leaf++ )
  {
    //
    // get leaf index
    //
    int ileaf=active_leaves[index_in_active_leaf];
    //
    // verify that the index is correct
    //
    if ( ileaf<0 || ileaf>=leaf_list.size() )
    {
      cout << "Error: Leaf::nextFreeLeaf - incorrect leaf index in active_leaf vector" << endl;
      exit(0);
    }
    //
    // get the processor list for this leaf
    //
    vector<int> proc_list=leaf_list[ileaf].procDomain();
    //
    // test if those proc are free 
    //
    bool leaf_free=true;
    //
    for ( int i=0 , stop=proc_list.size() ; i<stop ; i++ ) leaf_free=( leaf_free && free_procs[proc_list[i]] );
    //
    // exit loop if this leaf is free
    //
    if ( leaf_free ) break;
  }
  //
  // if we found a free leaf
  //
  if ( index_in_active_leaf<active_leaves.size() )
  {
    return active_leaves[index_in_active_leaf];
  }
  else
  {
    return -1;
  }
}

// ***************************************************
// Tree::Tree 
//
//   constructor
//
// ***************************************************

template <class Type>
IntersectionTree<Type>::IntersectionTree(Context &context) : 
context_(context)
{
  //
  // start life timer
  //
#ifdef TIMING
  tmap_["total life time"].start();
#endif
  //
  // set context properties
  //
  my_proc_=context.myProc();
  n_procs_=context.nProcs();
  //
  // indicate that the tree is not branched yet
  //
  branched_=false;
  //
  // indicate that the tree is not reduced yet
  //
  reduced_=false;
}

// ***************************************************
// Tree::reset
//
//   reset the tree leaves, clearing memory and 
//   resetting the original types
//
// ***************************************************

template <class Type>
void IntersectionTree<Type>::reset( void )
{ 
  //
  // reset initial types
  //
  for ( int ileaf=0 ; ileaf<init_type_.size() ; ileaf++ )
  {
    leaf_list_[ileaf].leaf_type_=init_type_[ileaf];
  }
  //
  // delete object pointed by last leaf if any 
  //
  if ( leaf_list_[leaf_list_.size()-1].subspace_ptr_!=NULL )
  {
    if ( leaf_list_[leaf_list_.size()-1].leaf_type_=="complex kernel" )
    {
      delete ((Subspace<complex<Type> > *)leaf_list_[leaf_list_.size()-1].subspace_ptr_);
    }
    else
    {
      delete ((Subspace<Type> *)leaf_list_[leaf_list_.size()-1].subspace_ptr_);
    }
  }
  //
  // set all subspace pointers to NULL
  // 
  for ( int ileaf=0 ; ileaf<leaf_list_.size() ; ileaf++ )
  {
    leaf_list_[ileaf].subspace_ptr_=NULL;
  }
}

// ***************************************************
// Tree::addLeaf
//
//   create a new leaf from leaves leaf1 and leaf2
//   and discard the corresponding processors in
//   freeProc list
//
// ***************************************************

template <class Type>
void IntersectionTree<Type>::addLeaf( string leafType, vector<int> domain )
{ 
  //
  // create a new leaf 
  //
  Leaf<Type> newleaf;
  //
  // keep track of domain indices
  //
  newleaf.domain_index_list_=domain;
  //
  // map domain on processors
  //
  for ( int iproc=0 ; iproc<n_procs_ ; iproc++ )
  {
    //
    // get domain index on proc iproc
    //
    vector<int> domain_on_proc_i=context_.getIndicesOnProc(iproc);
    //
    // intersect with domain indices 
    //
    vector<int> intersection = domain && domain_on_proc_i;
    //
    // if the intersection is non null, add proc to list
    //
    if ( intersection.size()>0 ) newleaf.list_proc_domain_.push_back(iproc);
  }
  //
  // set parents and child
  //
  newleaf.parent[0]=-1;
  newleaf.parent[1]=-1;
  newleaf.child=-1;
  //
  // set leaf type
  //
  if ( ! ( leafType=="part" || leafType=="complex part" ) )
  {
    cout << "Error:incorrect leaf type " << leafType << " in - IntersectionTree::addLeaf -" << endl;
    exit(0);  
  }
  newleaf.leaf_type_=leafType;
  init_type_.push_back(leafType);
  //
  // set level in execution stack
  //
  newleaf.level=0;
  //
  // add leaf to list
  //
  leaf_list_.push_back(newleaf);    
}

template <class Type>
void IntersectionTree<Type>::addLeaf( int ileaf1, int ileaf2, vector<bool> &freeProc, int level)        
{        
  //
  // form the new list of procs
  //
  vector<int> list_proc_domain= leaf_list_[ileaf1].list_proc_domain_ || leaf_list_[ileaf2].list_proc_domain_;
  //
  // form the new domain
  //
  vector<int> domain_index_list= leaf_list_[ileaf1].domain_index_list_ || leaf_list_[ileaf2].domain_index_list_;
  //
  // create a new leaf 
  //
  Leaf<Type> newleaf;
  //
  newleaf.list_proc_domain_=list_proc_domain; 
  newleaf.domain_index_list_=domain_index_list; 
  //
  if ( !( leaf_list_[ileaf1].leaf_type_=="complex kernel" || leaf_list_[ileaf2].leaf_type_=="complex kernel" ) 
    && !( leaf_list_[ileaf1].leaf_type_=="complex part" || leaf_list_[ileaf2].leaf_type_=="complex part" ) )
  {
    newleaf.leaf_type_="kernel";
  }
  else
  {
    newleaf.leaf_type_="complex kernel";
  }
  //
  // set child and parents
  //
  newleaf.parent[0]=ileaf1;
  newleaf.parent[1]=ileaf2;
  newleaf.child=-1;
  //
  // set level in execution stack
  //
  newleaf.level=level;
  //
  // set parent's child
  //
  leaf_list_[ileaf1].child=leaf_list_.size();
  leaf_list_[ileaf2].child=leaf_list_.size();
  //
  // add leaf to list
  //
  leaf_list_.push_back(newleaf);    
  //
  // discard processors
  //
  for ( int i=0 ; i<list_proc_domain.size() ; i++ ) freeProc[list_proc_domain[i]]=false;
}

// ***************************************************
// Tree::setPartPtr
//
//   set the part pointer for a base leaf
//
// ***************************************************

template <class Type>
void IntersectionTree<Type>::setPartPtr( int ileaf , void *part )
{
  //
  // check that this leaf exist
  //
  if ( ileaf >= leaf_list_.size() )
  {
    cout << "Error: invalid index in - IntersectionTree::setPartPtr -" << endl; 
    exit(0);
  }
  //
  // check that this leaf level is 0
  //  
  if ( leaf_list_[ileaf].level!=0 )
  {
    cout << "Error: trying to asign a part to a non base leaf in - IntersectionTree::setPartPtr -" << endl; 
    exit(0);   
  }
  //
  // assign the part pointer 
  //
  leaf_list_[ileaf].part_ptr_=part;
}

// ***************************************************
// Tree::getPartPtr
//
//   get the part pointer from a base leaf
//
// ***************************************************

template <class Type>
void* IntersectionTree<Type>::getPartPtr( int ileaf )
{
  //
  // check that this leaf exist
  //
  if ( ileaf >= leaf_list_.size() )
  {
    cout << "Error: invalid index in - IntersectionTree::getPartPtr -" << endl; 
    exit(0);
  }
  //
  // check that this leaf level is 0
  //  
  if ( leaf_list_[ileaf].level!=0 )
  {
    cout << "Error: trying to get a part from a non base leaf in - IntersectionTree::getPartPtr -" << endl; 
    exit(0);   
  }
  //
  // check that the pointer has been assigned
  //
  if ( leaf_list_[ileaf].part_ptr_==NULL )
  {
    cout << "Error: trying to get a part from a non-initialized leaf in - IntersectionTree::getPartPtr -" << endl; 
    exit(0);   
  }
  //
  // return the part pointer 
  //
  return leaf_list_[ileaf].part_ptr_;
}

// ***************************************************
// Tree::branch
//
//   determine a reasonable way to instersect 
//   the different kernels
//
// ***************************************************

template <class Type>
void IntersectionTree<Type>::branch(void)
{
#ifdef TIMING
  tmap_["branching"].start();
#endif
  //
  // verify that the tree is not branched yet
  //
  if ( branched_ )
  {
    cout << "Error: Tree::branch - tree object is already branched" << endl;
    exit(0);
  }
  //
  int  level=0;
  bool treeComplete=false;
  //
  while ( !treeComplete )
  {
    //
    // increment level
    //
    level++;
    //
    // init list of free proc
    //
    vector<bool> procFree(n_procs_, true);
    //
    // create: list of leaves without child    
    // create: list of leaves without child and refering to a kernel object   
    // create: list of leaves without child and refering to a part object   
    //
    vector<int> free_leaves;
    vector<int> kernel_leaves;
    vector<int> part_leaves;
    //
    for ( int i=0 ; i<leaf_list_.size() ; i++ ) 
    {
      if ( leaf_list_[i].child==-1 ) 
      {
        free_leaves.push_back(i);       
        //
        if ( leaf_list_[i].leaf_type_=="kernel" || leaf_list_[i].leaf_type_=="complex kernel" ) 
        {
          kernel_leaves.push_back(i);
        }
        else
        { 
          part_leaves.push_back(i);
        }      
      }
    }
    //
    // check if all the leaves have been branched
    //
    if ( free_leaves.size()>1 )
    {
      //
      // sort lists according to the number of proc they occupy
      //
      {
        vector<int> level( free_leaves.size() );
        //
        for ( int i=0 , stop=free_leaves.size() ; i<stop ; i++ ) level[i]=leaf_list_[free_leaves[i]].list_proc_domain_.size();
        //
        if ( free_leaves.size()>1 ) sort<int>( &level[0], level.size(), &free_leaves[0] );
      }
      {
        vector<int> level( kernel_leaves.size() );
        //
        for ( int i=0 , stop=kernel_leaves.size() ; i<stop ; i++ ) level[i]=leaf_list_[kernel_leaves[i]].list_proc_domain_.size();
        //
        if ( kernel_leaves.size()>1 ) sort<int>( &level[0], level.size(), &kernel_leaves[0] );
      }
      {
        vector<int> level( part_leaves.size() );
        //
        for ( int i=0 , stop=part_leaves.size() ; i<stop ; i++ ) level[i]=leaf_list_[part_leaves[i]].list_proc_domain_.size();
        //
        if ( part_leaves.size()>1 ) sort<int>( &level[0], level.size(), &part_leaves[0] );
      }
      //
      // loop first on kernel leafs to ensure that we have only one kernel by proc
      //
      int index_in_kernel_leaves=0;
      //
      while ( index_in_kernel_leaves<kernel_leaves.size() )
      {
        //
        // get the next free leaf in the list
        //
        int ileaf=nextFreeLeaf( leaf_list_, kernel_leaves, procFree, index_in_kernel_leaves );
        //
        // if there is a free leaf 
        //
        if ( ileaf>-1 )
        {
          //
          // get the list of free leaves supported by the same procs
          //
          vector<int> leaves_on_same_procs=leaf_list_[ileaf].onSameProcs( leaf_list_ , free_leaves);
          //
          // find the best match for the intersection
          //
          int isecondleaf=leaf_list_[ileaf].bestMatch( leaf_list_, leaves_on_same_procs, procFree, ileaf );
          //
          // if there is a leaf intersecting this one
          //
          if ( isecondleaf>-1 )
          {
            //
            // create a new leaf
            //
            addLeaf(ileaf,isecondleaf,procFree,level);
          }
          //
          // if no matching leaf has been found on the same group of procs, check the complete list
          //
          else
          {
            //
            // finf the best match in the whole free_leaves list
            //
            isecondleaf=leaf_list_[ileaf].bestMatch( leaf_list_, free_leaves, procFree, ileaf );
            //
            // if there is a leaf intersecting this one
            //
            if ( isecondleaf>-1 )
            {
              //
              // create a new leaf
              //
              addLeaf(ileaf,isecondleaf,procFree,level);
            }
          }
          //
          // increment kernel leaf index
          //
          index_in_kernel_leaves++;
        }
        //
        // if there is no more free leaves
        //
        else
        {
          break;
        }
      }
      //
      // repeat the operation with free part leaves
      //
      int index_in_part_leaves=0;
      //
      while ( index_in_part_leaves<part_leaves.size() )
      {
        //
        // get the next free leaf in the list
        //
        int ileaf=nextFreeLeaf( leaf_list_, part_leaves, procFree, index_in_part_leaves );
        //
        // if there is a free leaf 
        //
        if ( ileaf>-1 )
        {
          //
          // get the list of free leaves supported by the same procs
          //
          vector<int> leaves_on_same_procs=leaf_list_[ileaf].onSameProcs( leaf_list_ , part_leaves);
          //
          // find the best match for the intersection
          //
          int isecondleaf=leaf_list_[ileaf].bestMatch( leaf_list_, leaves_on_same_procs, procFree, ileaf );
          //
          // if there is a leaf intersecting this one
          //
          if ( isecondleaf>-1 )
          {
            //
            // create a new leaf
            //
            addLeaf(ileaf,isecondleaf,procFree,level);
          }
          //
          // if no matching leaf has been found on the same group of procs, check the complete list
          //
          else
          {
            //
            // finf the best match in the whole free_leaves list
            //
            isecondleaf=leaf_list_[ileaf].bestMatch( leaf_list_, part_leaves, procFree, ileaf );
            //
            // if there is a leaf intersecting this one
            //
            if ( isecondleaf>-1 )
            {
              //
              // create a new leaf
              //
              addLeaf(ileaf,isecondleaf,procFree,level);
            }
          }
          //
          // increment part leaf index
          //
          index_in_part_leaves++;
        }
        //
        // if there is no more free leaves
        //
        else
        {
          break;
        }
      }
    }
    //
    // else, if free_leaves contains only 1 leaf 
    //
    else
    {
      //
      // indicate that the tree is complete
      //
      treeComplete=true;
    }
  }
  //
  // indicate that the tree is now branched
  //
  branched_=true;
#ifdef TIMING
  tmap_["branching"].stop();  
#endif
}


template <class Type>
void IntersectionTree<Type>::reduceDomains(vector<int> &finalDomain)
{
  //
  // verify that the tree is already branched
  //
  if ( !branched_ )
  {
    cout << "Error: Tree::reduceDomains _ trying to reduce a tree which is not branched yet" << endl;
    exit(0);
  }
  if ( finalDomain.size()==0 )
  {
    cout << "Error: Tree::reduceDomains _ trying to reduce a tree to a null domain" << endl;
    exit(0);
  }
  //
  // reverse loop on tree leaves (parents are always before childern in the list) 
  //
  for ( int ileaf=leaf_list_.size()-1 ; ileaf>=0 ; ileaf-- )
  {
    //
    // if this is the final leaf
    //
    if ( leaf_list_[ileaf].child==-1 )
    {
      //
      // reduce domain to the final domain
      //
      vector<int> intersection = leaf_list_[ileaf].domain_index_list_ && finalDomain;
      //
      // inform
      //
      if ( my_proc_==0 )
      {
        cout << "  keeping " << intersection.size() << " states for final leaf domain" << endl;
      }
      //
      // set as new domain
      //
      leaf_list_[ileaf].domain_index_list_=intersection;
    }
    else
    {
      //
      // find the index of the other leaf to be intesected with
      //
      int iother = ( leaf_list_[leaf_list_[ileaf].child].parent[0]!=ileaf ? leaf_list_[leaf_list_[ileaf].child].parent[0] : leaf_list_[leaf_list_[ileaf].child].parent[1] );
      //
      // get indices from child, current, and other
      // 
      vector<int> child_indices = leaf_list_[leaf_list_[ileaf].child].domain_index_list_;
      vector<int> other_indices = leaf_list_[iother].domain_index_list_;
      vector<int>  leaf_indices = leaf_list_[ileaf].domain_index_list_;
      //
      // current should keep only indices common with other and child 
      //
      vector<int>   use_indices = child_indices || other_indices;
      vector<int> myNew_indices = leaf_indices && use_indices;
      //
      leaf_list_[ileaf].domain_index_list_ = myNew_indices;
      //
      // check integrity
      //
      vector<int> tmp_indices_1 = myNew_indices || other_indices; 
      vector<int> tmp_indices_2 = child_indices && tmp_indices_1; 
      //
      if ( child_indices.size() != tmp_indices_2.size() )
      {
        cout << "Error: lost " << child_indices.size()-tmp_indices_2.size() << " states during intersection of leaves " << ileaf << " and " << iother << endl;
        exit(0);
      }
      //
      // set the involved processors
      //
      leaf_list_[ileaf].list_proc_domain_.resize(0);      
      for ( int iproc=0 ; iproc<n_procs_ ; iproc++ )
      {
        // get index on this proc
        vector<int> indicesOnProc=context_.getIndicesOnProc(iproc);
        // search for correspondances within the indices of this part
        vector<int> intersection = indicesOnProc && leaf_list_[ileaf].domain_index_list_;
        // if the intersection is non null
        if ( intersection.size()>0 ) leaf_list_[ileaf].list_proc_domain_.push_back(iproc);
      }
    }
  }
  //
  // indicate that the tree has been reduced
  //
  reduced_=true;
}

template <class Type>
void IntersectionTree<Type>::printTree(void)
{
  //
  // verify that the tree is already branched
  //
  if ( !branched_ )
  {
    cout << "Error: Tree::printTree _ trying to reduce a tree which is not branched yet" << endl;
    exit(0);
  }
  //
  // add some garbage
  //
  cout << endl;
  cout << "-------------------------------------------------------------------------" << endl;
  cout << "IntersectionTree :" << endl << endl;
  cout << " ;-)" << endl;
  //
  // print the tree
  //
  if ( my_proc_==0 ) printLeaf_(leaf_list_.size()-1, "  |_", "    ");
  //
  // add yet some more garbage
  //
  cout << endl;
  cout << "-------------------------------------------------------------------------" << endl;
}

template <class Type>
void IntersectionTree<Type>::printLeaf_(int ileaf, string line_link, string line_empty)
{
  // create new lines
  string line_link1=line_link+"__";
  string line_link2=line_empty+"|_";
  string line_empty1=line_empty+"| ";
  string line_empty2=line_empty+"  ";
  // print parent 0
  if ( leaf_list_[ileaf].parent[0]<0 )
  {
    cout << line_link1 << "_[" << ileaf << "]" << endl;
  }
  else
  {
    printLeaf_(leaf_list_[ileaf].parent[0], line_link1, line_empty1);
    printLeaf_(leaf_list_[ileaf].parent[1], line_link2, line_empty2);
  }
}


template <class Type>
void IntersectionTree<Type>::print(void)
{
  if (my_proc_==0) 
  {
    int nproc=0;
    int level=-1;
    //
    // reverse loop on tree leaves (parent are always before childern in the list) 
    //
    for ( int ileaf=leaf_list_.size()-1 ; ileaf>=0 && leaf_list_[ileaf].parent[0]>=0 && leaf_list_[ileaf].parent[1]>=0 ; ileaf-- )
    {
      if ( level!=leaf_list_[ileaf].level ) 
      {
        cout << endl;
        level=leaf_list_[ileaf].level;
        cout << "level " << level << ":  ";
      }
      else
      {
        cout << "|  ";
      }
      cout << ileaf << " => " << leaf_list_[ileaf].parent[0] << " && " << leaf_list_[ileaf].parent[1];
      if ( leaf_list_[ileaf].list_proc_domain_.size()>0 ) 
      {
        cout << " (procs " << leaf_list_[ileaf].list_proc_domain_ << ") ";
      }
      else
      {
        cout << " (proc " << leaf_list_[ileaf].list_proc_domain_ << ") ";
      }
      // estimate workload
      nproc+=leaf_list_[ileaf].list_proc_domain_.size();
    }
    cout << endl;
    cout << endl;
    cout << "Average workload=" << nproc/(double)leaf_list_[leaf_list_.size()-1].level/(double)n_procs_;
    cout << endl;
  }
}

template <class Type>
void IntersectionTree<Type>::printRepartition( void )
{
  if (my_proc_==0) 
  {
    cout << "Parts repartition on the processors:" << endl;
    // loop on the leaf
    for ( int ileaf=0 ; ileaf<leaf_list_.size() ; ileaf++ )
    {
      cout << "leaf " << ileaf << " domain is spread on: " << leaf_list_[ileaf].list_proc_domain_ << endl;
    }
    cout << endl;
  }
}
 
#ifdef TIMING
  #undef TIMING
#endif 
    
#endif

#ifndef RESERVOIR_H
#define RESERVOIR_H

#include "Basis.h"
#include "Lapack.h"
#include "Subspace.h"
#include "IndexList.h"

template <class Precision>
class Reservoir
{
  protected:
    
    // label
    string label_;
    
    // communicator 
    Context   &context_;
    int       my_proc_;
    int       n_procs_;
     
    // range/domain indices
    vector<int>           global_indices_;    // indices of this reservoir
    map<int,vector<int> > local_indices_;     // repartition of the indices on the proc grid
    
    // range proc index lists 
    vector<int> list_proc_;                   // list of processors supporting the reservoir 
    vector<int> Nloc_;                        // local dimension on supporting processors
    int         my_Nloc_;                     // local dimension on this processor
    
    // channel indices by type
    vector<int>    explosive_channel_indices_;   // indices of explosive channels
    vector<int>    evanescent_channel_indices_;  // indices of evanescent channels
    vector<int>    incoming_channel_indices_;    // indices of incoming channels
    vector<int>    outgoing_channel_indices_;   // indices of outcoming channels
        
    // coefficients arrays
    vector<complex<Precision> >    dual_coeff_;     // local coefficients for the channels duals : Cij is the ith coeff of channel j dual. 
    vector<complex<Precision> >    channel_coeff_;  // local coefficients for the channels : Cij is the is the jth coeff of channel i.
   
  public:
  
    // label access
    string label(void) { return label_; }
    
    // constructor
    template <class BasisType>
    Reservoir(BasisType &basis, Context &context, int ireservoir);
    
    // accessing the chanel indices
    vector<int> explosiveChannelIndices(void)  { return explosive_channel_indices_;  }
    vector<int> evanescentChannelIndices(void) { return evanescent_channel_indices_; }
    vector<int> incomingChannelIndices(void)   { return incoming_channel_indices_;   }
    vector<int> outgoingChannelIndices(void)   { return outgoing_channel_indices_;   }
    
    // resolution of the channels 
    virtual bool findChannelSolutions(double Energy)=0;
    
    // move subspaces onto channel representation
    virtual void moveToChannelRepresentation(Subspace<complex<Precision> > &subspace); 
    virtual Subspace<complex<Precision> >* moveToChannelRepresentation(Subspace<Precision> &subspace); 
    
    // move back channels in local representation
    virtual void moveToLocalRepresentation(Subspace<complex<Precision> > &subspace);
    
    // synchronization of channels indices on all the procs of the context
    virtual void synchronizeChannelsIndices(void)=0;
};

// constructor
template <class Precision>
template <class BasisType>
Reservoir<Precision>::Reservoir(BasisType &basis, Context &context, int ireservoir) : context_(context)
{
  //
  // keep grid parameters
  //
  my_proc_=context_.myProc();
  n_procs_=context_.nProcs();
  //
  // get global indices for this reservoir
  //
  global_indices_=basis.getReservoirIndices(ireservoir);
  //
  // verify indices
  //
  if ( global_indices_.size()==0 )
  {
    cout << "Error: null size index array in Reservoir instanciation" << endl;
    exit(0);
  }
  //
  // map the range indices on the proc array
  //
  for ( int i_proc=0 ; i_proc<n_procs_ ; i_proc++ )
  {
    //
    // get global indices on processor i_proc
    //
    vector<int> global_indices_on_proc_i=context.getIndicesOnProc(i_proc);
    //
    // intersect range indices with the global indices on processor i_proc
    //
    vector<int> intersection=global_indices_&&global_indices_on_proc_i;
    //
    // if the intersection if non null
    //
    if ( intersection.size()>0 )
    {
      //
      // map the indices 
      //
      local_indices_[i_proc]=intersection;
      //
      // add this proc to the list of proc suporting the range
      //
      list_proc_.push_back( i_proc );
      //
      // keep the corresponding number of range indices
      //
      Nloc_.push_back( intersection.size() );
    }
    //
    // keep the dimension of the local range
    //
    if ( my_proc_==i_proc ) my_Nloc_=intersection.size();
  }
}


template <class Precision> 
void Reservoir<Precision>::moveToChannelRepresentation( Subspace<complex<Precision> > &subspace )
{
  //
  // verify that the dual coefficients have been set 
  //
  if ( dual_coeff_.size()==0 && my_Nloc_!=0 )
  {
    cout << "Error: in - Reservoir::moveToChannelRepresentation - trying to move a subspace to channel representation while no dual has been defined" << endl;  
    exit(0); 
  }
  if ( dual_coeff_.size()!=global_indices_.size()*my_Nloc_ )
  {
    cout << "Error: in - Reservoir::moveToChannelRepresentation - incorrect dual size" << endl;  
    exit(0);
  }
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_indices_;
  //
  vector<complex<Precision> > coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_,coeff); // <= domain_indices contains now the indices of the gathered coeffs
  //
  // make sure that we found all the coeff of the contact for that subspace
  // or none at all
  //
  if ( global_indices_.size()!=domain_indices.size() && domain_indices.size()!=0 )
  {
    cout << "Error: trying to move an incomplete contact subspace to channel representation in - moveToChannelRepresentation -" << endl;
    exit(0);
  }
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_indices_.count(my_proc_) )
  {
    //
    // find local indices correspondance between subspace coeffs and dual coeffs
    //  
    vector<int> local_indices = domain_indices <= global_indices_;
    //
    // keep dimensions of the subspace
    //
    int ncols=local_indices.size();
    int nrows=subspace.nVectors();
    //
    // reorder subspace coeffs tho match the one of the dual
    //
    for ( int icol=0 ; icol<ncols ; icol++ )
    {
      //
      // if there is something to be done
      //
      if ( local_indices[icol]!=icol )
      {
        // 
        // swap coefficients 
        //
        complex<Precision> tmp;
        complex<Precision> *p1=&coeff[icol*nrows];
        complex<Precision> *p2=&coeff[local_indices[icol]*nrows];
        for ( int i=0 ; i<nrows ; i++ )
        {
          tmp=p1[i];
          p1[i]=p2[i];
          p2[i]=tmp;
        }
        //
        // find index irow position in local_indices 
        //
        int index = icol <= local_indices;
        //
        // correct order in local_indices
        //
        local_indices[index]=local_indices[icol];
        local_indices[icol]=icol;
      } 
    }
    //
    // align subspace indices with dual channel range indices
    //
    subspace.setCoeffOnTop(local_indices_[my_proc_]);
    //
    // Now, the three objects are aligned, we can project using gemm.
    // As a result of the projection, we should have te coeff stored 
    // in the subspace way, that is columns-wise. Thus, we have to 
    // multiply the dual as row-vectors times the subspace as column 
    // vectors.
    // 
    {
      char TRANSA='T';
      char TRANSB='T';
      int M=local_indices_[my_proc_].size(); // number of chanel coeff on this proc   
      int N=subspace.nVectors();   // number of vector in subspace
      int K=local_indices.size();  // dimension of the reservoir 
      complex<Precision> ALPHA=1.0;
      complex<Precision> *A=&dual_coeff_[0];
      int LDA=local_indices.size();
      complex<Precision> *B=&coeff[0];
      int LDB=subspace.nVectors();
      complex<Precision> BETA=0.0;
      complex<Precision> *C=subspace.coeffPtr();
      int LDC=subspace.localDomain().size();
      lapack::gemm<complex<Precision> >(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
    }
  }
}

template <class Precision> 
Subspace<complex<Precision> >* Reservoir<Precision>::moveToChannelRepresentation( Subspace<Precision> &subspace )
{
  //
  // verify that the dual coefficients have been set 
  //
  if ( dual_coeff_.size()==0 && my_Nloc_!=0 )
  {
    cout << "Error: in - Reservoir::moveToChannelRepresentation - trying to move a subspace to channel representation while no dual has been defined" << endl;  
    exit(0);
  }
  if ( dual_coeff_.size()!=global_indices_.size()*my_Nloc_ )
  {
    cout << "Error: in - Reservoir::moveToChannelRepresentation - incorrect dual size" << endl;  
    exit(0);
  }
  //
  // create a new complex subspace from the current one
  //
  Subspace<complex<Precision> >* complex_subspace = new Subspace<complex<Precision> >(context_);
  (*complex_subspace) = subspace;
  //
  // gather complex subspace coefficients on range processor
  //
  vector<int> domain_indices=global_indices_;
  //
  vector<complex<Precision> > coeff;
  complex_subspace->gatherCoeffsOnProcs(domain_indices,list_proc_,coeff); // <= domain_indices contains now the indices of the gathered coeffs
  //
  // make sure that we found all the coeff of the contact for that subspace
  // or none at all
  //
  if ( global_indices_.size()!=domain_indices.size() && domain_indices.size()!=0 )
  {
    cout << "Error: trying to move an incomplete contact subspace to channel representation in - moveToChannelRepresentation -" << endl;
    exit(0);
  }
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_indices_.count(my_proc_) )
  {
    //
    // find local indices correspondance between subspace coeffs and dual coeffs
    //  
    vector<int> local_indices = domain_indices <= global_indices_;
    //
    // keep dimensions of the subspace
    //
    int ncols=local_indices.size();
    int nrows=complex_subspace->nVectors();
    //
    // reorder subspace coeffs tho match the one of the dual
    //
    for ( int icol=0 ; icol<ncols ; icol++ )
    {
      //
      // if there is something to be done
      //
      if ( local_indices[icol]!=icol )
      {
        // 
        // swap coefficients 
        //
        complex<Precision> tmp;
        complex<Precision> *p1=&coeff[icol*nrows];
        complex<Precision> *p2=&coeff[local_indices[icol]*nrows];
        for ( int i=0 ; i<nrows ; i++ )
        {
          tmp=p1[i];
          p1[i]=p2[i];
          p2[i]=tmp;
        }
        //
        // find index irow position in local_indices 
        //
        int index = icol <= local_indices;
        //
        // correct order in local_indices
        //
        local_indices[index]=local_indices[icol];
        local_indices[icol]=icol;
      } 
    }
    //
    // align subspace indices with dual channel range indices
    //
    complex_subspace->setCoeffOnTop(local_indices_[my_proc_]);
    //
    // Now, the three objects are aligned, we can project using gemm.
    // As a result of the projection, we should have te coeff stored 
    // in the subspace way, that is columns-wise. Thus, we have to 
    // multiply the dual as row-vectors times the subspace as column 
    // vectors.
    // 
    {
      char TRANSA='T';
      char TRANSB='T';
      int M=local_indices_[my_proc_].size();           // number of chanel coeff on this proc
      int N=complex_subspace->nVectors();    // number of vector in subspace
      int K=local_indices.size();            // dimension of the reservoir 
      complex<Precision> ALPHA=1.0;
      complex<Precision> *A=&dual_coeff_[0];
      int LDA=local_indices.size();
      complex<Precision> *B=&coeff[0];
      int LDB=subspace.nVectors();
      complex<Precision> BETA=0.0;
      complex<Precision> *C=complex_subspace->coeffPtr();
      int LDC=complex_subspace->localDomain().size();
      lapack::gemm<complex<Precision> >(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
    }
  }
  //
  // return subspace object address
  //
  return complex_subspace;
}



template <class Precision> 
void Reservoir<Precision>::moveToLocalRepresentation( Subspace<complex<Precision> > &subspace )
{
  //
  // verify that the dual coefficients have been set 
  //
  if ( channel_coeff_.size()==0 && my_Nloc_!=0 )
  {
    cout << "Error: in - Reservoir::moveToChannelRepresentation - trying to move a subspace to channel representation while no dual has been defined" << endl;  
    exit(0);
  }
  if ( channel_coeff_.size()!=global_indices_.size()*my_Nloc_ )
  {
    cout << "Error: in - Reservoir::moveToChannelRepresentation - incorrect dual size" << endl;  
    exit(0);
  }
  //
  // gather subspace coefficients on range processor
  //
  vector<int> domain_indices=global_indices_;
  //
  vector<complex<Precision> > coeff;
  subspace.gatherCoeffsOnProcs(domain_indices,list_proc_,coeff); // <= domain_indices contains now the indices of the gathered coeffs
  //
  // make sure that we found all the coeff of the contact for that subspace
  // or none at all
  //
  if ( global_indices_.size()!=domain_indices.size() && domain_indices.size()!=0 )
  {
    cout << "Error: trying to move an incomplete contact subspace to channel representation in - moveToChannelRepresentation -" << endl;
    exit(0);
  }
  //
  // if this proc support the matrix_pencil range
  //
  if ( local_indices_.count(my_proc_) )
  {
    //
    // find local indices correspondance between subspace coeffs and dual coeffs
    //  
    vector<int> local_indices = domain_indices <= global_indices_;
    //
    // keep dimensions of the subspace
    //
    int ncols=local_indices.size();
    int nrows=subspace.nVectors();
    //
    // reorder subspace coeffs tho match the one of the dual
    //
    for ( int icol=0 ; icol<ncols ; icol++ )
    {
      //
      // if there is something to be done
      //
      if ( local_indices[icol]!=icol )
      {
        // 
        // swap coefficients 
        //
        complex<Precision> tmp;
        complex<Precision> *p1=&coeff[icol*nrows];
        complex<Precision> *p2=&coeff[local_indices[icol]*nrows];
        for ( int i=0 ; i<nrows ; i++ )
        {
          tmp=p1[i];
          p1[i]=p2[i];
          p2[i]=tmp;
        }
        //
        // find index irow position in local_indices 
        //
        int index = icol <= local_indices;
        //
        // correct order in local_indices
        //
        local_indices[index]=local_indices[icol];
        local_indices[icol]=icol;
      } 
    }
    //
    // align subspace indices with dual channel range indices
    //
    subspace.setCoeffOnTop(local_indices_[my_proc_]);
    //
    // Now, the three objects are aligned, we can project using gemm.
    // As a result of the projection, we should have te coeff stored 
    // in the subspace way, that is columns-wise. Thus, we have to 
    // multiply the dual as row-vectors times the subspace as column 
    // vectors.
    // 
    {
      char TRANSA='T';
      char TRANSB='T';
      int M=local_indices_[my_proc_].size(); // number of chanel coeff on this proc   
      int N=subspace.nVectors();   // number of vector in subspace
      int K=local_indices.size();  // dimension of the reservoir 
      complex<Precision> ALPHA=1.0;
      complex<Precision> *A=&channel_coeff_[0];
      int LDA=local_indices.size();
      complex<Precision> *B=&coeff[0];
      int LDB=subspace.nVectors();
      complex<Precision> BETA=0.0;
      complex<Precision> *C=subspace.coeffPtr();
      int LDC=subspace.localDomain().size();
      lapack::gemm<complex<Precision> >(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
    }
  }
}

#endif

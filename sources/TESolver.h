/*! \file TESolver.h
    \brief TESolver class declaration. Contains the implementation of solution solving methods and export methods.
*/

#ifndef TESOLVER_H
#define TESOLVER_H

#include <iostream>
#include "Timer.h"
#include "Context.h"
#include "Reservoir.h"
#include "IntersectionTree.h"

#define TIMING
//#define DEBUG

/// size of vector chunks used for moving to channel representation
#define SIZE_SPLIT_SUBSPACE 256
/// maximum number of iteration to enforce symmetry of the S matrix
#define NMAX_ITERATION_SYMM  50

//! TESolver Class.
/// This class template is intended to implement solution solving methods and export methods. 
template <class Precision, class ReservoirType>
class TESolver
{
  private:
    
    // timers   
    TimerMap tmap_;
    
    // refrence on the tree_
    IntersectionTree<Precision> &tree_;
    
    // reference on the context_
    Context &context_;
    
    // reference on the reservoir list 
    vector<ReservoirType *> &reservoir_list_;
    
    // pointer to the complex subspace object 
    Subspace<complex<Precision> > *complex_subspace_;
    
    // current energy
    double energy_;
    
    // scattering matrix
    vector<complex<Precision> > scattering_coeff_;
    
    // resolution methods
    bool real_parts_method1_(double energy);
    
    // indices for expansion of the kernel before moving to channels
    vector<int> contact_indices_;
    
  public:
    
    // solvind method
    bool solve(int method, double energy);
    
    // printing methods
    void printTransmisionMatrix(ostream & str, string comment, double (*convertEigenvalue)(double)=NULL);
    void printReducedTransmisionMatrix(ostream & str, string comment, double (*convertEigenvalue)(double)=NULL);
    
    // channels export
    template <class BasisType, class StateType>
    void exportL2Channels(string fileName, int reservoir_source, BasisType &basis);
     
    // constructor
    template <class BasisType>
    TESolver(BasisType &basis, vector<ReservoirType *> &reservoir_list, IntersectionTree<Precision> &tree, Context &context);
    
    // destructor
    ~TESolver(void);
};

// constructor
template <class Precision, class ReservoirType>
template <class BasisType>
TESolver<Precision,ReservoirType>::TESolver(BasisType &basis, vector<ReservoirType *> &reservoir_list, IntersectionTree<Precision> &tree, Context &context) :
reservoir_list_(reservoir_list) , tree_(tree) , context_(context)
{
#ifdef TIMING
  //
  // start global timer
  //
  tmap_["total life time"].start();
  //
  // indicate that no subspace is intanciated
  // 
  complex_subspace_=NULL;
#endif
  //
  // get the contact indices for subspace expansion
  //
  contact_indices_=basis.getContactStateIndexList();
}

// destructor
template <class Precision, class ReservoirType>
TESolver<Precision,ReservoirType>::~TESolver(void)
{
#ifdef TIMING
  //
  // stop global timer
  //
  tmap_["total life time"].stop();
  //
  // print the different timers
  //
  tmap_.print(cout,"TESolver:",context_);  
#endif
  //
  // delete complex subspace object if any
  //
  if ( complex_subspace_!=NULL ) delete complex_subspace_;
}

//! Export of the channel coefficients. 
/// So far, the output format is pecific the HarmonicOscillatorBasis

template <class Precision, class ReservoirType>
template <class BasisType, class StateType>
void TESolver<Precision,ReservoirType>::exportL2Channels(string fileName, int reservoir_source, BasisType &basis)
{
  //
  // check that the subspace is set
  //
  if ( complex_subspace_==NULL ) 
  {
    cout << "Error: trying to export density current before setting solution" << endl;
    exit(0); 
  }
  //
  // construct the list of incoming channels from the other reservoirs 
  //
  vector<int> incoming_indices;
  for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
  {
    if ( ireservoir!=reservoir_source )
    {
      vector<int> incoming_tmp=reservoir_list_[ireservoir]->incomingChannelIndices();
      incoming_indices.insert(incoming_indices.end(),incoming_tmp.begin(),incoming_tmp.end());
    }
  }
  //
  // remove the solutions with incoming channels from the other reservoirs 
  //
  complex_subspace_->findOrthogonal(incoming_indices);
  //
  // align with the incoming channels of the selected reservoir
  //
  incoming_indices=reservoir_list_[reservoir_source]->incomingChannelIndices();
  complex_subspace_->alignOnSet(incoming_indices);
  //
  // move back to real representation
  //
  for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
  {
    reservoir_list_[ireservoir]->moveToLocalRepresentation(*complex_subspace_); 
  }
  //
  // keep number of channels
  //
  int nChannels=complex_subspace_->nVectors();
  //
  // export the channels to a file 
  //
  ofstream fstr;
  //
  // loop on each processors
  //
  for ( int iproc=0 ; iproc<context_.nProcs() ; iproc++ )
  {
    //
    // if this is the current proc
    //
    if ( iproc==context_.myProc() )
    {
      if ( iproc==0 ) 
      {     
        fstr.open(fileName.data());
        fstr << "nChannels= " << nChannels << endl;
      }
      else 
      {
        fstr.open(fileName.data(),ofstream::app);
      }
      //
      // get a pointer on the channels coeff
      //
      complex<Precision> *coeff_ptr=complex_subspace_->coeffPtr(0);
      //
      // get the local domain indices
      //
      vector<int> local_domain=complex_subspace_->localDomain();
      //
      // keep size of local domain
      //
      int size_domain=local_domain.size();
      //
      // inform
      // 
      cout << "writting " << size_domain << " states for proc " << iproc << endl;
      //
      // get corresppnding list of local states
      //
      vector<StateType> local_states=basis.getStateList(local_domain);
      //
      // loop on the domain
      //
      for ( int ilocdomain=0 ; ilocdomain<size_domain ; ilocdomain++ )
      {
        fstr.precision(4);
        //
        // get the corresponding domain index
        //
        int idomain=local_domain[ilocdomain];
        //
        // first column is global index of the state
        //
        fstr << local_states[ilocdomain].atomName() << " " << local_states[ilocdomain].direction();
        fstr.width(10);
        fstr << local_domain[ilocdomain] << " ";
        //
        // then print the position
        //
        fstr << fixed << local_states[ilocdomain].center();
        //
        // loop to print the coefficients
        //
        fstr.precision(14);
        for ( int icoeff=0 ; icoeff<nChannels ; icoeff++ )
        {
          fstr.width(26);
          fstr << scientific << coeff_ptr[icoeff*size_domain+ilocdomain].real();
          fstr.width(22);
          fstr << scientific << coeff_ptr[icoeff*size_domain+ilocdomain].imag();    
        }
        fstr << endl;
      }
      //
      // close the file
      //
      fstr.close();
    }
    //
    // synchronize the processors
    //
    MPI_Barrier(MPI_COMM_WORLD);
    //
    // wait a bit
    //
    usleep(1000000);
  }
}
 
//! Driver interface for solving methods

template <class Precision, class ReservoirType>
bool TESolver<Precision,ReservoirType>::solve(int method, double energy)
{
  switch (method)
  {
    case 1:
      
      return real_parts_method1_(energy);
      
    default:
    
      cout << "Error: unknown method " << method << " in - TESolver::solve -" << endl;
      exit(0);
  }
  return 0;
}

//! Solving method #1
/// - Start by finding reservoir channels
/// - Intersect defect solution tree
/// - Expand solutions on to entire contact area  
/// - Move solution into channels coeffs
/// - Remove explosive components
/// - Compute S matrix

template <class Precision, class ReservoirType>
bool TESolver<Precision,ReservoirType>::real_parts_method1_(double energy)
{
#ifdef TIMING
  tmap_["1 - channel solutions"].start();
#endif  
#ifdef DEBUG
  cout << "1 - channel solutions" << endl;
#endif
  //
  // delete previous complex subspace object if defined 
  //
  if ( complex_subspace_!=NULL ) 
  {
    delete complex_subspace_;
    //
    // indicate that no subspace is intanciated
    // 
    complex_subspace_=NULL;
  }
  //
  // keep track of this energy
  //
  energy_=energy;
  //
  // find reservoirs solutions
  //
  {
    int failedReservoir=-1;
    //
    for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
    {
      //
      // find the solutuions for this reservoir
      //
      int gotThisSolution=reservoir_list_[ireservoir]->findChannelSolutions(energy);
      //
      // keep track of return value
      //
      failedReservoir = gotThisSolution ? failedReservoir : ireservoir;  
    }
    //
    // broadcast result
    //
    int result=0;
    int gotSolutions = ( failedReservoir < 0 ) ? 1 : 0;
    MPI_Allreduce( (void *) &gotSolutions, (void *) &result, 1, MPI_INT, MPI_MIN, context_.comm());
    //
    // return if solution was not found
    //
    if ( !result ) 
    {
      if ( failedReservoir>=0 ) cout << "Warning: could not converge channel solutions of reservoir " << failedReservoir 
                                     << " for energy " << energy << " on proc " << context_.myProc() << endl; 
      return false;
    }
  }
#ifdef TIMING
  tmap_["1 - channel solutions"].stop();
  tmap_["2 - channel index lists"].start();
#endif  
#ifdef DEBUG
  cout << "2 - channel index lists" << endl;
#endif
  //
  // synchronize channels
  //
  for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ ) 
  {
    reservoir_list_[ireservoir]->synchronizeChannelsIndices();
  }
  //
  // get the list of explosive, incoming and outgoing channels
  //
  vector<int> explosiv_indices;
  vector<int> evanesce_indices;
  vector<int> incoming_indices;
  vector<int> outgoing_indices;
  for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
  {
    vector<int> explosiv_tmp=reservoir_list_[ireservoir]->explosiveChannelIndices();
    vector<int> evanesce_tmp=reservoir_list_[ireservoir]->evanescentChannelIndices();
    vector<int> incoming_tmp=reservoir_list_[ireservoir]->incomingChannelIndices();
    vector<int> outgoing_tmp=reservoir_list_[ireservoir]->outgoingChannelIndices();
    explosiv_indices.insert(explosiv_indices.end(),explosiv_tmp.begin(),explosiv_tmp.end());
    evanesce_indices.insert(evanesce_indices.end(),evanesce_tmp.begin(),evanesce_tmp.end());
    incoming_indices.insert(incoming_indices.end(),incoming_tmp.begin(),incoming_tmp.end());
    outgoing_indices.insert(outgoing_indices.end(),outgoing_tmp.begin(),outgoing_tmp.end());
  }
#ifdef TIMING
  tmap_["2 - channel index lists"].stop();
#endif  
  //
  // init scattering coeffs
  //
  scattering_coeff_.resize(0);
  //
  // if there is some propagative channels
  //
  if ( incoming_indices.size()>0 )
  {
#ifdef TIMING
    tmap_["3 - tree_ execution"].start();
#endif  
#ifdef DEBUG
    cout << "3 - tree_ execution" << endl;
#endif
    //
    // execute the tree_
    // 
    Subspace<Precision> *subspace=(Subspace<Precision> *)tree_.execute(-energy);
#ifdef TIMING
    tmap_["3 - tree_ execution"].stop();
    tmap_["4 - subspace expansion"].start();
#endif  
#ifdef DEBUG
    cout << "4 - subspace expansion" << endl;
#endif
    //
    // expand the subspace
    // 
    subspace->expand(contact_indices_);
#ifdef TIMING
    tmap_["4 - subspace expansion"].stop();
    tmap_["5 - move to channel"].start();
#endif  
#ifdef DEBUG
    cout << "5 - move to channel" << endl;
    cout << "local subspace size on proc " << context_.myProc() << " is " << subspace->nVectors() << " * " << subspace->localDomain().size() 
         << " , total size is " << subspace->nVectors() << " * " << subspace->domain().size() << endl;
#endif    
    //
    // move to channel representation
    //
    if ( 0 )
    {
      complex_subspace_=reservoir_list_[0]->moveToChannelRepresentation(*subspace); 
      //
      for ( int ireservoir=1 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
      {
        reservoir_list_[ireservoir]->moveToChannelRepresentation(*complex_subspace_); 
      }
      //
      // delete subspace
      //
      tree_.reset();        
    }
    else
    {
      //
      // start by splitting the subspace into blocks
      //
      vector<Subspace<Precision> *> split_subspace;
      //
      for ( int i=subspace->nVectors() ; i>0 ; i-=SIZE_SPLIT_SUBSPACE )
      {
        //
        // copy subspace
        //
        int istart=max(0,i-SIZE_SPLIT_SUBSPACE);
        int nVectors=i-istart;
        split_subspace.push_back(new Subspace<Precision>(*subspace,istart,nVectors));
        //
        // remove copied coefficients
        //
        subspace->discardDirections(istart,nVectors);
      }
      //
      // delete subspace
      //
      tree_.reset();        
      //
      // loop on subspace blocks
      //     
      for ( int i=0 ; i<split_subspace.size() ; i++ ) 
      {
        //
        // move this subspace block into channel representation
        //
        Subspace<complex<Precision> > *complex_split_subspace=reservoir_list_[0]->moveToChannelRepresentation(*(split_subspace[i]));
        //
        // release memory
        //
        delete split_subspace[i];
        //
        // continue moving to channel representation
        //
        for ( int ireservoir=1 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
        {
          reservoir_list_[ireservoir]->moveToChannelRepresentation(*complex_split_subspace); 
        }
        //
        // remove evanescent components if possible (i.e. is the tree has been reduced, so we don't want to keep all coeffs)
        //
        if ( tree_.reduced() ) complex_split_subspace->discardCoeffs(evanesce_indices);
        //
        // append the direction to the first subspace
        //
        if ( i==0 )
        {
          //
          // just keep block as seed for the complex subspace
          //
          complex_subspace_=complex_split_subspace;
        }
        else
        {
          //
          // add the direction to complex subspace
          //
          complex_subspace_->addDirections(*complex_split_subspace);
          //
          // release memory
          //
          delete complex_split_subspace;
        }
      }
    }
#ifdef TIMING
    tmap_["5 - move to channel"].stop();
    tmap_["6 - remove explosive components"].start();
#endif  
#ifdef DEBUG
  cout << "6 - remove explosive components" << endl;
#endif
    //
    // orthogonalize the subspace with explosive channels
    //
    complex_subspace_->findOrthogonal(explosiv_indices);
#ifdef TIMING
    tmap_["6 - remove explosive components"].stop();
    tmap_["7 - gather coeffs"].start();
#endif  
#ifdef DEBUG
    cout << "7 - gather coeffs" << endl;
#endif
    //
    // form the list of propagative channels
    //
    vector<int> propagative_channels=incoming_indices||outgoing_indices;
    //
    // gather the propagative channel on proc 0
    //
    vector<int> list_proc(1,0);
    vector<complex<Precision> > coeff;
    complex_subspace_->gatherCoeffsOnProcs(propagative_channels, list_proc, coeff);
#ifdef TIMING
    tmap_["7 - gather coeffs"].stop();
#endif  
    //
    // if we are on proc 0
    //
#ifdef TIMING
      tmap_["8 - get Transmission on proc 0"].start();
#endif  
#ifdef DEBUG
      cout << "8 - get Transmission on proc 0" << endl;
#endif
    if ( context_.myProc()==0 )
    {
      //
      // find local indices for incoming and outgoing coeff 
      //
      vector<int> local_incoming = incoming_indices <= propagative_channels;
      vector<int> local_outgoing = outgoing_indices <= propagative_channels;
      //
      // allocate arrays for coeff
      //
      vector<complex<Precision> > incoming_coeff(local_incoming.size()*complex_subspace_->nVectors());
      vector<complex<Precision> > outgoing_coeff(local_outgoing.size()*complex_subspace_->nVectors());
      //
      // extract coeff
      //
      for ( int icoeff=0 , ncoeff=incoming_indices.size() ; icoeff<ncoeff ; icoeff++ )
      {
        for ( int ivect=0 , nvect=complex_subspace_->nVectors() ; ivect<nvect ; ivect++ )
        {
          incoming_coeff[icoeff+ivect*ncoeff]=coeff[local_incoming[icoeff]*nvect+ivect];
          outgoing_coeff[icoeff+ivect*ncoeff]=coeff[local_outgoing[icoeff]*nvect+ivect];
        }
      }
      // 
      // inverse outgoing coeffs
      //
      {
        int LDB=outgoing_indices.size();
        int M=LDB;
        int N=LDB;
        int INFO;
        int LWORK;
        vector<int> IPIV(LDB);
        vector<complex<Precision> > COMPLEX_WORK(1);
        //
        // first factorize
        //
        lapack::getrf<complex<Precision> >( &M, &N, &outgoing_coeff[0], &LDB, &IPIV[0], &INFO );
        //
        // check status
        //
        if ( INFO!=0 )
        {
          cout << "Error: in - TESolver::real_part_method1_ - problem during dual inversion in xGETRF, INFO = " << INFO << endl;
          return false; 
        }
        //
        // optimal memory request
        //
        LWORK=-1;
        lapack::getri<complex<Precision> >( &N, &outgoing_coeff[0], &LDB, &IPIV[0], &COMPLEX_WORK[0], &LWORK, &INFO );
        //
        // allocate memory if necessary
        //
        if ( (int)COMPLEX_WORK[0].real()>COMPLEX_WORK.size() ) COMPLEX_WORK.resize((int)COMPLEX_WORK[0].real());
        LWORK=COMPLEX_WORK.size();
        //
        // compute inverse
        //
        lapack::getri<complex<Precision> >( &N, &outgoing_coeff[0], &LDB, &IPIV[0], &COMPLEX_WORK[0], &LWORK, &INFO );
        //
        // check status
        //
        if ( INFO!=0 )
        {
          cout << "Error: in - TESolver::real_part_method1_ - problem during dual inversion in xGETRI, INFO = " << INFO << endl;
          return false; 
        }
      }
      //
      // allocate memory for scattering coefficients
      //
      scattering_coeff_.resize(outgoing_indices.size()*outgoing_indices.size());
      //
      // multiply incomig * outgoing^-1 to find the scattering matrix
      //
      {
        char TRANSA='N';
        char TRANSB='N';
        int  M=outgoing_indices.size();
        int  N=outgoing_indices.size();
        int  K=outgoing_indices.size();
        int  LDA=outgoing_indices.size();
        int  LDB=outgoing_indices.size();
        int  LDC=outgoing_indices.size();
        complex<Precision> ALPHA=1.0;
        complex<Precision>  BETA=0.0;
        complex<Precision> *A=&incoming_coeff[0];
        complex<Precision> *B=&outgoing_coeff[0];
        complex<Precision> *C=&scattering_coeff_[0];
        lapack::gemm<complex<Precision> > (&TRANSA,&TRANSB,&M,&N,&K,&ALPHA,A,&LDA,B,&LDB,&BETA,C,&LDC);
      }
    } // end over my_proc_ == 0 condition
#ifdef TIMING      
      tmap_["8 - get Transmission on proc 0"].stop();
#endif  
  } // end over propagative channel existence condition
  //
  // indicate that everything went well
  //
  return true;
}

//! Full transmission matrix output
/// Give the fully detailled transmission between each channel

template <class Precision, class ReservoirType>
void TESolver<Precision,ReservoirType>::printTransmisionMatrix(ostream &str, string comment, double (*convertEigenvalue)(double))
{
  //
  // if we are on head proc
  //
  if ( context_.myProc()==0 )
  {
    //
    // get the list of incoming and outgoing channels
    //
    vector<int> incoming_indices;
    vector<int> outgoing_indices;
    for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
    {
      vector<int> incoming_tmp=reservoir_list_[ireservoir]->incomingChannelIndices();
      vector<int> outgoing_tmp=reservoir_list_[ireservoir]->outgoingChannelIndices();
      incoming_indices.insert(incoming_indices.end(),incoming_tmp.begin(),incoming_tmp.end());
      outgoing_indices.insert(outgoing_indices.end(),outgoing_tmp.begin(),outgoing_tmp.end());
    }
    //
    // compute line length
    //
    int line_lenght=outgoing_indices.size()*16+reservoir_list_.size()*3+1;
    //
    // init total transmission for precision
    //
    Precision T_total=0;
    //
    // set format
    //
    str.precision(8);
    //
    // print header
    //
    str << endl;
    str << "-------------------------------------------------------------------------" << endl;
    str << "Transmission matrix: " << comment << endl << endl;
    //
    // print the energy
    //
    if ( convertEigenvalue==NULL )
      str << "    energy        = " << energy_ << endl; 
    else
      str << "    energy        = " << (*convertEigenvalue)(energy_) << endl; 
    //
    // print the reservoirs in order
    //
    str << "    reservoir set = { "; 
    for ( int ir=0 ; ir<reservoir_list_.size()-1 ; ir++ )
    {
      str << reservoir_list_[ir]->label() << " , ";
    }
    str << reservoir_list_.back()->label() << " }" << endl << endl;
    //
    // draw the top line
    //
    str << " ";
    for ( int i=0 ; i<line_lenght ; i++ ) str << "-";
    str << endl;
    //
    // draw the matrix
    //
    for ( int ir_in=0 ; ir_in<reservoir_list_.size() ; ir_in++ )
    {
      //
      // get incoming coeff for this reservoir
      //  
      vector<int> in_channels=reservoir_list_[ir_in]->incomingChannelIndices();
      //
      // get local indices for those channels
      //
      vector<int> local_in_channels=in_channels<=incoming_indices;
      //
      // loop on those channels
      //
      for ( int i_in=0 ; i_in<local_in_channels.size() ; i_in++ )
      {
        str << " |";
        //
        // loop on outgoig channel
        //
        for ( int ir_out=0 ; ir_out<reservoir_list_.size() ; ir_out++ )
        {
          //
          // get incoming coeff for this reservoir
          //  
          vector<int> out_channels=reservoir_list_[ir_out]->outgoingChannelIndices();
          //
          // get local indices for those channels
          //
          vector<int> local_out_channels=out_channels<=outgoing_indices;
          //
          // loop on those channels
          //
          for ( int i_out=0 ; i_out<local_out_channels.size() ; i_out++ )
          {
            str.width(16);
            str << scientific << norm(scattering_coeff_[local_in_channels[i_in]+local_out_channels[i_out]*outgoing_indices.size()]);
            T_total+=norm(scattering_coeff_[local_in_channels[i_in]+local_out_channels[i_out]*outgoing_indices.size()]);
          }
          str << "  |";
        }
        str << endl;  
      }
      //
      // draw a line
      //
      str << " ";
      for ( int i=0 ; i<line_lenght ; i++ ) str << "-";
      str << endl;
    }
    //
    // display precision
    //
    str << endl;
    str << "    precision = " << scientific << fabs(T_total-outgoing_indices.size())<< endl;
    str << endl;
    str << "-------------------------------------------------------------------------" << endl; 
  }
}

//! Brief transmission matrix output
/// Give the transmission between each reservoir

template <class Precision, class ReservoirType>
void TESolver<Precision,ReservoirType>::printReducedTransmisionMatrix(ostream &str, string comment, double (*convertEigenvalue)(double))
{
  //
  // if we are on head proc
  //
  if ( context_.myProc()==0 )
  {
    //
    // get the list of incoming and outgoing channels
    //
    vector<int> incoming_indices;
    vector<int> outgoing_indices;
    for ( int ireservoir=0 ; ireservoir<reservoir_list_.size() ; ireservoir++ )
    {
      vector<int> incoming_tmp=reservoir_list_[ireservoir]->incomingChannelIndices();
      vector<int> outgoing_tmp=reservoir_list_[ireservoir]->outgoingChannelIndices();
      incoming_indices.insert(incoming_indices.end(),incoming_tmp.begin(),incoming_tmp.end());
      outgoing_indices.insert(outgoing_indices.end(),outgoing_tmp.begin(),outgoing_tmp.end());
    }
    //
    // compute the transmission matrix
    //
    vector<Precision> Tmatrix(scattering_coeff_.size());
    {
      typename vector<complex<Precision> >::iterator s=scattering_coeff_.begin();
      //
      for ( typename vector<Precision>::iterator t=Tmatrix.begin() , stop=Tmatrix.end() ; t<stop ; t++ , s++ ) (*t)=norm(*s);
    }
    //
    // enforce symetry of the transmission matrix
    //
    bool precisionNotOK=true;
    //
    for ( int it=0 ; it<NMAX_ITERATION_SYMM && precisionNotOK ; it++ )
    {
      //
      // compute col sums
      //
      vector<Precision> col_sum;
      col_sum.reserve(outgoing_indices.size());
      //
      for ( Precision *pcol=&Tmatrix[0], *stop=&Tmatrix[0]+Tmatrix.size() ; pcol<stop ;  )
      {
        Precision sum_col=0.0;
        for ( Precision *stop_col=pcol+incoming_indices.size() ; pcol<stop_col ; pcol++ ) sum_col+=(*pcol);
        col_sum.push_back(sum_col);
      }
      //
      // renorm the columns
      //
      {
        int i=0;
        for ( Precision *pcol=&Tmatrix[0], *stop=&Tmatrix[0]+Tmatrix.size() ; pcol<stop ; i++ )
        {
          Precision factor=1.0/col_sum[i];
          for ( Precision *stop_col=pcol+incoming_indices.size() ; pcol<stop_col ; pcol++ ) (*pcol)*=factor;
        }
      }
      //
      // compute the row sums
      //
      vector<Precision> row_sum;
      row_sum.reserve(incoming_indices.size());
      //
      long inc=incoming_indices.size();
      for ( int irow=0, stop=incoming_indices.size() ; irow<stop ; irow++ )
      {
        Precision sum_row=0.0;
        for ( Precision *prow=&Tmatrix[irow] , *stop=prow+Tmatrix.size() ; prow<stop ; prow+=inc ) sum_row+=(*prow);
        row_sum.push_back(sum_row);
      }
      //
      // renorm the rows
      //
      for ( int irow=0, stop=incoming_indices.size() ; irow<stop ; irow++ )
      {
        Precision factor=1.0/row_sum[irow];
        for ( Precision *prow=&Tmatrix[irow] , *stop=prow+Tmatrix.size() ; prow<stop ; prow+=inc ) (*prow)*=factor;
      }
      //
      // check the precision 
      //
      precisionNotOK=false;
      for ( int i=0 , stop=row_sum.size() ; i<stop ; i++ )
      { 
        Precision prec_this_row=fabs(1.0-row_sum[i]);
        Precision prec_this_col=fabs(1.0-col_sum[i]);
        //
        if ( prec_this_row>1E-10 || prec_this_col>1E-10 )
        {
          precisionNotOK=true;
          break;
        }
      }
    }
    //
    // compute line length
    //
    int line_lenght=reservoir_list_.size()*19+1;
    //
    // set format
    //
    str.precision(8);
    //
    // print header
    //
    str << endl;
    str << "-------------------------------------------------------------------------" << endl;
    str << "Transmission matrix: " << comment << endl << endl;
    //
    // print the energy
    //
    str << scientific;
    if ( convertEigenvalue==NULL )
      str << "    energy        = " << energy_ << endl; 
    else
      str << "    energy        = " << (*convertEigenvalue)(energy_) << endl; 
    //
    // print the reservoirs in order
    //
    str << "    reservoir set = { "; 
    for ( int ir=0 ; ir<reservoir_list_.size()-1 ; ir++ )
    {
      //
      // get number of channels
      //
      int nchannels=reservoir_list_[ir]->incomingChannelIndices().size();
      //
      // print details
      //
      str << reservoir_list_[ir]->label() << " (" << nchannels << "ch.) , ";
    }
    //
    // print last reservoir details
    //
    str << reservoir_list_.back()->label() << " (" 
        << reservoir_list_.back()->incomingChannelIndices().size() << "ch.) }" << endl << endl;
    //
    // draw the top line
    //
    str << " ";
    for ( int i=0 ; i<line_lenght ; i++ ) str << "-";
    str << endl;
    //
    // init total transmission for precision
    //
    Precision T_total=0;
    Precision T12=0;
    //  
    // loop on reservoirs
    //
    for ( int ir_in=0 ; ir_in<reservoir_list_.size() ; ir_in++ )
    {
      str << " |";
      //
      // get incoming coeff for this reservoir
      //  
      vector<int> in_channels=reservoir_list_[ir_in]->incomingChannelIndices();
      //
      // get local indices for those channels
      //
      vector<int> local_in_channels=in_channels<=incoming_indices;
      //
      // loop on outgoig channel
      //
      for ( int ir_out=0 ; ir_out<reservoir_list_.size() ; ir_out++ )
      {
        //
        // get incoming coeff for this reservoir
        //  
        vector<int> out_channels=reservoir_list_[ir_out]->outgoingChannelIndices();
        //
        // get local indices for those channels
        //
        vector<int> local_out_channels=out_channels<=outgoing_indices;
        //
        // loop on those channels
        //
        Precision Tij=0.0;
        //
        for ( int i_in=0 ; i_in<local_in_channels.size() ; i_in++ )
        {
          for ( int i_out=0 ; i_out<local_out_channels.size() ; i_out++ )
          {
            Tij+=Tmatrix[local_in_channels[i_in]+local_out_channels[i_out]*outgoing_indices.size()];
          }
        }
        //
        // display coeff
        //
        str.width(16);
        str << scientific << Tij;
        str << "  |";  
        //
        // add to transmission total
        //
        T_total+=Tij;
        //
        // keep if T12
        //
        if ( ir_in==0 && ir_out==1 ) T12=Tij;
      }
      //
      // go to the line
      //    
      str << endl;  
      //
      // draw a line
      //
      str << " ";
      for ( int i=0 ; i<line_lenght ; i++ ) str << "-";
      str << endl;
    }
    //
    // display precision
    //
    str << endl;
    if ( convertEigenvalue==NULL )
      str << "    T12( " << scientific << energy_ << " )= " << T12 << endl; 
    else
      str << "    T12( " << scientific << (*convertEigenvalue)(energy_) << " )= " << T12 << endl; 
    str << "    precision = " << scientific << fabs(T_total-outgoing_indices.size())<< endl;
    str << endl;
    str << "-------------------------------------------------------------------------" << endl;
  }
}          

#ifdef TIMING
  #undef TIMING
#endif  
#ifdef DEBUG
  #undef DEBUG
#endif  
      
#endif    

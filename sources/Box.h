/*! \file Box.h
    \brief Class representing a box aligned with xyz directions
*/

#ifndef BOX_H
#define BOX_H

#include <vector>
#ifdef CSTDLIB
  #include <cstdlib>
#endif  
#include "Vector3D.h"

//! Box Class.
/// This class template is intended to help manipulate boxes aligned along the xyz directions. 
/// It implements mainly boxes intersection and initialization from points.

template <class Precision>
class Box
{
  private:
    
    Precision xmin_;    ///< xmin value storage
    Precision lx_;      ///< lx length storage
    Precision ymin_;    ///< ymin value storage
    Precision ly_;      ///< ly length storage
    Precision zmin_;    ///< zmin value storage
    Precision lz_;      ///< lz length storage
    
    bool initialized_;  ///< initialization flag
    
  public:
    
    // constructor
    Box(void);
    
    // initialization methods
    void setFromPoints(std::vector<Vector3D<Precision> > points, Vector3D<Precision> T1, Vector3D<Precision> T2);
    void setFromCoordinates(Precision xmin, Precision ymin, Precision zmin, Precision xmax, Precision ymax, Precision zmax);
     
    //! interaction test
    bool interact(Box<Precision> otherbox, Precision cutoff);
    
    //! point appartenance test 
    bool hasPoint(Vector3D<Precision>);
    
    // member access
    Precision xmin(void) { return xmin_; }       ///< xmin value access
    Precision ymin(void) { return ymin_; }       ///< ymin value access
    Precision zmin(void) { return zmin_; }       ///< zmin value access
    Precision xmax(void) { return xmin_+lx_; }   ///< xmax value access
    Precision ymax(void) { return ymin_+ly_; }   ///< ymax value access
    Precision zmax(void) { return zmin_+lz_; }   ///< zmax value access
    Precision lx(void) { return lx_; }           ///< lx length access
    Precision ly(void) { return ly_; }           ///< ly length access
    Precision lz(void) { return lz_; }           ///< lz length access
    
    // destructor
    ~Box(void){}
};

//! constructor 
/// Set initialisation flag to false
template <class Precision>
Box<Precision>::Box(void)
{
  // set dimensions to 0
  lx_=0.0;
  ly_=0.0;
  lz_=0.0;
  xmin_=0.0;
  ymin_=0.0;
  zmin_=0.0;
  
  // set flag so that we know the box is not initialized 
  initialized_=false;
}

//! Box initialization from corner coordinates
template <class Precision> 
void Box<Precision>::setFromCoordinates(Precision xmin, Precision ymin, Precision zmin, Precision xmax, Precision ymax, Precision zmax)
{
  // set box attribute
  xmin_=xmin;
  ymin_=ymin;
  zmin_=zmin;
  lx_=xmax-xmin;
  ly_=ymax-ymin;
  lz_=zmax-zmin;
  
  // test corner values
  if ( lx_<0.0 || ly_<0.0 || lz_<0.0 )
  {
    cout << "Error: incorrect parameters in initialization of Box object";
    exit(0);
  }

  // set flag so that we know the box is initialized 
  initialized_=true;
}

//! Box initialization from point list
template <class Precision> 
void Box<Precision>::setFromPoints(std::vector<Vector3D<Precision> > points, Vector3D<Precision> T1, Vector3D<Precision> T2)
{
  // init min and max to big values 
  xmin_=1.0E10;
  Precision xmax=-1.0E10;
  ymin_=1.0E10;
  Precision ymax=-1.0E10;
  zmin_=1.0E10;
  Precision zmax=-1.0E10;
  
  // loop on the point set 
  for ( int ipoint=0 , stop=points.size() ; ipoint<stop ; ipoint++ )
  {
    if ( xmin_>points[ipoint][0] ) xmin_=points[ipoint][0]; 
    if ( xmax <points[ipoint][0] ) xmax =points[ipoint][0]; 
    if ( ymin_>points[ipoint][1] ) ymin_=points[ipoint][1]; 
    if ( ymax <points[ipoint][1] ) ymax =points[ipoint][1]; 
    if ( zmin_>points[ipoint][2] ) zmin_=points[ipoint][2]; 
    if ( zmax <points[ipoint][2] ) zmax =points[ipoint][2]; 
  }
  
  // get the corner lenghts
  lx_=xmax-xmin_;
  ly_=ymax-ymin_;
  lz_=zmax-zmin_;
  
  // take periodicity into account
  if ( T1[0]!=0.0 || T2[0]!=0.0 ) { lx_=-1.0; }
  if ( T1[1]!=0.0 || T2[1]!=0.0 ) { ly_=-1.0; }
  if ( T1[2]!=0.0 || T2[2]!=0.0 ) { lz_=-1.0; }
  
  // set flag so that we know the box is initialized 
  initialized_=true;
}


//! point containing test
/// \return true when the the point is in or on the box.
template <class Precision> 
bool Box<Precision>::hasPoint(Vector3D<Precision> point)
{
  //
  // test point
  //
  if ( point[0]>=xmin_ && point[0]<=xmin_+lx_ )
  if ( point[1]>=ymin_ && point[1]<=ymin_+ly_ )
  if ( point[2]>=zmin_ && point[2]<=zmin_+lz_ )
  {
    return true;
  }
  
  //
  // if one of the conditions failed
  //
  return false;
}

//! Boxes interaction test
/// \return true when the two boxes are possibly closer than the cutoff distance.
template <class Precision> 
bool Box<Precision>::interact(Box<Precision> otherbox, Precision cutoff)
{
  // verify that the boxes have been initialized
  if ( !initialized_ || !otherbox.initialized_ ) { cout << "Error: trying to intersect boxes before correct initialization" << endl; exit(0); }
  
  // get difference vector between the two corners 
  Precision dx=xmin_-otherbox.xmin_;
  Precision dy=ymin_-otherbox.ymin_;
  Precision dz=zmin_-otherbox.zmin_;
    
  // check intersection for each direction
  bool interact_x = ( dx > 0.0 ) ? ( dx-cutoff < otherbox.lx_ ) : ( -dx-cutoff < lx_ );
  bool interact_y = ( dy > 0.0 ) ? ( dy-cutoff < otherbox.ly_ ) : ( -dy-cutoff < ly_ );
  bool interact_z = ( dz > 0.0 ) ? ( dz-cutoff < otherbox.lz_ ) : ( -dz-cutoff < lz_ );
  
  // check periodicity
  interact_x = interact_x || ( otherbox.lx_<0.0 || lx_<0.0 );
  interact_y = interact_y || ( otherbox.ly_<0.0 || ly_<0.0 );
  interact_z = interact_z || ( otherbox.lz_<0.0 || lz_<0.0 );
 
  // return the result
  return interact_x && interact_y && interact_z;
}

#endif

#ifndef WORKARRAY_H
#define WORKARRAY_H

// Q_ matrix pencil workarray
extern vector<float>            Q_MatrixPencil_WorkArray_float;             // contains some columns of the Q transform, float case
extern vector<double>           Q_MatrixPencil_WorkArray_double;            // contains some columns of the Q transform, double case
extern vector<complex<float> >  Q_MatrixPencil_WorkArray_complex_float;     // contains some columns of the Q transform, complex float case
extern vector<complex<double> > Q_MatrixPencil_WorkArray_complex_double;    // contains some columns of the Q transform, complex double case

template <class Type>
vector<Type>& Q_MatrixPencil_WorkArray(void) 
{
  cout << "Error, undefined type used with Q_MatrixPencil_WorkArray" << endl;
  exit(0);
}  
template <>
vector<float>&            Q_MatrixPencil_WorkArray<float>(void) { return Q_MatrixPencil_WorkArray_float; }
template <>
vector<double>&           Q_MatrixPencil_WorkArray<double>(void) { return Q_MatrixPencil_WorkArray_double; }
template <>
vector<complex<float> >&  Q_MatrixPencil_WorkArray<complex<float> >(void) { return Q_MatrixPencil_WorkArray_complex_float; }
template <>
vector<complex<double> >& Q_MatrixPencil_WorkArray<complex<double> >(void) { return Q_MatrixPencil_WorkArray_complex_double; }

// BUFF_ matrix pencil workarray
extern vector<vector<float> >            BUFF_MatrixPencil_WorkArray_float;           // send buffer for repartition of Q, float case
extern vector<vector<double> >           BUFF_MatrixPencil_WorkArray_double;          // send buffer for repartition of Q, double case
extern vector<vector<complex<float> > >  BUFF_MatrixPencil_WorkArray_complex_float;   // send buffer for repartition of Q, complex float case
extern vector<vector<complex<double> > > BUFF_MatrixPencil_WorkArray_complex_double;  // send buffer for repartition of Q, complex double case

template <class Type>
vector<vector<Type> >& BUFF_MatrixPencil_WorkArray(void) 
{
  cout << "Error, undefined type used with BUFF_MatrixPencil_WorkArray" << endl;
  exit(0);
}  
template <>
vector<vector<float> >&            BUFF_MatrixPencil_WorkArray<float>(void) { return BUFF_MatrixPencil_WorkArray_float; }
template <>
vector<vector<double> >&           BUFF_MatrixPencil_WorkArray<double>(void) { return BUFF_MatrixPencil_WorkArray_double; }
template <>
vector<vector<complex<float> > >&  BUFF_MatrixPencil_WorkArray<complex<float> >(void) { return BUFF_MatrixPencil_WorkArray_complex_float; }
template <>
vector<vector<complex<double> > >& BUFF_MatrixPencil_WorkArray<complex<double> >(void) { return BUFF_MatrixPencil_WorkArray_complex_double; }


#endif

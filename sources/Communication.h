#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#ifndef TAG_MPI_COMMUNICATION
#define TAG_MPI_COMMUNICATION

  #define TAG_KERNEL_COEFF      0
  #define TAG_MPI_LIST_BCAST    1
  #define TAG_MPI_LIST_GATHERV  2
  #define TAG_MPI_LIST_SCATTERV 3

#endif

#include <vector>
#include <iostream>
#include "Context.h"

#define TIMING

using namespace std;

// ************************************************
//
// Data type for MPI routines
//

template <class Type>
MPI_Datatype mpi_type(void) 
{
  cout << "Error: undefined type in template function mpi_type" << endl;
  exit(0); 
}

template <>
MPI_Datatype mpi_type<int>(void) { return MPI_INT; }

template <>
MPI_Datatype mpi_type<float>(void) { return MPI_FLOAT; }

template <>
MPI_Datatype mpi_type<double>(void) { return MPI_DOUBLE; }

template <>
MPI_Datatype mpi_type<complex<float> >(void) { return MPI_FLOAT; }

template <>
MPI_Datatype mpi_type<complex<double> >(void) { return MPI_DOUBLE; }

//
// ************************************************

// ************************************************
//
// Data size factor for MPI routines
//

template <class Type>
int mpi_size(void) 
{
  cout << "Error: undefined type in template function mpi_type" << endl;
  exit(0); 
}

template <>
int mpi_size<int>(void) { return 1; }

template <>
int mpi_size<float>(void) { return 1; }

template <>
int mpi_size<double>(void) { return 1; }

template <>
int mpi_size<complex<float> >(void) { return 2; }

template <>
int mpi_size<complex<double> >(void) { return 2; }

//
// ************************************************

namespace communication {

// ************************************************
//
// immediate send function:
//
//     emulate ISend without having to care about MPI_TYPE 
//

// untimed
template <class Type>
void immediate_send( Type *send_buff, int count, int irecv, int TAG, MPI_Comm comm, MPI_Request *request)
{
  MPI_Isend((void *) send_buff, mpi_size<Type>()*count, mpi_type<Type>(), irecv, TAG, comm, request);
}

// timed
template <class Type>
void immediate_send( Type *send_buff, int count, int irecv, int TAG, MPI_Comm comm, MPI_Request *request, Timer &timer)
{
  timer.start();
  MPI_Isend((void *) send_buff, mpi_size<Type>()*count, mpi_type<Type>(), irecv, TAG, comm, request);
  timer.stop();
}

// ************************************************
//
// immediate receive function:
//
//     emulate IRecv without having to care about MPI_TYPE 
//

// untimed
template <class Type>
void immediate_recv( Type *recv_buff, int count, int isend, int TAG, MPI_Comm comm, MPI_Request *request)
{
  MPI_Irecv((void *) recv_buff, mpi_size<Type>()*count, mpi_type<Type>(), isend, TAG, comm, request);
}

// timed
template <class Type>
void immediate_recv( Type *recv_buff, int count, int isend, int TAG, MPI_Comm comm, MPI_Request *request, Timer &timer)
{
  timer.start();
  MPI_Irecv((void *) recv_buff, mpi_size<Type>()*count, mpi_type<Type>(), isend, TAG, comm, request);
  timer.stop();
}

// ************************************************
//
// broadcast function:
//
//     broadcast count elements of buff from iroot to the 
//     procs defined in the index list
//

// timed
template <class Type>
void broadcast( Type *buff , int count, int iroot, vector<int> idest, Context &context, TimerMap &tmap, string tmap_comment )
{
#ifdef TIMING
  tmap[tmap_comment+" total time"].start();
#endif
  //
  // get processor rank in current context
  //
  int my_proc=0;
  MPI_Comm_rank(context.comm(), &my_proc);
  //
  // add iroot to destination list if not in 
  //
  {
    int i;
    for ( i=0 ; i<idest.size() ; i++ ) if ( idest[i]==iroot ) break;
    //
    // return if the process is not part of the list
    //
    if ( i>=idest.size() ) idest.push_back(iroot);
  }  
  //
  // fast return if possible
  //
  {
    int i;
    for ( i=0 ; i<idest.size() ; i++ ) if ( idest[i]==my_proc ) break;
    //
    // return if the process is not part of the list
    //
    if ( i>=idest.size() ) 
    {
#ifdef TIMING
      tmap[tmap_comment+" total time"].stop();
#endif
      return;
    }
  }  
#ifdef TIMING
  tmap[tmap_comment+" tree"].start();
#endif
  //
  // reorder list with iroot as the first index
  //
  for ( int i=0 ; i<idest.size() ; i++ ) if ( idest[i]==iroot ) { idest[i]=idest[0]; idest[0]=iroot; break; }
  //
  // form the tree 
  //
  vector<int> sendTo;
  int         receiveFrom;
  //
  for ( int first_recv=1 ; first_recv<idest.size() ; first_recv*=2 )
  {
    //
    // determine senders and receivers
    //
    for ( int isend=0 , irecv=first_recv ; isend<first_recv && irecv<idest.size() ; isend++ , irecv++ )
    {
      if ( idest[isend]==my_proc ) 
      {
        sendTo.push_back( idest[irecv] );
      }
      if ( idest[irecv]==my_proc )
      {
        receiveFrom=idest[isend];
      }
    }
  }
#ifdef TIMING
  tmap[tmap_comment+" tree"].stop();
#endif
  //
  // if have something to receive
  // 
  if ( my_proc!=iroot )
  {
    //
    // receive data
    //
#ifdef TIMING
  tmap[tmap_comment+" receive"].start();
#endif
    MPI_Status status;
    MPI_Recv( (void *)buff, mpi_size<Type>()*count, mpi_type<Type>(), receiveFrom, TAG_MPI_LIST_BCAST, context.comm(), &status );
#ifdef TIMING
  tmap[tmap_comment+" receive"].stop();
#endif
  }
  //
  // if have something to send
  //
  vector<MPI_Request> request_list;
  //
  for ( int i=0 ; i<sendTo.size() ; i++ )
  {
#ifdef TIMING
  tmap[tmap_comment+" send"].start();
#endif
    MPI_Request request;
    MPI_Isend( (void *)buff, mpi_size<Type>()*count, mpi_type<Type>(), sendTo[i], TAG_MPI_LIST_BCAST, context.comm(), &request );
    request_list.push_back(request); 
#ifdef TIMING
  tmap[tmap_comment+" send"].stop();
#endif
  }
  //
  // wait for end of comm
  //
  for ( int i=0 ; i<sendTo.size() ; i++ )
  {
#ifdef TIMING
  tmap[tmap_comment+" wait"].start();
#endif
    MPI_Status status;
    MPI_Wait( &request_list[i], &status );
#ifdef TIMING
  tmap[tmap_comment+" wait"].stop();
#endif
  }
#ifdef TIMING
  tmap[tmap_comment+" total time"].stop();
#endif  
}

// untimed
template <class Type>
void broadcast( Type *buff , int count, int iroot, vector<int> idest, Context &context)
{
  //
  // dummy timer and comment
  //
  TimerMap tmap;
  string tmap_comment;
  //
  // start broadcast
  //
  broadcast<Type>(buff , count, iroot, idest, context, tmap, tmap_comment );
}

//
// ************************************************


// ************************************************
//
// gather function:
//
//     gather count elements of each procs defined in the 
//     index list on proc root
//

// untimed
template <class Type>
void gather( Type *sendbuf, Type *recvbuf, vector<int> count, int iroot, vector<int> isend, Context &context )
{
  //
  // check argument dimension
  //  
  if ( isend.size()!=count.size() )
  {
    cout << "Error: isend.size()!=count.size() in MPI_List_Gatherv" << endl;
    exit(0);
  }
  //
  // get processor rank in current context
  //
  int my_proc=0;
  MPI_Comm_rank(context.comm(), &my_proc);
  //
  // find index of proc in active list
  //
  int my_index;
  {
    for ( my_index=0 ; my_index<isend.size() ; my_index++ ) if ( isend[my_index]==my_proc ) break;
    //
    // return if the process is not part of the list
    //
    if ( my_index>=isend.size() ) 
    {
      if ( my_proc==iroot )
      {
        //
        // add empty count and isend to the process
        //
        isend.push_back(iroot);
        count.push_back(0);
      }
      else
      {
        //
        // simply return 
        //
        return;
      }
    }
  }
  //
  // compute displacement for data reception
  //
  vector<int> disp(1,0);
  for ( int i=0 ; i<count.size() ; i++ ) disp.push_back( disp[i]+count[i] );
  //
  // if we are on root process
  //
  if ( my_proc==iroot )
  {
    //
    // start recv
    //
    vector<MPI_Request> request_list;
    //
    for ( int i=0 ; i<isend.size() ; i++ )
    {
      //
      // recv only from a different process
      //
      if ( isend[i]!=my_proc ) 
      {
        MPI_Request request;
        MPI_Irecv( (void *)(recvbuf+disp[i]), mpi_size<Type>()*count[i], mpi_type<Type>(), isend[i], TAG_MPI_LIST_GATHERV, context.comm(), &request );
        request_list.push_back(request); 
      }
    }
    //
    // copy the local array
    //
    Type *source=sendbuf;
    Type *destination=recvbuf+disp[my_index];
    //
    for ( int k=0 , stop=count[my_index] ; k<stop ; k++ ) destination[k]=source[k];
    //
    // wait for end of communications
    //
    for ( int i=0 ; i<request_list.size() ; i++ ) { MPI_Status status; MPI_Wait( &request_list[i], &status ); }
  }
  //
  // if we are on a semnding process
  //
  else
  {
    //
    // send the data
    // 
    MPI_Send( (void *)sendbuf, mpi_size<Type>()*count[my_index], mpi_type<Type>(), iroot, TAG_MPI_LIST_GATHERV, context.comm() );
  }
}

template <class Type>
void gather( Type *sendbuf, Type *recvbuf, vector<int> count, int iroot, vector<int> isend, Context &context, Timer &timer )
{
  timer.start();
  gather<Type>( sendbuf, recvbuf, count, iroot, isend, context );
  timer.stop();
}

//
// ************************************************


// ************************************************
//
// scatter function:
//
//     scatter count elements on each procs defined in the 
//     index list from proc root
//

// untimed
template <class Type>
void scatter( Type *sendbuf, Type *recvbuf, vector<int> count, int iroot, vector<int> irecv, Context &context )
{
  //
  // check argument dimension
  //  
  if ( irecv.size()!=count.size() )
  {
    cout << "Error: isend.size()!=count.size() in MPI_List_Gatherv" << endl;
    exit(0);
  }
  //
  // get processor rank in current context
  //
  int my_proc=0;
  MPI_Comm_rank(context.comm(), &my_proc);
  //
  // find index of proc in active list
  //
  int my_index;
  {
    for ( my_index=0 ; my_index<irecv.size() ; my_index++ ) if ( irecv[my_index]==my_proc ) break;
    //
    // return if the process is not part of the list
    //
    if ( my_index>=irecv.size() ) return;
  }
  //
  // compute displacement for data reception
  //
  vector<int> disp(1,0);
  for ( int i=0 ; i<count.size() ; i++ ) disp.push_back( disp[i]+count[i] );
  //
  // if we are on root process
  //
  if ( my_proc==iroot )
  {
    //
    // start sending
    //
    vector<MPI_Request> request_list;
    //
    for ( int i=0 ; i<irecv.size() ; i++ )
    {
      //
      // send only to a different process
      //
      if ( irecv[i]!=my_proc ) 
      {
        MPI_Request request;
        MPI_Isend( (void *)(sendbuf+disp[i]), mpi_size<Type>()*count[i], mpi_type<Type>(), irecv[i], TAG_MPI_LIST_SCATTERV, context.comm(), &request );
        request_list.push_back(request); 
      }
    }
    //
    // copy the local array
    //
    Type *source=sendbuf+disp[my_index];
    Type *destination=recvbuf;
    //
    for ( int k=0 , stop=count[my_index] ; k<stop ; k++ ) destination[k]=source[k];
    //
    // wait for end of communications
    //
    for ( int i=0 ; i<request_list.size() ; i++ ) { MPI_Status status; MPI_Wait( &request_list[i], &status ); }
  }
  //
  // if we are on a sending process
  //
  else
  {
    //
    // send the data
    // 
    MPI_Status status;
    MPI_Recv( (void *)recvbuf, mpi_size<Type>()*count[my_index], mpi_type<Type>(), iroot, TAG_MPI_LIST_SCATTERV, context.comm(), &status );
  }
}

// timed
template <class Type>
void scatter( Type *sendbuf, Type *recvbuf, vector<int> count, int iroot, vector<int> irecv, Context &context , Timer &timer)
{
  timer.start();
  scatter<Type>( sendbuf, recvbuf, count, iroot, irecv, context );
  timer.stop();
}

//
// ************************************************

}

//
// remove local definitions
//
#ifdef TIMING
  #undef TIMING
#endif

#endif

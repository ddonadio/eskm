#include <vector>
#include <iostream>

using namespace std;

template <class Precision>
void ExternalCoeffInterface(char *fileName, vector<int> &range, vector<int> &domain, vector<int> &full_index_list, Precision *H_coeff)
{
  //
  // open dynamical matrix file
  // 
  ifstream filestr;
  filestr.open (fileName);
  //
  // test opening
  //
  if ( filestr.fail() )
  {
    cout << "could not find file " << fileName << ". Use corresponding .xyz atom list to generate the dynamical matrix" << endl;
    exit(0);  
  }
  //
  // get dimensions
  //
  int size_coeff_array=full_index_list.size();
  int size_range=range.size();
  int size_domain=domain.size();
  //
  // find local indices of the domain
  //
  vector<int> local_index_domain = tryExtraction(domain,full_index_list);
  //
  // form list of extraction indices
  // multiply the domain indices by the H_coeff range size to avoid
  // doing it during extraction 
  //
  vector<int> domain_extraction_indices(size_coeff_array,-1);
  for ( int i=0 ; i<local_index_domain.size() ; i++ ) 
  {
    if ( local_index_domain[i]>=0 ) domain_extraction_indices[local_index_domain[i]]=i;
  }
  //
  // find local indices of the range
  //
  vector<int> local_index_range = tryExtraction(range,full_index_list);
  //
  // form list of extraction indices 
  //
  vector<int> range_extraction_indices(size_coeff_array,-1);
  for ( int i=0 ; i<local_index_range.size() ; i++ )
  {
    if ( local_index_range[i]>=0 ) range_extraction_indices[local_index_range[i]]=i*size_domain;
  }
  //
  // loop on range of dyn matrice stored in the file
  //
  for ( int i_range=0 ; i_range<size_coeff_array ; i_range++ )
  {
    //
    // if this index is part of the range
    //
    if ( range_extraction_indices[i_range]>=0 )
    {
      //
      // keep local range index
      //
      int local_range=range_extraction_indices[i_range];
      //
      // loop on domain of dynamical matrix stored in the file
      //
      for ( int i_domain=0 ; i_domain<size_coeff_array ; i_domain++ )
      {
        //
        // if this index is part of the domain
        //
        if ( domain_extraction_indices[i_domain]>=0 )
        {
          filestr >> skipws >> H_coeff[local_range+domain_extraction_indices[i_domain]];
        }
        //
        // else, skip the coefficient
        //
        else
        {
          char temp[256];
          filestr >> skipws >> temp;
        }
      }
    }
    //
    // else, skip the whole line
    //
    else
    {
      // store number in a char array to avoid conversion
      char temp[256];
      for ( int i=0 ; i<size_coeff_array ; i++ ) filestr >> skipws >> temp;
    }
  }
  // 
  // close the file 
  //
  filestr.close();
}
  


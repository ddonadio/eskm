#include <complex>
#include <vector>

// working arrays for matrix pencils
std::vector<std::vector<float> >                 BUFF_MatrixPencil_WorkArray_float;           // send buffer for repartition of Q, float case
std::vector<std::vector<double> >                BUFF_MatrixPencil_WorkArray_double;          // send buffer for repartition of Q, double case
std::vector<std::vector<std::complex<float> > >  BUFF_MatrixPencil_WorkArray_complex_float;   // send buffer for repartition of Q, complex float case
std::vector<std::vector<std::complex<double> > > BUFF_MatrixPencil_WorkArray_complex_double;  // send buffer for repartition of Q, complex double case

std::vector<float>                      Q_MatrixPencil_WorkArray_float;          // contains some columns of the Q transform, float case
std::vector<double>                     Q_MatrixPencil_WorkArray_double;         // contains some columns of the Q transform, double case
std::vector<std::complex<float> >       Q_MatrixPencil_WorkArray_complex_float;  // contains some columns of the Q transform, complex float case
std::vector<std::complex<double> >      Q_MatrixPencil_WorkArray_complex_double; // contains some columns of the Q transform, complex double case

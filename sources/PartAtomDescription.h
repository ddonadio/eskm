#ifndef PARTATOMDESCRIPTION_H
#define PARTATOMDESCRIPTION_H

#include <fstream>

#include "Atom.h"
#include "FileHandler.h"

class PartAtomDescription
{
  private:
    
    // list of atoms for this part
    vector<Atom<double> > atoms_;
    
    // translation symmetry
    vector<Vector3D<double> > translation_;
    
    // label and flags
    string         label_;
    vector<string> flags_;
    
    // method
    vector<PartAtomDescription *> subdivide_(Vector3D<double> direction, int nDivisions);
    
  public:
    
    // methods
    const vector<Atom<double> >&  atoms(void) { return atoms_; }
    vector<PartAtomDescription *> subdivide(bool verbose=true);
    
    // flags and labels handling
    bool hasFlag(string flag);
    string label(void) { return label_; }
    vector<string> flags(void) { return flags_; }
    
    // translation access
    vector<Vector3D<double> > translation(void) { return translation_; }
    
    // constructors
    PartAtomDescription(void) {}
    PartAtomDescription(FileHandler &fileHandler);
    
    // destructor
    ~PartAtomDescription(void); 
        
  friend class ReservoirAtomDescription;  
};

#endif



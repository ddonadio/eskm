#include <iostream>
#include <math.h>
#include <string>
#include <map>

#include "Sort.h"
#include "PartAtomDescription.h"

PartAtomDescription::~PartAtomDescription(void)
{
}


PartAtomDescription::PartAtomDescription(FileHandler &fileHandler)
{
  char *buff;
  //
  // get part label if any
  //
  buff=fileHandler.lastWord();
  //
  if ( !strcmp( buff,"<part" ) )
  {
    buff=fileHandler.readNextWord();
    char *tmp=strstr(buff,">");
    if ( tmp!=NULL ) 
    { 
      *tmp = 0;
      label_.assign(buff);
    }
  } 
  //
  // allocate memory for list of atoms
  //
  vector<Atom<double> > list_atoms;
  //
  // flag for end of description
  //
  bool endOfPart=false;
  //
  // while there is something to read
  //
  while ( !endOfPart )
  {
    //
    // get the current tag
    //
    buff=fileHandler.readNextWord();
    //
    // if we are on a cell definition 
    //
    if ( !strcmp( buff,"<atoms" ) || !strcmp( buff,"<atoms>" ) )
    {
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // get the atom description
      //
      bool endOfAtoms=false;
      //
      while ( !endOfAtoms )
      {        
        buff=fileHandler.readNextWord();
        //
        if ( !strcmp( buff,"</atoms" ) || !strcmp( buff,"</atoms>" ) )
        {
          endOfAtoms=true;
        }
        else
        {
          //
          // generate atom
          //
          Atom<double> atom;
          atom.name.assign(buff);
          atom.x_pos=atof(fileHandler.readNextWord());
          atom.y_pos=atof(fileHandler.readNextWord());
          atom.z_pos=atof(fileHandler.readNextWord());  
          //
          // add atoms to list
          //        
          list_atoms.push_back(atom);
        }
      }
    }
    //
    // if we are on a repetition definition 
    //
    if ( !strcmp( buff,"<repetition" ) || !strcmp( buff,"<repetition>" ) || !strcmp( buff,"<repetitions" ) || !strcmp( buff,"<repetitions>" ) )
    {
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // get the flags description
      //
      bool endOfRepetitions=false;
      //
      while ( !endOfRepetitions )
      {
        buff=fileHandler.readNextWord();
        //
        if ( !strcmp( buff,"</repetition" ) || !strcmp( buff,"</repetition>" ) || !strcmp( buff,"</repetitions" ) || !strcmp( buff,"</repetitions>" ) )
        {
          endOfRepetitions=true;
        }
        else
        {
          //
          // check that we have less than 2 repetitions
          //
          if ( translation_.size() == 2 )
          {
            cout << "Error: more than 2 repetitions were found in part definition" << endl;
            exit(0);
          }
          //
          // read the repetition
          //
          double x,y,z;
          x=atof(buff);
          y=atof(fileHandler.readNextWord());
          z=atof(fileHandler.readNextWord());
          //
          // add to the list of repetition
          //
          translation_.push_back(Vector3D<double>(x,y,z));  
        }
      }
    } 
    //
    // if we are on a flag definition 
    //
    if ( !strcmp( buff,"<flag" ) || !strcmp( buff,"<flags" ) || !strcmp( buff,"<flag>" ) || !strcmp( buff,"<flags>" ) )
    {
      //
      // go to the line
      //
      fileHandler.skipLine();
      //
      // get the flags description
      //
      bool endOfFlags=false;
      //
      while ( !endOfFlags )
      {
        //
        // read the flag
        //
        fileHandler.readNextWord();
        //
        if ( !strcmp( buff,"</flag" ) || !strcmp( buff,"</flag>" ) || !strcmp( buff,"</flags" ) || !strcmp( buff,"</flags>" ) )
        {
          endOfFlags=true;
        } 
        else
        {
          //
          // get the flag in a string
          //
          string flag;
          flag.assign(buff);
          //
          // append the string to the flag list
          //
          flags_.push_back(flag);
        } 
      }
    }
    //
    // if we hit the end of the reservoir description 
    //   
    if ( !strcmp( buff,"</part" ) || !strcmp( buff,"</part>" ) ) endOfPart=true; 
    //
    // if we hit the end of the file, there is a problem
    //
    if ( fileHandler.eof() )
    {
      cout << "Error while reading part description" << endl;
      exit(0);
    }
  }
  //
  // check that the description is complete
  //
  if ( list_atoms.size()==0 )
  {
    cout << "Error: a defect part definition requires at least some atoms" << endl;
    exit(0);
  }
  //
  // keep list of atoms
  //
  atoms_=list_atoms;
}

bool PartAtomDescription::hasFlag(string flag)
{
  for ( int i=0 ; i<flags_.size() ; i++ ) if ( flags_[i]==flag ) return true;
  return false;
}

vector<PartAtomDescription *> PartAtomDescription::subdivide(bool verbose)
{
  //
  // get init number of atoms
  //
  int n_atoms_init=atoms_.size();
  //
  // get the subdivisions parameters
  //
  vector<Vector3D<double> > directions;
  vector<int>               nSubdivisions;
  //
  // extract from the flag
  //
  for ( int i=0 ; i<flags_.size() ; i++ )
  {
    if ( flags_[i]=="subdivide" )
    {
      //
      // check at least that there is enough arguments...
      //
      if ( i+4>=flags_.size() )
      {
        cout << "Error: subdivide flag takes 4 arguments (subdivide xDir yDir zDir nDiv)" << endl;
        exit(0); 
      }
      //
      // extract arguments
      //
      double x,y,z;
      x=atof(flags_[i+1].data());
      y=atof(flags_[i+2].data());
      z=atof(flags_[i+3].data());
      directions.push_back(Vector3D<double>(x,y,z));
      nSubdivisions.push_back(atoi(flags_[i+4].data()));
    }
  }
  //
  // loop on the subdivision
  // 
  vector<PartAtomDescription *> part_list;
  part_list.push_back(this);
  //
  for ( int iDiv=0 ; iDiv<nSubdivisions.size() ; iDiv++ )
  {
    //
    // init the new part list
    //
    vector<PartAtomDescription *> new_part_list;
    //
    // loop on the current part list
    //
    for ( int ipart=0 ; ipart<part_list.size() ; ipart++ )
    {
      //
      // subdivide this part
      //
      vector<PartAtomDescription *> part_list_tmp=part_list[ipart]->subdivide_(directions[iDiv], nSubdivisions[iDiv]);
      //
      // insert in new part list
      //
      new_part_list.insert(new_part_list.end(),part_list_tmp.begin(),part_list_tmp.end());
      //
      // delete old part
      //
      if ( part_list[ipart]!=this ) delete part_list[ipart]; 
    }
    //
    // keep this new list
    //
    part_list=new_part_list;
  }
  //
  // count the number of atoms in each parts
  //
  map<int,int> n_atoms;
  for ( int ipart=0 ; ipart<part_list.size() ; ipart++ )
  {
    n_atoms[ part_list[ipart]->atoms_.size() ]++;
  }
  //
  // recapitulate
  //
  if ( verbose )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "Part description subdivision:" << endl << endl;
    for ( map<int,int>::iterator it=n_atoms.begin() ; it!=n_atoms.end() ; it++ )
    {
      cout << "    created " <<  it->second << " part(s) containing " << it->first << " atoms" << endl;
    } 
    cout << "-------------------------------------------------------------------------" << endl;
  }
  //
  // return the part list
  //
  return part_list;
}

vector<PartAtomDescription *> PartAtomDescription::subdivide_(Vector3D<double> direction, int nDivisions)
{
  //
  // check that the number of divisions is lower than the actual number of atoms
  //
  if ( nDivisions>atoms_.size() )
  {
    cout << "Error: number of subdivision " << nDivisions << " too high for part" << endl;
    exit(0);
  }
  //
  // create a basis from the given direction
  //
  Vector3D<double> V1=direction/sqrt(direction*direction);
  Vector3D<double> V2; V2[0]=V1[1]; V2[1]=V1[2]; V2[2]=-V1[0]; V2=V2-V1*V2; V2=V2/sqrt(V2*V2);
  Vector3D<double> V3=V1^V2; V3=V3/sqrt(V3*V3);
  //
  // copy atom list
  //
  vector<Atom<double> > list_atoms=atoms_;
  //
  // move atom list in the new coordinates
  //  
  for ( int i=0 ; i<list_atoms.size() ; i++ ) list_atoms[i].changeBasis(V3,V2,V1);
  //
  // construct a list of indices
  //
  vector<int> index_list(list_atoms.size());
  for ( int i=0 ; i<list_atoms.size() ; i++ ) index_list[i]=i;
  //
  // sort the list of atoms and keep the order
  //
  sort<Atom<double> >(&list_atoms[0],list_atoms.size(),&index_list[0]);
  //
  // compute the size of the new lists
  //
  vector<int> size_lists(nDivisions,list_atoms.size()/nDivisions);
  for ( int i=0 , j=nDivisions*(list_atoms.size()/nDivisions) ; j<list_atoms.size() && i<nDivisions ; i++ , j++ ) size_lists[i]++;
  //
  // create the new PartAtomDescription
  //
  vector<PartAtomDescription *> part_description(nDivisions);
  //
  for ( int ipart=0 , ilist=0 ; ipart<nDivisions ; ipart++ )
  {
    part_description[ipart]=new PartAtomDescription();
    //
    // set repetitions label and flags
    //
    part_description[ipart]->label_ = label_;
    part_description[ipart]->flags_ = flags_;
    part_description[ipart]->translation_ = translation_;
    //
    // set atoms of this part 
    //
    part_description[ipart]->atoms_.resize(size_lists[ipart]);
    for ( int i=0 ; i<size_lists[ipart] ; i++ , ilist++ ) 
    {
      part_description[ipart]->atoms_[i]=atoms_[index_list[ilist]];
    }
  }
  //
  // return the list of new parts
  //
  return part_description;
}



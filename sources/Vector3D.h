#ifndef VECTOR3D_H
#define VECTOR3D_H

#include <iostream>

using namespace std;

template <class CoordinatePrecision>
class Vector3D
{
  private:
    
    CoordinatePrecision V[3];
     
  public:
    
    // constructor
    Vector3D( void );
    Vector3D( const Vector3D &other ) { V[0]=other.V[0]; V[1]=other.V[1]; V[2]=other.V[2]; }
    Vector3D( CoordinatePrecision x, CoordinatePrecision y, CoordinatePrecision z ) { V[0]=x; V[1]=y; V[2]=z; }
    
    // comparison operators
    bool operator==( Vector3D<CoordinatePrecision> other );
    bool operator!=( Vector3D<CoordinatePrecision> other );

    // coefficient access
    CoordinatePrecision& operator[](int i) { return V[i]; }  

    // extern (scalar) product
    CoordinatePrecision  operator* ( Vector3D<CoordinatePrecision> other );
    
    // copy operator
    Vector3D<CoordinatePrecision>& operator =( Vector3D<CoordinatePrecision> other );
    
    // vectorial product
    Vector3D<CoordinatePrecision>  operator ^( Vector3D<CoordinatePrecision> other );
    
    // addition operator
    Vector3D<CoordinatePrecision>  operator +( Vector3D<CoordinatePrecision> other );
    Vector3D<CoordinatePrecision>& operator+=( Vector3D<CoordinatePrecision> other );
    
    // soustraction operator
    Vector3D<CoordinatePrecision>  operator -( Vector3D<CoordinatePrecision> other );
    Vector3D<CoordinatePrecision>& operator-=( Vector3D<CoordinatePrecision> other );
        
    // scalar addition
    Vector3D<CoordinatePrecision>  operator +( CoordinatePrecision a );
    Vector3D<CoordinatePrecision>& operator+=( CoordinatePrecision a );
    
    // scalar soustraction
    Vector3D<CoordinatePrecision>  operator -( CoordinatePrecision a );
    Vector3D<CoordinatePrecision>& operator-=( CoordinatePrecision a );

    // scalar multiplication
    Vector3D<CoordinatePrecision>  operator *( CoordinatePrecision a );
    Vector3D<CoordinatePrecision>& operator*=( CoordinatePrecision a );
    
    // scalar division
    Vector3D<CoordinatePrecision>  operator /( CoordinatePrecision a );
    Vector3D<CoordinatePrecision>& operator/=( CoordinatePrecision a );
    
    // desctructor
    ~Vector3D(void) {}
};

// constructors
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>::Vector3D( void ) 
{ 
  V[0]=0.0; 
  V[1]=0.0; 
  V[2]=0.0;
}

// scalar product
template <class CoordinatePrecision>
CoordinatePrecision Vector3D<CoordinatePrecision>::operator *( Vector3D<CoordinatePrecision> other ) 
{ 
  return V[0]*other.V[0]+V[1]*other.V[1]+V[2]*other.V[2]; 
}

// copy operator
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator =( Vector3D<CoordinatePrecision> other ) 
{ 
  V[0]=other.V[0]; 
  V[1]=other.V[1]; 
  V[2]=other.V[2]; 
  return *this; 
}

// comparison operator
template <class CoordinatePrecision>
bool Vector3D<CoordinatePrecision>::operator==( Vector3D<CoordinatePrecision> other ) 
{ 
  return V[0]==other.V[0] && V[1]==other.V[1] && V[2]==other.V[2]; 
}

// comparison operator
template <class CoordinatePrecision>
bool Vector3D<CoordinatePrecision>::operator!=( Vector3D<CoordinatePrecision> other ) 
{ 
  return V[0]!=other.V[0] || V[1]!=other.V[1] || V[2]!=other.V[2]; 
}

// addition
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator+=( Vector3D<CoordinatePrecision> other ) 
{ 
  V[0]+=other.V[0];
  V[1]+=other.V[1]; 
  V[2]+=other.V[2]; 
  return *this; 
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator +( Vector3D<CoordinatePrecision> other )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[0]+other.V[0];
  tmp[1]=V[1]+other.V[1];
  tmp[2]=V[2]+other.V[2];
  return tmp;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator+=( CoordinatePrecision a ) 
{ 
  V[0]+=a;
  V[1]+=a; 
  V[2]+=a; 
  return *this; 
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator +( CoordinatePrecision a )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[0]+a;
  tmp[1]=V[1]+a;
  tmp[2]=V[2]+a;
  return tmp;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> operator +( CoordinatePrecision a , Vector3D<CoordinatePrecision> other )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=other[0]+a;
  tmp[1]=other[1]+a;
  tmp[2]=other[2]+a;
  return tmp;
}

// soustraction
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator-=( Vector3D<CoordinatePrecision> other ) 
{ 
  V[0]-=other.V[0]; 
  V[1]-=other.V[1]; 
  V[2]-=other.V[2]; 
  return *this; 
}
   
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator -( Vector3D<CoordinatePrecision> other )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[0]-other.V[0];
  tmp[1]=V[1]-other.V[1];
  tmp[2]=V[2]-other.V[2];
  return tmp;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator-=( CoordinatePrecision a ) 
{ 
  V[0]-=a;
  V[1]-=a; 
  V[2]-=a; 
  return *this; 
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator -( CoordinatePrecision a )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[0]-a;
  tmp[1]=V[1]-a;
  tmp[2]=V[2]-a;
  return tmp;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> operator -( CoordinatePrecision a , Vector3D<CoordinatePrecision> other )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=a-other[0];
  tmp[1]=a-other[1];
  tmp[2]=a-other[2];
  return tmp;
}

// scalar multiplication
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator*=( CoordinatePrecision a ) 
{ 
  V[0]*=a; 
  V[1]*=a; 
  V[2]*=a; 
  return *this; 
}
    
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator *( CoordinatePrecision a )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[0]*a;
  tmp[1]=V[1]*a;
  tmp[2]=V[2]*a;
  return tmp;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> operator *( CoordinatePrecision a , Vector3D<CoordinatePrecision> other )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=other[0]*a;
  tmp[1]=other[1]*a;
  tmp[2]=other[2]*a;
  return tmp;
}

// scalar division
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Vector3D<CoordinatePrecision>::operator/=( CoordinatePrecision a ) 
{ 
  V[0]/=a; 
  V[1]/=a; 
  V[2]/=a; 
  return *this; 
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator /( CoordinatePrecision a )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[0]/a;
  tmp[1]=V[1]/a;
  tmp[2]=V[2]/a;
  return tmp;
}

// vectorial product
template <class CoordinatePrecision>
Vector3D<CoordinatePrecision> Vector3D<CoordinatePrecision>::operator ^( Vector3D<CoordinatePrecision> other )
{
  Vector3D<CoordinatePrecision> tmp;
  tmp[0]=V[1]*other.V[2]-V[2]*other.V[1];
  tmp[1]=V[2]*other.V[0]-V[0]*other.V[2];
  tmp[2]=V[0]*other.V[1]-V[1]*other.V[0];
  return tmp;
}

// display function
template <class CoordinatePrecision>
ostream& operator<<( ostream &str , Vector3D<CoordinatePrecision> v )
{
  str << "( " << v[0] << " , " << v[1] << " , " << v[2] << " )";
  //
  return str;
} 

#endif

/*! \file ProcessorGrid.h
    \brief Grid repartition saving and loading functions prototypes
*/

#ifndef PROCESSORGRID_H
#define PROCESSORGRID_H

#include <vector>

/// saveGroups save the groups generated through spectral clustering 
/// to a text editable file. This file can be modify manually or used as 
/// it is for the next runs using the same processorgrid for the same system. 
template <class StateType, class SystemDescriptionType>
void saveGroups(string fileName, Basis<StateType,SystemDescriptionType> &basis, vector<vector<int> > groups)
{
  //
  // open the file for writing 
  //
  ofstream filestr;
  filestr.open (fileName.data());
  //
  // write first the number of groups
  //
  filestr << "<nClusters " << groups.size() << ">" << endl << endl;
  //
  // write the processor mapping of the groups
  //
  filestr << "<processorMapping>" << endl;
  for ( int i=0 ; i<groups.size() ; ) 
  {
    for ( int j=0 ; j<10 && i<groups.size() ; j++ , i++ )
    {
      filestr.width(6); filestr << i; 
    }
    filestr << endl;
  }
  filestr << "</processorMapping>" << endl << endl;
  //
  // write the groups 
  //
  for ( int igroup=0 ; igroup<groups.size() ; igroup++ )
  {
    filestr << "<group " << igroup << ">" << endl;
    for ( int iel=0 , stop=groups[igroup].size() ; iel<stop ; )
    {
      for ( int i=0 ; i<10 && iel<stop ; i++ , iel++ )
      {
        filestr.width(7); filestr << groups[igroup][iel];
      }
      filestr << endl;
    }
    filestr << "</group>" << endl << endl;
  }
  //
  // collect indices of the contact
  //
  map<int,vector<int> > contact_parts;
  for ( int ipart=0 , ireservoir=0 ; ipart<basis.nDefectParts() ; ipart++ )
  {
    if ( basis.isContact(ipart,&ireservoir) ) contact_parts[ireservoir].push_back(ipart);
  }
  //
  // print indices of the contacts for info
  //
  for ( map<int,vector<int> >::iterator it=contact_parts.begin() ; it!=contact_parts.end() ; it++ )
  {
    filestr << "<contact " << it->first << ">" << endl;
    for ( int iel=0 , stop=it->second.size() ; iel<stop ; )
    {
      for ( int i=0 ; i<10 && iel<stop ; i++ , iel++ )
      {
        filestr.width(7); filestr << it->second[iel];
      }
      filestr << endl;
    }
    filestr << "</contact>" << endl << endl;
  }
  //
  // close the file
  //
  filestr.close();
}

/// loadGroups load the groups generated through spectral clustering 
/// from a text editable file. 
template <class StateType, class SystemDescriptionType>
vector<vector<int> > loadGroups(string fileName, Basis<StateType,SystemDescriptionType> &basis)
{
  //
  // init the groups
  //
  int nParts=0;
  int nClusters=0;
  vector<int> processorMapping;
  vector<vector<int> > groups;
  vector<vector<int> > contacts;
  //
  // check if the file exist and return if not 
  //
  ifstream filestr;
  filestr.open( fileName.data() );
  if ( filestr.fail() ) return groups;
  //
  // open the file with a file handler
  //
  FileHandler fileHandler;
  fileHandler.open(fileName.data());
  //
  // read the file
  //
  char *buff;
  while ( !fileHandler.eof() )
  {
    buff=fileHandler.readNextWord();
    //
    // if we are on a group definition
    // 
    if ( !strcmp(buff,"<nClusters") ) 
    {
      //
      // read the number of clusters
      //
      buff=fileHandler.readNextWord();
      char *tmp=strstr(buff,">");
      if ( tmp!=NULL ) *tmp = 0;
      nClusters=atoi(buff);
    }
    if ( !strcmp(buff,"<nClusters>") ) 
    {
      //
      // read the number of clusters
      //
      buff=fileHandler.readNextWord();
      nClusters=atoi(buff);
    }
    //
    // if we are on a group definition
    //
    if ( !strcmp(buff,"<group") ) 
    {
      //
      // read the index of this group
      //
      buff=fileHandler.readNextWord();
      char *tmp=strstr(buff,">");
      if ( tmp!=NULL ) *tmp = 0;
      int igroup=atoi(buff);
      //
      // allocate memory if necessary
      //
      if ( igroup>= groups.size() ) groups.resize(igroup+1);
      //
      // read the part indices in this group
      //
      bool endOfGroup=false;
      while ( !endOfGroup )
      {
        buff=fileHandler.readNextWord();
        if ( !strcmp(buff,"</group>") ) endOfGroup=true;
        else 
        {
          groups[igroup].push_back( atoi(buff) ); 
          nParts++; 
        }
      }
    }
    //
    // if we are on a contact definition
    //
    if ( !strcmp(buff,"<contact") ) 
    {
      //
      // read the index of this group
      //
      buff=fileHandler.readNextWord();
      char *tmp=strstr(buff,">");
      if ( tmp!=NULL ) *tmp = 0;
      int icontact=atoi(buff);
      //
      // allocate memory if necessary
      //
      if ( icontact>= contacts.size() ) contacts.resize(icontact+1);
      //
      // read the part indices in this group
      //
      bool endOfContact=false;
      while ( !endOfContact )
      {
        buff=fileHandler.readNextWord();
        if ( !strcmp(buff,"</contact>") ) endOfContact=true;
        else contacts[icontact].push_back( atoi(buff) );
      }
    }
    //
    // if we are on a mapping definition
    //
    if ( !strcmp(buff,"<processorMapping>") ) 
    {
      //
      // read the group indices
      //
      bool endOfMapping=false;
      while ( !endOfMapping )
      {
        buff=fileHandler.readNextWord();
        if ( !strcmp(buff,"</processorMapping>") ) endOfMapping=true;
        else processorMapping.push_back( atoi(buff) );
      }
    }
  }
  //
  // check that everyting is correct
  //
  if ( nParts!=basis.nDefectParts() ) 
  {
    cout << "Error: incorrect number of part " << nParts << " found in processor grid file. ";
    cout << basis.nDefectParts() << " expected instead." << endl;
    exit(0);
  }
  if ( nClusters!=groups.size() )
  {
    cout << "Error: incorrect number of groups " << groups.size() << " defined in processor grid file.";
    cout << nClusters << " expected instead." << endl;
    exit(0);
  }
  if ( processorMapping.size()!=0 && processorMapping.size()!=nClusters )
  {
    cout << "Error: incorrect number of processor indices " << processorMapping.size() << " for processor mapping in processor grid file.";
    cout << nClusters << " expected instead." << endl;
    exit(0);
  }
  //
  // order groups according to processor mapping
  //
  vector<vector<int> > ordered_groups;
  if ( processorMapping.size()==0 ) ordered_groups=groups;
  else
  {
    ordered_groups.resize(groups.size());
    for ( int igroup=0 ; igroup<groups.size() ; igroup++ ) ordered_groups[processorMapping[igroup]]=groups[igroup];
  }
  //
  // return the result
  //
  return ordered_groups;
}

#endif

/*! \file Dos.h
    \brief Dos class and related function declarations
*/

#ifndef DOS_H
#define DOS_H

#include "Sort.h"

//! Dos Class.
/// \brief This class template is intended to help manipulating density of states for export. 

template <class ObjectType>
class Dos
{
  public:
  
    ObjectType object;        ///< object (atom for example) from wich the dos is defined
    double     value;         ///< value of the dos
    int        globalIndex;   ///< global index of the corresponding state
};

//! \brief Extract density of states from channels
/// \param  iglobal       contains global indices of the states 
/// \param  atom[i]       contains object associated to the states
/// \param  channel[i](j] contains coefficient of state i for the jth channel   
/// \retval dos           contains density of state list

template <class ObjectType>
void computeDos(vector<int> &iglobal, vector<ObjectType> &object_list, vector<vector<complex<double> > > &channel, vector<Dos<ObjectType> > &dos)
{
  //
  // init and reserve dos for every states
  //
  dos.resize(0);
  dos.reserve(channel.size());
  //
  for ( int istate=0 ; istate<channel.size() ; istate++ )
  {
    //
    // create and init new dos object
    //
    Dos<ObjectType> mydos;
    //
    mydos.value=0.0;
    mydos.object=object_list[istate];
    mydos.globalIndex=iglobal[istate];
    //
    // accumulate on the channels coeff for this state
    //
    for ( vector<complex<double> >::iterator it=channel[istate].begin() , stop=channel[istate].end() ; it!=stop ; it++ )
    {
      mydos.value+=norm(*it);
    }
    //
    // store state dos value
    //
    dos.push_back(mydos);
  }
}

//! \brief Sort dos according to object. 
/// \param dos reference a vector of Dos object
/// \retval dos the list is sorted such that i<j => dos[i].object<=dos[j].object

template <class ObjectType>
void sortByObject( vector<Dos<ObjectType> > &dos )
{
  //
  // allocate memory for object and index lists
  //
  vector<size_t> index_list; index_list.reserve(dos.size());
  vector<ObjectType> object_list; object_list.reserve(dos.size());
  //
  // fill the lists
  //
  for ( size_t idos=0 ; idos<dos.size() ; idos++ ) 
  {
    index_list.push_back(idos);
    object_list.push_back(dos[idos].object);
  }
  //
  // sort
  //
  sort<ObjectType>(&object_list[0],object_list.size(),&index_list[0]);
  //
  // reorder the dos
  //
  vector<Dos<ObjectType> > sorted_dos; sorted_dos.reserve(dos.size());
  for ( size_t idos=0 ; idos<dos.size() ; idos++ ) sorted_dos.push_back(dos[index_list[idos]]);
  //
  // set ordered dos list
  //
  dos=sorted_dos;
}

//! \brief Construct object list from the dos.
/// \param dos reference a vector of dos object
/// \return return the list of object constructed from the object attributes 

template <class ObjectType>
vector<ObjectType> dosObjectList( vector<Dos<ObjectType> > &dos )
{
  //
  // start by sorting the dos list according to objects
  //
  sortByObject<ObjectType>( dos );
  //
  // allocate object list
  //
  vector<ObjectType> object_list;
  //
  // init object list
  //
  if ( dos.size()>0 ) object_list.push_back(dos[0].object);
  //
  // loop on the remaining dos
  //
  for ( size_t idos=1 , stop=dos.size() ; idos<stop ; idos++ )
  {
    if ( !( object_list.back() == dos[idos].object ) ) object_list.push_back(dos[idos].object);
  }
  //
  // return the list of objects
  //
  return object_list;
}

//! \brief Construct list of global indices from the dos.
/// \param dos reference a vector of Dos object
/// \retval globalIndex contains the list of indices constructed from globalIndex attribute 

template <class ObjectType>
void dosGlobalIndexLists( vector<Dos<ObjectType> > &dos, vector<int> &globalIndex )
{
  //
  // reset lists of global indices 
  //
  globalIndex.resize(0);
  //
  // reserve memory
  //
  globalIndex.reserve(dos.size());
  //
  // loop on dos to construct list
  //
  for ( size_t idos=0 ; idos<dos.size() ; idos++ )
  {
    globalIndex.push_back(dos[idos].globalIndex);
  }
}

//! \brief Duplicate dos object according to possible translations symmetry 
/// \retval dos is extended with periodic elements

template <class BasisType, class ObjectType> 
void dosUtil_applyPartTranslations_(BasisType &basis, vector<Dos<ObjectType> > &dos)
{
  //
  // form the lists of global indices in the dos list
  //
  vector<int> ilist; 
  dosGlobalIndexLists( dos, ilist );
  //
  // apply translation for each part
  //
  for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ )
  {
    //
    // get defect part translations
    //  
    vector<Vector3D<double> > translations=basis.getPartTranslations(ipart);
    //
    // if there is any translation for this part
    //
    if ( translations.size()>0 )
    {
      // 
      // get global index list for this part
      //
      vector<int> part_indices=basis.getDefectPartIndices(ipart);
      //
      // extract the dos indices belonging to this part
      //
      vector<int> idos=weakExtraction( part_indices, ilist );
      //
      // swich according to number of translations
      //
      switch ( translations.size() )
      {
        case 1:
        {
          //
          // loop on the local flux
          //
          for ( int i=0 ; i<idos.size() ; i++ )
          {
            //
            // duplicate dos
            //
            Dos<ObjectType> mydos=dos[idos[i]];
            //
            // translate and add duplicates
            //
            mydos.object+=translations[0];
            dos.push_back(mydos);
            mydos.object-=translations[0];
            mydos.object-=translations[0];
            dos.push_back(mydos);
          }
          //
          // break 
          //
          break;
        }  
        case 2:
        {
          //
          // loop on the local flux
          //
          for ( int i=0 ; i<idos.size() ; i++ )
          {
            //
            // duplicate dos
            //
            Dos<ObjectType> mydos=dos[idos[i]];
            //
            // translate and add duplicates
            //
            mydos.object+=translations[0];
            dos.push_back(mydos);
            mydos.object+=translations[1];
            dos.push_back(mydos);
            mydos.object-=translations[0];
            dos.push_back(mydos);
            mydos.object-=translations[0];
            dos.push_back(mydos);
            mydos.object-=translations[1];
            dos.push_back(mydos);
            mydos.object-=translations[1];
            dos.push_back(mydos);
            mydos.object+=translations[0];
            dos.push_back(mydos);
            mydos.object+=translations[0];
            dos.push_back(mydos);
          }
          //
          // break 
          //
          break;
        }
        default:
        {
          cout << "Error: dosUtil_applyPartTranslations_ not implemented for more than 2 translations on a single part" << endl;
          exit(0);
        }  
      }      
    }
  }
}

//! \brief Apply reservoir propagation symmetry on the dos
/// \retval dos has been extended with nrep repetitions of the reservoirs

template <class BasisType, class SystemDescription, class ObjectType> 
void dosUtil_extendReservoirDos_(BasisType &basis, SystemDescription &system_description, vector<Dos<ObjectType > > &dos)
{
  //
  // form the lists of global indices in dos list
  //
  vector<int> ilist; 
  dosGlobalIndexLists( dos, ilist );
  //
  // apply reservoir translation on reservoir dos 
  //
  for ( int ireservoir=0 ; ireservoir<basis.nReservoirs() ; ireservoir++ )
  {
    // 
    // get global index list for this reservoir
    //
    vector<int> reservoir_indices=basis.getReservoirIndices(ireservoir);
    //
    // get the reservoir propagation
    //
    Vector3D<double> propagation=2.0*system_description.reservoir(ireservoir)->propagation();
    //
    // extract the indices of reservoir dos
    //
    vector<int> idos=weakExtraction( reservoir_indices, ilist );
    //
    // inform
    //
    cout << "duplicating " << idos.size() << " LDOS for reservoir " <<  ireservoir << " with propagation " << propagation << endl;
    //
    // add translated dos
    //
    for ( int i=0 ; i<idos.size() ; i++ )
    {
      //
      // copy dos info
      //
      Dos<ObjectType> mydos=dos[idos[i]];
      //
      // discard global indices
      //
      mydos.globalIndex=-1;
      //
      // translate object
      // 
      mydos.object+=propagation;
      //
      // add dos to the list
      //
      dos.push_back(mydos);
    }
  }
}

//! \brief Compress the ldos on similar objects
/// \retval dos is now composed by a list of distinct object

template <class ObjectType> 
void dosUtil_compressDos_(vector<Dos<ObjectType > > &dos)
{
  //
  // start sorting according to objects
  //
  sortByObject( dos );
  //
  // compress dos on similar objects
  //
  int icomp=0;
  //
  for ( int idos=1 , stop=dos.size() ; idos<stop ; idos++ )
  {
    //
    // if this object differs from the compressed one
    //
    if ( dos[idos].object!=dos[icomp].object )
    {
      //
      // increment icomp
      //
      icomp++;
      //
      // set new compressed object
      //
      dos[icomp].object=dos[idos].object;
      //
      // set new compresed global index
      //
      dos[icomp].globalIndex=dos[idos].globalIndex;
      //
      // set dos value
      //
      dos[icomp].value=dos[idos].value;
    }
    //
    // else, if this object is the same as the compressed
    //
    else
    {
      //
      // accumulate dos
      //
      dos[icomp].value+=dos[idos].value;
    }
  }
  //
  // resize dos list
  //
  dos.resize(icomp);
}



//! \brief export flux to a povray df3 density file
/// \param fileName file for output
/// \param flux list of flux to be exported
/// \param box box containing the flux to be exported (full range if not initialized)
/// \param clip_box list of clipping boxes applied to the representation
/// \param nx number of points for x direction
/// \param ny number of points for y direction
/// \param nz number of points for z direction
/// \param cutoff cutoff distance for flux computation
/// \param alpha width of gaussian for flux density representation
/// \param projection direction of projection for flux representation

template <class Precision, class ObjectType>
void printLDosDensityGrid(string fileName, vector<Dos<ObjectType> > &dos, Box<Precision> box, vector<Box<Precision> > clip_box, int nx, int ny, int nz, Precision cutoff, Precision alpha)
{
 
#define BLOC_SIZE 16        
#define INTERNAL_PRECISION float
#define INTERNAL_EPSILON 1E-4

  //
  // determine the box
  //
  INTERNAL_PRECISION xmin= 1E10;
  INTERNAL_PRECISION xmax=-1E10;
  INTERNAL_PRECISION ymin= 1E10;
  INTERNAL_PRECISION ymax=-1E10;
  INTERNAL_PRECISION zmin= 1E10;
  INTERNAL_PRECISION zmax=-1E10;
  //
  if ( box.xmin() || box.xmax() || box.ymin() || box.ymax() || box.zmin() || box.zmax() )
  {
    xmin=box.xmin();
    xmax=box.xmax();
    ymin=box.ymin();
    ymax=box.ymax();
    zmin=box.zmin();
    zmax=box.zmax();
  }
  else
  {
    for ( int idos=0 ; idos<dos.size() ; idos++ )
    {
      Vector3D<Precision> position=dos[idos].object.coordinates();
      //
      if ( position[0]>xmax ) xmax=position[0];
      if ( position[0]<xmin ) xmin=position[0];
      if ( position[1]>ymax ) ymax=position[1];
      if ( position[1]<ymin ) ymin=position[1];
      if ( position[2]>zmax ) zmax=position[2];
      if ( position[2]<zmin ) zmin=position[2];
    }
  }
  
  //
  // define grid spacing
  //
  INTERNAL_PRECISION dx=(xmax-xmin)/(nx-1);
  INTERNAL_PRECISION dy=(ymax-ymin)/(ny-1);
  INTERNAL_PRECISION dz=(zmax-zmin)/(nz-1);  
  //
  // divide into subdomains of size >=cutoff
  //
  int Nx=max( 1 , (int) ((xmax-xmin)/(cutoff)) );
  int Ny=max( 1 , (int) ((ymax-ymin)/(cutoff)) );
  int Nz=max( 1 , (int) ((zmax-zmin)/(cutoff)) );
  //
  // get size of subdomains
  //
  INTERNAL_PRECISION lx=(xmax-xmin+dx)/Nx;
  INTERNAL_PRECISION ly=(ymax-ymin+dy)/Ny;
  INTERNAL_PRECISION lz=(zmax-zmin+dz)/Nz;

cout << "LDOS Extraction:" << Nx << endl; 
cout << "Nx  =" << Nx << endl; 
cout << "Ny  =" << Ny << endl; 
cout << "Nz  =" << Nz << endl; 
cout << "lx  =" << lx << endl; 
cout << "ly  =" << ly << endl; 
cout << "lz  =" << lz << endl; 
cout << "dx  =" << dx << endl;
cout << "dy  =" << dy << endl;
cout << "dz  =" << dz << endl;
cout << "xmin=" << xmin << endl;
cout << "ymin=" << ymin << endl;
cout << "zmin=" << zmin << endl;
cout << "xmax=" << xmax << endl;
cout << "ymax=" << ymax << endl;
cout << "zmax=" << zmax << endl;

  //
  // precompute integral
  //
  int  n_step_x=2048;
  INTERNAL_PRECISION srq_cutoff=cutoff*cutoff;
  INTERNAL_PRECISION one_over_sqrt_pi=0.564189583547756;
  INTERNAL_PRECISION sqrt_alpha=sqrt(alpha);
  INTERNAL_PRECISION step_x=2.0*cutoff/n_step_x*alpha;
//  INTERNAL_PRECISION step_x_times_alpha=step_x*alpha;
//  INTERNAL_PRECISION one_over_step_x_times_alpha=1.0/step_x_times_alpha;
//  INTERNAL_PRECISION minus_one_over_step_x_times_alpha=-1.0/step_x_times_alpha;
//  INTERNAL_PRECISION x_max_in_precomp_integral=2.0*cutoff*alpha;
//  INTERNAL_PRECISION minus_x_max_in_precomp_integral=-2.0*cutoff*alpha;
  
  int n_step_h_sqr=2048;
  INTERNAL_PRECISION one_over_alpha_sqr=1.0/(alpha*alpha);
  INTERNAL_PRECISION step_h_sqr=srq_cutoff/n_step_h_sqr;
  INTERNAL_PRECISION one_over_step_h_sqr=1.0/step_h_sqr;
  vector<INTERNAL_PRECISION> precomp_exponential(n_step_h_sqr+1);
  for ( int i=0 ; i<n_step_h_sqr+1 ; i++ ) precomp_exponential[i]=exp(-i*step_h_sqr*one_over_alpha_sqr);  
  
  //
  // loop on the subdomains
  //
  vector<Vector3D<INTERNAL_PRECISION> > density(nx*ny*nz);
  //
  for ( int ix=0 , it=0 ; ix<Nx ; ix++ )
  {
    INTERNAL_PRECISION x_min_at=xmin+ix*lx-cutoff;
    INTERNAL_PRECISION x_max_at=xmin+ix*lx+cutoff+lx;
    //
    // construct list of objects possibly in the range
    //
    vector<Dos<ObjectType > > local_dos_x;
    local_dos_x.reserve(dos.size());
    for ( int idos=0 ; idos<dos.size() ; idos++ ) 
    {
      //
      // get position of this object
      //
      Vector3D<Precision> position=dos[idos].object.coordinates();
      //
      // keep dos if its object is in the box 
      //
      if ( x_min_at <= position[0] && position[0] <= x_max_at ) local_dos_x.push_back(dos[idos]);
    }
        
    //
    // loop on y coordinate
    //
    for ( int iy=0 ; iy<Ny ; iy++ )
    {
      INTERNAL_PRECISION y_min_at=ymin+iy*ly-cutoff;
      INTERNAL_PRECISION y_max_at=ymin+iy*ly+cutoff+ly;
      //
      // construct list of object potentially in the range
      //
      vector<Dos<ObjectType > > local_dos_y;
      local_dos_y.reserve(local_dos_x.size());
      for ( int idos=0 ; idos<local_dos_x.size() ; idos++ ) 
      {
        //
        // get position of this object
        //
        Vector3D<Precision> position=local_dos_x[idos].object.coordinates();
        //
        // keep dos if its object is in the box 
        //
        if ( y_min_at <= position[1] && position[1] <= y_max_at ) local_dos_y.push_back(local_dos_x[idos]);
      }
      
      //
      // loop on z coordinate
      //
      for ( int iz=0 ; iz<Nz ; iz++ )
      {
        INTERNAL_PRECISION z_min_at=zmin+iz*lz-cutoff;
        INTERNAL_PRECISION z_max_at=zmin+iz*lz+cutoff+lz;
        //
        // construct list of object in the range
        //
        vector<Dos<ObjectType > > local_dos_z;
        local_dos_z.reserve(local_dos_y.size());
        for ( int idos=0 ; idos<local_dos_y.size() ; idos++ ) 
        {
          //
          // get position of this object
          //
          Vector3D<Precision> position=local_dos_y[idos].object.coordinates();
          //
          // keep dos if its object is in the box 
          //
          if ( z_min_at <= position[2] && position[2] <= z_max_at ) local_dos_z.push_back(local_dos_y[idos]);
        }
        
        //
        // now we have a list of restricted dos => loop on the local grid points 
        //
        INTERNAL_PRECISION local_minx=xmin+ix*lx-INTERNAL_EPSILON;
        INTERNAL_PRECISION local_maxx=xmin+ix*lx+lx-INTERNAL_EPSILON;
        INTERNAL_PRECISION x_start=xmin; int ix_start=0; while ( x_start<local_minx ) { ix_start++; x_start+=dx; }
        //
        INTERNAL_PRECISION local_miny=ymin+iy*ly-INTERNAL_EPSILON;
        INTERNAL_PRECISION local_maxy=ymin+iy*ly+ly-INTERNAL_EPSILON;
        INTERNAL_PRECISION y_start=ymin; int iy_start=0; while ( y_start<local_miny ) { iy_start++; y_start+=dy; }
        //
        INTERNAL_PRECISION local_minz=zmin+iz*lz-INTERNAL_EPSILON;
        INTERNAL_PRECISION local_maxz=zmin+iz*lz+lz-INTERNAL_EPSILON;
        INTERNAL_PRECISION z_start=zmin; int iz_start=0; while ( z_start<local_minz ) { iz_start++; z_start+=dz; }
        //
        // increase upper boundaries if we are on the last chunk to avoid missing last point
        //
        if ( ix==Nx-1 ) local_maxx+=dx;
        if ( iy==Ny-1 ) local_maxy+=dy; 
        if ( iz==Nz-1 ) local_maxz+=dz; 
        //
        INTERNAL_PRECISION x,y,z;
        int                i,j,k;
                
        //
        // print info
        //

        if ( it==100 )
        {
          cout << " stage ";
          cout.width(4); cout << ix+1 << " ";
          cout.width(4); cout << iy+1 << " ";
          cout.width(4); cout << iz+1 << "    of ";
          cout.width(4); cout << Nx << " ";
          cout.width(4); cout << Ny << " ";
          cout.width(4); cout << Nz << "         nflux=" << local_dos_z.size() << endl;
          it=0;
        }
        it++;
        //
        // bloc the algo to avoid cache miss on density[index]
        // => work on part of density of dim BLOC_SIZE³
        //
        int ibloc=1;
        int ix_start_;
        int iy_start_;
        int iz_start_;
        INTERNAL_PRECISION x_start_;
        INTERNAL_PRECISION y_start_;
        INTERNAL_PRECISION z_start_;    
        //    
        for ( ix_start_=ix_start , x_start_=x_start ; x_start_<local_maxx ; ix_start_+=BLOC_SIZE , x_start_+=BLOC_SIZE*dx )
        for ( iy_start_=iy_start , y_start_=y_start ; y_start_<local_maxy ; iy_start_+=BLOC_SIZE , y_start_+=BLOC_SIZE*dy )
        for ( iz_start_=iz_start , z_start_=z_start ; z_start_<local_maxz ; iz_start_+=BLOC_SIZE , z_start_+=BLOC_SIZE*dz )
        {  
          //
          // compute this bloc stops (substract a small epsilon to avoid floating point comparison problem) 
          //
          INTERNAL_PRECISION local_maxx_=min(local_maxx,x_start_+BLOC_SIZE*dx-(INTERNAL_PRECISION)(INTERNAL_EPSILON));
          INTERNAL_PRECISION local_maxy_=min(local_maxy,y_start_+BLOC_SIZE*dy-(INTERNAL_PRECISION)(INTERNAL_EPSILON));
          INTERNAL_PRECISION local_maxz_=min(local_maxz,z_start_+BLOC_SIZE*dz-(INTERNAL_PRECISION)(INTERNAL_EPSILON));
          //
          // check that the stops are ok
          //
          for ( i=ix_start_ , x=x_start_ ; x<local_maxx_ ; i++ , x+=dx );
          while  ( i>nx ) { local_maxx_-=dx; i--; }
          //   
          for ( j=iy_start_ , y=y_start_ ; y<local_maxy_ ; j++ , y+=dy );
          while  ( j>ny ) { local_maxy_-=dy; j--; }
          //
          for ( k=iz_start_ , z=z_start_ ; z<local_maxz_ ; k++ , z+=dz );
          while  ( k>nz ) { local_maxz_-=dz; k--; }
          
          //
          // loop on the dos close to this area  
          //
          for ( int idos=0 ; idos<local_dos_z.size() ; idos++ ) 
          {        
            //
            // get position in internal precision
            //
            Vector3D<Precision> position_user_perc=local_dos_z[idos].object.coordinates();
            Vector3D<INTERNAL_PRECISION> position(position_user_perc[0],position_user_perc[1],position_user_perc[2]);
            
            //
            // get value for this dos
            //
            INTERNAL_PRECISION value=local_dos_z[idos].value;
                      
            //
            // loop on the points in this subdomain
            //          
            for ( i=ix_start_ , x=x_start_ ; x<local_maxx_ ; i++ , x+=dx )
            for ( j=iy_start_ , y=y_start_ ; y<local_maxy_ ; j++ , y+=dy )
            for ( k=iz_start_ , z=z_start_ ; z<local_maxz_ ; k++ , z+=dz )
            {
              
              //
              // compute distance
              //
              INTERNAL_PRECISION dx=x-position[0];
              INTERNAL_PRECISION dy=y-position[1];
              INTERNAL_PRECISION dz=z-position[2];
              INTERNAL_PRECISION d_sqr=dx*dx+dy*dy+dz*dz;
              
              //
              // if the distance is small enough
              //
              if ( d_sqr<srq_cutoff )
              {
                //
                // get the corresponding index 
                //
                int index=i+j*nx+k*nx*ny;
                // 
                // estimate the value of the gaussian
                // those have been calculated in advance to gain some time
                //
                /*
                INTERNAL_PRECISION prefactor = exp(-d_sqr*one_over_alpha_sqr);
                */
                INTERNAL_PRECISION prefactor = precomp_exponential[d_sqr*one_over_step_h_sqr];
                // 
                // add contribution to the density
                //
                density[index]+=value*prefactor;
              }
            }
          }
        }
      } 
    }
  }
  //
  // gat max and min value of the density
  //
  INTERNAL_PRECISION dmax=0.0;
  INTERNAL_PRECISION dmin=1E+10;
  for ( int i=0 ; i<density.size() ; i++ )
  {
    INTERNAL_PRECISION value=density[i]*density[i];
    if ( dmax<value ) dmax=value;
    if ( dmin>value ) dmin=value;
  }
  dmax=sqrt(dmax);
  dmin=sqrt(dmin);
  //
  // inform 
  //
  cout << "density min = " << dmin << endl;
  cout << "density max = " << dmax << endl;
  //
  // print density slices on standard output
  //
  for ( int i=0 , j=0 , k=0 ; i<density.size() ; i++ , j++ )
  {
    if ( j==nx ) 
    {
      cout << endl;
      j=0;
      k++;
    }
    if ( k==ny ) 
    {
      cout << endl;
      k=0;
    }
    INTERNAL_PRECISION value=sqrt(density[i]*density[i]);
    cout << (int)(9*value/dmax);
  }
  
  char buff[256];
  ofstream filestr;
  //
  // density map for povray
  //
  sprintf(buff,"%s.df3",fileName.data());
  filestr.open (buff);
  filestr << (unsigned char)(nx >> 8);
  filestr << (unsigned char)(nx & 0xff);
  filestr << (unsigned char)(ny >> 8);
  filestr << (unsigned char)(ny & 0xff);
  filestr << (unsigned char)(nz >> 8);
  filestr << (unsigned char)(nz & 0xff);
  for ( int i=0 ; i<density.size() ; i++ )
  {
    INTERNAL_PRECISION v=sqrt(density[i]*density[i]);
    int value=min(255,max(0,(int)(255*(v-dmin)/dmax)));
    // red component
    filestr << (unsigned char)value;
  }
  filestr.close();
  
  //
  // density vector field
  //
  sprintf(buff,"%s.vec",fileName.data());
  filestr.open (buff);
  filestr << "ngrid = " << nx << " " << ny << " " << nz << endl;
  filestr << "xmin  = " << xmin        << " , ymin = " << ymin      << " , zmin = " << zmin << endl;
  filestr << "Lx    = " << xmax-xmin   << " , Ly   = " << ymax-ymin << " , Lz   = " << zmax-zmin << endl;
  
  for ( int i=0 ; i<nx ; i++ )
  for ( int j=0 ; j<ny ; j++ )
  for ( int k=0 ; k<nz ; k++ )
  {
    filestr << i << " " << j << " " << k << " " << density[i+j*nx+k*nx*ny] << endl;
  }
  filestr.close();
}


#endif


#ifndef L2RESERVOIRCELLS_H
#define L2RESERVOIRCELLS_H

class L2ReservoirCells
{
  private:
  
    vector<vector<vector<int> > > cell_indices_;
    
  public:
    
    // number of repetitions
    int nrep1(void) { return cell_indices_.size(); }
    int nrep2(void) { return cell_indices_.size()>0 ? cell_indices_[0].size() : 0; }
    
    // access to the indices 
    vector<int> repIndexList(int irep1, int irep2)
    {
      // check indices
      if ( irep1>=cell_indices_.size() )
      {
        cout << "Error: incorect repetition index " << irep1 << " for repetition 1 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      if ( cell_indices_.size()==0 || irep2>=cell_indices_[0].size() )
      {
        cout << "Error: incorect repetition index " << irep2 << " for repetition 2 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      // return list
      return cell_indices_[irep1][irep2];
    }
    
    // access to a slice indices
    vector<int> sliceIndexList(int islice)
    {
      // check indices
      if ( islice>3 or islice<1 )
      {
        cout << "Error: incorect slice index " << islice << " for repetition 1 in - L2ReservoirCells::rsliceIndexList" << endl;
        exit(0);
      }
      // get dimension of one slice
      int size_center=cell_indices_[0][0].size()/3;
      // form list
      vector<int> indices;
      for ( int irep1=0 ; irep1<cell_indices_.size() ; irep1++ )
      for ( int irep2=0 ; irep2<cell_indices_[irep1].size() ; irep2++ )
      {
        indices.insert(indices.end(),cell_indices_[irep1][irep2].begin()+size_center*(islice-1),cell_indices_[irep1][irep2].begin()+size_center*islice);
      }
      // return list
      return indices;
    }
    
    // access to the slice 1 indices
    vector<int> slice1RepIndexList(int irep1, int irep2)
    {
      // check indices
      if ( irep1>=cell_indices_.size() )
      {
        cout << "Error: incorect repetition index " << irep1 << " for repetition 1 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      if ( cell_indices_.size()==0 || irep2>=cell_indices_[0].size() )
      {
        cout << "Error: incorect repetition index " << irep2 << " for repetition 2 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      // get dimension of one slice
      int size_center=cell_indices_[0][0].size()/3;
      // form list
      vector<int> indices;
      indices.insert(indices.end(),cell_indices_[irep1][irep2].begin(),cell_indices_[irep1][irep2].begin()+size_center);
      // return list
      return indices;
    }
    
    // access to the slice 2 indices
    vector<int> slice2RepIndexList(int irep1, int irep2)
    {
      // check indices
      if ( irep1>=cell_indices_.size() )
      {
        cout << "Error: incorect repetition index " << irep1 << " for repetition 1 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      if ( cell_indices_.size()==0 || irep2>=cell_indices_[0].size() )
      {
        cout << "Error: incorect repetition index " << irep2 << " for repetition 2 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      // get dimension of one slice
      int size_center=cell_indices_[0][0].size()/3;
      // form list
      vector<int> indices;
      indices.insert(indices.end(),cell_indices_[irep1][irep2].begin()+size_center,cell_indices_[irep1][irep2].begin()+2*size_center);
      // return list
      return indices;
    }
    
    // access to the slice 3 indices
    vector<int> slice3RepIndexList(int irep1, int irep2)
    {
      // check indices
      if ( irep1>=cell_indices_.size() )
      {
        cout << "Error: incorect repetition index " << irep1 << " for repetition 1 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      if ( cell_indices_.size()==0 || irep2>=cell_indices_[0].size() )
      {
        cout << "Error: incorect repetition index " << irep2 << " for repetition 2 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      // get dimension of one slice
      int size_center=cell_indices_[0][0].size()/3;
      // form list
      vector<int> indices;
      indices.insert(indices.end(),cell_indices_[irep1][irep2].begin()+2*size_center,cell_indices_[irep1][irep2].end());
      // return list
      return indices;
    }
    
    // access to the slices 1 and 2 indices
    vector<int> slices12RepIndexList(int irep1, int irep2)
    {
      // check indices
      if ( irep1>=cell_indices_.size() )
      {
        cout << "Error: incorect repetition index " << irep1 << " for repetition 1 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      if ( cell_indices_.size()==0 || irep2>=cell_indices_[0].size() )
      {
        cout << "Error: incorect repetition index " << irep2 << " for repetition 2 in - L2ReservoirCells::repIndexList" << endl;
        exit(0);
      }
      // get dimension of one slice
      int size_center=cell_indices_[0][0].size()/3;
      // form list
      vector<int> indices;
      indices.insert(indices.end(),cell_indices_[irep1][irep2].begin(),cell_indices_[irep1][irep2].begin()+2*size_center);
      // return list
      return indices;
    }
    
    // constructor
    L2ReservoirCells(vector<vector<vector<int> > > cell_indices) { cell_indices_=cell_indices; }
    
    // destructor
    ~L2ReservoirCells(){}
};

#endif

#ifndef TPCHART_H
#define TPCHART_H

// std lib includes
#include <map>
#include <vector>
#include <iostream>

// specific includes
#include "Sort.h"

// macro defines 
#define STOCK       1
#define REQUIREMENT 2
#define HORIZONTAL  1
#define VERTICAL    2
//#define DEBUG

// using std lib namespace
using namespace std;

// ********************************************************
// class TPCell represent one cell in the TPChart
// TPChart should be arranged as an array of TPCells
// Each cell contains info on previous/next cells in terms
// of cost along both columns and rows
// ********************************************************

template <class FlowPrecision, class CostPrecision>
class TPCell
{
  public:
    
    // navigation in the Chart
    TPCell *prev_cost_in_col;
    TPCell *next_cost_in_col;
    TPCell *prev_cost_in_row;
    TPCell *next_cost_in_row;
    
    // values associated to the cell   
    int   irow;
    int   icol;
    FlowPrecision flow;
    CostPrecision cost;
    CostPrecision dual;
    
    // constructor/destructor
    TPCell(){}
   ~TPCell(){}
};

// ********************************************************
// class TPRequirement represent the requirements of the 
// TPChart. For each requirements, it also keeps the first 
// and second best associated TPCell (i.e. in the column) 
// in terms of cost.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
class TPRequirement
{
  public:
    
    FlowPrecision  value;
    CostPrecision  deficit;
    TPCell<FlowPrecision,CostPrecision> *firstBestCell;
    TPCell<FlowPrecision,CostPrecision> *secondBestCell;
    
    // constructor/destructor
    TPRequirement(){}
   ~TPRequirement(){}
};

// ********************************************************
// class TPStock represent the stocks of the TPChart. 
// For each requirements, it also keeps the first and 
// second best associated TPCell (i.e. in the row) in 
// terms of cost.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
class TPStock
{
  public:
    
    FlowPrecision  value;
    CostPrecision  deficit;
    TPCell<FlowPrecision,CostPrecision> *firstBestCell;
    TPCell<FlowPrecision,CostPrecision> *secondBestCell;
    
    // constructor/destructor
    TPStock(){}
   ~TPStock(){}
};

// ********************************************************
// class TPFlow represent the flows of the TPChart. 
// ********************************************************

template <class FlowPrecision>
class TPFlow
{
  public:
  
    int istock;
    int irequirement;
    FlowPrecision flow;

    // constructor/destructor
    TPFlow(){};
   ~TPFlow(){};   
};

// ********************************************************
// class BSLeaf represent the leaf of the Basic Feasible 
// Solution. 
// ********************************************************

template <class FlowPrecision>
class BFSLeaf
{
  public: 
  
    int icol;
    int irow;
    FlowPrecision flow;
    
    BFSLeaf<FlowPrecision>           *parent;
    vector<BFSLeaf<FlowPrecision> *> children;
    
    // constructor/destructor
    BFSLeaf(){};
   ~BFSLeaf(){};       
};

// ********************************************************
// class TPDeficit is used to sort the deficit of the 
// stocks and requirements
// ********************************************************

class TPDeficit
{
  public:
    
    int type;
    int index;
    
    // constructor/destructor
    TPDeficit(){}
   ~TPDeficit(){}
};

// ********************************************************
// class TPChart is the head class. it represent completely
// the Transportation problem.
// The cost, requirements and stocks are give at the 
// creation of the class object.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
class TPChart
{
  private:
    
    // private attribute
    int                                                     m_;                 // number of rows of the chart (i.e number of stock, including dummy if total requirement > total stock )
    int                                                     n_;                 // number of columns of the chart (i.e number of require, including dummy if total requirement < total stock )
    int                                                     nFlow_;              
    bool                                                    BFS_set_;
    bool                                                    dummy_stock_;       // flag indicated if a dummy has been added at creation
    bool                                                    dummy_requirement_; // flag indicated if a dummy has been added at creation
    FlowPrecision                                           epsilon_flow_;      // tolerance for a stock/requirement to be considered as empty, should be non zero if dealing with floating point flows
    CostPrecision                                           epsilon_slack_;      // tolerance for a dual slack to be considered as empty, should be non zero if dealing with floating point flows
    vector<CostPrecision>                                   dual_stock_;        // duals of stock, used only for optimization of the BFS
    vector<CostPrecision>                                   dual_requirement_;  // duals of requirements, used only for optimization of the BFS
    vector<TPCell<FlowPrecision,CostPrecision> >            cell_;              // cells of the TPChart
    vector<TPStock<FlowPrecision,CostPrecision> >           stock_;             // stocks of the TPChart 
    vector<TPRequirement<FlowPrecision,CostPrecision> >     requirement_;       // requirements of the TPChart
    multimap<CostPrecision,TPDeficit>                       deficit_;           // deficites for either stocks of requirements (see macro definition for type correspondance)
    map<int,map<int,FlowPrecision> >                        flowMap_;            // contains the optimized solution
    map<int,map<int,FlowPrecision> >                        remainingFlowMap_;   // contains the optimized solution
    
    // private method
    bool                    getDuals_(void);
    bool                    iterateBFS_(void);
    CostPrecision           getMaxDualSlack_(int &irow, int &icol);
    bool                    BFSTreeIterate_(vector<BFSLeaf<FlowPrecision> > &bfsTree , BFSLeaf<FlowPrecision> &currentLeaf, int direction, bool last_branch);
    BFSLeaf<FlowPrecision>* BFSLoopIterate_(vector<BFSLeaf<FlowPrecision> > &bfsTree , BFSLeaf<FlowPrecision> &currentLeaf, BFSLeaf<FlowPrecision> &targetLeaf, int direction);
    
  public:
    
    // access
    CostPrecision                  cost(int i, int j);
    vector<TPFlow<FlowPrecision> > getFlows(void);
    
    // BFS construction
    void VogelApproximantion(void);
    
    // BFS optimization
    bool optimizeBFS(int nIterationMax=0);
    
    // solution printing
    void printFlows(void);
    void printCosts(void);
    void printDualSlack(void);
     
    // constructor/destructor
    TPChart(vector<CostPrecision> &costs, vector<FlowPrecision> &requirements, vector<FlowPrecision> &stocks);
   ~TPChart(){}
};

// ********************************************************
// Chart construction:
// assign costs, requirements and stocks. Add dummy if 
// necessary i.e. if total stocks \= total requirements. 
// Compute and sort deficits.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
TPChart<FlowPrecision,CostPrecision>::TPChart(vector<CostPrecision> &costs, vector<FlowPrecision> &requirements, vector<FlowPrecision> &stocks)
{
  //
  // inint flags
  //
  dummy_requirement_=false;
  dummy_stock_=false;
  // get sizes
  int n=requirements.size();
  int m=stocks.size();
  // check if the total requirement are different from the total stocks
  FlowPrecision total_requirement=0;
  for ( int i=0 ; i<n ; i++ ) total_requirement+=requirements[i];
  FlowPrecision total_stock=0;
  for ( int i=0 ; i<m ; i++ ) total_stock+=stocks[i];
  CostPrecision costMax=0;
  // redimension problem if necessary 
  if ( total_stock>total_requirement )
  {
    // add a dummy requirement
    requirements.push_back(total_stock-total_requirement);
    // keep the dimensions of the array
    m_=m;
    n_=n+1;
    // resize the cell array
    cell_.resize(m_*n_);
    // init the cells with the costs
    for ( int i=0 , stop=m*n ; i<stop ; i++ )
    {
      cell_[i].flow=0;
      cell_[i].dual=0;
      cell_[i].cost=costs[i];
      // keep maximum cost for precision
      if ( costs[i]>costMax ) costMax=costs[i];
    }
    // set dummy column costs to 0
    for ( int i=m*n , stop=m_*n_ ; i<stop ; i++ )
    {
      cell_[i].flow=0;
      cell_[i].dual=0;
      cell_[i].cost=0;
    }
    // keep info about dummy col/row
    dummy_requirement_=true;
  } 
  else if ( total_stock==total_requirement )
  {
    // keep the dimensions of the array
    m_=m;
    n_=n;
    // resize the cell array
    cell_.resize(m_*n_);
    // init the cells 
    for ( int i=0 , stop=m*n ; i<stop ; i++ )
    {
      cell_[i].flow=0;
      cell_[i].dual=0;
      cell_[i].cost=costs[i];
      // keep maximum cost for precision
      if ( costs[i]>costMax ) costMax=costs[i];
    }  
  } 
  else 
  {
    cout << "requirement > stock not implemented in TPChart" << endl;
    exit(0);
  }
  // determine machine precision
  FlowPrecision precisionFlow=1;
  while ( precisionFlow+1 > precisionFlow && precisionFlow*2 > precisionFlow ) precisionFlow*=2;
  CostPrecision precisionSlack=1;
  while ( precisionSlack+1 > precisionSlack && precisionSlack*2 > precisionSlack ) precisionSlack*=2;
  // compute affordable precision for the flows
  epsilon_flow_=(total_requirement+total_stock)/precisionFlow*max(n_,m_);
  // compute affordable precision for the slack
  epsilon_slack_=(CostPrecision)costMax/precisionSlack*max(total_requirement,total_stock);
#ifdef DEBUG
  {
    // get display flags
    ios_base::fmtflags fflags=cout.flags();
    // inform user on precision choosen
    cout << "TPChart::TPChart - using precision " << scientific << epsilon_flow_ << " as epsilon value for stocks and requirements" << endl;
    cout << "TPChart::TPChart - using precision " << scientific << epsilon_slack_ << " as epsilon value for dual slacks" << endl;
    // set back display flags
    cout.flags(fflags);
  } 
#endif  
  // set cell indices
  for ( int i=0 ; i<m_ ; i++ )
  for ( int j=0 ; j<n_ ; j++ )
  {
    cell_[i+j*m_].irow=i;
    cell_[i+j*m_].icol=j;
  }
  // allocate array for requirement and stocks
  stock_.resize(m_);
  requirement_.resize(n_);
  // establish the order among the columns
  for ( int i=0 ; i<n_ ; i++ )
  {
    // sort costs
    vector<int>  index(m_);
    vector<CostPrecision> cost(m_);
    for ( int j=0 ; j<m_ ; j++ )
    {
      index[j]=j;
      cost[j]=cell_[i*m_+j].cost;
    }
    sort<CostPrecision>(&cost[0], cost.size(), &index[0]);
    // set pointers on previous costs
    cell_[i*m_+index[0]].prev_cost_in_col=NULL;
    for ( int j=1 ; j<m_ ; j++ ) cell_[i*m_+index[j]].prev_cost_in_col = &cell_[i*m_+index[j-1]];
    // set pointers on next costs
    cell_[i*m_+index[m_-1]].next_cost_in_col=NULL;
    for ( int j=1 ; j<m_ ; j++ ) cell_[i*m_+index[j-1]].next_cost_in_col = &cell_[i*m_+index[j]];
    // set requirement for this column
    requirement_[i].value=requirements[i];
    // set the best cell associated to this column
    requirement_[i].firstBestCell= &cell_[i*m_+index[0]];
    // set the second best cell associated to this column if any
    requirement_[i].secondBestCell= ( m_>1 ? &cell_[i*m_+index[1]] : &cell_[i*m_+index[0]] );
    // set the deficit associated to this column 
    requirement_[i].deficit = ( m_>1 ? cell_[i*m_+index[1]].cost-cell_[i*m_+index[0]].cost : 0.0 );
    // map deficit with this requirement
    TPDeficit deficit; deficit.type=REQUIREMENT; deficit.index=i;
    deficit_.insert( pair<CostPrecision,TPDeficit>( requirement_[i].deficit , deficit ) );
  }    
  // establish the order among the rows
  for ( int i=0 ; i<m_ ; i++ )
  {
    // sort costs
    vector<int>  index(n_);
    vector<CostPrecision> cost(n_);
    for ( int j=0 ; j<n_ ; j++ )
    {
      index[j]=j;
      cost[j]=cell_[i+j*m_].cost;
    }
    sort<CostPrecision>(&cost[0], cost.size(), &index[0]);
    // set pointers on previous costs
    cell_[i+index[0]*m_].prev_cost_in_row=NULL;
    for ( int j=1 ; j<n_ ; j++ ) cell_[i+index[j]*m_].prev_cost_in_row = &cell_[i+index[j-1]*m_];
    // set pointers on next costs
    cell_[i+index[n_-1]*m_].next_cost_in_row=NULL;
    for ( int j=1 ; j<n_ ; j++ ) cell_[i+index[j-1]*m_].next_cost_in_row = &cell_[i+index[j]*m_];
    // set stock for this row
    stock_[i].value=stocks[i];
    // set the best cell associated to this row
    stock_[i].firstBestCell= &cell_[i+index[0]*m_];
    // set the second best cell associated to this column if any
    stock_[i].secondBestCell= ( n_>1 ? &cell_[i+index[1]*m_] : &cell_[i+index[0]*m_] );
    // set the deficit associated to this row 
    stock_[i].deficit = ( n_>1 ? cell_[i+index[1]*m_].cost-cell_[i+index[0]*m_].cost : 0.0 );
    // map deficit with this stock
    TPDeficit deficit; deficit.type=STOCK; deficit.index=i;
    deficit_.insert( pair<CostPrecision,TPDeficit>( stock_[i].deficit , deficit ) );
  }   
  // init dual variables
  dual_stock_.resize(m_);
  dual_requirement_.resize(n_); 
  // indicate that BFS haven't been formed yet
  BFS_set_=false;
}

// ********************************************************
// TPChart::VogelApproximantion:
// Construct Basic Feasible Solution using the Vogel 
// approximation.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
void TPChart<FlowPrecision,CostPrecision>::VogelApproximantion(void)
{
  int iteration=0;
  // declare cell variables
  int                                  irow;
  int                                  icol;
  TPCell<FlowPrecision,CostPrecision> *cell;
  // reset number of flows
  nFlow_=0;
  // reset flow map
  flowMap_.clear();
  // loop on the defifits
  while ( !deficit_.empty() )
  {
    // get higher deficit
    typename multimap<CostPrecision,TPDeficit>::reverse_iterator max_deficit;
    max_deficit=deficit_.rbegin();
    // get indices of the corresponding cell
    if ( max_deficit->second.type==REQUIREMENT )
    {
      // get requirement index
      icol=max_deficit->second.index;
      // get reference to the best cell
      cell=requirement_[icol].firstBestCell;
      // get cell row indices 
      irow=cell->irow;
    }
    else
    {
      // get stock index
      irow=max_deficit->second.index;
      // get reference to the best cell
      cell=stock_[irow].firstBestCell;
      // get cell column indices 
      icol=cell->icol;
    }
    // get maximum possible flow
    cell->flow=min(stock_[irow].value,requirement_[icol].value);
    // set new requirements/stocks
    stock_[irow].value-=cell->flow;
    requirement_[icol].value-=cell->flow;
    // add the flow to the flow map
    nFlow_++;
    flowMap_[irow][icol]=cell->flow;
    // recompute stock deficits if necessary
    if ( requirement_[icol].value<=epsilon_flow_ )
    {
      // remove corresponding deficit from the queue
      {
        // find reference on the corresponding deficit
        typename multimap<CostPrecision,TPDeficit>::iterator deficit=deficit_.lower_bound(requirement_[icol].deficit);
        typename multimap<CostPrecision,TPDeficit>::iterator end_def=deficit_.upper_bound(requirement_[icol].deficit);
        for ( ; deficit!=end_def ; deficit++ )
        {
          if ( deficit->second.type==REQUIREMENT && deficit->second.index==icol ) break;
        }
        // remove from the the deficit queue
        deficit_.erase(deficit);
      }
      // go through stock to correct deficits
      for ( int istock=0 , stop=stock_.size() ; istock<stop ; istock++ )
      {
        // if the stock is active and the first best cell is concerned
        if ( stock_[istock].value>epsilon_flow_ && stock_[istock].firstBestCell->icol==icol )
        {
          // find reference on the corresponding deficit
          typename multimap<CostPrecision,TPDeficit>::iterator deficit=deficit_.lower_bound(stock_[istock].deficit);
          typename multimap<CostPrecision,TPDeficit>::iterator end_def=deficit_.upper_bound(stock_[istock].deficit);
          for ( ; deficit!=end_def ; deficit++ )
          {
            if ( ( deficit->second.type==STOCK ) && ( deficit->second.index==istock ) ) break;
          }
          // remove deficit from the queue
          deficit_.erase(deficit);
          // compute new first best cells
          TPCell<FlowPrecision,CostPrecision>* second_cell=stock_[istock].secondBestCell;
          stock_[istock].firstBestCell=second_cell;
          // compute new second best cells
          second_cell=second_cell->next_cost_in_row;
          while ( second_cell!=NULL && requirement_[second_cell->icol].value==0.0 ) second_cell=second_cell->next_cost_in_row;
          stock_[istock].secondBestCell = ( second_cell!=NULL ? second_cell : stock_[istock].firstBestCell );
          // compute new deficit
          stock_[istock].deficit=stock_[istock].secondBestCell->cost-stock_[istock].firstBestCell->cost;
          // place the new deficit in the queue
          TPDeficit tpdeficit; tpdeficit.type=STOCK; tpdeficit.index=istock;
          deficit_.insert( pair<CostPrecision,TPDeficit>( stock_[istock].deficit , tpdeficit ) );
        }
        // else, if the stock is active and the second best cell is concerned
        else if ( stock_[istock].value>epsilon_flow_ && stock_[istock].secondBestCell->icol==icol )
        {
          // find reference on the corresponding deficit
          typename multimap<CostPrecision,TPDeficit>::iterator deficit=deficit_.lower_bound(stock_[istock].deficit);
          typename multimap<CostPrecision,TPDeficit>::iterator end_def=deficit_.upper_bound(stock_[istock].deficit);
          for ( ; deficit!=end_def ; deficit++ )
          {
            if ( deficit->second.type==STOCK && deficit->second.index==istock ) break;
          }
          // remove deficit from the queue
          deficit_.erase(deficit);
          // compute new second best cells
          TPCell<FlowPrecision,CostPrecision>* second_cell=stock_[istock].secondBestCell->next_cost_in_row;          
          while ( second_cell!=NULL && requirement_[second_cell->icol].value==0.0 ) second_cell=second_cell->next_cost_in_row;
          stock_[istock].secondBestCell = ( second_cell!=NULL ? second_cell : stock_[istock].firstBestCell );
          // compute new deficit
          stock_[istock].deficit=stock_[istock].secondBestCell->cost-stock_[istock].firstBestCell->cost;
          // place the new deficit in the queue
          TPDeficit tpdeficit; tpdeficit.type=STOCK; tpdeficit.index=istock;
          deficit_.insert( pair<CostPrecision,TPDeficit>( stock_[istock].deficit , tpdeficit ) );
        }
      }
    }
    // recompute requirement deficits if necessary
    if ( stock_[irow].value<=epsilon_flow_ )
    {
      // remove corresponding deficit from the queue
      {
        // find reference on the corresponding deficit
        typename multimap<CostPrecision,TPDeficit>::iterator deficit=deficit_.lower_bound(stock_[irow].deficit);
        typename multimap<CostPrecision,TPDeficit>::iterator end_def=deficit_.upper_bound(stock_[irow].deficit);
        for ( ; deficit!=end_def ; deficit++ )
        {
          if ( deficit->second.type==STOCK && deficit->second.index==irow ) break;
        }
        // remove from the the deficit queue
        deficit_.erase(deficit);
      }
      // go through requirements to correct deficits
      for ( int irequirement=0 , stop=requirement_.size() ; irequirement<stop ; irequirement++ )
      { 
        // if the requirement is active and the first best cell is concerned
        if ( requirement_[irequirement].value>epsilon_flow_ && requirement_[irequirement].firstBestCell->irow==irow )
        {
          // find reference on the corresponding deficit
          typename multimap<CostPrecision,TPDeficit>::iterator deficit=deficit_.lower_bound(requirement_[irequirement].deficit);
          typename multimap<CostPrecision,TPDeficit>::iterator end_def=deficit_.upper_bound(requirement_[irequirement].deficit);
          for ( ; deficit!=end_def ; deficit++ )
          {
            if ( deficit->second.type==REQUIREMENT && deficit->second.index==irequirement ) break;
          }
          // remove deficit from the queue
          deficit_.erase(deficit);
          // compute new first best cells
          TPCell<FlowPrecision,CostPrecision>* second_cell=requirement_[irequirement].secondBestCell;
          requirement_[irequirement].firstBestCell=second_cell;
          // compute new second best cells
          second_cell=second_cell->next_cost_in_col;
          while ( second_cell!=NULL && stock_[second_cell->irow].value==0.0 ) second_cell=second_cell->next_cost_in_col;
          requirement_[irequirement].secondBestCell = ( second_cell!=NULL ? second_cell : requirement_[irequirement].firstBestCell ); 
          // compute new deficit
          requirement_[irequirement].deficit=requirement_[irequirement].secondBestCell->cost-requirement_[irequirement].firstBestCell->cost;
          // place the new deficit in the queue
          TPDeficit tpdeficit; tpdeficit.type=REQUIREMENT; tpdeficit.index=irequirement;
          deficit_.insert( pair<CostPrecision,TPDeficit>( requirement_[irequirement].deficit , tpdeficit ) );
        }
        // else, if the stock is active and the second best cell is concerned
        else if ( requirement_[irequirement].value>epsilon_flow_ && requirement_[irequirement].secondBestCell->irow==irow )
        {
          // find reference on the corresponding deficit
          typename multimap<CostPrecision,TPDeficit>::iterator deficit=deficit_.lower_bound(requirement_[irequirement].deficit);
          typename multimap<CostPrecision,TPDeficit>::iterator end_def=deficit_.upper_bound(requirement_[irequirement].deficit);
          for ( ; deficit!=end_def ; deficit++ )
          {
            if ( deficit->second.type==REQUIREMENT && deficit->second.index==irequirement ) break;
          }
          // remove deficit from the queue
          deficit_.erase(deficit);
          // compute new second best cells
          TPCell<FlowPrecision,CostPrecision>* second_cell=requirement_[irequirement].secondBestCell->next_cost_in_col;
          while ( second_cell!=NULL && stock_[second_cell->irow].value==0.0 ) second_cell=second_cell->next_cost_in_col;
          requirement_[irequirement].secondBestCell = ( second_cell!=NULL ? second_cell : requirement_[irequirement].firstBestCell );
          // compute new deficit
          requirement_[irequirement].deficit=requirement_[irequirement].secondBestCell->cost-requirement_[irequirement].firstBestCell->cost;
          // place the new deficit in the queue
          TPDeficit tpdeficit; tpdeficit.type=REQUIREMENT; tpdeficit.index=irequirement;
          deficit_.insert( pair<CostPrecision,TPDeficit>( requirement_[irequirement].deficit , tpdeficit ) );
        }
      }
    }
    // check that we got either 0 or at least 2 deficits in order to detect arithmetic problems
    if ( deficit_.size()==1 )
    {
      cout << "Error: arithmetic problem within - TPChart::VogelApproximantion - try incresing a bit the epsilon_flow_ value" << endl;
      exit(0);
    }
  }
  // indicate that BFS is formed now
  BFS_set_=true;
}

// ********************************************************
// TPChart::VogelApproximantion:
// Construct Basic Feasible Solution using the Vogel 
// approximation.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
bool TPChart<FlowPrecision,CostPrecision>::BFSTreeIterate_(vector<BFSLeaf<FlowPrecision> > &bfsTree , BFSLeaf<FlowPrecision> &currentLeaf, int direction, bool last_branch)
{
  // set flag to detect closed loops
  bool closed_loop=false;
  // if we work along the horizontal direction
  if ( direction==HORIZONTAL && flowMap_.size()>0 )
  {
    // get the map on this row elements
    typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.find(currentLeaf.irow);
    // check that the tree is connected
    // add a connection only if this is the last item of the tree 
    // and make sure there is room for that
    if ( it_row==flowMap_.end() && nFlow_<m_+n_-1 && last_branch ) 
    {
      // add a leaf to the flow map connecting current leaf 
      // and the first element of the flow map
      typename map<int,FlowPrecision>::iterator it_col=flowMap_.begin()->second.begin();
      flowMap_[currentLeaf.irow][it_col->first]=0;
      remainingFlowMap_[currentLeaf.irow][it_col->first]=0;
      // get again the map on this row
      it_row=flowMap_.find(currentLeaf.irow);
      // increment number of flow
      nFlow_++;
    }
    if ( it_row!=flowMap_.end() )
    {
      // copy local map
      map<int,FlowPrecision> local_copy=it_row->second;
      // remove this row from the flow map
      flowMap_.erase(it_row);
      // loop on the leaves of this row
      for ( typename map<int,FlowPrecision>::iterator it_col=local_copy.begin() ; it_col!=local_copy.end() ; it_col++ )
      {
        // add the corresponding leaf to the tree
        BFSLeaf<FlowPrecision> newLeaf;
        newLeaf.irow=currentLeaf.irow;
        newLeaf.icol=it_col->first;
        newLeaf.flow=it_col->second;
        newLeaf.parent=&currentLeaf;
        // check that there is still some room in the tree
        if ( bfsTree.size()>=m_+n_-1 ) { closed_loop=true; break; }
        // add this leaf to the tree
        bfsTree.push_back(newLeaf);
        // get the dual requirement
        dual_requirement_[newLeaf.icol]=cell_[newLeaf.irow+newLeaf.icol*m_].cost-dual_stock_[newLeaf.irow];
        // add the new leaf to the children list
        currentLeaf.children.push_back(&bfsTree.back());
        // iterate from this leaf if necessary
        typename map<int,FlowPrecision>::iterator it_col_next=it_col; it_col_next++;
        if ( !flowMap_.empty() ) if ( !BFSTreeIterate_(bfsTree , bfsTree.back(), VERTICAL, last_branch && it_col_next==local_copy.end()) ) { closed_loop=true; break; }
      }
    }
  }
  // if we work along the vertical direction
  else if ( flowMap_.size()>0 )
  {
    // init list of leaves
    vector<BFSLeaf<FlowPrecision> > listLeaf;
    // loop on the rows to construct a list of flows
    for ( typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.begin() ; it_row!=flowMap_.end() ; it_row++ )
    {
      // search flow in the corresponding column
      typename map<int,FlowPrecision>::iterator it_col=it_row->second.find(currentLeaf.icol);
      // if we found one
      if ( it_col!=it_row->second.end() )
      {
        // add a new leaf to the tree
        BFSLeaf<FlowPrecision> newLeaf;
        newLeaf.icol=currentLeaf.icol;
        newLeaf.irow=it_row->first;
        newLeaf.flow=it_col->second;
        newLeaf.parent=&currentLeaf;
        listLeaf.push_back(newLeaf);
      }
    }
    // if the tree is not connected
    if ( listLeaf.size()==0 && nFlow_<m_+n_-1 && last_branch )
    {
      // add a leaf connecting current leaf and the first element of the flow map
      typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.begin();
      flowMap_[it_row->first][currentLeaf.icol]=0;
      remainingFlowMap_[it_row->first][currentLeaf.icol]=0;
      BFSLeaf<FlowPrecision> newLeaf;
      newLeaf.icol=currentLeaf.icol;
      newLeaf.irow=it_row->first;
      newLeaf.flow=0;
      listLeaf.push_back(newLeaf);
      // increment number of flow
      nFlow_++;
    }
    // clean map
    for ( int i=0 ; i<listLeaf.size() ; i++ )
    {
      // find the corresponding iterator
      typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.find(listLeaf[i].irow);
      typename map<int,FlowPrecision>::iterator it_col=it_row->second.find(listLeaf[i].icol);
      // remove flow from the map
      it_row->second.erase(it_col);
      if (it_row->second.empty()) flowMap_.erase(it_row);
    }
    // loop on the flow list
    for ( int i=0 ; i<listLeaf.size() ; i++ )
    {
      // check that there is still some room in the tree
      if ( bfsTree.size()>=m_+n_-1 ) { closed_loop=true; break; }
      // if there is still room, add this leaf to the tree
      bfsTree.push_back(listLeaf[i]);
      // add the new leaf to the children list
      currentLeaf.children.push_back(&bfsTree.back());  
      // get the dual stock
      dual_stock_[listLeaf[i].irow]=cell_[listLeaf[i].irow+listLeaf[i].icol*m_].cost-dual_requirement_[listLeaf[i].icol];        
      // iterate from this leaf if necessary
      if ( flowMap_.size()>0 ) if ( !BFSTreeIterate_(bfsTree , bfsTree.back(), HORIZONTAL, last_branch && i+1==listLeaf.size() ) ) { closed_loop=true; break; }
    } 
  } 
  // indicate that hit normally the bottom of this one
  return !closed_loop;       
}

// ********************************************************
// TPChart::getDuals_:
// Compute dual stocks and requirements
// ********************************************************

template <class FlowPrecision, class CostPrecision>
bool TPChart<FlowPrecision,CostPrecision>::getDuals_(void)
{
  // set the remaining flow Map
  remainingFlowMap_=flowMap_;
  // initialize the first leaf of the tree
  vector<BFSLeaf<FlowPrecision> > bfsTree(1);
  bfsTree.reserve(3*(m_+n_-0));
  // get col, row and flow for first leaf of flowmapprintFlows(
  typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.begin();
  typename map<int,FlowPrecision>::iterator it_col=it_row->second.begin();
  bfsTree[0].parent=NULL;
  bfsTree[0].irow=it_row->first;
  bfsTree[0].icol=it_col->first;
  bfsTree[0].flow=it_col->second;
  // remove this flow from the map
  it_row->second.erase(it_col);
  if ( it_row->second.empty() ) flowMap_.erase(it_row);
  // init dual stock
  dual_stock_[bfsTree[0].irow]=0.0;
  dual_requirement_[bfsTree[0].icol]=cell_[bfsTree[0].irow+bfsTree[0].icol*m_].cost;
  // construct the tree 
  bool got_dual=true;
  got_dual &= BFSTreeIterate_(bfsTree , bfsTree[0], HORIZONTAL, false);
  if (got_dual)
  {
    got_dual &= BFSTreeIterate_(bfsTree , bfsTree[0], VERTICAL, true);
  }
  // indicate result of the search
  if (!got_dual) cout << "Warning: in - TPChart::getDuals_ - solution seems to be degenerated. Returning without optimization" << endl; 
  // restore the map
  flowMap_=remainingFlowMap_;
  // return the result of the search
  return got_dual;
}

// ********************************************************
// TPChart::getMaxDualSlack_:
// Find indices of the TPCell with the max dual slack
// ********************************************************

template <class FlowPrecision, class CostPrecision>
CostPrecision TPChart<FlowPrecision,CostPrecision>::getMaxDualSlack_(int &irow, int &icol)
{
  // init dual slack value
  CostPrecision max_dual_slack=cell_[0].cost-dual_stock_[0]-dual_requirement_[0];
  irow=0;
  icol=0;
  // loop on coeffs
  for ( int i=0 ; i<m_ ; i++ )
  for ( int j=0 ; j<n_ ; j++ )
  {
    // compute dual slack for this coeff
    CostPrecision dual_slack=cell_[i+m_*j].cost-dual_stock_[i]-dual_requirement_[j];
    // keep slack if necesssary
    if ( max_dual_slack>dual_slack ) 
    {
      max_dual_slack=dual_slack;
      irow=i;
      icol=j;
    }
  }
  // return result
  return max_dual_slack;
}

// ********************************************************
// TPChart::BFSLoopIterate_:
// return the path between two leaves as a tree
// ********************************************************

template <class FlowPrecision, class CostPrecision>
BFSLeaf<FlowPrecision>* TPChart<FlowPrecision,CostPrecision>::BFSLoopIterate_(vector<BFSLeaf<FlowPrecision> > &bfsTree , BFSLeaf<FlowPrecision> &currentLeaf, BFSLeaf<FlowPrecision> &targetLeaf, int direction)
{
  // if we work along the horizontal direction
  if ( direction==HORIZONTAL )
  {
    // get the map on this row elements
    typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.find(currentLeaf.irow);
    // check that the tree is connected
    if ( it_row!=flowMap_.end() )
    {
      // copy local map
      map<int,FlowPrecision> local_copy=it_row->second;
      // remove this row from the flow map
      flowMap_.erase(it_row);
      // reinsert the target flow in case it has been erased
      flowMap_[targetLeaf.irow][targetLeaf.icol]=targetLeaf.flow;
      // loop on the leaves of this row
      for ( typename map<int,FlowPrecision>::iterator it_col=local_copy.begin() ; it_col!=local_copy.end() ; it_col++ )
      {
        // if this leaf is different from the current one
        if ( it_col->first != currentLeaf.icol )
        {
          // add the corresponding leaf to the tree
          BFSLeaf<FlowPrecision> newLeaf;
          newLeaf.irow=currentLeaf.irow;
          newLeaf.icol=it_col->first;
          newLeaf.flow=it_col->second;
          newLeaf.parent=&currentLeaf;
          bfsTree.push_back(newLeaf);
          // add the new leaf to the children list
          currentLeaf.children.push_back(&bfsTree.back());
          // if this leaf is the one we are looking for
          if ( targetLeaf.icol==newLeaf.icol && targetLeaf.irow==newLeaf.irow ) 
          {
            // restore the map
            flowMap_[currentLeaf.irow]=local_copy;
            // return the pointer on this leaf
            return &bfsTree.back();
          }
          else
          {
            // iterate from this leaf if necessary
            if ( !flowMap_.empty() ) 
            {
              // get the size of the current tree
              int size_tree=bfsTree.size(); 
              // explore tree 
              BFSLeaf<FlowPrecision>* leafPtr=BFSLoopIterate_(bfsTree , bfsTree.back(), targetLeaf, VERTICAL);
              // check if we found the solution
              if ( leafPtr!=NULL ) 
              {
                // restore the map
                flowMap_[currentLeaf.irow]=local_copy;
                // return the pointer on this leaf 
                return leafPtr;
              }
              // if the solution is not found from this leaf, remove it from the tree
              else
              {
                bfsTree.resize(size_tree);
              }
            }
          }
        }
      }
      // restore the map
      flowMap_[currentLeaf.irow]=local_copy;
    }
  }
  // if we work along the vertical direction
  else
  {
    // init list of leaves
    vector<BFSLeaf<FlowPrecision> > listLeaf;
    // loop on the rows to construct a list of flows
    for ( typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.begin() ; it_row!=flowMap_.end() ; it_row++ )
    {
      // search flow in the corresponding column
      typename map<int,FlowPrecision>::iterator it_col=it_row->second.find(currentLeaf.icol);
      // if we found one
      if ( it_col!=it_row->second.end() && it_row->first!=currentLeaf.irow )
      {
        // add a new leaf to the tree
        BFSLeaf<FlowPrecision> newLeaf;
        newLeaf.icol=currentLeaf.icol;
        newLeaf.irow=it_row->first;
        newLeaf.flow=it_col->second;
        newLeaf.parent=&currentLeaf;
        listLeaf.push_back(newLeaf);
      }
    }
    // clean map
    for ( int i=0 ; i<listLeaf.size() ; i++ )
    {
      // find the corresponding iterator
      typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.find(listLeaf[i].irow);
      typename map<int,FlowPrecision>::iterator it_col=it_row->second.find(listLeaf[i].icol);
      // remove flow from the map
      it_row->second.erase(it_col);
      if (it_row->second.empty()) flowMap_.erase(it_row);
    }
    // reinsert the target flow in case it has been erased
    flowMap_[targetLeaf.irow][targetLeaf.icol]=targetLeaf.flow;  
    // loop on the flow list
    for ( int i=0 ; i<listLeaf.size() ; i++ )
    {
      // add leaf to the tree
      bfsTree.push_back(listLeaf[i]);
      // add the new leaf to the children list
      currentLeaf.children.push_back(&bfsTree.back());  
      // if this leaf is the one we are looking for
      if ( targetLeaf.icol==listLeaf[i].icol && targetLeaf.irow==listLeaf[i].irow ) 
      {
        // restore the map
        for ( int i=0 ; i<listLeaf.size() ; i++ ) flowMap_[listLeaf[i].irow][listLeaf[i].icol]=listLeaf[i].flow;
        // return the pointer on this leaf
        return &bfsTree.back();
      }
      else
      {
        // iterate from this leaf if necessary
        if ( !flowMap_.empty() ) 
        {
          // get the size of the current tree
          int size_tree=bfsTree.size(); 
          // explore tree 
          BFSLeaf<FlowPrecision>* leafPtr=BFSLoopIterate_(bfsTree , bfsTree.back(), targetLeaf, HORIZONTAL);
          // check if we found the solution
          if ( leafPtr!=NULL ) 
          {
            // restore the map
            for ( int i=0 ; i<listLeaf.size() ; i++ ) flowMap_[listLeaf[i].irow][listLeaf[i].icol]=listLeaf[i].flow;
            // return the pointer on this leaf
            return leafPtr;
          }
          // if we did not found the solution from this leaf, remove all trash from the tree
          else
          {
            bfsTree.resize(size_tree);
          }
        }
      }
    } 
    // restore the map
    for ( int i=0 ; i<listLeaf.size() ; i++ ) flowMap_[listLeaf[i].irow][listLeaf[i].icol]=listLeaf[i].flow;
  }  
  // if we did not found anything, return 0
  return NULL;      
}

// ********************************************************
// TPChart::iterateBFS_:
// apply one iteration on the FlowMap if there is dual slack
// return true if there is no dual slack
// ********************************************************

template <class FlowPrecision, class CostPrecision>
bool TPChart<FlowPrecision,CostPrecision>::iterateBFS_(void)
{
  // get maximum dual slack
  int irow;
  int icol;
  CostPrecision max_dual_slack=getMaxDualSlack_(irow,icol);
  // if there is room for optimization 
  if ( max_dual_slack<-epsilon_slack_ )
  {
    // add corresponding flow to the map
    flowMap_[irow][icol]=(FlowPrecision)0.0;
    // initialize the first leaf of the tree
    vector<BFSLeaf<FlowPrecision> > bfsTree(1);
    bfsTree.reserve(m_+n_+1);
    // get col, row and flow for this leaf
    bfsTree[0].parent=NULL;
    bfsTree[0].irow=irow;
    bfsTree[0].icol=icol;
    bfsTree[0].flow=(FlowPrecision)0.0;
    // find the loop 
    BFSLeaf<FlowPrecision> *endLeaf=BFSLoopIterate_(bfsTree, bfsTree[0], bfsTree[0], HORIZONTAL);
#ifdef DEBUG
    // print the loop
    if ( endLeaf!=NULL )
    {
      BFSLeaf<FlowPrecision> *leaf=endLeaf;
      cout << "leaf(" << leaf->irow << "," << leaf->icol << ")";  
      leaf=leaf->parent;
      while (leaf!=NULL)
      {
        cout << " => leaf(" << leaf->irow << "," << leaf->icol << ")";
        leaf=leaf->parent;
      }
      cout << endl;
    }    
#endif
    // walk the loop
    if ( endLeaf!=NULL )
    {
      BFSLeaf<FlowPrecision> *leaf=endLeaf->parent;
      FlowPrecision max_flow=leaf->flow;
      int   irow_max_flow=leaf->irow;
      int   icol_max_flow=leaf->icol;
      while (leaf!=NULL)
      {
        leaf=leaf->parent;
        if ( leaf!=NULL )
        {
          leaf=leaf->parent;
          if ( leaf!=NULL && max_flow>leaf->flow ) 
          {
            max_flow=leaf->flow;
            irow_max_flow=leaf->irow;
            icol_max_flow=leaf->icol;
          }
        }
      }
      // correc the flows
      leaf=endLeaf;
      while (leaf->parent!=NULL)
      {
        // correct the flow
        flowMap_[leaf->irow][leaf->icol]=(flowMap_[leaf->irow][leaf->icol]+max_flow);
        // change flow sign
        max_flow=-max_flow;
        // step in loop
        leaf=leaf->parent;
      }
      // find the iterator corresponding to the unused flow
      typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.find(irow_max_flow);
      typename map<int,FlowPrecision>::iterator it_col=it_row->second.find(icol_max_flow);
      // remove the unused flow
      it_row->second.erase(it_col);
      if ( it_row->second.empty() ) flowMap_.erase(it_row);
      // indicate that something changed
      return false;
    }
    // if we did not found a loop, exit optimization loop 
    else
    {
      cout << "Warning: exiting optimization loop without optimal solution" << endl; 
      return true;
    }
  }
  else
  {
    // indicate that the solution is optimal
    return true;
  }
}

// ********************************************************
// TPChart::optimizeBFS:
// iterate on the BFS until it is found optimal or we reach
// the maximum number of iteration
// ********************************************************

template <class FlowPrecision, class CostPrecision>
bool TPChart<FlowPrecision,CostPrecision>::optimizeBFS(int nIterationMax)
{
  // make sure that we have a BFS
  if ( !BFS_set_ )
  {
    cout << "Error: trying to optimise flow chart in - TPChart::optimizeBFS - without defining a BFS" << endl;
    exit(0);  
  }
  // set iteration counter
  int nIt=0;
  // set convergence flag
  bool converged=false;
  // iterate solution
  while ( getDuals_() && !converged && ( nIt<nIterationMax || nIterationMax==0 ) )
  {
    converged=iterateBFS_(); 
    nIt++;   
  }
  // check for what reason we escaped
  if ( nIt==nIterationMax && nIterationMax!=0 )
  {
    cout << "Warning: in - TPChart::optimizeBFS - reached maximum iteration number, solution may not be optimal" << endl;
  }
  // check if the result is converged
  return converged; 
}

// ********************************************************
// TPChart::getFlows:
// return the flows as a vector of TPFlow objects.
// Filter the dummy flows if any have been used to
// find the solution, and the zero valued flows.
// ********************************************************

template <class FlowPrecision, class CostPrecision>
vector<TPFlow<FlowPrecision> > TPChart<FlowPrecision,CostPrecision>::getFlows(void)
{
  // make sure that we have a BFS
  // it up to the user wether to optimize the solution or not
  if ( !BFS_set_ )
  {
    cout << "Error: trying to get flow chart in - TPChart::getFlows - without defining at least a BFS" << endl;
    exit(0);  
  }
  // init vector of flows
  vector<TPFlow<FlowPrecision> > flows;
  // iterate on the flow map
  for ( typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.begin() ; it_row!=flowMap_.end() ; it_row++ )
  {
    for ( typename map<int,FlowPrecision>::iterator it_col=it_row->second.begin() ; it_col!=it_row->second.end() ; it_col ++ )
    {
      // get corresponding row/column indices
      int irow=it_row->first;
      int icol=it_col->first;
      // get corresponding flow
      FlowPrecision flow=it_col->second;
      // check that this is not a dummy flow
      if ( ( irow<m_-1 || !dummy_stock_ ) && ( icol<n_-1 || !dummy_requirement_ ) && flow>epsilon_flow_ )
      {
        // add this flow to the flow list
        TPFlow<FlowPrecision> tpflow;
        tpflow.irequirement=icol;
        tpflow.istock=irow;
        tpflow.flow=flow;
        flows.push_back(tpflow);
      } 
    }
  }
  // return flow vector
  return flows;
}

// ********************************************************
// TPChart::printFlows:
// formatted output of the flow chart
// ********************************************************

template <class FlowPrecision, class CostPrecision>
void TPChart<FlowPrecision,CostPrecision>::printFlows(void)
{
  // save current formatting info
  ios_base::fmtflags ff=cout.flags();
  // set precision format 
  cout.precision(3);
  // print header
  cout << endl;
  cout << "Flows :" << endl;
  //
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "----" <<  endl;
  for ( int i=0 ; i<m_ ; i++ )
  {
    // output result  
    cout << "|  ";
    for ( int j=0 ; j<n_ ; j++ )
    {
      // find the iterator corresponding to the unused flow
      typename map<int,map<int,FlowPrecision> >::iterator it_row=flowMap_.find(i);
      if ( it_row!=flowMap_.end() )
      {
        typename map<int,FlowPrecision>::iterator it_col=it_row->second.find(j);
        if ( it_col!=it_row->second.end() )
        {
          cout.width(6);
          cout << fixed << it_col->second << "  ";
        }
        else
        {
          cout.width(6);
          cout << fixed << 0.0 << "  ";
        }
      }
      else
      {
        cout.width(6);
        cout << fixed << 0.0 << "  ";
      }
    }
    cout << "|" << endl;
  }
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "----" <<  endl;
  cout  <<  endl;
  // restore output format
  cout.flags(ff);
}

// ********************************************************
// TPChart::printCosts:
// formatted output of the costs
// ********************************************************

template <class FlowPrecision, class CostPrecision>
void TPChart<FlowPrecision,CostPrecision>::printCosts(void)
{
  // save current formatting info
  ios_base::fmtflags ff=cout.flags();
  // set precision format 
  cout.precision(3);
  // print header
  cout << endl;
  cout << "Costs :" << endl;
  //
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "--------------------------" <<  endl;
  for ( int i=0 ; i<m_ ; i++ )
  {
    cout << "|  ";
    for ( int j=0 ; j<n_ ; j++ )
    {
      cout.width(6);
      cout << fixed << cell_[i+m_*j].cost << "  ";
    }
    cout << "|  ";
    cout.width(6);
    cout << fixed << stock_[i].value;
    cout << "  |  ";
    cout.width(6);
    cout << fixed <<  stock_[i].deficit;
    cout <<  "  |" << endl ;
  }
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "--------------------------" <<  endl;
  cout << "|  ";
  for ( int j=0 ; j<n_ ; j++ )
  {
    cout.width(6);
    cout << fixed << requirement_[j].value << "  ";
  }
  cout << "|" << endl;
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "----" <<  endl;
  cout << "|  ";
  for ( int j=0 ; j<n_ ; j++ )
  {
    cout.width(6);
    cout << fixed << requirement_[j].deficit << "  ";
  }
  cout << "|" << endl;
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "----" <<  endl;
  cout << endl;
  // restore output format
  cout.flags(ff);
}

// ********************************************************
// TPChart::printDualSlack:
// formatted output of the dual slacks
// ********************************************************

template <class FlowPrecision, class CostPrecision>
void TPChart<FlowPrecision,CostPrecision>::printDualSlack(void)
{
  // save current formatting info
  ios_base::fmtflags ff=cout.flags();
  // set precision format 
  cout.precision(3);
  // print header
  cout << endl;
  cout << "Dual slack :" << endl;
  //
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "---------------" <<  endl;
  for ( int i=0 ; i<m_ ; i++ )
  {
    cout << "|  ";
    for ( int j=0 ; j<n_ ; j++ )
    {
      cout.width(6);
      cout << fixed << cell_[i+m_*j].cost-dual_stock_[i]-dual_requirement_[j] << "  ";
    }
    cout << "|  ";
    cout.width(6);
    cout << fixed << dual_stock_[i];
    cout << "  |" << endl ;
  }
  
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "---------------" <<  endl;
  cout << "|  ";
  for ( int j=0 ; j<n_ ; j++ )
  {
    cout.width(6);
    cout << fixed << dual_requirement_[j] << "  ";
  }
  cout << "|" << endl;
  for ( int j=0 ; j<n_ ; j++ ) cout << "--------"; cout << "----" <<  endl;
  cout << endl;
  // restore output format
  cout.flags(ff);
}

#endif

#ifndef POSTPROCESSCHANNELS_H
#define POSTPROCESSCHANNELS_H

#include "Box.h"
#include "LDos.h"
#include "Flux.h"
#include "Context.h"
#include "IndexList.h"
#include "Export2Povray.h"

using namespace std;

//! Limit after wich a flux is considered to be 0
#define THRESHOLD_FLUX 1E-10

// function for reading the channels from a file
void ReadChannelsFromFile(string fileName, int &nChannels, vector<int> &axe, vector<int> &iglobal, vector<Atom<double> > &atom, vector<vector<complex<double> > > &channel);

//! \brief Compute the fluxes given the channels info and the basis
/// \param context       should be clear enough...
/// \param basis         basis of the system, generated from the same input file used to compute the channels
/// \param iglobal       global indices of the states 
/// \param atom          atom associated to the states
/// \param channel[i](j] coefficient of state i for the jth channel   
/// \retval flux         contains the information about fluxes between atoms
template <class BasisType> 
void computeFluxes(BasisType &basis,Context &context,vector<Flux<Atom<double> > > &flux,
                   vector<int> &iglobal, vector<Atom<double> > &atom, vector<vector<complex<double> > > &channel)
{
  //
  // get the number of channels
  //
  int nChannels=channel[0].size();
  //
  // loop on the parts of the system to compute the currents between neigbors
  //
  for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ )
  {
    //
    // keep initial flux number for the count
    // 
    int init_size=flux.size();
    //
    // create the part
    //
    HarmonicOscillatorPart<double> *part = new HarmonicOscillatorPart<double>(basis,context,ipart);
    //
    // getpointer on the matrix pencil object
    //
    MatrixPencil<double> *pencil=(*part).matrixPencilPtr();
    //
    // get the local range indices for this part
    // 
    vector<int> range=pencil->localRange();
    vector<int> domain=pencil->domain();
    //
    // find the corresponding indices in the channel array
    //
    vector<int> local_range=range<=iglobal;
    vector<int> local_domain=domain<=iglobal;
    //
    // loop on the local range indices
    //
    for ( int irange=0 ; irange<local_range.size() ; irange++ )
    {
      //
      // get corresponding coeff
      //
      vector<double> coeff(local_domain.size(),0.0);
      for ( double *src=pencil->coeffPtr(irange*local_domain.size()) , 
                   *dest=&coeff[0] , *stop=src+local_domain.size() ; src<stop ; src++ , dest++ ) (*dest)=(*src); 
      //
      // init flux values
      //
      vector<double> J(local_domain.size(),0.0);
      //
      // loop on the domain indices
      //
      for ( int idomain=0 , stop=local_domain.size() ; idomain<stop ; idomain++ )
      {
        //
        // loop only if there is an interaction
        //
        if ( coeff[idomain]!=0.0 )
        {
          //
          // point on this range coeffs 
          //
          complex<double> *range_coeff=&channel[local_range[irange]][0];
          //
          // point on this domain coeffs
          //
          complex<double> *domain_coeff=&channel[local_domain[idomain]][0];
          //
          // loop on the channels
          //
          complex<double> complex_j=0.0;
          // equivalent to: for ( int ichannel=0 ; ichannel<nChannels ; ichannel++ ), but faster on pwr6 system... 
          for ( complex<double> *stop=&channel[local_domain[idomain]].back() ; domain_coeff<=stop ; domain_coeff++ , range_coeff++ )
          {
            complex_j += (*domain_coeff) * conj(*range_coeff);
          }
          //
          // instead of taking J= 2.0 * imag( <phi[A,H]phi> ) , use only a
          // factor 1.0 as we compress fluxes later and thus double the values...
          //
          J[idomain] += coeff[idomain] * complex_j.imag();
        }
      }
      //
      // keep J > to the threshold
      //
      for ( int idomain=0 , stop=local_domain.size() ; idomain<stop ; idomain++ )
      {
        if ( fabs(J[idomain])>THRESHOLD_FLUX )
        {
          //
          // compute distance between the atoms
          //
          Vector3D<double> dist=atom[local_range[irange]].coordinates()-atom[local_domain[idomain]].coordinates();
          //
          // add a flux only if the two atoms are different
          //
          double d=dist*dist;
          //
          if ( d>0 )
          {
            flux.resize(flux.size()+1);
            flux.back().fromObject=atom[local_range[irange]];
            flux.back().toObject=atom[local_domain[idomain]];
            flux.back().fromGlobalIndex=iglobal[local_range[irange]];
            flux.back().toGlobalIndex=iglobal[local_domain[idomain]];
            flux.back().value=J[idomain];   
          }
        }
      }
    }
    //
    // inform
    //
    cout << "added " << flux.size()-init_size << " flux for part " << ipart << endl;
    //
    // delete this part
    //
    delete part;
  }
}

//! \brief Get translations for a specific part and complete the set with large vectors.
/// \return true if the part has a least one translation.
template <class BasisType> 
bool fluxUtil_GetPartTranslation_(BasisType &basis, int ipart, vector<Vector3D<double> > &translations)
{
  //
  // get defect part translations
  //  
  translations=basis.getPartTranslations(ipart);
  //
  // if there is any translation for this part
  //
  if ( translations.size()>0 )
  {
    //
    // complete translation vector if necessary
    //
    if ( translations.size()==1 )
    {
      Vector3D<double> v2(translations[0][1]*10000,translations[0][2]*10000,-translations[0][0]*10000);
      Vector3D<double> v3=translations[0]^v2;
      translations.push_back(v2);
      translations.push_back(v3);
    }
    else if ( translations.size()==2 )
    {
      Vector3D<double> v3=translations[0]^translations[1]*10000;
      translations.push_back(v3);
    }
    //
    // indicates that translations where found
    //
    return true;
  }
  //
  // otherwise, complete the translations
  //
  translations.push_back(Vector3D<double>(10000.0,0.0,0.0));
  translations.push_back(Vector3D<double>(0.0,10000.0,0.0));
  translations.push_back(Vector3D<double>(0.0,0.0,10000.0));
  //
  // indicates that no translations where found
  //
  return false;
} 

//! \brief Minimal image convention on the fluxes
/// \retval flux has the long fluxes duplicated and reduced according to the minimal image convention

template <class BasisType> 
void fluxUtil_allpyMinimalImageConvention_(BasisType &basis, vector<Flux<Atom<double> > > &flux)
{
  //
  // form the lists of global indices in the flux list
  //
  vector<int> ilist1; 
  vector<int> ilist2; 
  fluxGlobalIndexLists( flux, ilist1, ilist2 );
  //
  // apply minimal image convention: start by duplicating long fluxes
  //
  for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ )
  {
    //
    // init part translations
    //  
    vector<Vector3D<double> > translations;
    //
    // if there is any translation for this part
    //
    if ( fluxUtil_GetPartTranslation_(basis,ipart,translations) )
    {
      //
      // create a Basis3D object from the translations
      //
      Basis3D<double> basis3d(translations);
      // 
      // get global index list for this part
      //
      vector<int> part_indices=basis.getDefectPartIndices(ipart);
      //
      // extract the indices flux belonging to this part
      //
      vector<int> iflux1=weakExtraction( part_indices, ilist1 );
      vector<int> iflux2=weakExtraction( part_indices, ilist2 );
      //
      // loop on the local flux
      //
      for ( int i=0 ; i<iflux1.size() ; i++ )
      {
        Atom<double> &atom_i1=flux[iflux1[i]].fromObject;
        Atom<double> &atom_i2=flux[iflux1[i]].toObject;
        //
        // get bond coordinates in translation basis
        //
        Vector3D<double> diff=atom_i2.coordinates()-atom_i1.coordinates();
        Vector3D<double> dual=basis3d.getCoordinatesOf(diff);
        //
        // fold into BZ
        //
        double ntx=0.0; 
        while ( dual[0]>=0.5 ) { ntx+=1.0; dual[0]-=1.0; }
        while ( dual[0]<-0.5 ) { ntx-=1.0; dual[0]+=1.0; }
        double nty=0.0; 
        while ( dual[1]>=0.5 ) { nty+=1.0; dual[1]-=1.0; }
        while ( dual[1]<-0.5 ) { nty-=1.0; dual[1]+=1.0; }
        double ntz=0.0; 
        while ( dual[2]>=0.5 ) { ntz+=1.0; dual[2]-=1.0; }
        while ( dual[2]<-0.5 ) { ntz-=1.0; dual[2]+=1.0; }
        //
        // duplicate this flux with minimal image convention 
        //
        if ( ntx!=0.0 || nty!=0.0 || ntz!=0.0 )
        {
          Flux<Atom<double> > myflux=flux[iflux1[i]];
          //
          Vector3D<double> T=ntx*translations[0]+nty*translations[1]+ntz*translations[2];
          myflux.fromObject+=T;
          //
          flux.push_back(myflux);
        }
      }
      //
      for ( int i=0 ; i<iflux2.size() ; i++ )
      {
        Atom<double> &atom_i1=flux[iflux2[i]].fromObject;
        Atom<double> &atom_i2=flux[iflux2[i]].toObject;
        //
        // get bond coordinates in translation basis
        //
        Vector3D<double> diff=atom_i2.coordinates()-atom_i1.coordinates();
        Vector3D<double> dual=basis3d.getCoordinatesOf(diff);
        //
        // fold into BZ
        //
        double ntx=0.0; 
        while ( dual[0]>=0.5 ) { ntx+=1.0; dual[0]-=1.0; }
        while ( dual[0]<-0.5 ) { ntx-=1.0; dual[0]+=1.0; }
        double nty=0.0; 
        while ( dual[1]>=0.5 ) { nty+=1.0; dual[1]-=1.0; }
        while ( dual[1]<-0.5 ) { nty-=1.0; dual[1]+=1.0; }
        double ntz=0.0; 
        while ( dual[2]>=0.5 ) { ntz+=1.0; dual[2]-=1.0; }
        while ( dual[2]<-0.5 ) { ntz-=1.0; dual[2]+=1.0; }
        //
        // duplicate this flux with minimal image convention 
        //
        if ( ntx!=0.0 || nty!=0.0 || ntz!=0.0 )
        {
          Flux<Atom<double> > myflux=flux[iflux2[i]];
          //
          Vector3D<double> T=ntx*translations[0]+nty*translations[1]+ntz*translations[2];
          myflux.toObject-=T;
          //
          flux.push_back(myflux);
        }
      }
    }
  }
  //
  // second step for minimal image convention: remove long fluxes
  //
  for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ )
  {
    //
    // init defect part translations
    //  
    vector<Vector3D<double> > translations;
    //
    // if there are translation for this part
    //
    if ( fluxUtil_GetPartTranslation_(basis,ipart,translations) )
    {
      //
      // create a basis object
      //
      Basis3D<double> basis3d(translations);
      // 
      // get global index list for this part
      //
      vector<int> part_indices=basis.getDefectPartIndices(ipart);
      //
      // extract the indices of fluxes belonging to this part
      //
      vector<int> iflux1=weakExtraction( part_indices, ilist1 );
      vector<int> iflux2=weakExtraction( part_indices, ilist2 );
      //
      // loop on the local flux
      //
      for ( int i=0 ; i<iflux1.size() ; i++ )
      {
        Atom<double> &atom_i1=flux[iflux1[i]].fromObject;
        Atom<double> &atom_i2=flux[iflux1[i]].toObject;
        //
        // get bond coordinates in translation basis
        //
        Vector3D<double> diff=atom_i2.coordinates()-atom_i1.coordinates();
        Vector3D<double> dual=basis3d.getCoordinatesOf(diff);
        //
        // fold into BZ
        //
        double ntx=0.0; 
        while ( dual[0]>=0.5 ) { ntx+=1.0; dual[0]-=1.0; }
        while ( dual[0]<-0.5 ) { ntx-=1.0; dual[0]+=1.0; }
        double nty=0.0; 
        while ( dual[1]>=0.5 ) { nty+=1.0; dual[1]-=1.0; }
        while ( dual[1]<-0.5 ) { nty-=1.0; dual[1]+=1.0; }
        double ntz=0.0; 
        while ( dual[2]>=0.5 ) { ntz+=1.0; dual[2]-=1.0; }
        while ( dual[2]<-0.5 ) { ntz-=1.0; dual[2]+=1.0; }
        //
        // remove long flux
        //
        if ( ntx!=0.0 || nty!=0.0 || ntz!=0.0 ) flux[iflux1[i]].value=0.0;
      }
      //
      for ( int i=0 ; i<iflux2.size() ; i++ )
      {
        Atom<double> &atom_i1=flux[iflux2[i]].fromObject;
        Atom<double> &atom_i2=flux[iflux2[i]].toObject;
        //
        // get bond coordinates in translation basis
        //
        Vector3D<double> diff=atom_i2.coordinates()-atom_i1.coordinates();
        Vector3D<double> dual=basis3d.getCoordinatesOf(diff);
        //
        // fold into BZ
        //
        double ntx=0.0; 
        while ( dual[0]>=0.5 ) { ntx+=1.0; dual[0]-=1.0; }
        while ( dual[0]<-0.5 ) { ntx-=1.0; dual[0]+=1.0; }
        double nty=0.0; 
        while ( dual[1]>=0.5 ) { nty+=1.0; dual[1]-=1.0; }
        while ( dual[1]<-0.5 ) { nty-=1.0; dual[1]+=1.0; }
        double ntz=0.0; 
        while ( dual[2]>=0.5 ) { ntz+=1.0; dual[2]-=1.0; }
        while ( dual[2]<-0.5 ) { ntz-=1.0; dual[2]+=1.0; }
        //
        // remove long flux
        //
        if ( ntx!=0.0 || nty!=0.0 || ntz!=0.0 ) flux[iflux2[i]].value=0.0;
      }
    }
  }
  //
  // reduce the flux list to non nul fluxes
  //
  cleanEmptyFluxes<Atom<double> >( flux );
}


//! \brief Apply reservoir propagation symmetry on the fluxes
/// \retval flux has been extended with nrep repetitions of the reservoit fluxes

template <class BasisType, class SystemDescription> 
void fluxUtil_extendReservoirFluxes_(BasisType &basis, SystemDescription &system_description, vector<Flux<Atom<double> > > &flux, int nrep)
{
  //
  // form the lists of global indices in flux list
  //
  vector<int> ilist1; 
  vector<int> ilist2; 
  fluxGlobalIndexLists<Atom<double> >( flux, ilist1, ilist2 );
  //
  // apply reservoir translation on reservoir flux 
  //
  for ( int ireservoir=0 ; ireservoir<basis.nReservoirs() ; ireservoir++ )
  {
    // 
    // get global index list for this reservoir
    //
    vector<int> reservoir_indices=basis.getReservoirIndices(ireservoir);
    //
    // get the reservoir propagation
    //
    Vector3D<double> propagation=system_description.reservoir(ireservoir)->propagation();
    //
    // extract the indices of reservoir fluxes
    //
    vector<int> iflux1=weakExtraction( reservoir_indices, ilist1 );
    vector<int> iflux2=weakExtraction( reservoir_indices, ilist2 );    
    //
    // minimize duplicates
    //
    vector<int> iflux;
    vector<int> reservoir_flux=weakExtraction( iflux1, iflux2 );
    for ( int i=0 ; i<reservoir_flux.size() ; i++ ) iflux.push_back(iflux2[reservoir_flux[i]]);
    
//    vector<int> iflux = iflux1 || iflux2;
    //
    // inform
    //
    cout << "duplicating " << nrep << "x" << iflux.size() << " flux for reservoir " <<  ireservoir << " with propagation " << propagation << endl;
    //
    // add translated fluxes
    //
    for ( int i=0 ; i<iflux.size() ; i++ )
    {
      //
      // copy flux info
      //
      Flux<Atom<double> > myflux=flux[iflux[i]];
      //
      // discard global indices
      //
      myflux.fromGlobalIndex=-1;
      myflux.toGlobalIndex=-1;
      //
      // loop over repetitions
      //
      for ( int irep=0 ; irep<nrep ; irep++ )
      {
        //
        // translate atoms
        // 
        myflux.fromObject+=propagation;
        myflux.toObject+=propagation;
        //
        // add flux to the list
        //
        flux.push_back(myflux);
      }
    }
  }
  //
  // remove the fluxes that are present twice
  //
  removeFluxCopies<Atom<double> >( flux );
}

//! \brief Post process a given channel file to extract the flux
/// So far, this function is intended to run on a single proc
  
template <class BasisType,class SystemDescriptionType>
void postProcessChannelFile( Context &context, BasisType &basis, SystemDescriptionType &system_description, string fileName, vector<string> options )
{
  //
  // allocate structures for channels info
  //
  int nChannels;
  vector<int> axe;  // extracted but not used in the following
  vector<int> iglobal;
  vector<Atom<double> > atom;
  vector<vector<complex<double> > > channel;
  //
  // allocate structures for flux
  //
  vector<Flux<Atom<double> > > flux;
  //
  // bounding box of the system
  //
  Box<double> defect_box;
  //
  // list of atoms of the system
  //
  vector<Atom<double> > atom_list;

  int nx=512;
  int ny=128;
  int nz=128;
  double cutoff=7.0;
  double alpha=3.0;
  double bond_cutoff=2.7;
  vector<Box<double> > clip_box;
  
  string fName=fileName;
  string key=".txt";
  size_t found=fName.rfind(key);
  if (found!=string::npos)
    fName.replace (found,key.length(),"");
  
  
  if (1)
  {
  //
  // read the channel file 
  //
  ReadChannelsFromFile(fileName,nChannels,axe,iglobal,atom,channel);
  //
  // inform
  //
  cout << "reading channels from file ok" << endl;


  //
  // determine the defect box before extending the reservoir fluxes
  //
  {
    double xmin= 1E10;
    double xmax=-1E10;
    double ymin= 1E10;
    double ymax=-1E10;
    double zmin= 1E10;
    double zmax=-1E10;
    //
    // loop on the atoms of the fluxes
    //
    for ( int iat=0 ; iat<atom.size() ; iat++ )
    {
      //
      if ( atom[iat].x_pos>xmax ) xmax=atom[iat].x_pos;
      if ( atom[iat].x_pos<xmin ) xmin=atom[iat].x_pos;
      if ( atom[iat].y_pos>ymax ) ymax=atom[iat].y_pos;
      if ( atom[iat].y_pos<ymin ) ymin=atom[iat].y_pos;
      if ( atom[iat].z_pos>zmax ) zmax=atom[iat].z_pos;
      if ( atom[iat].z_pos<zmin ) zmin=atom[iat].z_pos;
      //
//      if ( atom_i2.x_pos>xmax ) xmax=atom_i2.x_pos;
//      if ( atom_i2.x_pos<xmin ) xmin=atom_i2.x_pos;
//      if ( atom_i2.y_pos>ymax ) ymax=atom_i2.y_pos;
//      if ( atom_i2.y_pos<ymin ) ymin=atom_i2.y_pos;
//      if ( atom_i2.z_pos>zmax ) zmax=atom_i2.z_pos;
//      if ( atom_i2.z_pos<zmin ) zmin=atom_i2.z_pos;
    }
    //
    defect_box.setFromCoordinates(xmin,ymin,zmin,xmax,ymax,zmax);
  }


  //
  // compute ldos
  //
  vector<Dos<Atom<double> > > dos;
  computeDos<Atom<double> >(iglobal,atom,channel,dos);
  
  cout << "channel size:" << channel.size() << endl;
  cout << "dos size:" << dos.size() << endl;
  //
  // apply periodicity
  //
  dosUtil_applyPartTranslations_<BasisType,Atom<double> >(basis,dos);

  cout << "dos size:" << dos.size() << endl;

  //
  // extend ldos on reservoirs 
  //
  dosUtil_extendReservoirDos_<BasisType,HarmonicOscillatorSystemDescription,Atom<double> >(basis,system_description,dos);

  cout << "dos size:" << dos.size() << endl;
  //
  // compress
  //
  dosUtil_compressDos_<Atom<double> >(dos);

  cout << "dos size:" << dos.size() << endl;
  //
  // export dos for povray
  //
  printLDosDensityGrid<double,Atom<double> >(fName+"_ldos",dos,defect_box,clip_box,nx,ny,nz,cutoff,alpha);

  
  //
  // compute the fluxes
  //
  computeFluxes<BasisType>(basis,context,flux,iglobal,atom,channel);
  //
  // release memory hold by the channels (not needed anymore)
  //
  channel.resize(0);
  //
  // inform
  //
  cout << "flux computation ok" << endl;
  
  
  //
  // construct the list of atoms
  //
  atom_list=fluxObjectList<Atom<double> >( flux );
  
  //
  // fold the fluxes corresponding the same pair of atoms
  //
  foldFluxes<Atom<double> >( flux );
  //
  // inform
  //
  cout << "flux folding ok" << endl;
  
  
  //
  // keep only one flux per atom pair
  //
  removeFluxCopies<Atom<double> >( flux );
  //
  // inform
  //
  cout << "remove doubles ok" << endl;
       
       
  //
  // apply minimal image convention on the flux
  //
  fluxUtil_allpyMinimalImageConvention_<HarmonicOscillatorBasis>( basis, flux );
  //
  // inform
  //
  cout << "minimal image convention ok" << endl;
  
  //
  // print the list of flux
  //
  saveFluxes("fluxes.dat", flux); 
  
  }
  else
  {
    loadFluxes("fluxes.dat", flux );
    
    //
    // determine the defect box before extending the reservoir fluxes
    //
    {
      double xmin= 1E10;
      double xmax=-1E10;
      double ymin= 1E10;
      double ymax=-1E10;
      double zmin= 1E10;
      double zmax=-1E10;
      //
      // loop on the atoms of the fluxes
      //
      for ( int iflux=0 ; iflux<flux.size() ; iflux++ )
      {
        Atom<double> &atom_i1=flux[iflux].fromObject;
        Atom<double> &atom_i2=flux[iflux].toObject;
        //
        if ( atom_i1.x_pos>xmax ) xmax=atom_i1.x_pos;
        if ( atom_i1.x_pos<xmin ) xmin=atom_i1.x_pos;
        if ( atom_i1.y_pos>ymax ) ymax=atom_i1.y_pos;
        if ( atom_i1.y_pos<ymin ) ymin=atom_i1.y_pos;
        if ( atom_i1.z_pos>zmax ) zmax=atom_i1.z_pos;
        if ( atom_i1.z_pos<zmin ) zmin=atom_i1.z_pos;
        //
        if ( atom_i2.x_pos>xmax ) xmax=atom_i2.x_pos;
        if ( atom_i2.x_pos<xmin ) xmin=atom_i2.x_pos;
        if ( atom_i2.y_pos>ymax ) ymax=atom_i2.y_pos;
        if ( atom_i2.y_pos<ymin ) ymin=atom_i2.y_pos;
        if ( atom_i2.z_pos>zmax ) zmax=atom_i2.z_pos;
        if ( atom_i2.z_pos<zmin ) zmin=atom_i2.z_pos;
      }
      //
      defect_box.setFromCoordinates(xmin,ymin,zmin,xmax,ymax,zmax);
    }
    
    //
    // construct atom list
    //
    atom_list=fluxObjectList<Atom<double> >( flux );
  }
  
  //
  // extend flux list on and beyond the reservoirs
  //
  int nrep=2;
  fluxUtil_extendReservoirFluxes_<HarmonicOscillatorBasis,HarmonicOscillatorSystemDescription>( basis, system_description,flux, nrep );
  //
  // inform
  //
  cout << "reservoir flux extended" << endl;
        
  //
  // print  the system
  //
  atom2POV("system.pov", atom_list, bond_cutoff, defect_box, clip_box );
    
  //
  // print the flux
  //    
  printFluxDensityGrid(fName+"_flux", flux, defect_box, clip_box, nx, ny, nz, cutoff, alpha );
}

#endif


#ifndef HARMONICOSCILLATORRESERVOIR_H
#define HARMONICOSCILLATORRESERVOIR_H

#include "L2Basis.h"
#include "L2Reservoir.h"

#define REAL_SYSTEM_TYPE    1
#define COMPLEX_SYSTEM_TYPE 2
#define TWO_PI              6.283185307179586

template <class Precision>
class HarmonicOscillatorReservoir : public L2Reservoir<Precision>
{
  public:
  
    // constructor
    HarmonicOscillatorReservoir(HarmonicOscillatorBasis &basis, Context &context, int ireservoir);
  
    // destructor
    ~HarmonicOscillatorReservoir() {}
};

template <class Precision>
HarmonicOscillatorReservoir<Precision>::HarmonicOscillatorReservoir(HarmonicOscillatorBasis &basis, Context &context, int ireservoir) :
L2Reservoir<Precision>(basis,context,ireservoir) 
{
  //
  // get reservoir label
  //
  this->label_=basis.reservoirLabel(ireservoir);
  //
  // set system type
  //
  this->system_type_=REAL_SYSTEM_TYPE;
  //
  // write some garbage
  // 
  if ( context.myProc()==0 && context.myContext()<=0 )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "HarmonicOscillatorReservoir:" << endl << endl;
    cout << "*** initializing real system for reservoir " << ireservoir << " ***" << endl;
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
  }
  //
  // if we are on te working proc
  //
  if ( context.myProc()==this->working_proc_ )
  {
    //
    // get reference on the cell indices
    //
    vector<vector<vector<int> > > &cell_indices=this->cell_indices_;
    //
    // get reference on the interaction arrays
    //
    vector<vector<vector<complex<Precision> > > > &H_coeff=this->H_coeff_;
    //
    // get number of repetitions and working proc
    //
    int nrep1=this->nrep1_;
    int nrep2=this->nrep2_;
    int working_proc=this->working_proc_;
    //
    // allocate memory for the array coefficients  
    // 
    H_coeff.resize(nrep1);
    for ( int irep1=0 ; irep1<nrep1 ; irep1++ ) H_coeff[irep1].resize(nrep2);
    // 
    // get the translations for this reservoir
    //
    vector<Vector3D<double> > translations=basis.getReservoirTranslations(ireservoir);
    //
    // determine how far are the nearest neigbor
    //
    int n_neighbor_1;
    int n_neighbor_2;
    //
    if ( translations.size()>0 )
      n_neighbor_1 = min( nrep1-1, max( (int)(basis.cutoff()/sqrt(translations[0]*translations[0])*nrep1) , 1 ) ); 
    else
      n_neighbor_1 = 0;
    //
    if ( translations.size()>1 )
      n_neighbor_2 = min( nrep2-1, max( (int)(basis.cutoff()/sqrt(translations[1]*translations[1])*nrep2) , 1 ) ); 
    else
      n_neighbor_2 = 0;
    //
    // get size of the center
    //
    int size_center=cell_indices[0][0].size()/3;
    //
    // get indices of the central slice
    //
    vector<int> center_indices;
    center_indices.insert(center_indices.end(),cell_indices[0][0].begin()+size_center,cell_indices[0][0].begin()+2*size_center);
    //
    // construct a list of central slice+neighbors
    //
    vector<int> list_index_1;
    //
    for ( int i=-n_neighbor_1 ; i<=n_neighbor_1 ; i++ )
    for ( int j=-n_neighbor_2 ; j<=n_neighbor_2 ; j++ )
    {
      //
      // cyclisation of the indices  
      //
      int i_ = ( i<0 ) ? i+nrep1 : i;
      int j_ = ( j<0 ) ? j+nrep2 : j;
      i_ = ( i_>=nrep1 ) ? i_-nrep1 : i_;
      j_ = ( j_>=nrep2 ) ? j_-nrep2 : j_;
      //
      // add indices to the list
      //
      list_index_1 = list_index_1 || cell_indices[i_][j_];
    }
    //
    // loop on the repetitions
    //
    for ( int irep1=0 ; irep1<nrep1 ; irep1++ )
    for ( int irep2=0 ; irep2<nrep2 ; irep2++ )
    {
      //
      // create an ID 
      //
      char ID[256];
      sprintf(ID,"HOR_%i_r1_%i_r2_%i",ireservoir,irep1,irep2);
      //
      // construct a list of indices containing the neigboring cells
      //
      vector<int> list_index_2;
      //
      for ( int i=irep1-n_neighbor_1 ; i<=irep1+n_neighbor_1 ; i++ )
      for ( int j=irep2-n_neighbor_2 ; j<=irep2+n_neighbor_2 ; j++ )
      {
        //
        // cyclisation of the indices
        //
        int i_ = ( i<0 ) ? i+nrep1 : i;
        int j_ = ( j<0 ) ? j+nrep2 : j;
        i_ = ( i_>=nrep1 ) ? i_-nrep1 : i_;
        j_ = ( j_>=nrep2 ) ? j_-nrep2 : j_;
        //
        // add indices to the list
        //
        list_index_2 = list_index_2 || cell_indices[i_][j_];
      }
      //
      // check that the central indices are comprised on this second list
      //
      vector<int> intersection = center_indices && list_index_2;
      if ( intersection.size()>0 )
      {
        //
        // resize the coefficient array
        //
        H_coeff[irep1][irep2].resize(size_center*cell_indices[irep1][irep2].size());
        //
        // if we use external forces
        //
        if ( basis.hasReservoirFlag(ireservoir,"externalForces") )
        {
          //
          // form filename
          //
          sprintf(ID,"data/reservoir_%i.dyn",ireservoir);
          //
          // form the whole defetc index list
          //
          vector<int> full_index_list = basis.getReservoirIndices(ireservoir); 
          //
          // fill coeffs
          //
          ExternalCoeffInterface<complex<Precision> >(ID, center_indices, cell_indices[irep1][irep2], full_index_list, &(H_coeff[irep1][irep2][0])); 
        }
        //
        // otherwise, use DLpoly
        //
        else
        {
          //
          // merge the two lists
          //
          vector<int> list_index=list_index_1 && list_index_2;
          //
          // get the corresponding list of atoms
          //
          vector<Atom<double> > atom_list=basis.getAtomList(list_index);
          //
          // call the filling routine
          //
          DlpolyInterface<complex<Precision> >(ID, center_indices, cell_indices[irep1][irep2], list_index, atom_list, translations, &(H_coeff[irep1][irep2][0]));
        }
      }
      else
      {
        //
        // simply allocate those coeffs with 0
        //
        H_coeff[irep1][irep2].resize(size_center*cell_indices[irep1][irep2].size(),complex<Precision>(0.0,0.0));
      }
    }
    //
    // check the symmetry
    //
    this->checkSymmetry();
    //
    // symmetrize
    //
    this->symmetrize();
    //
    // reduce
    //
    this->reduce(1.0E-8,0.0);
  }
}

#endif

#include "FileHandler.h"

FileHandler::~FileHandler(void)
{
  //
  // close all the remaining files 
  //
  while ( filestreampile_.size()>0 )
  {
    filestreampile_.back()->close();
    delete filestreampile_.back();
    filestreampile_.pop_back();
  }
}

void  FileHandler::open(const char *fileName)
{
  //
  // check that there is no current file stream 
  //
  if ( filestreampile_.size()>0 )
  {
    cout << "Error: trying to open file " << fileName << " with a file Handler alreday busy" << endl;
    exit(0);
  }
  //
  // open the file stream
  //
  filestreampile_.push_back(new ifstream);
  filestreampile_.back()->open(fileName);
  //
  // check operation
  //
  if ( filestreampile_.back()->fail() )
  {
    cout << "Cannot open file " << fileName << " in - FileHandler::open -" << endl;
    exit(0);
  }
  //
  // set eof flag
  //
  eof_=filestreampile_.back()->eof();
}

void  FileHandler::close(void)
{
  while ( filestreampile_.size()>0 )
  {
    filestreampile_.back()->close();
    filestreampile_.pop_back();
  }
}

void FileHandler::skipLine(void)
{ 
  //
  // check that there is something to read
  //
  if ( filestreampile_.size()==0 )
  {
    cout << "nothing to read for - FileHandler::readNextWord -" << endl;
  }
  //
  // skip the whole line
  //
  filestreampile_.back()->getline(buff_,1024);
}

bool FileHandler::eof(void)
{
  return eof_;
}

char* FileHandler::readNextWord(void)
{
  //
  // check that there is something to read
  //
  if ( filestreampile_.size()==0 )
  {
    cout << "nothing to read for - FileHandler::readNextWord -" << endl;
  }
  //
  // read the content skipping white spaces
  //
  *(filestreampile_.back()) >> skipws >> buff_;
  //
  // test end of file
  //
  if ( filestreampile_.back()->eof() )
  {
    //
    // close file stream
    //
    filestreampile_.back()->close();
    //
    // delete file stream object
    //
    delete filestreampile_.back();
    //
    // return to previous stream if any
    //
    filestreampile_.pop_back();
    //
    // read next word if any
    // 
    if ( filestreampile_.size()>0 )
    {
      //
      // read the next word
      //
      readNextWord();
    }
    else
    {
      //
      // set eof flag if necesary
      //
      if ( filestreampile_.size()==0 ) eof_=true;
      //
      // set buff to an empty string
      //
      buff_[0]=0;
    }
    //
    // return pointer on buffer
    // 
    return buff_;
  }
  //
  // test input operation
  //
  if ( filestreampile_.back()->fail() )
  {
    cout << "Error while reading file" << endl;
    exit(0);
  }
  //
  // if we just returned from a file
  //
  if ( !strcmp( buff_,"</file>" ) )
  {
    //
    // read the content skipping white spaces
    //
    *(filestreampile_.back()) >> skipws >> buff_;
    //
    // test input operation
    //
    if ( filestreampile_.back()->fail() )
    {
      cout << "Error while reading file" << endl;
      exit(0);
    }
  }
  //
  // detect if the next command imply opening a new file
  //
  if ( !strcmp( buff_,"<file" ) || !strcmp( buff_,"<file>" ) )
  {
    //
    // get the name of the file
    //
    *(filestreampile_.back()) >> skipws >> buff_;
    //
    // test input operation
    //
    if ( filestreampile_.back()->fail() )
    {
      cout << "Error while reading file name in - FileHandler::readNextWord" << endl;
      exit(0);
    }
    //
    // isolate filename
    //
    char *tmp=strstr(buff_,">");
    if ( tmp!=NULL ) *tmp = 0;
    tmp=strstr(buff_,"<");
    if ( tmp!=NULL ) *tmp = 0;
    //
    // push back current file stream in the list
    //
    filestreampile_.push_back(new ifstream);
    //
    // open a new file stream 
    //
    filestreampile_.back()->open(buff_);
    //
    // check operation
    //
    if ( filestreampile_.back()->fail() )
    {
      cout << "Cannot open file " << buff_ << " in - FileHandler::open -" << endl;
      exit(0);
    }
    //
    // read the nex word in this new file 
    //
    readNextWord();
  }
  else if ( strstr(buff_,"<file" ) )
  {
    //
    // extract fileName 
    //
    char *tmp;
    if ( !strstr(buff_,"<file " ) ) tmp=strstr(buff_,">")+1;
    else tmp=strstr(buff_,"<file " )+6;
    //
    // remove end tag if any
    //
    char *end=strstr(tmp,"<");
    if ( end ) end[0]=0;
    //
    // push back current file stream in the list
    //
    filestreampile_.push_back(new ifstream);
    //
    // open a new file stream 
    //
    filestreampile_.back()->open(tmp);
    //
    // check operation
    //
    if ( filestreampile_.back()->fail() )
    {
      cout << "Cannot open file " << tmp << " in - FileHandler::open -" << endl;
      exit(0);
    }
    //
    // read the nex word in this new file 
    //
    readNextWord();
  }
  
  return buff_;
}






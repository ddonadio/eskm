#ifndef PART_H
#define PART_H

#include "Subspace.h"

template <class SolutionType>
class Part
{
  protected:
   
    // communicator 
    Context   &context_;
    int       my_proc_;
    int       n_procs_;
    
  public:
    
    // context access for derived classes
    Context &context(void) {return context_;}
    
    // access the local solution for a given energy 
    virtual Subspace<SolutionType>* getLocalSolution(vector<int> &index_list, double energy)=0;
    
    // allow to check a local solution for a given energy
    virtual void checkLocalSolution(Subspace<SolutionType> &subspace, double energy, double threshold=0)=0; 
    // unfortunately, the following is not permitted by c++ standards, 
    // so one should implement it specifically for each derived class.
    /*
    template <class SubspaceType>
    virtual bool checkLocalSolution(Subspace<SubspaceType> &subspace, double energy, double threshold)=0; 
    */
    
    // constructor
    template <class BasisType>
    Part(BasisType &basis, Context &context, int ipart);
    
    // check that everything if fine before running
    virtual bool check(void)=0;
    
    // destructor
    ~Part(){}
};

template <class SolutionType>
template <class BasisType>
Part<SolutionType>::Part(BasisType &basis, Context &context, int ipart) : context_(context)
{
  my_proc_=context_.myProc();
  n_procs_=context_.nProcs();
}

#endif

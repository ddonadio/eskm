#ifndef HARMONICOSCILLATORBASIS_H
#define HARMONICOSCILLATORBASIS_H

#include "L2Basis.h"
#include "HarmonicOscillatorState.h"
#include "HarmonicOscillatorSystemDescription.h"

class HarmonicOscillatorBasis : public L2Basis<HarmonicOscillatorState,HarmonicOscillatorSystemDescription>
{
  public:
    
    // virtual methods of the base class
    virtual void constructBasis(HarmonicOscillatorSystemDescription &system, bool verbose=true);
    
    // new method 
    vector<Atom<double> > getAtomList(vector<int> &index_list);
    
    // constructor
    HarmonicOscillatorBasis(void) {}
  
    // destructor
    ~HarmonicOscillatorBasis(void) {}
};

#endif

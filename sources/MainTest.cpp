#include "Context.h"
#include "TESolver.h"
#include "IntersectionTree.h"
#include "HarmonicOscillatorPart.h"
#include "HarmonicOscillatorReservoir.h"

// must be included after basis header files
#include "PostProcessChannelFile.h"

using namespace std;

#define PRECISION double
#define TIMING

extern TimerMap global_subspace_tmap;

// declared down there
void computeTESpectrum(Context &main_context, char *fileName, int nContexts, string tmpPath);
void processChannels(Context &context, char *inputFileName, char *channelFileName);

//! Usage display of the program
void printUsage(char *program)
{
  cout << endl;
  cout << program << " usage:" << endl;
  cout << "        " << program << " fileName [-split <nSubcontexts>] [-process channelFile] [-tmpPath pathName] [-npdlpoly <nProcessors>]" << endl;
  cout << endl;
  cout << " with :" << endl;
  cout << "        " << "-split <n>           -> split the processor grid in <n> contexts, distributing the energies to be computed." << endl;
  cout << "        " << "-process channelFile -> only compute and export the fluxes corresponding to the channels stored in channelFile." << endl;  
  cout << "        " << "-tmpPath pathName    -> use pathName/ for local disk storage of temporary datas." << endl;
  cout << "        " << "-npdlpoly <n>        -> invoke dlpoly with 'mpirun -np <n>'. Default value is 1" << endl;
  cout << endl; 
  exit(0);
}

int main(int argc, char **argv)
{
  //
  // check that we got at least one argument
  //
  if (argc<2) printUsage(argv[0]);
  //
  // extract program arguments
  //
  char *inputFileName=argv[1];
  //
  // temporary file storage path
  //
  string tmpPath="";
  
  //
  // set optional arguments default parameters
  // 
  int nContexts=1;
  int nFakeProcs=0;
  int nDlProcessors = 1;
  char *channelFileName=NULL;
  //
  // extract eventual optional arguments
  // 
  for ( int iarg=2 ; iarg<argc ; iarg++ )
  {
    if ( !strcmp(argv[iarg],"-process") )
    {
      //
      // get the name of the file to process
      //
      iarg++;
      if ( iarg<argc ) channelFileName=argv[iarg];
      else printUsage(argv[0]);  
    }
    else if ( !strcmp(argv[iarg],"-split") )
    {
      //
      // get the number of contexes to use
      //
      iarg++;
      if ( iarg<argc ) nContexts=atoi(argv[iarg]);
      else printUsage(argv[0]);  
    }
    else if ( !strcmp(argv[iarg],"-npdlpoly") )
    {
      //
      // get the number of processes to use with dlpoly
      //
      iarg++;
      if ( iarg<argc ) nDlProcessors=atoi(argv[iarg]);
      else printUsage(argv[0]);
    }
    else if ( !strcmp(argv[iarg],"-checkGrid") )
    {
      //
      // get the number of contexes to use
      //
      iarg++;
      if ( iarg<argc ) nFakeProcs=atoi(argv[iarg]);
      else printUsage(argv[0]);  
    }
    else if ( !strcmp(argv[iarg],"-tmpPath") )
    {
      //
      // get the number of contexes to use
      //
      iarg++;
      if ( iarg<argc ) tmpPath=argv[iarg];
      else printUsage(argv[0]);  
    }
    else
    {
      cout << "unrecognized optional argument " << argv[iarg] << endl;
      printUsage(argv[0]); 
    }
    dlpolyProcessors = nDlProcessors;
  }

  //
  // init MPI if necessary
  //
#ifdef WITH_MPI
  MPI_Init(&argc,&argv);
#endif
  {
    //
    // get number of procs
    //
    int n_procs=1;
#ifdef WITH_MPI
    MPI_Comm_size( MPI_COMM_WORLD, &n_procs);
#endif
    //
    // create the list of procs:
    //
    bool fakeContext;
    vector<int> proc_list;
    //
    // if this is a true context
    //
    if ( nFakeProcs==0 )
    {
      fakeContext=false;
      for ( int i=0 ; i<n_procs ; i++ ) proc_list.push_back(i);
    }
    else
    {
      fakeContext=true;
      for ( int i=0 ; i<nFakeProcs ; i++ ) proc_list.push_back(i);
    }
    //
    // create the main context
    //
    Context context(proc_list, MPI_COMM_WORLD,fakeContext);      
    //
    // set random seed
    //
    srand(context.myProc());
    
    //
    // if we want to procces a channel file
    //
    if ( channelFileName!=NULL )
    {
      processChannels(context, inputFileName, channelFileName);
    }
    
    //
    // otherwise, run a normal TE calculation
    //
    else
    {
      computeTESpectrum(context, inputFileName, nContexts, tmpPath);
    }
    
    //
    // inform that everything is ok before destroying the context
    //
    if ( context.myProc()==0 ) cout << endl << "exiting program normally" << endl << endl;
  }
  //
  // Finalise MPI now that context is released
  //
#ifdef WITH_MPI 
  MPI_Finalize();
#endif                 
  //
  // return
  //
  return 1;
}


void processChannels(Context &context, char *inputFileName, char *channelFileName)
{
  //
  // check that we are using only one proc
  //
  if ( context.nProcs()!=1 ) 
  {
    cout << "Error: processing channel file is implemented only to work serial" << endl;
    exit(0);
  }
  //
  // create the system description object  
  //
  HarmonicOscillatorSystemDescription system_description; 
  //
  // read description from file, verbose only on proc 0
  //
  system_description.readFromFile(inputFileName,true);
  //
  // create basis object
  //
  HarmonicOscillatorBasis basis;
  //
  // construct the basis from the system description, verbose only on proc 0
  //
  basis.constructBasis(system_description,true); 
  //
  // init context 
  //
  context.init(basis);
  //
  // process the channel file
  //
  vector<string> options;
  postProcessChannelFile( context, basis, system_description, channelFileName, options );
}


    
void computeTESpectrum(Context &main_context, char *inputFileName, int nContexts, string tmpPath)
{
  //
  // define timing utility if necessary
  //
#ifdef TIMING
  TimerMap tmap;
  tmap["total time"].start();
#endif
  //
  // create the system description object  
  //
  HarmonicOscillatorSystemDescription system_description; 
  //
  // read description from file, verbose only on proc 0
  //
  system_description.readFromFile(inputFileName,main_context.myProc()==0);
  //
  // create basis object
  //
  HarmonicOscillatorBasis basis;
  //
  // construct the basis from the system description, verbose only on proc 0
  //
  basis.constructBasis(system_description,main_context.myProc()==0); 
  //
  // divide context 
  //
  Context* new_context=main_context.divide(nContexts);
  //
  // display on wich context we are
  //
  if ( new_context!=NULL )
  {
    //
    // init context 
    //
    new_context->init(basis);
    //
    // init list of part
    //
    vector<HarmonicOscillatorPart<PRECISION> *> part_list;
    //
    // init list of reservoir
    //
    vector<HarmonicOscillatorReservoir<PRECISION> *> reservoir_list;
    //
    // create the reservoirs
    //
    for ( int ireservoir=0 ; ireservoir<basis.nReservoirs() ; ireservoir++ )
    {
      reservoir_list.push_back(new HarmonicOscillatorReservoir<PRECISION>(basis,*new_context,ireservoir));
      //
      // if we are only computing the interactions
      //
      if ( system_description.hasFlag("interactionsOnly") )
      {
        //
        // delete this reservoir to release memory
        //
        delete reservoir_list.back();
      }
    }
    //
    // create a tree
    //
    IntersectionTree<PRECISION> tree(*new_context);
#ifdef TIMING
    tmap["creating parts"].start();
#endif
    //
    // end calculation if we are only parsing the input file
    //
    if ( system_description.hasFlag("parseOnly") ) 
    {
      //
      // wait for everyone and exit
      //
#ifdef WITH_MPI     
      MPI_Barrier(MPI_COMM_WORLD);
#endif                 
      return;
    }
    //
    // create the parts and register
    //
    for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ )
    {
      //
      // create the part
      //
      part_list.push_back( new HarmonicOscillatorPart<PRECISION>(basis,*new_context,ipart,tmpPath) );
      //
      // if we are only computing the interactions
      //
      if ( system_description.hasFlag("interactionsOnly") )
      {
        //
        // delete this part to release memory
        //
        delete part_list.back();
      }
    }
#ifdef TIMING
    tmap["creating parts"].stop();
#endif
    //
    // end calculation if we are only computing the interations
    //
    if ( system_description.hasFlag("interactionsOnly") ) 
    {
      //
      // wait for everyone and exit
      //
#ifdef WITH_MPI     
      MPI_Barrier(MPI_COMM_WORLD);
#endif                 
      return;
    }
#ifdef TIMING
    tmap["checking parts"].start();
#endif
    //
    // check the parts if possible
    //
    if ( !main_context.isFake() )
    {
      for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ ) part_list[ipart]->check();
    }
#ifdef TIMING
    tmap["checking parts"].stop();
#endif
    //
    // register the parts once they are checked, 
    // that is now that the domains are reduced.
    //
    for ( int ipart=0 ; ipart<basis.nDefectParts() ; ipart++ )
    {
      //
      // add a leaf to the tree
      //
      tree.addLeaf( "part" , part_list[ipart]->domain() );
      //
      // register this part to the tree
      //
      tree.setPartPtr( ipart , part_list[ipart] ); 
    }
    //
    // branch the tree
    //
    tree.branch();
    //
    // print tree on proc 0
    //
    if ( main_context.myProc()==0 ) tree.printTree();
    //
    // create the list of indices on the contact
    //
    vector<int> contactIndices=basis.getContactStateIndexList();
    //
    // reduce the tree if possible
    //
    if ( main_context.myProc()==0 ) 
    {
      cout << endl;
      cout << "-------------------------------------------------------------------------" << endl;
      cout << "Tree reduction/checking:" << endl;
      cout << endl;
    }
    //    
    if ( !(system_description.hasFlag("exportChannels")) )
    {
      //
      // inform
      //
      if ( main_context.myProc()==0 ) 
      {
        cout << "  reduce intersection domains using " << contactIndices.size() << " contact states" << endl;
      } 
      //
      // reduce
      //
      tree.reduceDomains(contactIndices);
    }
    //
    // print tree graph
    //
    if ( main_context.myProc()==0 )
    {
      ofstream fstr;
      char buff[256];
      sprintf(buff,"TreeGraph.dot");
      fstr.open(buff);
      //
      // print graph
      //
      tree.printDotGraph(fstr);
      fstr.close();
    }    
    //
    // test intersection dimension
    //
    int kernelDimension=tree.testDimension( 0.0 );
    {
      //
      // get state indices for kernel
      //
      vector<int> kernelDomain=tree.solutionStateIndices();
      //
      // extract missing states
      // 
      vector<int> missingStates = contactIndices!=kernelDomain;
      //
      // check dimension after subspace expansion
      //
      if ( missingStates.size()+kernelDimension != contactIndices.size() / 2 )
      {
        cout << "Error: missing kernel directions: " << kernelDimension << " + " << missingStates.size() << " (expansion) != " << contactIndices.size()/2 << endl;
        exit(0);
      }
      else
      {
        if ( main_context.myProc()==0 ) 
        {
         cout << "  found kernel dimension of " << kernelDimension << " to be expanded by " << missingStates.size() << " trivial directions" << endl;  
        }
      }
    }
    if ( main_context.myProc()==0 ) 
    {
      cout << "-------------------------------------------------------------------------" << endl;
    }
    //
    // end calculation if we are only checking the dimension of the kernel
    //
    if ( system_description.hasFlag("checkDimensionOnly") || main_context.isFake() ) 
    {
      //
      // wait for everyone and exit
      //
#ifdef WITH_MPI     
      MPI_Barrier(MPI_COMM_WORLD);
#endif                 
      return;
    }
    //
    // init the TE solver
    //
    TESolver<PRECISION,HarmonicOscillatorReservoir<PRECISION> > solver(basis,reservoir_list,tree,*new_context);
    //
    // get the energy ramp
    //
    vector<double> eigenvalues=system_description.eigenvalues();
    vector<double> myEigenvalues;
    //
    // split the eigen values among the contexts
    //
    {
      vector<int> nEigenvalues(nContexts,eigenvalues.size()/nContexts);
      for ( int i=0 , n=nEigenvalues[0]*nContexts ; n<eigenvalues.size() ; i++ , n++ ) nEigenvalues[i]++;
      //
      int istart=0;
      for ( int i=0 ; i<new_context->myContext() ; i++ ) istart+=nEigenvalues[i];
      int iend=istart+nEigenvalues[new_context->myContext()];
      //
      myEigenvalues.insert(myEigenvalues.end(),eigenvalues.begin()+istart,eigenvalues.begin()+iend);
    }
    //
    // open a different files for each context
    //
    ofstream fstr;
    {
      char buff[256];
      sprintf(buff,"transmission_part_%i",new_context->myContext());
      fstr.open(buff);
    }
    //
    // loop on local energies
    //
    for ( int i=0 ; i<myEigenvalues.size() ; i++ )
    {
      //
      // solve 
      //
      if ( solver.solve(1,myEigenvalues[i]) )
      {
        //
        // export channels if requested
        //         
        if ( system_description.hasFlag("exportChannels") )
        {
          char buff[256]; sprintf(buff,"channels_%f.txt",HarmonicOscillatorSystemDescription::convertEigenvalue(myEigenvalues[i]));
          solver.exportL2Channels<HarmonicOscillatorBasis,HarmonicOscillatorState>(buff, 0, basis);
        }
        //
        // print
        //
        solver.printReducedTransmisionMatrix(fstr,"test",&HarmonicOscillatorSystemDescription::convertEigenvalue);
//         solver.printTransmisionMatrix(fstr,"test",&HarmonicOscillatorSystemDescription::convertEigenvalue);
      }
    }  
    //
    // close the file
    //
    fstr.close();
    // 
    // delete reservoir objects
    //
    for ( int ireservoir=0 ; ireservoir<reservoir_list.size() ; ireservoir++ ) delete reservoir_list[ireservoir];
    // 
    // delete parts objects
    //
    for ( int ipart=0 ; ipart<part_list.size() ; ipart++ ) delete part_list[ipart];
  } // end condition over context
  //
  // wait for everyone
  //
#ifdef WITH_MPI     
  MPI_Barrier(MPI_COMM_WORLD);
#endif  
  //
  // print timing for subspace operations
  //
  for ( int i=0 ; i<main_context.nProcs() ; i++ )
  {
    //
    // generate header
    //
    char buff[256];
    sprintf(buff,"Global Subspace timings on proc %i:",i);
    string str=buff;
    //
    // print timings
    //
    global_subspace_tmap.print(cout,str,main_context,i);
  }
  //
  // print timing for this routine if wanted
  //
#ifdef TIMING
  tmap["total time"].stop();
  tmap.print(cout,"computeTESpectrum diver routine",main_context,0);
#endif
}






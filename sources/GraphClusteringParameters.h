/// \file GraphClusteringParameters.h
/// \brief Container class declarations for the parameters of the clusterinf method

#ifndef GRAPHCLUSTERINGPARAMETERS_H
#define GRAPHCLUSTERINGPARAMETERS_H

#include "FileHandler.h"

class GraphClusteringParameters
{
  public:
    
    // public parameters
    int  nClusters;             ///< number of cluster for which the kMeans algorithm is performed, should be untouched unless one want to study repartition for a given grid.
    int  kMeansDim;             ///< number of coordinates on which the kMeans algorithm is performed, if 0 this parameter is optimized
    int  kMeansMaxSteps;        ///< maximum number of steps for the kMeans algorithm
    int  kMeansDimMaxOptSteps;  ///< maximum number of steps when optimizing the dimension for the kMeans algorithm (0 to try all dimensions)
    int  slackAmount;           ///< amount of slack for the groups in percent, compared to a tigh equal repartiton
    bool refineGroups;          ///< if shall perform refining of the groups after kMeans clustering
    char fileLabel[256];        ///< label to identify the file to use for the gridding parameters
    
    /// constructor definnig default values
    GraphClusteringParameters(void)
    {
      nClusters=0;
      kMeansDim=0;
      kMeansMaxSteps=100;
      kMeansDimMaxOptSteps=0;
      slackAmount=10;
      refineGroups=true;
      fileLabel[0]=0;
    }
    
    /// reading values from a file handler
    void readFromFile(FileHandler &fileHandler)
    {
      char *buff;
      bool endOfParam=false;
      // while there is something to read
      while ( !endOfParam )
      { 
        // get last word
        buff=fileHandler.readNextWord();
        // nClusters value
        if ( !strcmp( buff,"nClusters" ) )
        {
          buff=fileHandler.readNextWord(); nClusters=atoi(buff); 
        }
        // kMeansDim value
        if ( !strcmp( buff,"kMeansDim" ) )
        {
          buff=fileHandler.readNextWord(); kMeansDim=atoi(buff); 
        }
        // kMeansMaxSteps value
        if ( !strcmp( buff,"kMeansMaxSteps" ) )
        {
          buff=fileHandler.readNextWord(); kMeansMaxSteps=atoi(buff); 
        }
        // kMeansDimMaxOptSteps value
        if ( !strcmp( buff,"kMeansDimMaxOptSteps" ) )
        {
          buff=fileHandler.readNextWord(); kMeansDimMaxOptSteps=atoi(buff); 
        }
        // slackAmount value
        if ( !strcmp( buff,"slackAmount" ) )
        {
          buff=fileHandler.readNextWord(); slackAmount=atoi(buff); 
        }
        // refineGroups value
        if ( !strcmp( buff,"refineGroups" ) )
        {
          buff=fileHandler.readNextWord(); refineGroups=atoi(buff); 
        }
        // fileLabel value
        if ( !strcmp( buff,"fileLabel" ) )
        {
          buff=fileHandler.readNextWord(); strcpy(fileLabel,buff); 
        }
        // if end of parameters definition
        if ( !strcmp( buff,"</graphClustering>" ) ) 
        { 
          endOfParam=true;
        }
      }
    }
};

#endif

/// \file Basis.h
/// \brief Basis base class declarations

#ifndef BASIS_H
#define BASIS_H

#include <memory>
#include <string>
#include <map>
#include <math.h>
#include <vector>
#include <iostream>
#include "HarmonicOscillatorState.h"
#include "IndexList.h"
#include "GraphClusteringParameters.h"

using namespace std;

//! Basis Class.
/// The class Basis serve as base class for every basis defined. 
/// It sets the methods and attributes that any basis should 
/// provide to allow the scattering calculation. 

template <class StateType, class SystemDescriptionType>
class Basis
{
  protected:
    
    // basis atributes
    string                      type_;            ///< type of the basis, just for indication so far
    bool                        hasOverlaps_;     ///< flag set to 1 if the states of the basis are non orhtogonals                          
    vector<map<int,StateType> > defectPart_;      ///< map the different states of each part
    vector<map<int,StateType> > reservoir_;       ///< map the different states of each reservoir
    vector<int>                 contactIndex_;    ///< contactIndex_[i] is the index of the reservoir in contact with defect part i (-1 if no contact).
    vector<string>              reservoirLabel_;  ///< name of the different reservoirs
    vector<string>              defectPartLabel_; ///< name of the different parts
    vector<vector<string> >     defectPartFlags_; ///< flags for the different defect parts 
    vector<vector<string> >     reservoirFlags_;  ///< flags for the different reservoirs 
    
    // clustering parameters
    GraphClusteringParameters   clusterParam_;    ///< parameters to use for the clustering of parts on the procesor grid
            
  public:

    // atributes access
    int          nDefectParts(void) { return defectPart_.size(); } ///< number of parts. @return the number of parts defined for the system (including contacts).
    int          nReservoirs(void)  { return reservoir_.size(); }  ///< number of reservoirs. @return the number of reservoirs defined for the system.
    bool         hasOverlaps(void)  { return hasOverlaps_; }       ///< basis orthonormality. @return true if the basis used is not orthonormal.
    
    // labels access
    string       reservoirLabel(int ireservoir) { return reservoirLabel_[ireservoir]; } ///< reservoir label access. @return the name of this reservoir if defined. 
    string       defectPartLabel(int ipart)     { return defectPartLabel_[ipart]; }     ///< part label access. \param i_part should be a valid part index. \return the name of this part if defined.
    
    // flags access
    vector<vector<string> >     defectPartFlags(int ipart) { return defectPartFlags_[ipart]; }         ///< flags for the defect part. \param ipart should be a valid part index. \return the list of flags for the requested part 
    vector<vector<string> >     reservoirFlags(int ireservoir) { return reservoirFlags_[ireservoir]; } ///< flags for the reservoir. \param ireservoir should be a valid reservoir index. \return the list of flags for the requested reservoir
    bool                        hasDefectPartFlag(int ipart, string flag);                            ///< flags test for the defect part. \param ipart should be a valid part index. \return true if this flag is known from the part 
    bool                        hasReservoirFlag(int ireservoir, string flag);                             ///< flags test for the reservoir. \param ireservoir should be a valid part index. \return true if this flag is known from the reservoir
    
    // base class method
    bool               isContact(int i_part, int *i_reservoir=NULL);           ///< contact test. If this part is a contact, i_reservoir is set to the corresponding index. 
    bool               isDefectPart(int index, int *i_part=NULL);              ///< part belonging. 
    bool               isReservoir(int index, int *i_reservoir=NULL);          ///< reservoir belonging.
    string             getDefectPartType(int i_part, int *i_reservoir=NULL);   ///< defect part type. \param i_part should be a valid part index. \return "contact" if this part if a contact, "defect" otherwise. i_reservoir is set to the value of contactIndex_[i_part]. 
    int                getDefectPartSize(int i_part);                          ///< defect part size. 
    int                getReservoirSize(int i_reservoir);                      ///< reservoir size. 
    vector<int>        getDefectIndices(void);                                 ///< defect indices. \return the index list for the full defect, ordered as they appear in the successive parts.
    vector<int>        getDefectPartIndices(int i_part);                       ///< defect part indices. \param i_part should be a valid part index. \return the index list for this part.
    vector<int>        getReservoirIndices(int i_reservoir);                   ///< reservoir indices. \param i_reservoir should be a valid reservoir index. \return the index list for this reservoir.
    vector<StateType>  getDefectPartStates(int i_part);                        ///< defect part states. \param i_part should be a valid part index. \return the list of states for this part.
    vector<StateType>  getReservoirStates(int i_reservoir);                    ///< reservoir states. \param i_reservoir should be a valid reservoir index. \return the list of states for this reservoir.
    vector<StateType>  getStateList(vector<int> index_list);                   ///< states access. \param index_list should be a vector of valid states indices. \return the list of states corresponding to the given index, respecting the order of the list.
    void               mergeParts( int i_part_merged, int i_part_on);          ///< merging two parts. 
    void               cleanPartList(void);                                    ///< empty part removing. 
    
    // clustering parameters access
    GraphClusteringParameters getClusteringParameters(void) {return clusterParam_;}      ///< graph clustering parameters access.
    
    // virtual methods
    virtual void         constructBasis(SystemDescriptionType &system, bool verbose)=0;  ///< basis construction. \param verbose let information be displayed during creation. Each derived basis should implement is own method.
    virtual vector<int>  getContactStateIndexList(void)=0;                               ///< contact state index list access. \return the complete list of states indices for every contact part. 
    virtual vector<int>  getContactStateIndexList(int i_part)=0;                         ///< contact state index list access. \param i_part should be a valid part index. \return the list of states indices for contact part i_part.
    virtual vector<int>  getNeighborhoodPartList(int i_part)=0;                          ///< neighbor access. \param i_part should be a valid part index. \return the index list of neighboring parts.
    virtual vector<int>  getNeighborhoodStateIndexList(int i_part)=0;                    ///< neighbor access. \param i_part should be a valid part index. \return the state list of all the neighboring parts.
           
    // construtor
    Basis(void);           ///< class constructor. 
    
    // destructor
    virtual ~Basis(void);  ///< class destructor.
};

/// \return simply set the hasOverlaps_=false flag as default. 
/// This can be changed in the derived basis constructor.
template <class StateType, class SystemDescriptionType>
Basis<StateType,SystemDescriptionType>::Basis(void)
{
  // set overlaps flag to 0 as default
  hasOverlaps_=false;
}
    
// destructor
template <class StateType, class SystemDescriptionType>
Basis<StateType,SystemDescriptionType>::~Basis(void)
{}

/// return true if this flag is known for a given part
///
template <class StateType, class SystemDescriptionType>
bool Basis<StateType,SystemDescriptionType>::hasDefectPartFlag(int i_part, string flag)
{
  //
  // check that the index is correct
  //
  if ( i_part>=defectPartFlags_.size() || i_part<0 )
  {
    cout << "Error: incorrect part index " << i_part << " in - Basis::hasDefectPartFlag -" << endl;
    exit(0); 
  }
  //
  // try to find flag
  //
  for ( int i=0 ; i<defectPartFlags_[i_part].size() ; i++ ) if ( defectPartFlags_[i_part][i]==flag ) return true;
  return false;
}

/// return true if this flag is known for a given part
///
template <class StateType, class SystemDescriptionType>
bool Basis<StateType,SystemDescriptionType>::hasReservoirFlag(int i_reservoir, string flag)
{
  //
  // check that the index is correct
  //
  if ( i_reservoir>=reservoirFlags_.size() || i_reservoir<0 )
  {
    cout << "Error: incorrect reservoir index " << i_reservoir << " in - Basis::hasReservoirFlag -" << endl;
    exit(0); 
  }
  //
  // try to find flag
  //
  for ( int i=0 ; i<reservoirFlags_[i_reservoir].size() ; i++ ) if ( reservoirFlags_[i_reservoir][i]==flag ) return true;
  return false;
}


/// isContact test if a part is also a contact (i.e. part of a reservoir).  
/// \param i_part should be a valid part index. 
/// \return true if this part is a contact.
template <class StateType, class SystemDescriptionType>
bool Basis<StateType,SystemDescriptionType>::isContact(int i_part, int *i_reservoir)
{
  //
  // check that the index is correct
  //
  if ( i_part>=defectPart_.size() || i_part<0 )
  {
    cout << "Error: incorrect part index " << i_part << " in - Basis::isContact -" << endl;
    exit(0); 
  }
  //
  // set corresponding reservoir index
  //
  if ( i_reservoir!=NULL ) (*i_reservoir)=contactIndex_[i_part];
  //
  // return result
  //
  return (contactIndex_[i_part]>=0);  
}

/// getDefectPartSize get the number of states of a defect part
/// \param i_part should be a valid part index. 
/// \return the number of states for this part.
template <class StateType, class SystemDescriptionType>
int  Basis<StateType,SystemDescriptionType>::getDefectPartSize(int i_part)
{
  //
  // check that the index is correct
  //
  if ( i_part>=defectPart_.size() || i_part<0 )
  {
    cout << "Error: incorrect part index " << i_part << " in - Basis::getDefectPartSize -" << endl;
    exit(0); 
  }
  //
  // return result
  //
  return defectPart_[i_part].size();  
}

/// getReservoirSize get the number of states of a reservoir
/// \param i_reservoir should be a valid reservoir index. 
/// \return the number of states for this reservoir.
template <class StateType, class SystemDescriptionType>
int  Basis<StateType,SystemDescriptionType>::getReservoirSize(int i_reservoir)
{
  //
  // check that the index is correct
  //
  if ( i_reservoir>=reservoir_.size() || i_reservoir<0 )
  {
    cout << "Error: incorrect reservoir index " << i_reservoir << " in - Basis::getReservoirSize -" << endl;
    exit(0); 
  }
  //
  // return result
  //
  return reservoir_[i_reservoir].size();  
}

// get the type of a defect part
template <class StateType, class SystemDescriptionType>
string  Basis<StateType,SystemDescriptionType>::getDefectPartType(int i_part, int *i_reservoir)
{
  //
  // check that the index is correct
  //
  if ( i_part>=contactIndex_.size() || i_part<0 )
  {
    cout << "Error: incorrect part index " << i_part << " in - Basis::getPartDomainIndices -" << endl;
    exit(0); 
  }
  //
  // check that the contact index is correct
  //
  if ( !( contactIndex_[i_part]<(int)reservoir_.size() ) )
  {
    cout << "Error: incorrect contact index " << contactIndex_[i_part] << " for part " << i_part << " in - Basis::getPartDomainIndices -" << endl;
    exit(0);
  }
  //
  // set reservoir index if necessary 
  //
  if ( i_reservoir!=NULL ) *i_reservoir = contactIndex_[i_part];
  //
  // return result
  //
  if ( contactIndex_[i_part]<0 )
  {
    return "defect";  
  }
  else
  {
    return "contact";  
  }
}

// access the indices of the defect parts and contact
template <class StateType, class SystemDescriptionType>
vector<int> Basis<StateType,SystemDescriptionType>::getDefectIndices(void)
{
  //
  // form index list
  //
  vector<int> index_list;
  for ( int i_part=0 ; i_part<defectPart_.size() ; i_part++ )
  {
    vector<int> tmp_list = getDefectPartIndices(i_part);
    index_list.insert(index_list.end(),tmp_list.begin(),tmp_list.end());
  }
  //
  // return result
  //
  return index_list;
}

// access the indices of a defect part
template <class StateType, class SystemDescriptionType>
vector<int> Basis<StateType,SystemDescriptionType>::getDefectPartIndices(int i_part)
{
  //
  // check that the index is correct
  //
  if ( i_part>=defectPart_.size() || i_part<0 )
  {
    cout << "Error: incorrect part index " << i_part << " in - Basis::getPartDomainIndices -" << endl;
    exit(0); 
  }
  //
  // check that the part is non empty
  //
  if ( defectPart_[i_part].size()==0 )
  {
    cout << "Error: trying to access domain indices of empty part " << i_part << " in - Basis::getPartDomainIndices -" << endl;
    exit(0);
  }
  //
  // initialize list of indices
  //
  vector<int> list_indices( defectPart_[i_part].size() );
  //
  // loop on this part map entries 
  //
  int *p_index=&list_indices[0];
  //
  for ( typename map<int,StateType>::iterator it=defectPart_[i_part].begin() , stop=defectPart_[i_part].end() ; it != stop ; it++ , p_index++ )
  {
    (*p_index) = (*it).first;
  }
  //
  // return the list
  //
  return list_indices;
}

// access the indices of a resevoir
template <class StateType, class SystemDescriptionType>
vector<int>  Basis<StateType,SystemDescriptionType>::getReservoirIndices(int i_reservoir)
{
  //
  // check that the index is correct
  //
  if ( i_reservoir>=reservoir_.size() || i_reservoir<0 )
  {
    cout << "Error: incorrect reservoir index " << i_reservoir << " in - Basis::getReservoirIndices -" << endl;
    exit(0); 
  }
  //
  // check that the reservoir is non empty
  //
  if ( reservoir_[i_reservoir].size()==0 )
  {
    cout << "Error: trying to access domain indices of empty reservoir " << i_reservoir << " in - Basis::getReservoirIndices -" << endl;
    exit(0);
  }
  //
  // initialize list of indices
  //
  vector<int> list_indices( reservoir_[i_reservoir].size() );
  //
  // loop on this resevoir map entries 
  //
  int *p_index=&list_indices[0];
  //
  for ( typename map<int,StateType>::iterator it=reservoir_[i_reservoir].begin() , stop=reservoir_[i_reservoir].end() ; it != stop ; it++ , p_index++ )
  {
    (*p_index) = (*it).first;
  }
  //
  // return the list
  //
  return list_indices;
}

// access the states of a defect part
template <class StateType, class SystemDescriptionType>
vector<StateType>  Basis<StateType,SystemDescriptionType>::getDefectPartStates(int i_part)
{
  //
  // check that the index is correct
  //
  if ( i_part>=defectPart_.size() || i_part<0 )
  {
    cout << "Error: incorrect part index " << i_part << " in - Basis::getDefectPartStates -" << endl;
    exit(0); 
  }
  //
  // check that the part is non empty
  //
  if ( defectPart_[i_part].size()==0 )
  {
    cout << "Error: trying to access domain states of empty part " << i_part << " in - Basis::getDefectPartStates -" << endl;
    exit(0);
  }
  //
  // initialize list of indices
  //
  vector<StateType> list_states;
  //
  // loop on this part map entries 
  //
  for ( typename map<int,StateType>::iterator it=defectPart_[i_part].begin() , stop=defectPart_[i_part].end() ; it != stop ; it++ )
  {
    list_states.push_back( (*it).second );
  }
  //
  // return the list
  //
  return list_states;
}

// access the states of a resevoir
template <class StateType, class SystemDescriptionType>
vector<StateType>  Basis<StateType,SystemDescriptionType>::getReservoirStates(int i_reservoir)
{
  //
  // check that the index is correct
  //
  if ( i_reservoir>=reservoir_.size() || i_reservoir<0 )
  {
    cout << "Error: incorrect reservoir index " << i_reservoir << " in - Basis::getReservoirStates -" << endl;
    exit(0); 
  }
  //
  // check that the reservoir is non empty
  //
  if ( reservoir_[i_reservoir].size()==0 )
  {
    cout << "Error: trying to access domain states of empty reservoir " << i_reservoir << " in - Basis::getReservoirStates -" << endl;
    exit(0);
  }
  //
  // initialize list of indices
  //
  vector<StateType> list_states( reservoir_[i_reservoir].size() );
  //
  // loop on this resevoir map entries 
  //
  StateType *p_states=&list_states[0];
  //
  for ( typename map<int,StateType>::iterator it=reservoir_[i_reservoir].begin() , stop=reservoir_[i_reservoir].end() ; it != stop ; it++ , p_states++ )
  {
    (*p_states) = (*it).second;
  }
  //
  // return the list
  //
  return list_states;
}

// access the states corresponding toa na index list
template <class StateType, class SystemDescriptionType>
vector<StateType>  Basis<StateType,SystemDescriptionType>::getStateList(vector<int> index_list)    
{
  //
  // init state list
  //
  vector<StateType> state_list;
  vector<int>    state_indices;
  //
  // loop on defect parts
  //
  for ( int ipart=0 ; ipart<defectPart_.size() ; ipart++ )
  {
    //
    // if this part is not a contact
    //
    if ( contactIndex_[ipart]<0 )
    {
      //
      // get the part indices
      //
      vector<int> part_indices=getDefectPartIndices(ipart);
      //
      // intersect with the index list
      //
      vector<int> intersection = part_indices && index_list;
      //
      // if the intersection is complete
      //
      if ( intersection.size()==part_indices.size() )
      {
        //
        // get the states for this part
        //
        vector<StateType> part_states=getDefectPartStates(ipart);
        //
        // insert the states in state list
        //
        state_list.insert(state_list.end(),part_states.begin(),part_states.end());
        //
        // insert the corresponding indices
        //
        state_indices.insert(state_indices.end(),part_indices.begin(),part_indices.end());
      }
      //
      // if the intersection is just partial
      //
      else if ( intersection.size()>0 )
      {
        //
        // loop on the indices of the intersection
        //
        for ( int i=0 ; i<intersection.size() ; i++ )
        {
          //
          // add this state to the list
          //
          state_list.push_back(defectPart_[ipart][intersection[i]]);
          //
          // add this index to the list
          //
          state_indices.push_back(intersection[i]);
        }
      }
    }
  }
  //
  // loop on the reservoirs
  //
  for ( int ireservoir=0 ; ireservoir<reservoir_.size() ; ireservoir++ )
  {
    //
    // get the part indices
    //
    vector<int> reservoir_indices=getReservoirIndices(ireservoir);
    //
    // intersect with the index list
    //
    vector<int> intersection = reservoir_indices && index_list;
    //
    // if the intersection is complete
    //
    if ( intersection.size()==reservoir_indices.size() )
    {
      //
      // get the states for this part
      //
      vector<StateType> reservoir_states=getReservoirStates(ireservoir);
      //
      // insert the states in state list
      //
      state_list.insert(state_list.end(),reservoir_states.begin(),reservoir_states.end());
      //
      // insert the corresponding indices
      //
      state_indices.insert(state_indices.end(),reservoir_indices.begin(),reservoir_indices.end());
    }  
    //
    // if the intersection is just partial
    //
    else if ( intersection.size()>0 )
    {
      //
      // loop on the indices of the intersection
      //
      for ( int i=0 ; i<intersection.size() ; i++ )
      {
        //
        // add this state to the list
        //
        state_list.push_back(reservoir_[ireservoir][intersection[i]]);
        //
        // add this index to the list
        //
        state_indices.push_back(intersection[i]);
      }
    }
  }
  //
  // reorder the list of states so that it correspond to the list of indices
  //
  vector<StateType> ordered_states;
  vector<int> correspondance = index_list <= state_indices;
  for ( int i=0 , stop=state_list.size() ; i<stop ; i++ ) ordered_states.push_back(state_list[correspondance[i]]);
  //
  // return the list of ordered states
  //   
  return ordered_states;
} 

/// merge the states of a part onto another, keep this first part as an empty one
/// \return After execution, part i_part_on is augmented of the 
/// indices of part i_part_merged. part i_part_merged is then let empty.
template <class StateType, class SystemDescriptionType>
void Basis<StateType,SystemDescriptionType>::mergeParts( int i_part_merged, int i_part_on )
{
  //
  // check that the first index is correct
  //
  if ( i_part_merged>=defectPart_.size() || i_part_merged<0 )
  {
    cout << "Error: incorrect part index " << i_part_merged << " for source part (arg. 1) in - Basis::mergeParts -" << endl;
    exit(0); 
  }
  //
  // check that the second index is correct
  //
  if ( i_part_on>=defectPart_.size() || i_part_on<0 )
  {
    cout << "Error: incorrect part index " << i_part_on << " for target part (arg. 2) in - Basis::mergeParts -" << endl;
    exit(0); 
  }
  //
  // check that the source part is non empty
  //
  if ( defectPart_[i_part_merged].size()==0 )
  {
    cout << "Error: trying to merge the empty part " << i_part_merged << " onto part " << i_part_on << " in - Basis::mergeParts -" << endl;
    exit(0);
  }
  //
  // copy the states to i_part_on
  //
  defectPart_[i_part_on].insert(defectPart_[i_part_merged].begin(),defectPart_[i_part_merged].end());
  //
  // remove the states from i_part_merged
  // 
  defectPart_[i_part_merged].clear();
}

/// remove the empty parts from the part list
/// \return After execution, the list is freed from empty 
/// parts that could have been created by calls to mergeParts().
template <class StateType, class SystemDescriptionType>
void Basis<StateType,SystemDescriptionType>::cleanPartList(void)
{
  int nParts=0;
  //
  // loop over the parts
  //
  for ( int i_part=0 ; i_part<defectPart_.size() ; i_part++ )
  {
    //
    // find next non empty part
    //
    while ( i_part<defectPart_.size() && defectPart_[i_part].size()==0 ) i_part++;
    //
    // if there is still a part
    //
    if ( i_part<defectPart_.size() )
    {
      //
      // copy this part
      //
      defectPart_[nParts]=defectPart_[i_part];
      //
      // increase the number of parts
      // 
      nParts++;
    }
  }
  //
  // resize the list of parts
  //
  defectPart_.resize(nParts);
}

/// isDefectPart return true if the state index passed is the one of a state
/// belonging to a part. This function can be used to determine for which
/// parts of the system the interactions are beeing computed.
/// \param index describe a state index. 
/// \return true if this state index belongs to a part and i_part is set to the corresponding part index.
template <class StateType, class SystemDescriptionType>
bool Basis<StateType,SystemDescriptionType>::isDefectPart(int index, int *i_part)
{
  if ( i_part!=NULL )
  {
    //
    // loop on each defect part
    //
    for ( (*i_part)=0 ; (*i_part)<defectPart_.size() ; (*i_part)++ )
    {
      if ( defectPart_[(*i_part)].count( index ) ) return true;
    }
    //
    // if this index is not part of the defect, 
    // invalidate index and return false
    //
    (*i_part)=-1;
    return false;
  }
  else
  {
    //
    // loop on each defect part
    //
    for ( int ipart=0 ; ipart<defectPart_.size() ; ipart++ )
    {
      if ( defectPart_[ipart].count( index ) ) return true;
    }
    //
    // if this index is not part of the defect, 
    // return false
    //
    return false;
  }
}

/// isReservoir return true if the state index passed is the one of a state
/// belonging to a reservoir. This function can be used to determine for which
/// parts of the system the interactions are beeing computed.
/// The function start comparing the index passed to the first index of every 
/// parts and reservoirs. If the index is not one of those, then it is 
/// tested over the entire basis. 
/// \param index describe a state index. 
/// \return true if this state index belongs to a reservoir and 
/// i_reservoir is set to the corresponding reservoir index.
template <class StateType, class SystemDescriptionType>
bool Basis<StateType,SystemDescriptionType>::isReservoir(int index, int *i_reservoir)
{
  if ( i_reservoir!=NULL )
  {
    //
    // loop on each reservoir
    //
    for ( (*i_reservoir)=0 ; (*i_reservoir)<reservoir_.size() ; (*i_reservoir)++ )
    {
      if ( reservoir_[(*i_reservoir)].count( index ) ) return true;
    }
    //
    // if this index is not part of the reservoir
    // invalidate index and return false
    //
    (*i_reservoir)=-1;
    return false;
  }
  else
  {
    //
    // loop on each reservoir
    //
    for ( int ireservoir=0 ; ireservoir<reservoir_.size() ; ireservoir++ )
    {
      if ( reservoir_[ireservoir].count( index ) ) return true;
    }
    //
    // if this index is not part of the reservoir
    // return false
    //
    return false;
  }
}

#endif

#ifndef SUBSPACE_H
#define SUBSPACE_H

#include <stdlib.h>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <cstring>
#include <typeinfo>

#include "Context.h"
#include "Timer.h"
#include "Lapack.h"
#include "Subspace.h"
#include "QRFactorization.h"
#include "IndexList.h"
#include "Communication.h"

using namespace std;


#define GLOBAL_TIMING

//
// global external timer for subspaces activities
//
#ifdef GLOBAL_TIMING
  extern TimerMap global_subspace_tmap;
#endif

template <class Type>
class Subspace
{
  private :
    
    // communicator 
    Context         &context_;
    int             my_proc_;
    int             n_procs_;
    
    // domain attributes
    vector<int>           list_proc_domain_;      // list of processors within context wich support the domain 
    vector<int>           global_domain_indices_; // contains the global indices of the basis set for wich we have the coefficients
    map<int,vector<int> > local_domain_indices_;  // repartition of the domain on the proc grid
    
    // subspace definition
    int            nVectors_;             // number of vectors defining this subspace
    vector<Type>   coeff_;                // local coefficients of the vectors defining the subspace
    
    // methods 
    int  sortFromWeight_(void);
    void orthonormalizeFromTop_(int n_local_indices);
    
  public :
    
    // constructors
    Subspace(Context &context);
    Subspace(vector<int> global_domain_indices, Context &context);
    Subspace(Subspace<Type> &subspace, int istart, int nVectors);
    
    // destructors
    ~Subspace();
    
    // attribute access
    vector<int> localDomain(void); 
    vector<int> localDomain(int i_proc); 
    vector<int> domain(void) { return global_domain_indices_; }
    vector<int> listProcDomain(void) { return list_proc_domain_; }
    Type*       coeffPtr(int i=0) { return &coeff_[i]; }
    int         nVectors(void) { return nVectors_; }
    
    // methods
    void    setSize(int n);
    void    randomize(void);
    void    canonical(void);
    void    keepCoeffs(vector<int> indices);
    void    expand(vector<int> indices);
    void    addDirections(Subspace<Type> &subspace);
    void    setCoeffOnTop(vector<int> coeff_index);
    void    discardCoeffs(vector<int> coeffs_indices);
    void    discardDirections(int istart, int nVectors);
    void    findOrthogonal(vector<int> &coeffs_indices);
    void    alignOnSet(vector<int> &coeffs_indices);
    void    printSet(string comments);
    void    gatherCoeffsOnProcs(vector<int> &domain_indices, vector<int> &list_proc, vector<Type> &coeff);
    void    intersectWith( Subspace<Type> &other , vector<int> &keepOnlyIndices ); 
    double  getMaxAbsoluteCoeff(void);
    
    template <class Type_other>
    void    intersectWith( Subspace<Type_other> &other , vector<int> &keepOnlyIndices ); 
    
    // operators
    void operator&=( Subspace<Type> &other );
    void operator =( Subspace<Type> &other );
    template <class Type_other>
    void operator&=( Subspace<Type_other> &other );
    template <class Type_other>
    void operator =( Subspace<Type_other> &other );
  
  friend class Subspace<float>;
  friend class Subspace<double>;
  friend class Subspace<complex<float> >;
  friend class Subspace<complex<double> >;
};

#ifndef CONJREAL_H
#define CONJREAL_H

// dummy function for compilation
float conj(float a)
{
  cout << "Error: attemp to conjugate a double in Subspace.h template" << endl;
  exit(0);
  //
  return a;
}

double conj(double a)
{
  cout << "Error: attemp to conjugate a double in Subspace.h template" << endl;
  exit(0);
  //
  return a;
}

#endif

#ifndef GETSIZE_H
#define GETSIZE_H

int getsize(float a) { return (int)a; }
int getsize(double a) { return (int)a; }
int getsize(complex<float> a) { return (int)a.real(); }
int getsize(complex<double> a) { return (int)a.real(); }

#endif
// null domain subspace constructor
template <class Type>
Subspace<Type>::Subspace(Context &context) :
context_(context)
{
  //
  // keep localisation within the context grid
  //  
  my_proc_=context.myProc();
  n_procs_=context.nProcs();
  //
  // init number of vectors
  //
  nVectors_=0;
}

// standard constructors 
template <class Type>
Subspace<Type>::Subspace(vector<int> global_domain_indices, Context &context) :
global_domain_indices_(global_domain_indices) , context_(context)
{
  //
  // keep localisation within the context grid
  //  
  my_proc_=context.myProc();
  n_procs_=context.nProcs();
  //
  // map the domain indices on the proc array
  //
  for ( int i_proc=0 ; i_proc<context.nProcs() ; i_proc++ )
  {
    //
    // get global indices on processor i_proc
    //
    vector<int> global_indices=context.getIndicesOnProc(i_proc);
    //
    // intersect domain indices with the global indices on processor i_proc
    //
    vector<int> intersection_domain=global_indices&&global_domain_indices;
    //
    // if the intersection is non null, map the indices 
    //
    if ( intersection_domain.size()>0 ) local_domain_indices_[i_proc]=intersection_domain;
  } 
  //
  // create the list of proc suporting the domain
  // 
  map<int,vector<int> >::iterator it_domain;
  for ( it_domain=local_domain_indices_.begin() ; it_domain!=local_domain_indices_.end() ; it_domain++ ) list_proc_domain_.push_back(it_domain->first);
  //
  // init number of vectors
  //
  nVectors_=0;
}

// extraction constructor
template <class Type>
Subspace<Type>::Subspace(Subspace<Type> &subspace, int istart, int nVectors) :
context_(subspace.context_)
{
  // check for the dimensions
  if ( nVectors+istart > subspace.nVectors_ )
  {
    cout << "Error: inconsistent dimension in Subspace extraction constructor: nVectors+istart > dim subspace" << endl;
    exit(0);
  }
  
  // copy private attributes
  my_proc_=subspace.my_proc_;
  n_procs_=subspace.n_procs_;
    
  // domain attributes
  list_proc_domain_=subspace.list_proc_domain_;     
  global_domain_indices_=subspace.global_domain_indices_; 
  local_domain_indices_=subspace.local_domain_indices_; 
    
  // subspace definition
  nVectors_=nVectors;             
  if ( local_domain_indices_.count(my_proc_) )
  {
#ifndef DUMMY
    // allocate memory
    coeff_.resize(local_domain_indices_[my_proc_].size()*nVectors_);
    
    // copy coefficients
    memcpy((void *)&coeff_[0],(void *)&(subspace.coeff_[local_domain_indices_[my_proc_].size()*istart]),local_domain_indices_[my_proc_].size()*nVectors_*sizeof(Type));      
#endif
  }
}

// destructors
template <class Type>
Subspace<Type>::~Subspace<Type>(void)
{}

// ****************************************************
//
// get the maximum absolute value of the coeficients
// this function should be used for testing purpose only, 
// as it involves blocking call on the ensemble of the 
// processors of this context
// 
// ****************************************************

#ifndef NORMREAL
#define NORMREAL

float norm(float a)
{
  return a*a;
}

double norm(double a)
{
  return a*a;
}

#endif

template <class Type>
void Subspace<Type>::discardDirections(int istart, int nVectors)
{
  // check the dimensions
  if ( nVectors+istart > nVectors_ )
  {
    cout << "Error: inconsistent dimension in - Subspace::discardDirections -, nVectors+istart > dim subspace detected" << endl;
    exit(0);
  }
  
#ifndef DUMMY
  // move the memory
  if ( local_domain_indices_.count(my_proc_) )
  {
    memmove((void *)&coeff_[istart*local_domain_indices_[my_proc_].size()],
            (void *)&coeff_[(istart+nVectors)*local_domain_indices_[my_proc_].size()],
            nVectors_-(istart+nVectors));
  }
#endif
          
  // release the memory
  nVectors_-=nVectors;
#ifndef DUMMY
  if ( local_domain_indices_.count(my_proc_) )
  {
    coeff_.resize(nVectors_*local_domain_indices_[my_proc_].size());
  }
#endif
}

template <class Type>
void Subspace<Type>::addDirections(Subspace<Type> &subspace)
{
  // check briefly the dimensions
  if ( local_domain_indices_.count(my_proc_) && subspace.local_domain_indices_.count(my_proc_) )
  {
    if ( local_domain_indices_[my_proc_].size() > 0 || subspace.local_domain_indices_[my_proc_].size() > 0 )
    { 
      if ( local_domain_indices_[my_proc_].size() != subspace.local_domain_indices_[my_proc_].size()
        || local_domain_indices_[my_proc_][0]     != subspace.local_domain_indices_[my_proc_][0] 
        || local_domain_indices_[my_proc_].back() != subspace.local_domain_indices_[my_proc_].back() )
      {
        cout << "Error: inconsistent dimension in - Subspace::addDirections -, local domain indices do not correspond" << endl;
        exit(0);
      }
    }
  } 
  // insert the coefficients
  nVectors_+=subspace.nVectors_;
#ifndef DUMMY
  coeff_.insert(coeff_.end(),subspace.coeff_.begin(),subspace.coeff_.end());
#endif  
}

template <class Type>
double Subspace<Type>::getMaxAbsoluteCoeff(void)
{
#ifndef DUMMY
  double maxCoeff=0.0;
  //
  // loop locally to find local max 
  //
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ )
  {
    if ( norm(coeff_[i])>maxCoeff ) maxCoeff=norm(coeff_[i]);
  }
  //
  // reduce the result of the search on proc 0
  //
  double maxCoeffGlobal;
  MPI_Reduce( &maxCoeff, &maxCoeffGlobal, 1, MPI_DOUBLE, MPI_MAX, 0, context_.comm());
  //
  // Broadcast the result of the search
  //
  MPI_Bcast( &maxCoeffGlobal, 1, MPI_DOUBLE, 0, context_.comm());
  //
  // return the result
  //
  return sqrt(maxCoeffGlobal);
#else
  cout << "Warning: tying to access max coeff of a dummy subspace. returned 0" << endl;
  return 0.0;
#endif    
}    

// ****************************************************
//
// access to local indices
// 
// ****************************************************

template <class Type>
vector<int> Subspace<Type>::localDomain(void)
{
  //
  // return the list of indices if any
  //
  if ( local_domain_indices_.count(my_proc_) ) return local_domain_indices_[my_proc_];
  //
  // else, return an empty list
  //
  vector<int> empty_list;
  return empty_list;
}

template <class Type>
vector<int> Subspace<Type>::localDomain(int i_proc) 
{
  //
  // return the list of indices if any
  //
  if ( local_domain_indices_.count(i_proc) ) return local_domain_indices_[i_proc];
  //
  // else, return an empty list
  //
  vector<int> empty_list;
  return empty_list;
}

// ****************************************************
//
// Subspace::setSize(int nVectors)
//
//   set the number of vectors defining the subspace 
//   and allocate the coefficient array if necessary
// 
// ****************************************************

template <class Type>
void Subspace<Type>::setSize(int nVectors)
{
  //
  // set the new dimension of the subspace
  //
  nVectors_=nVectors;
  //
  // allocate the array for the coefficients
  //
#ifndef DUMMY
  if ( local_domain_indices_.count(my_proc_) ) coeff_.resize( nVectors_*local_domain_indices_[my_proc_].size() );
#endif
} 


// ****************************************************
//
// Subspace::canonical(void)
//
//   set the subspace as the canonical ensemble
// 
// ****************************************************

template <class Type>
void Subspace<Type>::canonical(void)
{
#ifndef DUMMY
  //
  // set subspace size
  //
  setSize(global_domain_indices_.size());
  //
  // set diagonal coeff to 1
  //
  if ( local_domain_indices_.count(my_proc_) )
  {  
    for ( int i=0 ; i<nVectors_ ; i++ )
    {
      for ( int j=0 , stop=local_domain_indices_[my_proc_].size() ; j<stop ; j++ )
      {
        if ( local_domain_indices_[my_proc_][j]==global_domain_indices_[i] )
        {
          coeff_[j+i*stop]=1.0;
          break;
        }
      }
    }
  }
#else
  cout << "Warning: trying to set dummy subspace as canonical" << endl;
#endif
}
 
// ****************************************************
//
// Subspace::randomize(void)
//
//   randomize the vectors defining the subspace
// 
// ****************************************************

template <class Type>
void Subspace<Type>::randomize(void)
{
#ifndef DUMMY
  //
  // if the coefficients are real 
  //
  if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
  {
    for ( int icoeff=0 , stop=coeff_.size() ; icoeff<stop ; icoeff++ ) coeff_[icoeff]=(double)rand()/(double)RAND_MAX;
  }
  else
  {
    for ( int icoeff=0 , stop=coeff_.size() ; icoeff<stop ; icoeff++ ) coeff_[icoeff]=complex<double>((double)rand()/(double)RAND_MAX,(double)rand()/(double)RAND_MAX);
  }
#else
  cout << "Warning: trying to randomize dummy subspace" << endl;
#endif
}

// ****************************************************
//
// Subspace::setCoeffOnTop(vector<int> coeff_index)
//
//   given a list of indices, put the corresponding 
//   coefficients at the top of the local coeff lists
// 
// ****************************************************

template <class Type>
void Subspace<Type>::setCoeffOnTop(vector<int> coeff_indices)
{
#ifdef GLOBAL_TIMING
  global_subspace_tmap["scot total time"].start();
#endif
  //
  // get local leading dimension
  //
  int nCoeff = ( local_domain_indices_.count(my_proc_) ) ? local_domain_indices_[my_proc_].size() : 0;
  //
  // if there is something to do
  //
  if ( nCoeff>0 )
  {
#ifdef GLOBAL_TIMING
  global_subspace_tmap["scot index work"].start();
#endif
    //
    // get referemce on index lists
    // 
    vector<int> &local_indices=local_domain_indices_[my_proc_];
    int size_coeff_indices=coeff_indices.size();
    int *end_local_indices=&local_indices[0]+local_indices.size();
    int *start_local_indices=&local_indices[0];
    //
    // declare index list and reserve space
    //
    vector<int> index_list;
    index_list.reserve(size_coeff_indices);
    //
    // generate the list of local indices
    //
    for ( int j=0 ; j<size_coeff_indices ; j++ ) // here it is important to keep the order of this list
    {
      //
      // keep coeff index
      //
      int coeff_index=coeff_indices[j];
      //
      // loop on local indices 
      //
      for ( int *__restrict__ i=start_local_indices ; i<end_local_indices ; i++ )
      {
        //
        // if this global index is to be used
        //
        if ( (*i)==coeff_index )
        {
          //
          // add local index to the list
          //
          index_list.push_back(i-start_local_indices);
          break;
        }
      }
    }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["scot index work"].stop();
#endif
    //
    // swap the coefficients
    //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["scot swapping coeffs"].start();
#endif
    //
    // get references on loccal domain indices
    //
    vector<int> &local_domain_indices=local_domain_indices_[my_proc_];
    //
    // get index_list size and references pointers
    //
    int size_index_list=index_list.size();
    int *start_index_list=&index_list[0];
    int *end_index_list=&index_list[0]+size_index_list;
    //
    // get pointer refrences on coeff array
    //
#ifndef DUMMY
    Type *start_coeff=&coeff_[0];
    Type *end_coeff=&coeff_[0]+nVectors_*nCoeff;
#endif
    //
    // loop on index list
    //
    for ( int i=0 ; i<size_index_list ; i++ )
    {
      //
      // get corresponding index
      //
      int index=index_list[i];
      //
      // if something has to be done
      //
      if ( i!=index )
      {
        //
        // swap local domain indices
        //
        int i_global_temp=local_domain_indices[i];
        local_domain_indices[i]=local_domain_indices[index];
        local_domain_indices[index]=i_global_temp;
        //
        // swap the coefficients
        //
#ifndef DUMMY
        Type *__restrict__ p1=start_coeff+i;
        Type *__restrict__ p2=start_coeff+index;
        //
        for (  ; p1<end_coeff ; p1+=nCoeff , p2+=nCoeff )
        {
          Type coeff_temp=(*p1);
          (*p1)=(*p2);
          (*p2)=coeff_temp;
        } 
#endif
        //
        // correct index_list if necessary
        //
        for ( int *__restrict__ j=start_index_list+i+1 ; j<end_index_list ; j++ )
        {
          //
          // if i was also a target 
          //
          if ( (*j)==i ) 
          {
            //
            // indicate that i is now stored in index_list[i];
            //
            (*j)=index;
            break;
          }
        } 
      }
    }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["scot swapping coeffs"].stop();
#endif
  }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["scot total time"].stop();
#endif
}

// ****************************************************
//
// Subspace::discardCoeffs(vector<int> coeffs_indices)
//
//   remove the global coefficients given in the vector 
//   coeffs_indices from the description of the set.
// 
// ****************************************************

template <class Type>
void Subspace<Type>::discardCoeffs(vector<int> coeffs_indices)
{
  //
  // get local leading dimension
  //
  int nCoeff = ( local_domain_indices_.count(my_proc_) ) ? local_domain_indices_[my_proc_].size() : 0;
  //
  // if there is something to do
  //
  if ( nCoeff>0 )
  {
    //
    // get the indices on this proc
    //
    vector<int> local_indices=local_domain_indices_[my_proc_];
    // 
    // generate the list of the new local indices
    // 
    vector<int> new_indices(local_indices);
    for ( int i=0 ; i<local_indices.size() ; i++ ) new_indices[i]=i;
    // 
    // remove the local indices to be deleted
    // 
    for ( int i=0 ; i<local_indices.size() ; i++ )
    {
      for ( int j=0 ; j<coeffs_indices.size() ; j++ )
      {
        //
        // if this global index is to be deleted
        //
        if ( local_indices[i]==coeffs_indices[j] )
        {
          //
          // remove the index from global and local index lists
          //
          new_indices.erase(new_indices.begin()+i);
          local_indices.erase(local_indices.begin()+i);
          //
          // make sure that we check the very next index at the folowing step
          // 
          i--;
          //
          // stop searching in list of indices to be deleted
          //
          break;
        }
      }   
    }
    //
    // keep the list of indices on this proc
    //
    local_domain_indices_[my_proc_]=local_indices;
    //  
    // remove the indices from the list
    //  
#ifndef DUMMY
    for ( int j=0 ; j<nVectors_ ; j++ )
    { 
      //
      // point the first coefficient of the old vector
      //
      Type *p_old=&coeff_[j*nCoeff];
      //
      // point the first coefficient of the new vector
      //
      Type *p_new=&coeff_[j*new_indices.size()];
      //
      // copy the remaining coefficients
      // 
      for ( int i=0 ; i<new_indices.size() ; i++ )
      {
        p_new[i]=p_old[new_indices[i]]; 
      }
    }
    //
    // free memory
    //
    coeff_.resize(nVectors_*new_indices.size());
#endif
  }
  //
  // syncronize with the other proc
  //
  for ( int index_list_proc=0 ; index_list_proc<list_proc_domain_.size() ; index_list_proc++ )
  {
    //
    // get corresponding processor index
    // 
    int i_proc=list_proc_domain_[index_list_proc];
    //
    // get the indices on this proc
    //
    vector<int> local_indices=local_domain_indices_[i_proc];
    // 
    // remove the local indices to be deleted
    // 
    for ( int i=0 ; i<local_indices.size() ; i++ )
    {
      for ( int j=0 ; j<coeffs_indices.size() ; j++ )
      {
        //
        // if this global index is to be deleted
        //
        if ( local_indices[i]==coeffs_indices[j] )
        {
          //
          // remove the index from global and local index lists
          //
          local_indices.erase(local_indices.begin()+i);
          //
          // make sure that we check the very next index at the folowing step
          // 
          i--;
          //
          // stop searching in list of indices to be deleted
          //
          break;
        }
      }   
    }
    //
    // if the list is not empty
    //
    if ( local_indices.size()>0 )
    {
      //
      // keep the new list of indices on this proc
      //
      local_domain_indices_[i_proc]=local_indices;
    } 
    else
    {
      //
      // remove the proc from the local domain list
      //
      list_proc_domain_.erase(list_proc_domain_.begin()+index_list_proc);
      local_domain_indices_.erase(i_proc);
      //
      // decrement index_list_proc
      // 
      index_list_proc--;
    }
  }
  //
  // clean the global_domain_indices list
  //
  for ( int i=0 ; i<global_domain_indices_.size() ; i++ )
  {
    for ( int j=0 ; j<coeffs_indices.size() ; j++ )
    {
      //
      // if this global index is to be deleted
      //
      if ( global_domain_indices_[i]==coeffs_indices[j] )
      {
        //
        // remove the index from global and local index lists
        //
        global_domain_indices_.erase(global_domain_indices_.begin()+i);
        //
        // make sure that we check the very next index at the folowing step
        // 
        i--;
        //
        // stop searching in list of indices to be deleted
        //
        break;
      }
    }   
  }
}

// ****************************************************
//
// Subspace::keepCoeffs(vector<int> coeffs_indices)
//
//   remove the global coefficients not given in the vector 
//   coeffs_indices from the description of the set.
// 
// ****************************************************

template <class Type>
void Subspace<Type>::keepCoeffs(vector<int> coeffs_indices)
{
  //
  // get local leading dimension
  //
  int nCoeff = ( local_domain_indices_.count(my_proc_) ) ? local_domain_indices_[my_proc_].size() : 0;
  //
  // if there is something to do
  //
  if ( nCoeff>0 )
  {
    //
    // get the indices on this proc
    //
    vector<int> local_indices=local_domain_indices_[my_proc_];
    // 
    // generate the list of the new local indices
    // 
    vector<int> new_indices(local_indices);
    for ( int i=0 ; i<local_indices.size() ; i++ ) new_indices[i]=i;
    // 
    // remove the local indices to be deleted
    // 
    for ( int i=0 ; i<local_indices.size() ; i++ )
    {
      int j=0;
      //
      for ( int stop=coeffs_indices.size() ; j<stop ; j++ ) if ( local_indices[i]==coeffs_indices[j] ) break;
      //
      if ( j>=coeffs_indices.size() ) 
      {
        //
        // remove the index from global and local index lists
        //
        new_indices.erase(new_indices.begin()+i);
        local_indices.erase(local_indices.begin()+i);
        //
        // make sure that we check the very next index at the folowing step
        // 
        i--;
      }   
    }
    //
    // keep the list of indices on this proc
    //
    local_domain_indices_[my_proc_]=local_indices;
    //  
    // remove the indices from the list
    //  
#ifndef DUMMY
    for ( int j=0 ; j<nVectors_ ; j++ )
    { 
      //
      // point the first coefficient of the old vector
      //
      Type *p_old=&coeff_[j*nCoeff];
      //
      // point the first coefficient of the new vector
      //
      Type *p_new=&coeff_[j*new_indices.size()];
      //
      // copy the remaining coefficients
      // 
      for ( int i=0 ; i<new_indices.size() ; i++ )
      {
        p_new[i]=p_old[new_indices[i]]; 
      }
    }
    //
    // free memory
    //
    coeff_.resize(nVectors_*new_indices.size());
#endif
  }
  //
  // syncronize with the other proc
  //
  for ( int index_list_proc=0 ; index_list_proc<list_proc_domain_.size() ; index_list_proc++ )
  {
    //
    // get corresponding processor index
    // 
    int i_proc=list_proc_domain_[index_list_proc];
    //
    // get the indices on this proc
    //
    vector<int> local_indices=local_domain_indices_[i_proc];
    // 
    // remove the local indices to be deleted
    // 
    for ( int i=0 ; i<local_indices.size() ; i++ )
    {
      int j=0;
      //
      for ( int stop=coeffs_indices.size() ; j<stop ; j++ ) if ( local_indices[i]==coeffs_indices[j] ) break;
      //
      if ( j>=coeffs_indices.size() ) 
      {
        //
        // remove the index from global and local index lists
        //
        local_indices.erase(local_indices.begin()+i);
        //
        // make sure that we check the very next index at the folowing step
        // 
        i--;
      }   
    }
    //
    // if the list is not empty
    //
    if ( local_indices.size()>0 )
    {
      //
      // keep the new list of indices on this proc
      //
      local_domain_indices_[i_proc]=local_indices;
    } 
    else
    {
      //
      // remove the proc from the local domain list
      //
      list_proc_domain_.erase(list_proc_domain_.begin()+index_list_proc);
      local_domain_indices_.erase(i_proc);
      //
      // decrement index_list_proc
      // 
      index_list_proc--;
    }
  }
  // 
  // correct global domain indices
  //
  global_domain_indices_ = global_domain_indices_ && coeffs_indices;
}


// ****************************************************
//
// Subspace::expand(vector<int> coeffs_indices)
//
//   add trivial direction corresponding to the global input indices 
//   only the direction that are orthogonal to the current set.
// 
// ****************************************************

template <class Type>
void Subspace<Type>::expand(vector<int> indices)
{
  //
  // get local leading dimension
  //
  int nCoeff = ( local_domain_indices_.count(my_proc_) ) ? local_domain_indices_[my_proc_].size() : 0;
  //
  // isolate trivial solution from the list
  //
  vector<int> trivial_set = indices != global_domain_indices_; 
  //
  // if there is something to do 
  //
  if ( trivial_set.size()>0 )
  {
    //
    // compute local coefficients to be added
    //
    vector<int> local_indices = context_.getIndices();
    vector<int> local_trivial_set = trivial_set && local_indices;
    //
    // if there is something to be done
    // 
    if ( local_trivial_set.size()>0 )
    {
      //
      // resize the domain
      //
      int actual_size = local_domain_indices_[my_proc_].size();
      int new_size    = actual_size+local_trivial_set.size();
      //
#ifndef DUMMY
      coeff_.resize( new_size * ( nVectors_+trivial_set.size() ) );
      //
      // rearrange the current subspace coefficients
      //
      if ( actual_size>0 )
      {
        for ( int ivector=nVectors_-1 ; ivector>=0 ; ivector-- )
        {
          Type *psrc =&coeff_[ivector*actual_size];
          Type *pdest=&coeff_[ivector*new_size];
          memmove ( (void *)pdest, (void *)psrc, actual_size*sizeof(Type) );
        }
      }
#endif
      //
      // add the local trivial directions
      //
      vector<int> insertion_indices = local_trivial_set<=trivial_set;
      //
#ifndef DUMMY
      for ( int i=0 ; i<insertion_indices.size() ; i++ )
      {
        coeff_[(nVectors_+insertion_indices[i])*new_size+actual_size+i]=1.0;
      }
#endif
    }
#ifndef DUMMY
    else
    {
      coeff_.resize( local_domain_indices_[my_proc_].size() * ( nVectors_+trivial_set.size() ) , 0.0 );
    }
#endif
    //
    // set the new subspace dimension
    //
    nVectors_+=trivial_set.size();
    //
    // set the new global indices
    //
    global_domain_indices_ = global_domain_indices_ || indices;
    //
    // set the new local indices on each proc
    //
    for ( int iproc=0 ; iproc<n_procs_ ; iproc++ )
    {
      //
      // get the coefficients added for this proc
      //
      vector<int> proc_local_indices = context_.getIndicesOnProc(iproc);
      vector<int> proc_local_trivial_set = trivial_set && proc_local_indices;
      //
      // if there is something to do
      //
      if ( proc_local_trivial_set.size()>0 )
      {
        local_domain_indices_[iproc].insert(local_domain_indices_[iproc].end(),proc_local_trivial_set.begin(),proc_local_trivial_set.end());
      }
    }
  }
}

// ****************************************************
//
// Subspace::= copy operator
//
//   reduce subspace to the set with 0 component on the 
//   given global coefficients
// 
// ****************************************************

template <class Type> 
void Subspace<Type>::operator =( Subspace<Type> &other )
{
  // verify that the context corresponds
  if ( &context_!=&other.context_ )
  {
    cout << "Error: illegal call to subOperator::operator= - the two subspaces have different contexts" << endl;
  }

  // domain attributes
  list_proc_domain_=other.list_proc_domain_;      
  global_domain_indices_=other.global_domain_indices_; 
  local_domain_indices_=other.local_domain_indices_;  
    
  // subspace definition
  nVectors_=other.nVectors_;             
#ifndef DUMMY
  coeff_=other.coeff_;                
#endif
}

// explicit declaration for conversion from real to complex
template <> template <>
void Subspace<complex<float> >::operator =( Subspace<float> &other )
{
  // verify that the context corresponds
  if ( &context_!=&other.context_ )
  {
    cout << "Error: illegal call to subOperator::operator= - the two subspaces have different contexts" << endl;
  }

  // domain attributes
  list_proc_domain_=other.list_proc_domain_;      
  global_domain_indices_=other.global_domain_indices_; 
  local_domain_indices_=other.local_domain_indices_;  
    
  // subspace definition
  nVectors_=other.nVectors_;             
#ifndef DUMMY
  coeff_.resize(other.coeff_.size());                
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=other.coeff_[i];
#endif
}

// explicit declaration for conversion from real to complex
template <> template <>
void Subspace<complex<double> >::operator =( Subspace<double> &other )
{
  // verify that the context corresponds
  if ( &context_!=&other.context_ )
  {
    cout << "Error: illegal call to subOperator::operator= - the two subspaces have different contexts" << endl;
  }

  // domain attributes
  list_proc_domain_=other.list_proc_domain_;      
  global_domain_indices_=other.global_domain_indices_; 
  local_domain_indices_=other.local_domain_indices_;  
    
  // subspace definition
  nVectors_=other.nVectors_;             
#ifndef DUMMY
  coeff_.resize(other.coeff_.size());                
  for ( int i=0 , stop=coeff_.size() ; i<stop ; i++ ) coeff_[i]=other.coeff_[i];
#endif
}

// ****************************************************
//
// Subspace::&= intersection operator
//
// ****************************************************

template <class Type> 
void Subspace<Type>::operator&=( Subspace<Type> &other )
{
  vector<int> keepOnlyIndices;
  intersectWith( other , keepOnlyIndices );
}

template <class Type> 
void Subspace<Type>::intersectWith( Subspace<Type> &other , vector<int> &keepIndices )
{
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - total time"].start();
#endif
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - fast returns"].start();
#endif
  //
  // start getting the dimensions of the subspaces
  //
  int nVectors_left  = nVectors_;
  int nVectors_right = other.nVectors_;
  int nCoeff_left  = ( local_domain_indices_.count(my_proc_) )       ? local_domain_indices_[my_proc_].size()       : 0;
  int nCoeff_right = ( other.local_domain_indices_.count(my_proc_) ) ? other.local_domain_indices_[my_proc_].size() : 0;
  //
  // get global indices for intersection
  //
  vector<int> global_indices_intersection = global_domain_indices_ && other.global_domain_indices_;
  int         size_intersection=global_indices_intersection.size();
  //
  // get global indices for reunion
  //
  vector<int> global_indices_union        = global_domain_indices_ || other.global_domain_indices_;
  vector<int> keepOnlyIndices             = keepIndices && global_indices_union;
  //
  // reduce the union to only the indices to keep
  // 
  if ( keepOnlyIndices.size()>0 ) global_indices_union=keepOnlyIndices;
  //
  // create a new subspace object for the crossing of the two subspaces
  //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - subspace creation"].start();
#endif
  Subspace<Type> subspace( global_indices_union , context_ );
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - subspace creation"].stop();
#endif
  //
  // fast return if possible
  //
  if ( nCoeff_left==0 && nCoeff_right==0 )
  {
    //
    // set the dimension of the subspace
    //
    subspace.setSize( nVectors_left+nVectors_right-size_intersection );
    //
    // assign the new subspace to this one
    //
    *this = subspace;
#ifdef GLOBAL_TIMING
    global_subspace_tmap["intersection - total time"].stop();
#endif
#ifdef GLOBAL_TIMING
    global_subspace_tmap["intersection - fast returns"].stop();
#endif
    //
    // return as there is nothing else to do
    //
    return;
  }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - fast returns"].stop();
#endif
  //
  // place intersection coefficients on top of the arrays
  //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - indices construction"].start();
#endif
  setCoeffOnTop(global_indices_intersection);
  other.setCoeffOnTop(global_indices_intersection);
  //
  // form list of working proc and the associated dimensions of work array 
  //
  int         my_local_size_intersection=0;
  vector<int> local_size_intersection;
  vector<int> list_working_procs;
  //
  // if the domains are spread on several processors
  //
  if ( list_proc_domain_.size()>1 && other.list_proc_domain_.size()>1 )
  {
    //
    // construct the proc list containing the intersection 
    //
    for ( int i=0 ; i<list_proc_domain_.size() ; i++ )
    {
      //
      // get proc index
      //
      int i_proc=list_proc_domain_[i];
      //
      // if both domains are present on this proc
      //
      if ( other.local_domain_indices_.count(i_proc) )
      {
        //
        // get the local indices
        //
        vector<int> local_indices_left=local_domain_indices_[i_proc];
        vector<int> local_indices_right=other.local_domain_indices_[i_proc];
        //
        // get the local intersection
        //
        vector<int> local_indices_intersection = local_indices_left && local_indices_right;
        //
        // form list of working proc
        //
        if ( local_indices_intersection.size()>0 ) 
        { 
          list_working_procs.push_back( i_proc );
          local_size_intersection.push_back( local_indices_intersection.size() );
        }
        //
        // keep local size of intersection
        //
        if ( my_proc_==i_proc ) my_local_size_intersection=local_indices_intersection.size();
      }
    }
  }
  //
  // else, if there is only one proc involved for one of the two subspaces
  //
  else
  {
    //
    // keep the index of working proc
    //
    if ( list_proc_domain_.size()==1 )
    {
      list_working_procs.push_back(list_proc_domain_[0]);
      local_size_intersection.push_back(size_intersection);
    }
    else 
    {
      list_working_procs.push_back(other.list_proc_domain_[0]);
      local_size_intersection.push_back(size_intersection);
    }
    //
    my_local_size_intersection = ( my_proc_==list_working_procs[0] ? size_intersection : 0 );
  }    
  //
  // form list of listening procs
  //
  vector<int> list_listening_procs = list_proc_domain_ || other.list_proc_domain_;
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - indices construction"].stop();
#endif
  //
  // declare working arrays
  //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - coefficients copy"].start();
#endif
  int                     LDQ=nVectors_left+nVectors_right;
  int                     LDW=nVectors_left+nVectors_right;
  int                     NQ=LDQ-size_intersection;
#ifndef DUMMY
  vector<Type>            Q(LDQ*NQ,0.0);
  vector<Type>            work_array(LDQ*my_local_size_intersection+1);
#else
  vector<Type>            Q;
  vector<Type>            work_array;
#endif
  //
  // copy transpose of local intersection coefficients in work array
  //
#ifndef DUMMY
  if ( my_local_size_intersection>0 )
  {
    //
    // transpose coefficient if we deal with complex arrays
    //
    if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
    {  
      //
      // copy coefficient of left array in work
      //
      for ( int i_col=0 ; i_col<nVectors_left ; i_col++ )
      {
        //
        // point on first element of column i_col in left coeff array  
        //  
        Type *p_source=&coeff_[i_col*nCoeff_left];
        //
        // point on first column of destination row
        //
        Type *p_destination=&work_array[i_col];
        //
        // copy column coefficients
        //
        for ( int i_row=0 ; i_row<my_local_size_intersection ; i_row++ ) p_destination[i_row*LDQ]=p_source[i_row];
      }
      //
      // copy coefficient of right array in work
      //
      for ( int i_col=0 ; i_col<nVectors_right ; i_col++ )
      {
        //
        // point on first element of column i_col in left coeff array  
        //
        Type *p_source=&(other.coeff_[i_col*nCoeff_right]);
        //
        // point on first column of destination row
        //
        Type *p_destination=&work_array[i_col+nVectors_left];
        //
        // copy column coefficients
        //
        for ( int i_row=0 ; i_row<my_local_size_intersection ; i_row++ ) p_destination[i_row*LDQ]=p_source[i_row];
      }
    }
    else
    {  
      for ( int i_col=0 ; i_col<nVectors_left ; i_col++ )
      {
        //
        // point on first element of column i_col in left coeff array  
        //  
        Type *p_source=&coeff_[i_col*nCoeff_left];
        //
        // point on first column of destination row
        //
        Type *p_destination=&work_array[i_col];
        //
        // copy column coefficients
        //
        for ( int i_row=0 ; i_row<my_local_size_intersection ; i_row++ ) p_destination[i_row*LDQ]=conj(p_source[i_row]);
      }
      //
      // copy coefficient of right array in work
      //
      for ( int i_col=0 ; i_col<nVectors_right ; i_col++ )
      {
        //
        // point on first element of column i_col in left coeff array  
        //
        Type *p_source=&(other.coeff_[i_col*nCoeff_right]);
        //
        // point on first column of destination row
        //
        Type *p_destination=&work_array[i_col+nVectors_left];
        //
        // copy column coefficients
        //
        for ( int i_row=0 ; i_row<my_local_size_intersection ; i_row++ ) p_destination[i_row*LDQ]=conj(p_source[i_row]);
      }
    } 
  }  
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - coefficients copy"].stop();
#endif
  //
  // set coefficients of Q matrix
  //
  for ( int i_col=0; i_col<NQ ; i_col++ ) Q[size_intersection+i_col*(LDQ+1)]=1.0;
  //
  // compute QR transform
  //
  QRFactorization<Type>( my_proc_, list_working_procs, list_listening_procs, context_, &work_array[0], LDW, local_size_intersection, &Q[0], LDQ, NQ, global_subspace_tmap, "intersection - QR" );
  //
  // set the dimension of the new subspace
  //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - glue subspaces"].start();
#endif

#endif // dummy statment
  
  subspace.setSize( nVectors_left+nVectors_right-size_intersection );
  //
  // if there is no request for the indices to be kept
  //
  if ( keepOnlyIndices.size()==0 )
  {
    //
    // form the complete solutions
    //
    if ( nVectors_left+nVectors_right > size_intersection )
    {
#ifndef DUMMY
      //
      // aply transfo to coeffs from left
      //
      if ( nCoeff_left>0 )
      {
        char TRANSA = 'N';
        char TRANSB = 'N';
        int M=nCoeff_left;
        int N=NQ;
        int K=nVectors_left;
        int LDA=nCoeff_left;
        int LDB=LDQ;
        int LDC=subspace.local_domain_indices_[my_proc_].size();
        Type ALPHA=1.0;
        Type BETA=0.0;
        //
        Type *A=&coeff_[0];
        Type *B=&Q[0];
        Type *C=&subspace.coeff_[0];
        //
        lapack::gemm<Type>( &TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
      }
      //
      // complete with coeff from right
      //
      if ( nCoeff_right>my_local_size_intersection )
      {
        char TRANSA = 'N';
        char TRANSB = 'N';
        int M=nCoeff_right-my_local_size_intersection;
        int N=NQ;
        int K=nVectors_right;
        int LDA=nCoeff_right;
        int LDB=LDQ;
        int LDC=subspace.local_domain_indices_[my_proc_].size();
        Type ALPHA=-1.0;
        Type BETA=0.0;
        //
        Type *A=&other.coeff_[my_local_size_intersection];
        Type *B=&Q[nVectors_left];
        Type *C=&subspace.coeff_[nCoeff_left];
        //
        lapack::gemm<Type>( &TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
      }
#endif
    }
    else
    {
      cout << "Error: intersection of subspaces wich size sum < intersection not implemented yet" << endl;
      exit(0);
    } 
    //
    // assign the correct index mapping to the subspace
    //
    vector<int> new_local_indices;
    // 
    // append the indices form the left
    //
    if ( local_domain_indices_.count(my_proc_) ) new_local_indices=local_domain_indices_[my_proc_];
    //
    // append the indices from the right
    //
    if ( other.local_domain_indices_.count(my_proc_) ) 
      new_local_indices.insert( new_local_indices.end(), 
                                other.local_domain_indices_[my_proc_].begin()+my_local_size_intersection, 
                                other.local_domain_indices_[my_proc_].end() );
    //
    // set the correct local indices
    //
    subspace.local_domain_indices_[my_proc_]=new_local_indices;
  }
  //
  // else, if we keep only some indices
  //
  else
  {
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - reoder data"].start();
#endif
    //
    // form lists of indices to be kept
    // 
    vector<int> keep_local_indices =   keepOnlyIndices && local_domain_indices_[my_proc_];
    vector<int> indices_without_intersection = keepOnlyIndices != global_indices_intersection;  
    vector<int> keep_other_indices = indices_without_intersection && other.local_domain_indices_[my_proc_];  
    //
    // set indices on top of the arrays
    //
    setCoeffOnTop(keep_local_indices);
    other.setCoeffOnTop(keep_other_indices);
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - reoder data"].stop();
#endif
    //
    // form the reduced solutions
    //
    if ( nVectors_left+nVectors_right > size_intersection )
    {
#ifndef DUMMY
      //
      // aply transfo to coeffs from left
      //
      if ( keep_local_indices.size()>0 )
      {
        char TRANSA = 'N';
        char TRANSB = 'N';
        int M=keep_local_indices.size();
        int N=NQ;
        int K=nVectors_left;
        int LDA=nCoeff_left;
        int LDB=LDQ;
        int LDC=subspace.local_domain_indices_[my_proc_].size();
        Type ALPHA=1.0;
        Type BETA=0.0;
        //
        Type *A=&coeff_[0];
        Type *B=&Q[0];
        Type *C=&subspace.coeff_[0];
        //
        lapack::gemm<Type>( &TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
      }
      //
      // complete with coeff from right
      //
      if ( keep_other_indices.size()>0 )
      {
        char TRANSA = 'N';
        char TRANSB = 'N';
        int M=keep_other_indices.size();
        int N=NQ;
        int K=nVectors_right;
        int LDA=nCoeff_right;
        int LDB=LDQ;
        int LDC=subspace.local_domain_indices_[my_proc_].size();
        Type ALPHA=-1.0;
        Type BETA=0.0;
        //
        Type *A=&other.coeff_[0];
        Type *B=&Q[nVectors_left];
        Type *C=&subspace.coeff_[keep_local_indices.size()];
        //
        lapack::gemm<Type>( &TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
      }
#endif
    }
    else
    {
      cout << "Error: intersection of subspaces wich size sum < intersection not implemented yet" << endl;
      exit(0);
    } 
    //
    // assign the correct index mapping to the subspace
    //
    vector<int> new_local_indices;
    // 
    // append the indices form the left
    //
    new_local_indices.insert( new_local_indices.end(), local_domain_indices_[my_proc_].begin(), local_domain_indices_[my_proc_].begin()+keep_local_indices.size() );
    //
    // append the indices from the right
    //
    new_local_indices.insert( new_local_indices.end(), other.local_domain_indices_[my_proc_].begin(), other.local_domain_indices_[my_proc_].begin()+keep_other_indices.size() );
    //
    // set the correct local indices
    //
    subspace.local_domain_indices_[my_proc_]=new_local_indices;
  }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - glue subspaces"].stop();
#endif
  //
  // assign the new subspace to this one
  //
  *this = subspace;
  //
  // print the different timers if necessary
  //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["intersection - total time"].stop();
#endif
}

template <> template <>
void Subspace<complex<float> >::intersectWith( Subspace<float> &other , vector<int> &keepOnlyIndices )
{
  // create a complex subspace
  vector<int> domain_indices(0);
  Subspace<complex<float> > subspace_temp( domain_indices, context_ );
  
  // map real subspace onto the new complex one
  subspace_temp=other;
  
  // intersect subspaces
  intersectWith( subspace_temp , keepOnlyIndices );  
}

template <> template <>
void Subspace<complex<double> >::intersectWith( Subspace<double> &other , vector<int> &keepOnlyIndices )
{
  // create a complex subspace
  vector<int> domain_indices(0);
  Subspace<complex<double> > subspace_temp( domain_indices, context_ );
  
  // map real subspace onto the new complex one
  subspace_temp=other;
  
  // intersect subspaces
  intersectWith( subspace_temp , keepOnlyIndices );  
}

template <> template <>
void Subspace<complex<float> >::operator&=( Subspace<float> &other )
{
  // create a complex subspace
  vector<int> domain_indices(0);
  Subspace<complex<float> > subspace_temp( domain_indices, context_ );
  
  // map real subspace onto the new complex one
  subspace_temp=other;
  
  // intersect subspaces
  (*this)&=subspace_temp;
}

template <> template <>
void Subspace<complex<double> >::operator&=( Subspace<double> &other )
{
  // create a complex subspace
  vector<int> domain_indices(0);
  Subspace<complex<double> > subspace_temp( domain_indices, context_ );
  
  // map real subspace onto the new complex one
  subspace_temp=other;
  
  // intersect subspaces
  (*this)&=subspace_temp;
}

// ****************************************************
//
// Subspace::findOrthogonal(vector<int> &coeffs_indices)
//
//   reduce subspace to the set with 0 component on the 
//   given global coefficients
// 
// ****************************************************

template <class Type>
void Subspace<Type>::findOrthogonal(vector<int> &coeffs_indices)
{
  //
  // form list of working proc and the associated dimensions of work array 
  //
  int         size_intersection=0;
  int         my_local_size_intersection=0;
  vector<int> local_size_intersection;
  vector<int> list_working_procs;
  vector<int> my_local_indices_intersection;
  //
  // construct the proc list containing the indices
  //
  for ( int i=0 ; i<list_proc_domain_.size() ; i++ )
  {
    //
    // get proc index
    //
    int i_proc=list_proc_domain_[i];
    //
    // get the local indices
    //
    vector<int> local_indices=local_domain_indices_[i_proc];
    //
    // get the local intersection
    //
    vector<int> local_indices_intersection = local_indices && coeffs_indices;
    //
    // form list of working proc
    //
    if ( local_indices_intersection.size()>0 ) 
    { 
      list_working_procs.push_back( i_proc );
      local_size_intersection.push_back( local_indices_intersection.size() );
    }
    //
    // keep local size of intersection
    //
    if ( my_proc_==i_proc ) 
    {
      my_local_size_intersection=local_indices_intersection.size();
      my_local_indices_intersection=local_indices_intersection;
    }
    //
    // add to the total size of the intersection
    //
    size_intersection+=local_indices_intersection.size();
  }

#ifdef DUMMY
  //
  // set new size of the subspace
  //
  nVectors_=nVectors_-size_intersection;
  //
  // return
  //
  return;
#endif
  
  //
  // fast return if possible
  //
  if ( !local_domain_indices_.count(my_proc_) )
  {
    //
    // set new size of the subspace
    //
    nVectors_=nVectors_-size_intersection;
    //
    // discard coefficients
    //
    discardCoeffs(coeffs_indices);
    //
    // return
    //
    return;
  }
  //
  // allocate memory for the work arrays if necessary
  //
  int            LDW=nVectors_;
  int            LDQ=nVectors_;
  int            NQ=nVectors_-size_intersection;
  vector<Type> W( my_local_size_intersection*LDW ); 
  vector<Type> Q( LDQ*NQ , 0.0 );
  //
  // initialize the Q matrix
  //
  for ( int i=0 ; i<NQ ; i++ ) Q[i*LDQ+size_intersection+i]=1.0;
  //
  // copy the coefficients on the intersection in the work array => transpose
  //
  vector<int> local_indices=local_domain_indices_[my_proc_];
  int         nCoeff=local_indices.size();
  //
  // if the array is real
  //
  if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
  {
    for ( int i_row=0 ; i_row<my_local_size_intersection ; i_row++ )
    {
      // 
      // find corresponding row index in subspace array
      //
      int i_sub=0;
      int index=my_local_indices_intersection[i_row];
      //
      for ( int stop=local_indices.size() ; i_sub<stop ; i_sub++ ) if ( local_indices[i_sub]==index ) break;
      //
      // if we found this coeff
      //
      if ( i_sub<local_indices.size() )
      {
        //
        // point on the first column of this row
        //
        Type *p_source=&coeff_[i_sub];
        //
        // point on the first element of the destination column
        //
        Type *p_destination=&W[i_row*LDW];
        //
        // copy column coefficients into the work array
        //
        for ( int i_col=0 ; i_col<LDQ ; i_col++ ) p_destination[i_col]=p_source[i_col*nCoeff];
      }
      else
      {
        cout << "Error: could not find index " << coeffs_indices[i_row] << " in subspace set" << endl;
        exit(0);
      }
    }
  }
  //
  // if the array is complex
  //
  else
  {
    for ( int i_row=0 ; i_row<my_local_size_intersection ; i_row++ )
    {
      // 
      // find corresponding row index in subspace array
      //
      int i_sub=0;
      int index=my_local_indices_intersection[i_row];
      //
      for ( int stop=local_indices.size() ; i_sub<stop ; i_sub++ ) if ( local_indices[i_sub]==index ) break;
      //
      // if we found this coeff
      //
      if ( i_sub<local_indices.size() )
      {
        //
        // point on the first column of this row
        //
        Type *p_source=&coeff_[i_sub];
        //
        // point on the first element of the destination column
        //
        Type *p_destination=&W[i_row*LDW];
        //
        // copy column coefficients into the work array
        //
        for ( int i_col=0 ; i_col<LDQ ; i_col++ ) p_destination[i_col]=conj(p_source[i_col*nCoeff]);
      }
      else
      {
        cout << "Error: could not find index " << coeffs_indices[i_row] << " in subspace set" << endl;
        exit(0);
      }
    }
  }
  //
  // compute QR decomposition
  //
  QRFactorization<Type>( my_proc_, list_working_procs, list_proc_domain_, context_, &W[0], LDW, local_size_intersection, &Q[0], LDQ, NQ );
  //
  // allocate memory for transfo
  //
  vector<Type> coeff((LDQ-size_intersection)*nCoeff);
  //
  // aply transfo to coeffs from left (if there is something to do...)
  //
  if ( nCoeff>0 ) 
  {
    char TRANSA = 'N';
    char TRANSB = 'N';
    int M=nCoeff;
    int N=LDQ-size_intersection;
    int K=LDQ;
    int LDA=nCoeff;
    int LDB=LDQ;
    int LDC=nCoeff;
    Type ALPHA=1.0;
    Type BETA=0.0;
    //
    Type *A=&coeff_[0];
    Type *B=&Q[0];
    Type *C=&coeff[0];
    //
    lapack::gemm<Type>( &TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C, &LDC);
  }
  //
  // assign the new coeffs
  //  
  coeff_=coeff;
  //
  // assign the new dimension
  //
  nVectors_=LDQ-size_intersection;
  //
  // discard the coefficient
  //
//  discardCoeffs(coeffs_indices);
  //
  // print the different timers
  //
}

template <class Type>
void Subspace<Type>::alignOnSet(vector<int> &coeffs_indices)
{
#ifndef DUMMY
  //
  // init properties of the intersection
  //
  int         size_intersection=0;
  int         my_local_size_intersection=0;
  vector<int> local_size_intersection;
  vector<int> list_working_procs;
  vector<int> my_local_indices_intersection;
  //
  // loop on each domain processors
  //
  for ( int index_list_proc=0 ; index_list_proc<list_proc_domain_.size() ; index_list_proc++ )
  {
    //
    // find the corresponding processor index
    // 
    int i_proc=list_proc_domain_[index_list_proc];
    //
    // get the domain indices on this proc
    //
    vector<int> local_indices=local_domain_indices_[i_proc];
    //
    // get the local intersection
    // 
    vector<int> local_indices_intersection = local_indices && coeffs_indices;
    //
    // form list of working proc
    //
    if ( local_indices_intersection.size()>0 ) 
    { 
      list_working_procs.push_back( i_proc );
      local_size_intersection.push_back( local_indices_intersection.size()*nVectors_ ); // multiply by nVectors_ to get the number of coeff
    }
    //
    // keep local size of intersection
    //
    if ( my_proc_==i_proc ) 
    {
      my_local_size_intersection=local_indices_intersection.size();
      my_local_indices_intersection=local_indices_intersection;
    }
    //
    // add to the total size of the intersection
    //
    size_intersection+=local_indices_intersection.size();
  }
  //
  // verify that we can align the set
  //
  if ( nVectors_!=size_intersection )
  {
    cout << "Error: inconsistent dimension in Subspace::alignOnSet, nVectors=" << nVectors_ << " while size_intersection=" << size_intersection << endl;
    exit(0);
  }
  //
  // if there is something to do for this proc
  //
  if ( local_domain_indices_.count(my_proc_) )
  {
    //
    // put the coefficient concerned on top of the array
    //
    setCoeffOnTop(my_local_indices_intersection);
    //
    // get local array dimension
    //
    int nCoeff=local_domain_indices_[my_proc_].size();
    //
    // keep a copy of the coefficient for applying the transfo
    //
    vector<Type> coeff(nCoeff*nVectors_);
    //
    for ( int i_col=0 ; i_col<nVectors_ ; i_col++ )
    {
      //
      // point on first element of column source
      //
      Type *p_source=&coeff_[i_col*nCoeff];
      //
      // point on first element of column destination
      //
      Type *p_destination=&coeff[i_col*nCoeff];
      //
      // copy coefficients
      //
      for ( int k=0 , stop=nCoeff ; k<stop ; k++ ) p_destination[k]=p_source[k];
    }
    //
    // allocate memory for the transfo
    //
    int            LDQ=nVectors_;
    vector<Type> Q( LDQ*LDQ );
    //
    // if this proc is working
    //
    if ( my_local_size_intersection>0 )
    {
      //
      // copy coeff in the local array => transpose for comm
      // choose Q or W depending on communications invelved
      //
      int LDW = nVectors_; 
      vector<Type> W;
      //
      // allocate W only if necessary
      //
      if (list_working_procs.size()>1) W.resize(my_local_size_intersection*LDW);
      //
      if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
      {
        for ( int i_col=0 ; i_col<nVectors_ ; i_col++ )
        {
          //
          // point on first element of column source
          //
          Type *p_source=&coeff_[i_col*nCoeff];
          //
          // point on first element of column destination
          //
          Type *p_destination= (list_working_procs.size()>1) ? &W[i_col] : &Q[i_col];
          //
          // copy coefficients
          //
          for ( int k=0 , stop=my_local_size_intersection ; k<stop ; k++ ) p_destination[k*LDW]=p_source[k];
        }
      }
      else
      {
        for ( int i_col=0 ; i_col<nVectors_ ; i_col++ )
        {
          //
          // point on first element of column source
          //
          Type *p_source=&coeff_[i_col*nCoeff];
          //
          // point on first element of column destination
          //
          Type *p_destination= (list_working_procs.size()>1) ? &W[i_col] : &Q[i_col];
          //
          // copy coefficients
          //
          for ( int k=0 , stop=my_local_size_intersection ; k<stop ; k++ ) p_destination[k*LDW]=conj(p_source[k]);
        }
      }
      //
      // if there are several proc involved
      // 
#ifdef WITH_MPI
      if (list_working_procs.size()>1)
      {
        //my_local_indices_intersection
        // gather the coefficient on the first working proc
        //
        communication::gather<Type>( &W[0], &Q[0], local_size_intersection, list_working_procs[0], list_working_procs, context_ ); 
      }
#endif

#ifdef DEBUG
cout << "Gathered matrix for inversion" << endl;
if ( my_proc_==list_working_procs[0] )
{      
  for ( int i=0 ; i<LDW ; i++ )
  {
    for ( int j=0 ; j<nVectors_ ; j++ )
    {
      cout << Q[j+i*nVectors_] << "   ";
    }
    cout << endl;
  }
}
#endif

      //
      // if this is the first working proc, invert Q 
      //
      if ( my_proc_==list_working_procs[0] )
      {
        //
        // first factorize
        //
        int  INFO;
        vector<int> IPIV(LDQ);
        lapack::getrf<Type>( &LDQ, &LDQ, &Q[0], &LDQ, &IPIV[0], &INFO );
        //
        // check status
        //
        if ( INFO!=0 )
        {
          cout << "Subspace::alignOnSet : problem during inversion in ZGETRF, INFO = " << INFO << endl;
          return; 
        }
        //
        // optimal memory request
        //
        vector<Type> WORK(1);
        int LWORK=-1;
        lapack::getri<Type>( &LDQ, &Q[0], &LDQ, &IPIV[0], &WORK[0], &LWORK, &INFO );
        //
        // allocate memory if necessary
        //
        if ( getsize(WORK[0])>WORK.size() ) WORK.resize(getsize(WORK[0]));
        LWORK=WORK.size();
        //
        // compute inverse
        //
        lapack::getri<Type>( &LDQ, &Q[0], &LDQ, &IPIV[0], &WORK[0], &LWORK, &INFO );
        //
        // check status
        //
        if ( INFO!=0 )
        {
          cout << "Subspace::alignOnSet : problem during inversion in ZGETRI, INFO = " << INFO << endl;
          return; 
        }
      }
    }
#ifdef WITH_MPI
    //
    // if there is more than 1 proc supporting the domain
    //      
    if ( list_proc_domain_.size()>1 )
    {
      //
      // find the index of the root in domain referential
      //  
      int i_root;
      // 
      for ( i_root=0 ; i_root<list_proc_domain_.size() ; i_root++ ) if ( list_proc_domain_[i_root]==list_working_procs[0] ) break;
      //
      // get the transfo from root proc
      //
      communication::broadcast<Type>( &Q[0], Q.size(), i_root, list_proc_domain_, context_ );
    }
#endif
    //
    // apply transfo to the local coefficients
    //
    {
      char TRANSA='N';
      char TRANSB= ( ( !strstr(typeid(&coeff_[0]).name(),"complex") ) ? 'T' : 'C' );
      int  M=nCoeff;
      int  N=nVectors_;
      int  K=nVectors_;
      int  LDA=nCoeff;
      int  LDB=nVectors_;
      int  LDC=nCoeff;
      //
      Type ALPHA=1.0;
      Type BETA=0.0;
      //
      lapack::gemm<Type>( &TRANSA, &TRANSB, &M, &N, &K, &ALPHA, &coeff[0], &LDA, &Q[0], &LDB, &BETA, &coeff_[0], &LDC);
    }
  }
#endif // dummy statement  
}

// **********************************************
// gatherCoeffsOnProcs:
//
//    gather the coefficients of the subspace corresponding to
//    the intersection of the subspace domain indices and the
//    indices given in domain_indices input vector. On exit, 
//    the coefficient are gathered on processor defined in 
//    list_proc, the values in the domain_indices vector define
//    the domain indices of the intersection.
//    The coefficients are arranged row-wise, instead of the
//    column-wise arrangement in the subspace array.
// **********************************************

template <class Type>
void Subspace<Type>::gatherCoeffsOnProcs(vector<int> &domain_indices, vector<int> &list_proc, vector<Type> &coeff)
{
#ifdef GLOBAL_TIMING
  global_subspace_tmap["total gathering time"].start();
#endif
  //
  // determine if this proc is receiving anything
  //
  bool isListening = false;
  for ( int i=0 ; i<list_proc.size() ; i++ ) if ( list_proc[i]==my_proc_ ) { isListening=true; break; }
  //
  // if this proc is concerned
  //
  if ( local_domain_indices_.count(my_proc_) || isListening )
  {
#ifdef GLOBAL_TIMING
  global_subspace_tmap["gathering index work"].start();
#endif
    //
    // create the list of proc supporting the part of subspace 
    // intersecting with the domain indices
    //
    vector<int> isend;
    vector<int> size_local_inter;
    vector<int> size_local_coeff;
    vector<int> my_local_intersection;
    //
    for ( int index_list_proc=0 ; index_list_proc<list_proc_domain_.size() ; index_list_proc++ )
    {
      //
      // get processor index
      //
      int iproc=list_proc_domain_[index_list_proc];
      //
      // get local intersection
      //
      vector<int> intersection = local_domain_indices_[iproc] && domain_indices;
      //
      // if the intersection is non-null
      //
      if ( intersection.size()>0 )
      {
        //
        // keep the index of this proc
        //
        isend.push_back(iproc);
        //
        // keep the associated dimensions
        //
        size_local_inter.push_back(intersection.size());
        size_local_coeff.push_back(intersection.size()*nVectors_);
        //
        // keep local intersection
        //
        if ( my_proc_==iproc )
        {
          my_local_intersection=intersection;
        }
      }
    }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["gathering index work"].stop();
#endif
    //
    // if only one proc is involved
    //
    if ( isend.size()==1 && list_proc.size()==1 && isend[0]==list_proc[0] )
    {
      //
      // if this is the proc involved
      //
      if ( my_proc_==list_proc[0] )
      {
        //
        // transpose the coefficients directly in coeff
        //
        coeff.resize( my_local_intersection.size()*nVectors_ );
#ifndef DUMMY
        //
        // find local indices of intersection
        //
#ifdef GLOBAL_TIMING
  global_subspace_tmap["gathering index work"].start();
#endif
        vector<int> local_indices = my_local_intersection <= local_domain_indices_[my_proc_];
#ifdef GLOBAL_TIMING
  global_subspace_tmap["gathering index work"].stop();
#endif
        //
        // transpose corresponding coefficients in coeff array
        //
        int nCoeffTot=local_domain_indices_[my_proc_].size();
        int nCoeffInt=my_local_intersection.size();
        //
/*
        for ( int iVect=0 ; iVect<nVectors_ ; iVect++ )
        {
          for ( int iCoeff=0 ; iCoeff<nCoeffInt ; iCoeff++ )
          {
            coeff[iVect+iCoeff*nVectors_]=coeff_[local_indices[iCoeff]+iVect*nCoeffTot];
          }
        }
*/              
        //
        // unrolled loop
        //
        int *iCoeff=&local_indices[0];
        int *stop_iCoeff=iCoeff+nCoeffInt-3;
        const long n1=nVectors_;
        const long n2=nVectors_*2;
        const long n3=nVectors_*3;
        const long n4=nVectors_*4;
        Type *start=&coeff[0];
        Type *p_coeff=&coeff_[0];
        //
        for ( ; iCoeff<stop_iCoeff ; iCoeff+=4 , start+=n4 )
        {
          Type *__restrict__ src_ptr1=p_coeff+(*iCoeff); 
          const long         offset2=(*(iCoeff+1))-(*iCoeff); 
          const long         offset3=(*(iCoeff+2))-(*iCoeff);  
          const long         offset4=(*(iCoeff+3))-(*iCoeff); 
          //
          Type *__restrict__ dest_ptr1=start; 
          Type *__restrict__ dest_ptr2=start+n1;
          Type *__restrict__ dest_ptr3=start+n2;
          Type *__restrict__ dest_ptr4=start+n3;
          //
          for ( Type *stop_dest=start+n1 ; dest_ptr1<stop_dest ; 
                     dest_ptr1++ , dest_ptr2++ , dest_ptr3++ , dest_ptr4++ , src_ptr1+=nCoeffTot )
          {
            (*dest_ptr1)=(*src_ptr1);
            (*dest_ptr2)=(*(src_ptr1+offset2));
            (*dest_ptr3)=(*(src_ptr1+offset3));
            (*dest_ptr4)=(*(src_ptr1+offset4));
          }
        } 
        //
        // finish
        //
        for ( stop_iCoeff+=3 ; iCoeff<stop_iCoeff ; iCoeff++ , start+=n1 )
        {
          Type *__restrict__ src_ptr1=p_coeff+(*iCoeff); 
          //
          Type *__restrict__ dest_ptr1=start; 
          //
          for ( Type *stop_dest=start+n1 ; dest_ptr1<stop_dest ; 
                     dest_ptr1++ , src_ptr1+=nCoeffTot )
          {
            (*dest_ptr1)=(*src_ptr1);
          }
        }
#endif // DUMMY statement
        //
        // copy domain indices of the intersection
        // 
        domain_indices=my_local_intersection;
      }
    }
    //
    // if several procs are involved
    //
    else
    {
#ifndef DUMMY
#ifdef WITH_MPI  
      //
      // declare sending buffer without allocating
      //
      vector<Type> BUFF; 
      //
      // if there is something to send for this proc
      //
      if ( my_local_intersection.size()>0 )
      {  
        //
        // allocate buffer
        //
        BUFF.resize( my_local_intersection.size()*nVectors_ );
        //
        // find local indices of intersection
        //
        vector<int> local_indices = my_local_intersection <= local_domain_indices_[my_proc_];
        //
        // transpose corresponding coefficients in buffer 
        //
        int nCoeffTot=local_domain_indices_[my_proc_].size();
        int nCoeffInt=my_local_intersection.size();
        //
        
/*        
        for ( int iVect=0 ; iVect<nVectors_ ; iVect++ )
        {
          for ( int iCoeff=0 ; iCoeff<nCoeffInt ; iCoeff++ )
          {
            BUFF[iVect+iCoeff*nVectors_]=coeff_[local_indices[iCoeff]+iVect*nCoeffTot];
          }
        }
*/

        //
        // unrolled loop
        //
        int iCoeff=0;
        //
        for (  ; iCoeff<nCoeffInt-3 ; iCoeff+=4 )
        {
          Type *__restrict__ src_ptr1=&coeff_[local_indices[iCoeff]]; 
          const long         offset2=&coeff_[local_indices[iCoeff+1]]-&coeff_[local_indices[iCoeff]]; 
          const long         offset3=&coeff_[local_indices[iCoeff+2]]-&coeff_[local_indices[iCoeff]]; 
          const long         offset4=&coeff_[local_indices[iCoeff+3]]-&coeff_[local_indices[iCoeff]]; 
          //
          Type *__restrict__ dest_ptr1=&BUFF[iCoeff*nVectors_]; 
          Type *__restrict__ dest_ptr2=&BUFF[(iCoeff+1)*nVectors_];
          Type *__restrict__ dest_ptr3=&BUFF[(iCoeff+2)*nVectors_];
          Type *__restrict__ dest_ptr4=&BUFF[(iCoeff+3)*nVectors_];
          //
          for ( Type *stop_dest=&BUFF[iCoeff*nVectors_]+nVectors_ ; dest_ptr1<stop_dest ; 
                     dest_ptr1++ , dest_ptr2++ , dest_ptr3++ , dest_ptr4++ , src_ptr1+=nCoeffTot )
          {
            (*dest_ptr1)=(*src_ptr1);
            (*dest_ptr2)=(*(src_ptr1+offset2));
            (*dest_ptr3)=(*(src_ptr1+offset3));
            (*dest_ptr4)=(*(src_ptr1+offset4));
          }
        }
        //
        // finish
        //
        for (  ; iCoeff<nCoeffInt ; iCoeff++ )
        {
          Type *__restrict__ src_ptr1=&coeff_[local_indices[iCoeff]]; 
          //
          Type *__restrict__ dest_ptr1=&BUFF[iCoeff*nVectors_]; 
          //
          for ( Type *stop_dest=&BUFF[iCoeff*nVectors_]+nVectors_ ; dest_ptr1<stop_dest ; 
                     dest_ptr1++ , src_ptr1+=nCoeffTot )
          {
            (*dest_ptr1)=(*src_ptr1);
          }
        }  
      }
#endif // Dummy statement

      //
      // if this proc is receiving the coefficients  
      //
      if ( isListening )
      {
        //
        // compute total size of intersection
        //
        int size_inter_tot=0;
        for ( int i=0 ; i<size_local_inter.size() ; i++ ) size_inter_tot+=size_local_inter[i];
        //
        // allocate memory for reception of the coefficients
        //
        coeff.resize( size_inter_tot*nVectors_ );
        //
        // resize domain index list
        //
        domain_indices.resize( size_inter_tot );
      }
      
      //
      // gather the intersection indices on the procs
      //
#ifndef DUMMY
      for ( int iproc=0 ; iproc<list_proc.size() ; iproc++ )
      {
        communication::gather<int>( &my_local_intersection[0], &domain_indices[0], size_local_inter, list_proc[iproc], isend, context_ );   
      }
      //
      // gather the coefficients on the procs
      //
      for ( int iproc=0 ; iproc<list_proc.size() ; iproc++ )
      {
        communication::gather<Type>( &BUFF[0], &coeff[0], size_local_coeff, list_proc[iproc], isend, context_ );   
      }
#endif 


#else
    cout << "Error: reached a point not suposed to in serial build of Subspace::gatherCoeffsOnProcs" << endl;
    exit(0);
#endif
    }
  }
#ifdef GLOBAL_TIMING
  global_subspace_tmap["total gathering time"].start();
#endif      
}

#ifndef REALIMAG_H
#define REALIMAG_H

template <class Type> 
Type real( Type a)
{
  cout << "Error try to extract real/imag values from real number" << endl;
  exit(0);
  //
  return a;
} 

template <class Type> 
Type imag( Type a)
{
  cout << "Error try to extract real/imag values from real number" << endl;
  exit(0);
  //
  return 0.0;
}

#endif

#define N_VECTORS_PER_PAGE 5
template <class Type>
void Subspace<Type>::printSet(string comments)
{
#ifndef DUMMY
  //
  // if this proc is concerned
  //
  if ( local_domain_indices_.count(my_proc_) )
  {
    //
    // gather the coeff on first domain proc
    //
    vector<int> list_proc(1,list_proc_domain_[0]);
    //
    vector<Type> coeff;
    gatherCoeffsOnProcs(global_domain_indices_,list_proc,coeff);
    //
    // print the set from proc list_proc_domain_[0]
    //    
    if ( my_proc_==list_proc_domain_[0] )
    {
      int nCoeff=global_domain_indices_.size();
      //
      // print global header + comments firt
      //
      cout << endl;
      cout << "-------------------------------------------------------------------------" << endl;
      cout << "Vector Set: " << comments << endl;
      //
      // print vector N_VECTORS_PER_PAGE by N_VECTORS_PER_PAGE
      //
      int istart=0;
      //
      // real case
      //
      if ( !strstr(typeid(&coeff_[0]).name(),"complex") )
      {
        while (istart<nVectors_)
        {
          //
          // print header for this subset of vector
          //
          cout << "vectors: "; 
          //
          // adjust spacing for first vector index
          //
          if (istart<10) cout.width(8);
          else if (istart<100) cout.width(7);
          else if (istart<1000) cout.width(6);
          //
          // print vector indices
          //
          for ( int ivector=istart ; ivector<nVectors_ && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            cout << "(" << ivector << ")";
            //
            // adjust spacing for next vector index
            //
            if (ivector<9) cout.width(12);
            else if (ivector<99) cout.width(11);
            else if (ivector<999) cout.width(10);
          }
          //
          // flush
          //
          cout << endl;
          // 
          // print coefficients
          //
          for ( int icoeff=0 ; icoeff<nCoeff ; icoeff++ )
          {
            //
            // set spacing for coefficient index
            //
            if (global_domain_indices_[icoeff]<10) cout.width(4);
            else if (global_domain_indices_[icoeff]<100) cout.width(3);
            else if (global_domain_indices_[icoeff]<1000) cout.width(2);
            else if (global_domain_indices_[icoeff]<10000) cout.width(1);
            //
            // print coefficient index
            //
            cout << "(" << global_domain_indices_[icoeff] << ") :";
            cout.precision(5);
            for ( int ivector=istart ; ivector<nVectors_ && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
            {
              cout.width(14);
              cout << scientific << coeff[ivector+icoeff*nVectors_];
            } 
            cout << endl;
          }  
          //
          // increment istart
          //
          istart+=N_VECTORS_PER_PAGE;
        }
      }
      //
      // complex case
      //
      else
      {
        while (istart<nVectors_)
        {
          //
          // print header for this subset of vector
          //
          cout << "vectors: "; 
          //
          // adjust spacing for first vector index
          //
          if (istart<10) cout.width(13);
          else if (istart<100) cout.width(12);
          else if (istart<1000) cout.width(11);
          //
          // print vector indices
          //
          for ( int ivector=istart ; ivector<nVectors_ && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
          {
            cout << "(" << ivector << ")";
            //
            // adjust spacing for next vector index
            //
            if (ivector<9) cout.width(27);
            else if (ivector<99) cout.width(26);
            else if (ivector<999) cout.width(25);
          }
          //
          // flush
          //
          cout << endl;
          // 
          // print coefficients
          //
          for ( int icoeff=0 ; icoeff<nCoeff ; icoeff++ )
          {
            //
            // set spacing for coefficient index
            //
            if (global_domain_indices_[icoeff]<10) cout.width(4);
            else if (global_domain_indices_[icoeff]<100) cout.width(3);
            else if (global_domain_indices_[icoeff]<1000) cout.width(2);
            else if (global_domain_indices_[icoeff]<10000) cout.width(1);
            //
            // print coefficient index
            //
            cout << "(" << global_domain_indices_[icoeff] << ") :";
            cout.precision(5);
            for ( int ivector=istart ; ivector<nVectors_ && ivector-istart<N_VECTORS_PER_PAGE ; ivector++ )
            {
              cout.width(14);
              cout << scientific << real(coeff[ivector+icoeff*nVectors_]);
              if ( imag(coeff[ivector+icoeff*nVectors_])<0.0 )
              {
                cout.width(12);
              }
              else
              {
                cout << "+";
                cout.width(11);
              }
              cout << scientific << imag(coeff[ivector+icoeff*nVectors_]) << "*%i" ;
            } 
            cout << endl;
          }  
          //
          // increment istart
          //
          istart+=N_VECTORS_PER_PAGE;
        }      
      }
      cout << "-------------------------------------------------------------------------" << endl;
      cout << endl;
    }
  }
#else
  cout << "Warning: trying to print a dummy subspace" << endl;
#endif  
}

//
// discard local definitions if any
//
#ifdef GLOBAL_TIMING
  #undef GLOBAL_TIMING
#endif

#endif

#include "HarmonicOscillatorBasis.h"
#include <iostream>

// basis construction method 
void HarmonicOscillatorBasis::constructBasis(HarmonicOscillatorSystemDescription &system, bool verbose)
{
  //
  // set basis type
  //
  this->type_="Harmonic Oscillator Basis";
  //
  // set clustering parameters
  //
  this->clusterParam_=system.clusteringParameters();
  //
  // indicate that there is no ovelaps for this basis
  //
  this->hasOverlaps_=false;
  //
  // set cutoff
  //
  this->cutoff_=system.cutoff();
  //
  // global index
  //
  int global_index=0;
  //
  // allocate the list of stets for the reservoirs
  //
  this->reservoir_.resize(system.nReservoirs());
  //
  // get flags for this system
  //
  vector<string> system_flags = system.flags();
  //
  // construct the list of states for the reservoir
  //
  for ( int i_reservoir=0 ; i_reservoir<system.nReservoirs() ; i_reservoir++ )
  {
    //
    // create a list of cell indices for the reservoir
    //
    vector<vector<vector<int> > > cell_indices;
    //
    // get reference on the list of states for this reservoir
    //
    map<int,HarmonicOscillatorState> &reservoir_states=this->reservoir_[i_reservoir];
    //
    // point to the current reservoir
    //
    ReservoirAtomDescription *reservoir=system.reservoir(i_reservoir);
    //
    // get the repetitions for this reservoir
    //
    int nrep1=reservoir->nrep1();
    int nrep2=reservoir->nrep2();
    //
    // allocate cell_indices array
    //
    cell_indices.resize(nrep1);
    for ( int irep1=0 ; irep1<nrep1 ; irep1++ ) cell_indices[irep1].resize(nrep2);
    //
    // loop on the repetition of this reservoir
    //
    for ( int irep2=0 ; irep2<nrep2 ; irep2++ )
    for ( int irep1=0 ; irep1<nrep1 ; irep1++ )
    {
      //
      // get refence on the atoms describing this reservoir
      //
      vector<Atom<double> > atom_list=reservoir->atoms(irep1,irep2);
      //
      // for each atom of the reservoir
      //
      for ( int i_atom=0 ; i_atom<atom_list.size() ; i_atom++ )
      {
        //
        // create oscillator states and add indices to reservoir cell
        //
        HarmonicOscillatorState dx_state(atom_list[i_atom],HOB_axis_X,global_index);
        reservoir_states.insert(pair<const int,HarmonicOscillatorState>(global_index,dx_state));
        cell_indices[irep1][irep2].push_back(global_index);
        global_index++;
        //
        HarmonicOscillatorState dy_state(atom_list[i_atom],HOB_axis_Y,global_index);
        reservoir_states.insert(pair<const int,HarmonicOscillatorState>(global_index,dy_state));
        cell_indices[irep1][irep2].push_back(global_index);
        global_index++;
        //
        HarmonicOscillatorState dz_state(atom_list[i_atom],HOB_axis_Z,global_index);
        reservoir_states.insert(pair<const int,HarmonicOscillatorState>(global_index,dz_state));
        cell_indices[irep1][irep2].push_back(global_index);
        global_index++;
      }
    }
    //
    // keep label of this reservoir
    //
    this->reservoirLabel_.push_back(reservoir->label());
    //
    // keep flags for this part
    //
    this->reservoirFlags_.push_back(reservoir->flags());
    //
    // add flags inherited from the system
    //
    this->reservoirFlags_.back().insert(this->reservoirFlags_.back().end(),system_flags.begin(),system_flags.end());
    //
    // append the reservoir cell indices
    //
    this->reservoirs_cell_index_lists_.push_back(L2ReservoirCells(cell_indices));
    //
    // keep translation for this reservoir
    //
    this->reservoir_translations_.push_back(reservoir->translation());
  }
  //
  // allocate memory for the defect parts 
  //
  this->defectPart_.resize(system.nParts());
  //
  // construct lists of states for the defect parts
  //
  for ( int i_part=0 ; i_part<system.nParts() ; i_part++ )
  {
    PartAtomDescription *part=system.part(i_part);
    //
    // reference on this defect part atoms
    //
    const vector<Atom<double> > &atom_list=part->atoms();
    //
    // create a list of states for this part
    //
    map<int,HarmonicOscillatorState> &part_states=this->defectPart_[i_part];
    //
    // for each atoms of the part
    //
    for ( int i_atom=0 ; i_atom<atom_list.size() ; i_atom++ )
    {
      //
      // add oscillator states
      //
      HarmonicOscillatorState dx_state(atom_list[i_atom],HOB_axis_X,global_index);
      part_states.insert(pair<const int,HarmonicOscillatorState>(global_index,dx_state));
      global_index++;
      //
      HarmonicOscillatorState dy_state(atom_list[i_atom],HOB_axis_Y,global_index);
      part_states.insert(pair<const int,HarmonicOscillatorState>(global_index,dy_state));
      global_index++;
      //
      HarmonicOscillatorState dz_state(atom_list[i_atom],HOB_axis_Z,global_index);
      part_states.insert(pair<const int,HarmonicOscillatorState>(global_index,dz_state));
      global_index++;
    }
    // 
    // keep label of this part
    //
    this->defectPartLabel_.push_back(part->label());
    //
    // keep flags for this part
    //
    this->defectPartFlags_.push_back(part->flags());
    //
    // add flags inherited from the system
    //
    this->defectPartFlags_.back().insert(this->defectPartFlags_.back().end(),system_flags.begin(),system_flags.end());
    //
    // indicate that this part is not a contact
    //
    this->contactIndex_.push_back(-1);
    //
    // keep translation for this part
    //
    this->part_translations_.push_back(part->translation());
  }
  //
  // keep total number of states
  // 
  int n_states_tot=global_index;
  //
  // add contacts for the reservoirs
  //
  for ( int i_reservoir=0 , ipart=system.nParts() ; i_reservoir<system.nReservoirs() ; i_reservoir++ )
  {
    //
    // point to the current reservoir
    //
    ReservoirAtomDescription *reservoir=system.reservoir(i_reservoir);
    //
    // get the repetitions for this reservoir
    //
    int nrep1=reservoir->nrep1();
    int nrep2=reservoir->nrep2();
    //
    // get the number of contacts for this reservoir
    //
    int ncontacts=reservoir->nContacts();
    //
    // construct the ordered lists of atoms and indices of this reservoir
    //
    vector<Atom<double> > reservoir_atoms;
    vector<int>           reservoir_indices;
    //
    for ( int irep2=0 ; irep2<nrep2 ; irep2++ )
    for ( int irep1=0 ; irep1<nrep1 ; irep1++ )
    {
      //
      // get refence on the atoms describing this reservoir
      //
      vector<Atom<double> > atom_list=reservoir->atoms(irep1,irep2);
      //
      // insert in the atom list
      //
      reservoir_atoms.insert(reservoir_atoms.end(),atom_list.begin(),atom_list.end());
      //
      // get the corresponding list of indices
      //
      vector<int> index_list=this->reservoirs_cell_index_lists_[i_reservoir].repIndexList(irep1,irep2);
      //
      // insert in the index list
      //
      reservoir_indices.insert(reservoir_indices.end(),index_list.begin(),index_list.end());
    }
    //
    // loop on the contact list of this reservoir
    //
    for ( int icontact=0 ; icontact<ncontacts ; icontact++ )
    {
      //
      // point tho the current contact
      // 
      PartAtomDescription *contact=reservoir->contact(icontact);
      //
      // reference on this defect part atoms
      //
      vector<Atom<double> > atom_list=contact->atoms();
      //
      // get the indices of these atoms in the reservoir ordered atom list
      //
      vector<int> local_indices=getAtomIndices<double>(atom_list,reservoir_atoms);
      //
      // create a list of states for this part
      //
      map<int,HarmonicOscillatorState> new_map;
      this->defectPart_.push_back( new_map );
      map<int,HarmonicOscillatorState> &part_states=this->defectPart_.back();
      //
      // for each atoms of the part
      //
      for ( int i_atom=0 ; i_atom<atom_list.size() ; i_atom++ )
      {
        int index_x=reservoir_indices[local_indices[i_atom]*3];
        int index_y=reservoir_indices[local_indices[i_atom]*3+1];
        int index_z=reservoir_indices[local_indices[i_atom]*3+2];
        //
        // add oscillator states
        //
        HarmonicOscillatorState dx_state(atom_list[i_atom],HOB_axis_X,index_x);
        part_states.insert(pair<const int,HarmonicOscillatorState>(index_x,dx_state));
        //
        HarmonicOscillatorState dy_state(atom_list[i_atom],HOB_axis_Y,index_y);
        part_states.insert(pair<const int,HarmonicOscillatorState>(index_y,dy_state));
        //
        HarmonicOscillatorState dz_state(atom_list[i_atom],HOB_axis_Z,index_z);
        part_states.insert(pair<const int,HarmonicOscillatorState>(index_z,dz_state));
      }
      // 
      // keep label of this part
      //
      this->defectPartLabel_.push_back(contact->label());
      //
      // keep flags for this part
      //
      this->defectPartFlags_.push_back(reservoir->flags());
      //
      // add flags inherited from the system
      //
      this->defectPartFlags_.back().insert(this->defectPartFlags_.back().end(),system_flags.begin(),system_flags.end());
      //
      // indicate that this part is a contact
      //
      this->contactIndex_.push_back(i_reservoir);
      //
      // keep translation for this part
      //
      this->part_translations_.push_back(contact->translation());
    }
  }
  //
  // set the boxes for the parts
  //
  this->setPartBoxes();
  //
  // recapitulate
  //
  if ( verbose )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "Basis construction:" << endl << endl;
    cout << "    created " << n_states_tot << " states for " << system.nParts() 
         << "+"  << defectPart_.size()-system.nParts() << " defect parts and " 
         << reservoir_.size() << " reservoir" << ( ( reservoir_.size()>1) ? "s" : "" ) << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << endl;
  }
  
  //
  // generate the list of files if we use DFT force field
  //
  if ( system.hasFlag("externalForces") )
  {
    //
    // generate the list of atoms corresponding to the defect
    //
    { 
      //
      // generate state index list for the ensemble of defects 
      //
      vector<int> index_list = this -> getExtendedDefectIndices(); 
      //
      // get the corresponding atom list
      //
      vector<Atom<double> > atom_list = this-> getAtomList(index_list);
      //
      // check repetitions
      //
      for ( int i_part=0 ; i_part<part_translations_.size() ; i_part++ )
      {
        for ( int j_trans=0 ; j_trans<part_translations_[i_part].size() ; j_trans++ )
        {
          bool ok=false;
          for ( int i_trans=0 ; i_trans<part_translations_[0].size() ; i_trans++ )
          {
            if ( ( part_translations_[0][i_trans]-part_translations_[i_part][j_trans] )*( part_translations_[0][i_trans]-part_translations_[i_part][j_trans] ) < 1.0e-10 ) ok=true;
            if ( ( part_translations_[0][i_trans]+part_translations_[i_part][j_trans] )*( part_translations_[0][i_trans]+part_translations_[i_part][j_trans] ) < 1.0e-10 ) ok=true;
          }
          if ( !ok )
          {
            cout << "Error: inconsistent translations found between defect parts\n";
            exit(0);
          }
        }
      }
      // 
      // open the file for the defect
      //
      char buff[1024];
      ofstream filestr;
      sprintf(buff,"data/defect.xyz");
      filestr.open (buff);
      filestr.setf(ios::fixed);
      filestr.precision(8);
      //
      // print the number of atoms first
      //
      filestr << atom_list.size() << endl << endl;
      //
      // print the list of atoms
      //
      for ( int iat=0 ; iat<atom_list.size() ; iat++ )
      {
        filestr.width(4);
        filestr << atom_list[iat].name;
        filestr.width(20);
        filestr << atom_list[iat].x_pos;
        filestr.width(20);
        filestr << atom_list[iat].y_pos;
        filestr.width(20);
        filestr << atom_list[iat].z_pos << endl;
      }
      //
      // write repetitions
      //
      filestr << endl;
      if ( part_translations_[0].size()>0 ) 
      {
        filestr << "// periodicity:" << endl;
      }
      for ( int i_trans=0 ; i_trans<part_translations_[0].size() ; i_trans++ )
      {
        filestr.width(20);
        filestr << part_translations_[0][i_trans][0];
        filestr.width(20);
        filestr << part_translations_[0][i_trans][1];
        filestr.width(20);
        filestr << part_translations_[0][i_trans][2] << endl;
      }
      //
      // close file
      //
      filestr.close();
    }
    
    //
    // generate the list of atoms corresponding to each reservoir
    //
    for ( int i_reservoir=0 , ipart=system.nParts() ; i_reservoir<system.nReservoirs() ; i_reservoir++ )
    {
      //
      // generate state index list for the ensemble of defects 
      //
      vector<int> index_list = this -> getReservoirIndices(i_reservoir); 
      //
      // get the corresponding atom list
      //
      vector<Atom<double> > atom_list = this-> getAtomList(index_list);
      //
      // open the file for the reservoir
      //
      char buff[1024];
      ofstream filestr;
      sprintf(buff,"data/reservoir_%i.xyz",i_reservoir);
      filestr.open (buff);
      filestr.setf(ios::fixed);
      filestr.precision(8);
      //
      // print the number of atoms first
      //
      filestr << atom_list.size() << endl << endl;
      //
      // print the list of atoms
      //
      for ( int iat=0 ; iat<atom_list.size() ; iat++ )
      {
        filestr.width(4);
        filestr << atom_list[iat].name;
        filestr.width(20);
        filestr << atom_list[iat].x_pos;
        filestr.width(20);
        filestr << atom_list[iat].y_pos;
        filestr.width(20);
        filestr << atom_list[iat].z_pos << endl;
      }
      //
      // write repetitions
      //
      filestr << endl;
      filestr << "// periodicity:" << endl;
      //
      // write propagation direction
      //
      filestr.width(20);
      filestr << system.reservoir(i_reservoir)->propagation()[0]*3.0;
      filestr.width(20);
      filestr << system.reservoir(i_reservoir)->propagation()[1]*3.0;
      filestr.width(20);
      filestr << system.reservoir(i_reservoir)->propagation()[2]*3.0 << endl;
      //
      // write other periodicity if any
      //
      for ( int i_trans=0 ; i_trans<reservoir_translations_[0].size() ; i_trans++ )
      {
        filestr.width(20);
        filestr << reservoir_translations_[0][i_trans][0];
        filestr.width(20);
        filestr << reservoir_translations_[0][i_trans][1];
        filestr.width(20);
        filestr << reservoir_translations_[0][i_trans][2] << endl;
      }
      //
      // close file
      //
      filestr.close();
    }
  }
}

vector<Atom<double> > HarmonicOscillatorBasis::getAtomList(vector<int> &index_list)
{
  //
  // start by getting the corresponding states list
  //
  vector<HarmonicOscillatorState> state_list=this->getStateList(index_list);
  //
  // init the list of atoms
  //
  vector<Atom<double> > atom_list;
  //
  // loop on the states to set the atoms
  //
  for ( int istate=0 ; istate+2<state_list.size() ; istate+=3 )
  {
    //
    // init atom
    //
    Atom<double> atom;
    //
    // set atom name
    //
    atom.name=state_list[istate].atomName();
    //
    // set atom position
    //
    Vector3D<double> position=state_list[istate].center();
    atom.x_pos=position[0];
    atom.y_pos=position[1];
    atom.z_pos=position[2];
    //
    // verify that the states are consistently arranged
    //
    if ( ( atom.name != state_list[istate+1].atomName() ) || ( atom.name != state_list[istate+2].atomName() ) )
    {
      cout << "Error: the states indices of one atom are not grouped in - HarmonicOscillatorBasis::getAtomList -" << endl;
      exit(0);
    }
    //
    if (  ( state_list[istate+0].direction() != HOB_axis_X )
       || ( state_list[istate+1].direction() != HOB_axis_Y )
       || ( state_list[istate+2].direction() != HOB_axis_Z ) )
    {
      cout << "Error: the states indices of one atom are not correctely ordered in - HarmonicOscillatorBasis::getAtomList -" << endl;
      exit(0);
    }
    //
    if ( ( position!=state_list[istate+1].center() ) || ( position!=state_list[istate+2].center() ) )
    {
      cout << "Error: the coordinate of one atom differs on each state in - HarmonicOscillatorBasis::getAtomList -" << endl;
      exit(0);
    }
    //
    // add atom to the list
    //
    atom_list.push_back(atom);
  }
  //
  // return atom list
  //
  return atom_list;
}

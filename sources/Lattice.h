#ifndef LATTICE_H
#define LATTICE_H

#include <stdlib.h>
#include <vector>
#include <math.h>
#include <iostream>
#include "Vector3D.h"

using namespace std;

template <class CoordinatePrecision>
class Lattice
{
  private:
    
    // dimension attribute
    int dimension_;
    
    // lattice attribute
    vector<Vector3D<CoordinatePrecision> > latticeVector_;     // contains the reduced lattice vectors (reduced at creation)
    vector<Vector3D<CoordinatePrecision> > dualLatticeVector_; // contains the dual form of the reduced lattice vectors
    vector<Vector3D<CoordinatePrecision> > sqnrLatticeVector_; // contains the reduced lattice vectors divided by their square norm
    
    // folding method
    void fold1D_( Vector3D<CoordinatePrecision> &vector, Vector3D<CoordinatePrecision> &latticeVector, Vector3D<CoordinatePrecision> &measure );
    
  public:
    
    // constructor
    Lattice( vector<Vector3D<CoordinatePrecision> > &latticeVectors );
    
    // vector folding method
    Vector3D<CoordinatePrecision>& fold( Vector3D<CoordinatePrecision>& vetor );
    
    // destructor
    ~Lattice(void) {}
};

template <class CoordinatePrecision>
Lattice<CoordinatePrecision>::Lattice( vector<Vector3D<CoordinatePrecision> > &latticeVectors )
{
  //
  // test size of lattice vectors
  // 
  if ( latticeVectors.size()==0 )
  {
    cout << "Error: at least one lattice vector should be provided at construction in - Lattice::Lattice( Vector3D<CoordinatePrecision> &latticeVectors ) -" << endl;
    exit(0);
  }
  if ( latticeVectors.size()==3 )
  {
    cout << "3D Lattice not implemented yet in - Lattice::Lattice( Vector3D<CoordinatePrecision> &latticeVectors ) -" << endl;
    exit(0);
  }
  //
  // cas 1D
  //
  if ( latticeVectors.size()==1 )
  {
    //
    // set dimension
    //
    dimension_=1;
    //
    // copy lattice vector
    //
    latticeVector_=latticeVectors;
    //
    // compute norm
    //
    CoordinatePrecision norm=latticeVectors[0]*latticeVectors[0];
    //
    // store dual 
    //
    dualLatticeVector_.push_back(latticeVectors[0]/norm);
    sqnrLatticeVector_.push_back(latticeVectors[0]/norm);
    //
    // nothing else to be done
    //
    return;
  }
  //
  // cas 2D
  //
  if ( latticeVectors.size()==2 )
  {
    //
    // set dimension
    //
    dimension_=2;
    //
    // reduce to minimal lattice vectors
    //
    Vector3D<CoordinatePrecision> V1=latticeVectors[0];
    Vector3D<CoordinatePrecision> V2=latticeVectors[1];
    //
    // loop for is only for "safety", should exit within 1 or 2 iterations
    //
    for ( int count=0 , countMax=100 ; count<countMax ; count++ )
    {
      CoordinatePrecision prod12=V1*V2;
      CoordinatePrecision norm_1=V1*V1;
      CoordinatePrecision norm_2=V2*V2;
      CoordinatePrecision m1=round(prod12/norm_1);
      CoordinatePrecision m2=round(prod12/norm_2);
      //
      // determine if there is a smaller vector
      //
      if ( m1 || m2 )
      {
        if ( fabs(m1)>fabs(m2) ) 
        {
          m2=-1.0;
        }
        else
        {
          m1=-1.0;
        }
      }
      else
      {
        //
        // if no smaller vector is possible, exit loop
        //
        break;
      }
      //
      // form new lattice vectors
      //
      if ( norm_1<norm_2 )
      {
        V2=V1*m1+V2*m2;
      }
      else
      {
        V1=V1*m1+V2*m2;
      }
    }
    //
    // store reduced lattice vectors
    //
    latticeVector_.push_back(V1);
    latticeVector_.push_back(V2);
    //
    // compute duals of reduced lattice vectors
    //
    Vector3D<CoordinatePrecision> V3=V1^V2;
    Vector3D<CoordinatePrecision> d1=V2^V3; 
    Vector3D<CoordinatePrecision> d2=V3^V1; 
    dualLatticeVector_.push_back( d1/(d1*V1) );
    dualLatticeVector_.push_back( d2/(d2*V2) );
    //
    // compute weighted reduced lattice vectors
    //
    sqnrLatticeVector_.push_back( V1/(V1*V1) );
    sqnrLatticeVector_.push_back( V2/(V2*V2) );
    //
    // nothing else to be done
    //
    return;
  }
  //
  // should never get here
  //
  cout << "Error: to many lattice vectors in - Lattice::Lattice( Vector3D<CoordinatePrecision> &latticeVectors ) -" << endl;
  exit(0);
}

template <class CoordinatePrecision>
void Lattice<CoordinatePrecision>::fold1D_( Vector3D<CoordinatePrecision> &vector, Vector3D<CoordinatePrecision> &latticeVector, Vector3D<CoordinatePrecision> &measure )
{
  //
  // measure |vector> on |latticeVector> using <measure| 
  // and compute how much should be soustracted to get in
  // the brillouin zone. Use the fact that:
  //  
  //   a-floor(a+0.5) is in [-0.5 , 0.5] range
  //
  CoordinatePrecision m=floor(vector*measure+0.5);
  //
  // fold V along D
  //
  vector-=latticeVector*m;
}

template <class CoordinatePrecision>
Vector3D<CoordinatePrecision>& Lattice<CoordinatePrecision>::fold( Vector3D<CoordinatePrecision> &vector )
{
  //
  // dummy vector for trial
  //
  Vector3D<CoordinatePrecision> vector_tmp;     
  //
  // work according to the dimension
  //
  switch ( dimension_ )
  {
    case 1:
      
      //
      // only fold once along single lattice direction
      //
      fold1D_( vector , latticeVector_[0] , dualLatticeVector_[0] );
      break;
      
    case 2:
      
      //
      // fold first using dual form measures
      //
      fold1D_( vector , latticeVector_[0] , dualLatticeVector_[0] );
      fold1D_( vector , latticeVector_[1] , dualLatticeVector_[1] );
      //
      // get a copy of the vector
      //
      vector_tmp=vector;
      //
      // try folding using weighted reduced lattice vectors
      //
      fold1D_( vector     , latticeVector_[0] , sqnrLatticeVector_[0] );
      fold1D_( vector_tmp , latticeVector_[1] , sqnrLatticeVector_[1] );
      //
      // chose best answer
      //
      if ( vector_tmp*vector_tmp < vector*vector ) vector=vector_tmp; 
      break;
      
    default:
    
      //
      // should not arrive here
      //
      cout << "Error: Lattice dimension should be 1D or 2D in  - Lattice<CoordinatePrecision>::fold -" << endl;
      exit(0); 
  }
  //
  // return reference on the folded vector
  //
  return vector; 
}

#endif

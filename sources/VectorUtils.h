#ifndef VECTORUTILS_H
#define VECTORUTILS_H

// include vector STDLIB
#include <vector>

//! function intended to completely clear a vector and release its memory
/// 
template <class Type>
void clear(vector<Type> &my_vector)
{
  my_vector.clear();
  vector<Type>().swap(my_vector);
}

//! function intended to shrink memory usage of a vector to best fit
//
template <class Type>
void shrink(vector<Type> &my_vector)
{
  vector<Type>(my_vector).swap(my_vector);
}

#endif

#ifndef HARMONICOSCILLATORSTATE_H
#define HARMONICOSCILLATORSTATE_H

#include <string>

#include "Atom.h"
#include "L2State.h"

#define HOB_axis_X 1
#define HOB_axis_Y 2
#define HOB_axis_Z 3

class HarmonicOscillatorState : public L2State
{
  protected:
    
    // direction of the oscillator and related atom name
    int    axis_;
    string atom_name_;
    
  public:
   
    // method 
    string atomName(void)  { return atom_name_; }
    int    direction(void) { return axis_; }
    
    // default constructor
    HarmonicOscillatorState(void) : L2State() {}
 
    // copy constructors
    HarmonicOscillatorState(const HarmonicOscillatorState &other) : L2State(other)
    {
      //
      // copy attributes
      //
      axis_=other.axis_;
      atom_name_=other.atom_name_;  
    }
    
    // standard constructor
    HarmonicOscillatorState(Atom<double> atom, int axis, int global_index) : L2State(atom.coordinates(),global_index)
    {
      axis_=axis;
      atom_name_=atom.name;
    }
    
    // assignement operator
    HarmonicOscillatorState& operator=(const HarmonicOscillatorState &other)
    {
      //
      // base class copy
      //
      L2State::operator=(other);
      //
      // copy attributes
      //
      axis_=other.axis_;
      atom_name_=other.atom_name_;
      //
      // return reference on the object
      //  
      return *this;
    }

    // destructor
    ~HarmonicOscillatorState(void) {}
};

#endif

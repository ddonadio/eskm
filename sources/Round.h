/// \file Round.h
/// \brief rounding algorithm for atom position sorting

#ifndef ROUND_H
#define ROUND_H

///
/// local rounding function
///
/// the use of 0.5+eps instead of 0.5 
/// is to avoid rounding errors
/// during the comparison af atoms positions
/// (I did not really get why that is though...)
/// 
/// if the epsilon is too small, then the 
/// errors reappears
///
template <class Precision>
inline int localRound(Precision x)
{
  return ( x>=0.0 ? int(x+0.5000001) : int(x-0.5000001) );
}

#endif

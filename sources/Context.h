#ifndef CONTEXT_H
#define CONTEXT_H

#include <map>
#include <vector>
#include "mpi.h"
#include "Basis.h"
#include "IndexList.h"
#include "ProcessorGrid.h"
#include "GraphClustering.h"
#include "GraphClusteringParameters.h"

using namespace std;

class Context
{
  private:
    
    // this context attributes
    map<int,vector<int> > global_indices_on_proc_; 
    MPI_Group             group_handle_;
    MPI_Comm              comm_handle_;
    int                   my_proc_;
    int                   n_procs_;
    
    // parent context if any
    Context               *parent_;
    int                   child_rank_; 
    
    // children contexes 
    vector<Context*>       children_;
    
    // flag to indicate if this context is fake
    bool isFake_;
    
  public:
  
    // construtor
    Context(vector<int> list_proc, MPI_Comm comm, bool fake=false);
    
    // division
    Context* divide(int nContexts);
    
    // context construction from basis object
    template <class StateType, class SystemDescriptionType>
    void        init(Basis<StateType,SystemDescriptionType> &basis);
    
    // repartition of the basis on this context
    template <class StateType, class SystemDescriptionType>
    void printBasisRepartition(Basis<StateType,SystemDescriptionType> &basis);
    template <class StateType, class SystemDescriptionType>
    void printBasisRepartition(Basis<StateType,SystemDescriptionType> &basis, vector<vector<int> > groups);
    // global indices acces methods
    void        setIndicesOnProc(int i_proc, vector<int> indices);
    void        addIndicesOnProc(int i_proc, vector<int> indices);
    vector<int> getIndicesOnProc(int i_proc);
    vector<int> getIndices(void);
    
    // comm atribute access
    int        myProc(void)  { return my_proc_; }
    int        nProcs(void)  { return n_procs_; }
    MPI_Comm&  comm(void)    { return comm_handle_; }
    MPI_Group& group(void)   { return group_handle_; }
    
    // context attribute access
    int        myContext(void) { return child_rank_; }
    Context*   parent(void)    { return parent_; }
    bool       isFake(void)    { return isFake_; }
    
    // destructor
    ~Context(void);
};

// repartition of the basis on this context
template <class StateType, class SystemDescriptionType>
void Context::printBasisRepartition(Basis<StateType,SystemDescriptionType> &basis)
{
  // if we are on proc 0 
  if ( my_proc_==0 && child_rank_<=0 )
  {
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "Context: Basis repartition" << endl << endl;
    //
    // get the number of parts and reservoirs
    //
    int nparts=basis.nDefectParts();
    int nres=basis.nReservoirs();
    //
    // inform
    //
    cout << "  N leads = " << nres << endl; 
    cout << "  N parts = " << nparts << endl;
    cout << "  N procs = " << n_procs_ << endl;
    //
    // count the number of procs for each contact
    // count also the interacting procs with the contacts
    //
    map<int,map<int,int> > contact_procs;
    map<int,map<int,int> > contact_inter;
    //
    // loop on each part
    //
    for ( int ipart=0 ; ipart<nparts ; ipart++ )
    {
      //
      // check if this part is a contact
      //
      int ireservoir;
      if ( basis.isContact(ipart,&ireservoir) )
      {
        //
        // get this part range indices
        // 
        vector<int> partIndices=basis.getDefectPartIndices(ipart);
        //
        // search for the part indices on each proc
        //
        for ( int iproc=0 ; iproc<n_procs_ ; iproc++ )
        {
          //
          // get indices on this proc
          //
          vector<int> indicesOnProc=global_indices_on_proc_[iproc];
          //
          // search for correspondances within the indices of this part
          //
          vector<int> intersection = indicesOnProc && partIndices;
          //
          // if the intersection is non null, add this proc index to the map
          //
          if ( intersection.size()>0 ) contact_procs[ireservoir][iproc]=1;
        }
        //
        // get this part domain indices
        //
        vector<int> neighborIndices=basis.getNeighborhoodStateIndexList(ipart);
        //
        // search for the part indices on each proc
        //
        for ( int iproc=0 ; iproc<n_procs_ ; iproc++ )
        {
          //
          // get index on this proc
          //
          vector<int> indicesOnProc=global_indices_on_proc_[iproc];
          //
          // search for correspondances within the indices of this part
          //
          vector<int> intersection = indicesOnProc && neighborIndices;
          //
          // if the intersection is non null, add this proc index to the map
          //
          if ( intersection.size()>0 ) contact_inter[ireservoir][iproc]=1;
        }
      }
    }
    //
    // recapitulate procs and interactions for the contacts
    //
    map<int,map<int,int> >::iterator it_procs=contact_procs.begin();
    map<int,map<int,int> >::iterator it_inter=contact_inter.begin();
    for ( map<int,map<int,int> >::iterator end_procs=contact_procs.end() ; it_procs!=end_procs ; it_procs++, it_inter++ )
    {
      //
      // print head line
      //
      cout << "  Contact " << it_procs->first << endl;
      cout << "    contained on procs:  [ ";
      for ( map<int,int>::iterator it=it_procs->second.begin() ; it!=it_procs->second.end() ; it++ ) cout << it->first << " ";
      cout << "]" << endl;
      cout << "    interact with procs: [ ";
      for ( map<int,int>::iterator it=it_inter->second.begin() ; it!=it_inter->second.end() ; it++ ) cout << it->first << " ";
      cout << "]" << endl;
    }
    cout << endl;
    cout << "-------------------------------------------------------------------------" << endl;
  }
}

// repartition of the basis on this context
template <class StateType, class SystemDescriptionType>
void Context::printBasisRepartition(Basis<StateType,SystemDescriptionType> &basis, vector<vector<int> > groups)
{
  // if we are on proc 0 
  if ( my_proc_==0 && child_rank_<=0 )
  {
    cout << "  basis repartition" << endl;
    //
    // get the number of parts and reservoirs
    //
    int nparts=basis.nDefectParts();
    int nres=basis.nReservoirs();
    //
    // inform
    //
    cout << "    N leads = " << nres << endl; 
    cout << "    N parts = " << nparts << endl;
    cout << "    N procs = " << groups.size() << endl;
    //
    // count the number of procs for each contact
    // count also the interacting procs with the contacts
    //
    map<int,map<int,int> > contact_procs;
    //
    // loop on each part
    //
    for ( int igroup=0 ; igroup<groups.size() ; igroup++ )
    {
      for ( int iel=0 ; iel<groups[igroup].size() ; iel++ )
      {
        //
        // check if this part is a contact
        //
        int ireservoir;
        int ipart=groups[igroup][iel];
        if ( basis.isContact(ipart,&ireservoir) )
        {
          //
          // add this proc to list of contact
          //
          contact_procs[ireservoir][igroup]++;
        }
      }
    }
    //
    // recapitulate procs and interactions for the contacts
    //
    map<int,map<int,int> >::iterator it_procs=contact_procs.begin();
    for ( map<int,map<int,int> >::iterator end_procs=contact_procs.end() ; it_procs!=end_procs ; it_procs++ )
    {
      //
      // print head line
      //
      cout << "    Contact " << it_procs->first << endl;
      cout << "      contained on procs:   [ ";
      for ( map<int,int>::iterator it=it_procs->second.begin() ; it!=it_procs->second.end() ; it++ ) { cout.width(3); cout << it->first << " "; }
      cout << "]" << endl;
      cout << "      with the repartition: [ ";
      for ( map<int,int>::iterator it=it_procs->second.begin() ; it!=it_procs->second.end() ; it++ ) { cout.width(3); cout << it->second << " "; }
      cout << "]" << endl;
    }
  }
}


// construction from the basis
template <class StateType, class SystemDescriptionType>
void Context::init(Basis<StateType,SystemDescriptionType> &basis)
{
  //
  // indicate where we are
  //
  if ( my_proc_==0 && child_rank_<=0 )
  {
    cout << "-------------------------------------------------------------------------" << endl;
    cout << "Context initialization:" << endl << endl; 
  }
  //
  // preallocate group vectors
  //
  vector<vector<int> > groups(n_procs_);
  //
  // get graph size
  //
  int size_graph=basis.nDefectParts();
  //
  // get group through graph clustering
  //
  if (my_proc_ ==0 ) 
  {
    //
    // get clustering parameters
    //
    GraphClusteringParameters clusterParameters=basis.getClusteringParameters();
    int  nClusters=clusterParameters.nClusters;
    int  kMeansDim=clusterParameters.kMeansDim;             
    int  kMeansMaxSteps=clusterParameters.kMeansMaxSteps;        
    int  kMeansDimMaxOptSteps=clusterParameters.kMeansDimMaxOptSteps;  
    int  slackAmount=clusterParameters.slackAmount;           
    bool refineGroups=clusterParameters.refineGroups;          
    //
    // set the number of cluster if not defined
    //
    nClusters = nClusters==0 ? n_procs_ : nClusters;
    //
    // generate the processorGrib filename
    //
    char fileName[512];
    if ( clusterParameters.fileLabel[0]==0 ) sprintf(fileName,"gridMapping_%i_parts_%i_clusters",size_graph,nClusters);
    else sprintf(fileName,"gridMapping_%s_%i_parts_%i_clusters",clusterParameters.fileLabel,size_graph,nClusters);
    //
    // try to read the groups for thif file
    //
    groups=loadGroups(fileName,basis);
    //
    // if we could not find the groups definition
    //
    if ( groups.size()==0 )
    {
      //
      // inform where we are
      //
      cout << "  constructing part connectivity graph." << endl;
      //
      // init connectivity graph
      //
      vector<double> graph(size_graph*size_graph,0.0);
      //
      // construct the connectivity graph of the system
      //  
      for ( int ipart=0 ; ipart<size_graph ; ipart++ )
      {
        //
        // get interacting parts
        //
        vector<int>  neighbors_indices=basis.getNeighborhoodPartList(ipart);
        //
        // fill the graph_adjascent matrix
        //
        for ( int j=0 ; j<neighbors_indices.size() ; j++ )
        {
          //
          // get corresponding part index
          //
          int jpart=neighbors_indices[j];
          //
          // if we are not on the same part
          //
          if ( ipart!=jpart )
          {
            //
            // set interactions to 1.0
            //  
            graph[ipart+jpart*size_graph]=1.0;
          }
        }
      }
      //
      // init the graph 
      //
      GraphClustering<double> graphClustering;
      graphClustering.setGraph(graph, size_graph);
      //
      // compute the slack to relax 10% of the procs
      //
      int slack=(((size_graph+nClusters-1)/nClusters)*slackAmount)/100;
      //
      // inform where we are
      //
      if ( child_rank_<=0 ) 
      {
        cout << "  clustering the graph" << endl;
        cout << "    nClusters            = " << nClusters << endl;
        cout << "    kMeansDim            = " << kMeansDim << endl;
        cout << "    kMeansMaxSteps       = " << kMeansMaxSteps << endl;
        cout << "    kMeansDimMaxOptSteps = " << kMeansDimMaxOptSteps << endl;
        cout << "    slackAmount          = " << slackAmount << endl;
        cout << "    slack                = " << slack << endl;
        cout << "    refineGroups         = " << refineGroups << endl;
      }
      //
      // cluster the graph 
      //
      groups=graphClustering.clusterGraph(nClusters,slack,true,true,refineGroups,kMeansMaxSteps,kMeansDim,kMeansDimMaxOptSteps);
      //
      // save this config
      //
      saveGroups(fileName,basis,groups);
    }
    else
    {
      //
      // inform that we got the group from the file
      //
      cout << "  read groups form file " << fileName << endl;
    }
    //
    // print basis repartition
    //
    printBasisRepartition(basis,groups);
    //
    // if it just a test, terminate
    //
    if ( nClusters!=n_procs_ ) exit(0);
    //
    // send the result to the other procs within this context
    //
#ifdef WITH_MPI
    //
    // get sizes of the groups
    //
    vector<int> group_sizes;
    for ( int i=0 ; i<n_procs_ ; i++ ) group_sizes.push_back(groups[i].size());
    //
    // send sizes of the groups
    //
    MPI_Bcast(&group_sizes[0],n_procs_,MPI_INT,0,comm_handle_);
    for ( int i=0 ; i<n_procs_ ; i++ )
    {
      //
      // send indices
      //
      MPI_Bcast(&groups[i][0],group_sizes[i],MPI_INT,0,comm_handle_);
    }
#endif    
  }
  else
  {
    //
    // receive the broadcasted result
    //
#ifdef WITH_MPI
    //
    // get sizes of the groups
    //
    vector<int> group_sizes(n_procs_);
    MPI_Bcast(&group_sizes[0],n_procs_,MPI_INT,0,comm_handle_);
    for ( int i=0 ; i<n_procs_ ; i++ )
    {
      //
      // allocate memory for receive
      //
      groups[i].resize(group_sizes[i]);
      //
      // receive indices
      //
      MPI_Bcast(&groups[i][0],group_sizes[i],MPI_INT,0,comm_handle_);
    }
#endif
  }
  //
  // buil correspondance between parts and processors
  // 
  vector<int> index_proc_for_part(size_graph);
  for ( int index_proc=0 ; index_proc<groups.size() ; index_proc++ )
  {
    for ( int i=0 ; i<groups[index_proc].size() ; i++ )
    {
      index_proc_for_part[groups[index_proc][i]]=index_proc;
    }
  }
  //
  // init list of indices for the processors
  //
  vector<vector<int> > indicesOnProc(n_procs_);
  //
  // share the part domains among processors
  //
  for ( int ipart=0 ; ipart<size_graph ; ipart++ )
  {
    //
    // get part indices
    //
    vector<int> partIndices;
    if ( basis.getDefectPartType(ipart)=="contact" )
    {
      partIndices=basis.getContactStateIndexList(ipart);
    }
    else
    {
      partIndices=basis.getDefectPartIndices(ipart);
    }
    //
    // get corresponding processor index
    //
    int index_proc=index_proc_for_part[ipart];
    //
    // add indices to the list of this proc
    //
    indicesOnProc[index_proc].insert(indicesOnProc[index_proc].end(),partIndices.begin(),partIndices.end());
  }
  //
  // register the indices
  //
  for ( int iproc=0 ; iproc<n_procs_ ; iproc++ )
  {
    //
    // sort indices
    //
    sort<int>(&indicesOnProc[iproc][0],indicesOnProc[iproc].size());
    //
    // register indices
    //
    setIndicesOnProc(iproc,indicesOnProc[iproc]);
  }
  //
  // inform of the operations
  //
  if ( my_proc_==0 && child_rank_<=0 )
  {
    cout << "  initialization OK" << endl;
    cout << "-------------------------------------------------------------------------" << endl;
  }      
}

#endif

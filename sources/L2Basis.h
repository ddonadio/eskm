#ifndef L2BASIS_H
#define L2BASIS_H

#include "Box.h"
#include "Basis.h"
#include "IndexList.h"
#include "CenterList.h"
#include "L2ReservoirCells.h"

template <class StateType, class SystemDescriptionType>
class L2Basis : public Basis<StateType,SystemDescriptionType>
{
  protected:
    
    // cutoff distance for the basis
    double cutoff_;
    
    // translation symmetry for the elements of the system
    vector<vector<Vector3D<double> > >  part_translations_;
    vector<vector<Vector3D<double> > >  reservoir_translations_;
    
    // localization area of the parts
    vector<Box<double> > part_boxes_;
        
    // reservoir cell indices
    vector<L2ReservoirCells> reservoirs_cell_index_lists_;
    
  public:
  
    // pure virtual methods of the base class
    virtual void         constructBasis(SystemDescriptionType &system, bool verbose)=0;
    
    // virtual methods from the base class
    virtual vector<int>  getNeighborhoodPartList(int i_part);
    virtual vector<int>  getContactStateIndexList(void);
    virtual vector<int>  getContactStateIndexList(int i_part);
    virtual vector<int>  getNeighborhoodStateIndexList(int i_part);
    
    // specific methods
    double       cutoff(void) { return cutoff_; }
    vector<int>  getReservoirRepetitions(int ireservoir);
    vector<int>  getReservoirCellIndices(int ireservoir, int irep1, int irep2);
    vector<int>  getExtendedDefectIndices(void);
    
    // translation symmetry access
    vector<Vector3D<double> > getPartTranslations(int ipart);
    vector<Vector3D<double> > getReservoirTranslations(int ireservoir);
    
    // part boxes initialization
    void setPartBoxes(void);
    
    // constructor
    L2Basis(void) {}
    
    // destructor
    ~L2Basis(void) {}
};


// access the indices of the defect parts plus 2 first cells of each electrode (contact + one cell)
template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getExtendedDefectIndices(void)
{
  //
  // form index list of defect, including contacts
  //
  vector<int> index_list = this->getDefectIndices() ;
  //
  // add a cell for each reservoir
  //
  for ( int i_res=0 ; i_res<reservoirs_cell_index_lists_.size() ; i_res++ )
  {
    vector<int> tmp_list = reservoirs_cell_index_lists_[i_res].sliceIndexList(2);
    index_list.insert(index_list.end(),tmp_list.begin(),tmp_list.end());
  }
  //
  // return result
  //
  return index_list;
}


// get the indices of all the contacts
template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getContactStateIndexList(void)
{
  //
  // init index list 
  //
  vector<int> index_list;
  //
  // loop on the parts
  //
  for ( int ipart=0 ; ipart<this->nDefectParts() ; ipart++ )
  {
    //
    // if this part is a contact
    //
    if ( this->contactIndex_[ipart]>=0 )
    {
      //
      // add the contact indices to the list
      //
      vector<int> contact_indices=getContactStateIndexList(ipart);
      index_list = index_list || contact_indices;
    }
  }
  //
  // return the list
  //
  return index_list;
}


template <class StateType, class SystemDescriptionType>
vector<Vector3D<double> > L2Basis<StateType,SystemDescriptionType>::getPartTranslations(int ipart)
{
  //
  // check reservoir index
  //
  if ( ipart>=part_translations_.size() )
  {
    cout << "Error : unknown reservoir number " << ipart << " in - L2Basis::getReservoirRepetitions -" << endl;
    exit(0);
  }
  //
  // return translations
  //
  return part_translations_[ipart];
}  

template <class StateType, class SystemDescriptionType>
vector<Vector3D<double> > L2Basis<StateType,SystemDescriptionType>::getReservoirTranslations(int ireservoir)
{
  //
  // check reservoir index
  //
  if ( ireservoir>=reservoir_translations_.size() )
  {
    cout << "Error : unknown reservoir number " << ireservoir << " in - L2Basis::getReservoirRepetitions -" << endl;
    exit(0);
  }
  //
  // return translations
  //
  return reservoir_translations_[ireservoir];
}  


template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getReservoirRepetitions(int ireservoir)
{
  //
  // check reservoir index
  //
  if ( ireservoir>=reservoirs_cell_index_lists_.size() )
  {
    cout << "Error : unknown reservoir number " << ireservoir << " in - L2Basis::getReservoirRepetitions -" << endl;
    exit(0);
  }
  //
  // construct list of repetitions
  //
  vector<int> list_rep(2);
  list_rep[0]=reservoirs_cell_index_lists_[ireservoir].nrep1();
  list_rep[1]=reservoirs_cell_index_lists_[ireservoir].nrep2();
  //
  // return result
  // 
  return list_rep;
}

template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getReservoirCellIndices(int ireservoir, int irep1, int irep2)
{
  //
  // check reservoir index
  //
  if ( ireservoir>=reservoirs_cell_index_lists_.size() )
  {
    cout << "Error : unknown reservoir number " << ireservoir << " in - L2Basis::getReservoirCellIndices -" << endl;
    exit(0);
  }
  //
  // return the corresponding indices
  //
  return reservoirs_cell_index_lists_[ireservoir].repIndexList(irep1, irep2);
}

template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getContactStateIndexList(int i_part)
{
  //
  // get reference on the contact index list
  //
  vector<int> &contactIndex=this->contactIndex_;
  //
  // check that the contact list has been set
  //
  if ( contactIndex.size()!=this->defectPart_.size() )
  {
    cout << "Error: contactIndex_ list hasn't been properly set, detected in - L2Basis::getContactStateIndexList -" << endl;
    exit(0);
  }
  //
  // check that the part index is correct
  //
  if ( i_part>=contactIndex.size() ) 
  {
    cout << "Error: unknown part index " << i_part << " in - L2Basis::getContactStateIndexList -" << endl;
    exit(0);
  }
  //
  // get index of the corresponding reservoir
  //
  int ireservoir=contactIndex[i_part];
  //
  // fast return if possible
  //
  if ( ireservoir<0 )
  {
    //
    // create an empty list
    //
    vector<int> empty_list;
    //
    // return the empty list
    //
    return empty_list;
  }
  //
  // get indices for this part
  //
  vector<int> part_indices=this->getDefectPartIndices(i_part);
  //
  // get repetition for this reservoir
  //
  int nrep1=reservoirs_cell_index_lists_[ireservoir].nrep1();
  int nrep2=reservoirs_cell_index_lists_[ireservoir].nrep2();
  //
  // init index list
  //
  vector<int> index_list;
  //
  // loop on the repetition of the cell
  //
  for ( int irep1=0 ; irep1<nrep1 ; irep1++ )
  for ( int irep2=0 ; irep2<nrep2 ; irep2++ )
  {
    //
    // get cell indices for the first slice
    //
    vector<int> cell_indices=reservoirs_cell_index_lists_[ireservoir].slice1RepIndexList(irep1, irep2);
    //
    // intersect this repetition with the part
    //
    vector<int> intersection=cell_indices&&part_indices;
    //
    // if the intersection is non-zero
    //
    if ( intersection.size()>0 )
    {
      //
      // extract indices from this first slice
      //
      vector<int> local_indices=intersection<=cell_indices;
      //
      // get cell indices for the second slice
      //
      vector<int> cell_indices_2=reservoirs_cell_index_lists_[ireservoir].slice2RepIndexList(irep1, irep2);
      //
      // intersect with the indices of the part
      //
      vector<int> intersection_2=cell_indices_2&&part_indices;
      //
      // check that the intersection is empty
      //
      if ( intersection_2.size()!=0 )
      {
        cout << "Error: states of the second slice of an electrode cannot be included in a part, detected in - L2Basis::getContactStateIndexList -" << endl;
        exit(0);
      }
      //
      // add indices of intersection with the first slice
      //
      index_list.insert(index_list.end(),intersection.begin(),intersection.end());
      //
      // add corresponding indices of the second slice
      //
      for ( int i=0 ; i<local_indices.size() ; i++ ) index_list.push_back(cell_indices_2[local_indices[i]]);
    }
  }
  //
  // return the list of indices
  //
  return index_list;
}

template <class StateType, class SystemDescriptionType>
void L2Basis<StateType,SystemDescriptionType>::setPartBoxes(void)
{
  //
  // allocate memory for the boxes
  //
  part_boxes_.resize(this->nDefectParts());
  //
  // loop on the parts
  //
  for ( int i_part=0 ; i_part<this->nDefectParts() ; i_part++ )
  {
    //
    // gte translations for this part
    //
    Vector3D<double> T1;
    Vector3D<double> T2;
    if ( part_translations_[i_part].size()>=1 ) T1=part_translations_[i_part][0];
    if ( part_translations_[i_part].size()==2 ) T2=part_translations_[i_part][1];
    //
    // get the states list for this part 
    //
    vector<StateType>  part_states=this->getDefectPartStates(i_part);
    //
    // form the list of centers for this part
    //
    vector<Vector3D<double> > part_centers;
    for ( int istate=0 , nstates=part_states.size() ; istate<nstates ; istate++ ) 
    {
      part_centers.push_back(part_states[istate].center());
    }
    //
    // init the box with the corresponding centers
    //
    part_boxes_[i_part].setFromPoints(part_centers,T1,T2);
  }
}

template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getNeighborhoodPartList(int i_part)
{
  //
  // get translation for this part
  //
  vector<Vector3D<double> > T;
  if ( part_translations_[i_part].size()>=1 ) T.push_back(part_translations_[i_part][0]);
  if ( part_translations_[i_part].size()==2 ) T.push_back(part_translations_[i_part][1]);
  //
  // get the states for this part
  // 
  vector<StateType> part_i_states=this->getDefectPartStates(i_part);
  //
  // form the list of centers for this part
  //
  vector<Vector3D<double> > part_i_centers;
  for ( int istate=0 , nstates=part_i_states.size() ; istate<nstates ; istate++ ) 
  {
    part_i_centers.push_back(part_i_states[istate].center());
  }
  //
  // init list of neighbor parts
  //
  vector<int> part_list; 
  //
  // loop on the list of parts 
  //
  for ( int j_part=0 ; j_part<this->nDefectParts() ; j_part++ )
  {
    //
    // determine translation symmetries
    // if both parts have translations, 
    // make sure they are the same
    //
    Vector3D<double> T1;
    Vector3D<double> T2;
    bool same_translations=true;
    //
    if ( T.size()==0 )
    {
      //
      // set translation vectors as the ones of part j
      //
      if ( part_translations_[j_part].size()>=1 ) T1=part_translations_[j_part][0];
      if ( part_translations_[j_part].size()==2 ) T2=part_translations_[j_part][1];
    }
    else 
    {
      same_translations = part_translations_[j_part].size()==T.size() || part_translations_[j_part].size()==0;
      //
      // set translation vectors as the ones of part i
      //
      if ( T.size()>0 ) T1=T[0];
      if ( T.size()>1 ) T2=T[1];
      //
      // make sure translations of part j are the same
      //
      for ( int i=0 ; i<part_translations_[j_part].size() && same_translations ; i++ )
      {
        // 
        // look if the vector is already in the list
        //
        bool same_vector=false;
        for ( int j=0 ; j<T.size() ; j++ ) if ( part_translations_[j_part][i]==T[j] ) same_vector=true;
        //
        // turn on the flag if the symetries differs 
        //
        if ( !same_vector ) same_translations=false;
      }
    }
    //
    // if we found corresponding translations
    //
    if ( same_translations )
    {
      //
      // check if the part boxes intersect
      //
      if ( part_boxes_[i_part].interact(part_boxes_[j_part],cutoff_) )
      {
        //
        // get the states list for this part 
        //
        vector<StateType>  part_j_states=this->getDefectPartStates(j_part);
        //
        // form the list of centers for this part
        //
        vector<Vector3D<double> > part_j_centers;
        for ( int istate=0 , nstates=part_j_states.size() ; istate<nstates ; istate++ ) 
        {
          part_j_centers.push_back(part_j_states[istate].center());
        }
        //
        // if the two parts are neighbor
        //
        if ( areNeighbor(part_i_centers, part_j_centers, cutoff_, T1, T2) )
        {
          //
          // add index to the neighboring part list
          //
          part_list.push_back(j_part);
        }
      }
    }
  }
  //
  // return index list
  //
  return part_list;
}

template <class StateType, class SystemDescriptionType>
vector<int> L2Basis<StateType,SystemDescriptionType>::getNeighborhoodStateIndexList(int i_part)
{
  //
  // start by getting the neighboring parts
  //
  vector<int> part_list=getNeighborhoodPartList(i_part);
  //
  // init index list
  //
  vector<int> index_list;
  //
  // loop on the neighbors
  //
  for ( int i_neighbor=0 ; i_neighbor<part_list.size() ; i_neighbor++ )
  {
    //
    // get index of this part
    //
    int j_part=part_list[i_neighbor];
    //
    // init list of indices for this part
    //
    vector<int> part_index_list;
    //
    // fill the index list depending on the case
    // if both parts belongs to the same contact
    //
    if ( this->contactIndex_[i_part]>=0 && this->contactIndex_[j_part]>=0 && this->contactIndex_[i_part]==this->contactIndex_[j_part] )
    {
      //
      // get indices of the contact
      //
      part_index_list=getContactStateIndexList(j_part);
    }
    //
    // if one of the part is a regular one
    //
    else
    {
      //
      // get regular part indices
      //
      part_index_list=this->getDefectPartIndices(j_part);
    }
    //
    // add those indices to the list
    //
    index_list.insert(index_list.end(),part_index_list.begin(),part_index_list.end());
  }
  //
  // return the list of indices
  //
  return index_list;
}

#endif

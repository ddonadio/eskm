#ifndef QRFACTORIZATION_H
#define QRFACTORIZATION_H

#include <vector>
#include <iostream>
#include "Context.h"
#include "Lapack.h"
#include "IndexList.h"
#include "Communication.h"

#ifndef GETSIZE_H
#define GETSIZE_H

int getsize(float a) { return (int)a; }
int getsize(double a) { return (int)a; }
int getsize(complex<float> a) { return (int)a.real(); }
int getsize(complex<double> a) { return (int)a.real(); }

#endif

#define TIMING

// ************************************************
// Transposition character for lapack

template <class Type>
char lapackTransposeChar(void)
{
  cout << "Error: unknown Type in lapackTransposeChar()" << endl;
  exit(0);
}

template <>
char lapackTransposeChar<float>(void) { return 'T'; }

template <>
char lapackTransposeChar<double>(void) { return 'T'; }

template <>
char lapackTransposeChar<complex<float> >(void) { return 'C'; }

template <>
char lapackTransposeChar<complex<double> >(void) { return 'C'; }

//
// ************************************************

// timed
template <class Type>
void QRFactorization( int my_proc, vector<int> list_working_proc, vector<int> list_listening_proc, Context &context, Type *W, int LDW, vector<int> &NWloc, Type *Q, int LDQ, int nQloc, TimerMap &tmap ,string tmap_comment)
{
#ifdef TIMING
  tmap[tmap_comment+" total time"].start();
#endif
  //
  // verify that listening list contains working list
  //
  if ( ( list_listening_proc || list_working_proc ) != list_listening_proc )
  {
    cout << "Error: list_working_proc is not totally included in list_listening_proc in QRFactorization" << endl;
    exit(0);
  }
  //
  // fast exit if possible
  //
  {
    int i;
    for ( i=0 ; i<list_listening_proc.size() ; i++ ) if ( my_proc==list_listening_proc[i] ) break;
    //
    // return if the proc is not part of the listening list
    //
    if ( i>=list_listening_proc.size() ) 
    {
#ifdef TIMING
      tmap[tmap_comment+" total time"].stop();
#endif    
      return;
    }
  }
  //
  // verify sizes
  //
  if ( NWloc.size()!=list_working_proc.size() ) 
  {
    cout << "Error: NWloc.size()!=list_working_proc.size() in QRFactorization" << endl;
    exit(0);
  }
  //
  // determine wether or not this proc is working and the local dimension of W
  //
  int nWloc=0;
  bool applyTransfo=false;
  //
  for ( int index_list_proc=0 ; index_list_proc<list_working_proc.size()  ; index_list_proc++ ) 
  {
    if ( list_working_proc[index_list_proc]==my_proc ) 
    { 
      applyTransfo=true; 
      nWloc=NWloc[index_list_proc];
      break; 
    }    
  }
  //
  // allocate buffer
  //
  vector<vector<Type> > BUFF(list_working_proc.size());
  //
  // work array for lapack
  //
  vector<Type> WORK(1);
  //
  // index for starting_row in QR decomposition
  //
  int i_row_start=0;
  //
  // loop on the working processors
  //
  for ( int index_list_proc=0 ; index_list_proc<list_working_proc.size()  ; index_list_proc++ )
  {
    //
    // get the corresponding processor index
    //
    int i_proc=list_working_proc[index_list_proc];
    //
    // parameters for lapack
    //
    int M=LDW-i_row_start;
    int N=NWloc[index_list_proc];
    int K=min(M,N);
    //
    // allocate WBUFF
    //
    BUFF[index_list_proc].resize(M*N+K);
    //
    // if this proc is the current
    //
    if ( my_proc==i_proc )
    {
#ifdef TIMING
      tmap[tmap_comment+" geqrf"].start();
#endif
      //
      // work arrays for lapack 
      //
      vector<Type> TAU(K);
      int LWORK;
      int INFO;
      //
      // memory need query
      //
      LWORK=-1;
      lapack::geqrf<Type>( &M, &N, &W[i_row_start], &LDW, &TAU[0], &WORK[0], &LWORK, &INFO );
      //
      // allocate memory
      //
      WORK.resize(getsize(WORK[0]));
      LWORK=WORK.size();
      //
      // compute factorisation
      //
      lapack::geqrf<Type>( &M, &N, &W[i_row_start], &LDW, &TAU[0], &WORK[0], &LWORK, &INFO );
#ifdef TIMING
      tmap[tmap_comment+" geqrf"].stop();
#endif
      //
      // copy the content of W in BUFF
      //
#ifdef TIMING
      tmap[tmap_comment+" buffering"].start();
#endif
      for ( int i_col=0 ; i_col<N ; i_col++ )
      {
        Type *p_source=&W[i_row_start+i_col*LDW];
        Type *p_destination=&BUFF[index_list_proc][i_col*M];
        for ( int j=0 ; j<M ; j++ ) p_destination[j]=p_source[j];
      }
      //
      // add pivots
      //
      Type *p_destination=&BUFF[index_list_proc][M*N];
      for ( int i=0 ; i<K ; i++ ) p_destination[i]=TAU[i];
#ifdef TIMING
      tmap[tmap_comment+" buffering"].stop();
#endif
#ifdef WITH_MPI
      //
      // send the transfo
      //
      communication::broadcast<Type>( &BUFF[index_list_proc][0], BUFF[index_list_proc].size(), i_proc, list_listening_proc, context, tmap, tmap_comment+" bcastw");
#endif
      //
      // no need to appli the transfo to W anymore
      //
      applyTransfo=false;
    }
    else
    {
#ifdef WITH_MPI
      //
      // receive the content of BUFF
      //
      communication::broadcast<Type>( &BUFF[index_list_proc][0], BUFF[index_list_proc].size(), i_proc, list_listening_proc, context, tmap, tmap_comment+" bcastl");
      //
      // apply transfo to W if necessary
      //
      if ( applyTransfo )
      {
#ifdef TIMING
        tmap[tmap_comment+" mqr 1"].start();
#endif
        //
        // variables for lapack interface
        //
        int LWORK;
        int INFO;
        char SIDE='L';
        char TRANS=lapackTransposeChar<Type>(); 
        //
        // memory needs query
        //
        LWORK=-1;
        lapack::mqr<Type>( &SIDE, &TRANS, &M, &nWloc, &K, &BUFF[index_list_proc][0], &M, &BUFF[index_list_proc][M*N], &W[i_row_start], &LDW, &WORK[0], &LWORK, &INFO );
        //
        // reallocate memory if more space is needed
        //
        if ( getsize(WORK[0]) > WORK.size() ) WORK.resize(getsize(WORK[0]));
        LWORK=WORK.size();
        //
        // apply transfo
        //
        lapack::mqr<Type>( &SIDE, &TRANS, &M, &nWloc, &K, &BUFF[index_list_proc][0], &M, &BUFF[index_list_proc][M*N], &W[i_row_start], &LDW, &WORK[0], &LWORK, &INFO );
#ifdef TIMING
        tmap[tmap_comment+" mqr 1"].stop();
#endif
      }
#else
      //
      // should never reach this point
      //
      cout << "Error: reach a point not suposed to in serial build of QRFactorization" << endl;
      exit(0);
#endif
    }
    //
    // increment i_row_start
    // 
    i_row_start+=NWloc[index_list_proc];
  }
  //
  // apply transfo to Q from the left 
  //
#ifdef TIMING
  tmap[tmap_comment+" mqr 2"].start();
#endif
  for ( int index_list_proc=list_working_proc.size()-1 ; index_list_proc>=0  ; index_list_proc-- )
  {
    //
    // decrement i_row_start
    //
    i_row_start-=NWloc[index_list_proc];
    //
    // parameters for lapack
    //
    int M=LDW-i_row_start;
    int N=NWloc[index_list_proc];
    int K=min(M,N);
    int LWORK;
    int INFO;
    char SIDE='L';
    char TRANS='N'; 
    //
    // memory needs query
    //
    LWORK=-1;
    lapack::mqr<Type>( &SIDE, &TRANS, &M, &nQloc, &K, &BUFF[index_list_proc][0], &M, &BUFF[index_list_proc][M*N], &Q[i_row_start], &LDQ, &WORK[0], &LWORK, &INFO );
    //
    // reallocate memory if more space is needed
    //
    if ( getsize(WORK[0]) > WORK.size() ) WORK.resize(getsize(WORK[0]));
    LWORK=WORK.size();
    //
    // apply transfo
    //
    lapack::mqr<Type>( &SIDE, &TRANS, &M, &nQloc, &K, &BUFF[index_list_proc][0], &M, &BUFF[index_list_proc][M*N], &Q[i_row_start], &LDQ, &WORK[0], &LWORK, &INFO );
  }
#ifdef TIMING
  tmap[tmap_comment+" mqr 2"].stop();
#endif
#ifdef TIMING
  tmap[tmap_comment+" total time"].stop();
#endif
}

// untimed
template <class Type>
void QRFactorization( int my_proc, vector<int> list_working_proc, vector<int> list_listening_proc, Context &context, Type *W, int LDW, vector<int> &NWloc, Type *Q, int LDQ, int nQloc)
{
  //
  // unused tmap and comments
  //
  TimerMap tmap;
  string tmap_comment;
  //
  // apply QR
  //
  QRFactorization(my_proc,list_working_proc,list_listening_proc,context,W,LDW,NWloc,Q,LDQ,nQloc,tmap,tmap_comment);
}

//
// remove local definitions
//
#ifdef TIMING
  #undef TIMING
#endif

#endif

#ifndef L2PART_H
#define L2PART_H

#include "Part.h"
#include "Subspace.h"
#include "MatrixPencil.h"

template <class SolutionType, class StateType>
class L2Part : public Part<SolutionType>
{
  protected:
  
    // suboperator object describing the projected hamiltonian
    MatrixPencil<SolutionType> *matrix_pencil_;
    
  public:
    
    // print matrix pencil
    void printPencil(void) { matrix_pencil_->print("part matrix pencil"); }
    vector<int> range(void) { return matrix_pencil_->range(); }
    vector<int> domain(void) { return matrix_pencil_->domain(); }
    MatrixPencil<SolutionType>* matrixPencilPtr(void) { return matrix_pencil_; }
    
    // access the local solution for a given energy 
    virtual Subspace<SolutionType>* getLocalSolution(vector<int> &index_list, double energy);
    
    // allow to check a local solution for a given energy
    template <class SubspaceType>    
    void checkLocalSolution(Subspace<SubspaceType> &subspace, double energy, double threshold=0); 
    
    // constructor
    template <class BasisType>
    L2Part(BasisType &basis, Context &context, int ipart);
    
    // check that everything if fine before running
    virtual bool check(void)=0;
    
    // destructor
    ~L2Part(){}
};

//
// class template specialization for types
//
//    float
//    double
//    complex<float>
//    complex<double>
//

// float class template specialization
template <class StateType>
class L2Part<float,StateType> : public Part<float>
{
  protected:
  
    // suboperator object describing the projected hamiltonian
    MatrixPencil<float> *matrix_pencil_;
    
  public:
    
    // print matrix pencil
    void printPencil(void) { matrix_pencil_->print("part matrix pencil"); }
    vector<int> range(void) { return matrix_pencil_->range(); }
    vector<int> domain(void) { return matrix_pencil_->domain(); }
    MatrixPencil<float>* matrixPencilPtr(void) { return matrix_pencil_; }
    
    // access the local solution for a given energy 
    virtual Subspace<float>* getLocalSolution(vector<int> &index_list, double energy);
    
    // allow to check a local solution for a given energy
    virtual void checkLocalSolution(Subspace<float> &subspace, double energy, double threshold=0); 
    virtual void checkLocalSolution(Subspace<complex<float> > &subspace, double energy, double threshold=0); 
    
    // constructor
    template <class BasisType>
    L2Part(BasisType &basis, Context &context, int ipart);
    
    // destructor
    ~L2Part(){ delete matrix_pencil_; }
};

// complex float class template specialization
template <class StateType>
class L2Part<complex<float>,StateType> : public Part<complex<float> >
{
  protected:
  
    // suboperator object describing the projected hamiltonian
    MatrixPencil<complex<float> > *matrix_pencil_;
    
  public:
    
    // print matrix pencil
    void printPencil(void) { matrix_pencil_->print("part matrix pencil"); }
    vector<int> range(void) { return matrix_pencil_->range(); }
    vector<int> domain(void) { return matrix_pencil_->domain(); }
    MatrixPencil<complex<float> >* matrixPencilPtr(void) { return matrix_pencil_; }
    
    // access the local solution for a given energy 
    virtual Subspace<complex<float> >* getLocalSolution(vector<int> &index_list, double energy);
    
    // allow to check a local solution for a given energy
    virtual void checkLocalSolution(Subspace<float> &subspace, double energy, double threshold=0); 
    virtual void checkLocalSolution(Subspace<complex<float> > &subspace, double energy, double threshold=0); 
    
    // constructor
    template <class BasisType>
    L2Part(BasisType &basis, Context &context, int ipart);
    
    // destructor
    ~L2Part(){ delete matrix_pencil_; }
};

// double class template specialization
template <class StateType>
class L2Part<double,StateType> : public Part<double>
{
  protected: 
  
    // suboperator object describing the projected hamiltonian
    MatrixPencil<double> *matrix_pencil_;
    
  public:
    
    // print matrix pencil
    void printPencil(void) { matrix_pencil_->print("part matrix pencil"); }
    vector<int> range(void) { return matrix_pencil_->range(); }
    vector<int> domain(void) { return matrix_pencil_->domain(); }
    MatrixPencil<double>* matrixPencilPtr(void) { return matrix_pencil_; }
    
    // access the local solution for a given energy 
    virtual Subspace<double>* getLocalSolution(vector<int> &index_list, double energy);
    
    // allow to check a local solution for a given energy
    virtual void checkLocalSolution(Subspace<double> &subspace, double energy, double threshold=0); 
    virtual void checkLocalSolution(Subspace<complex<double> > &subspace, double energy, double threshold=0); 
    
    // constructor
    template <class BasisType>
    L2Part(BasisType &basis, Context &context, int ipart);
    
    // destructor
    ~L2Part(){ delete matrix_pencil_; }
};

// complex double class template specialization
template <class StateType>
class L2Part<complex<double>,StateType> : public Part<complex<double> >
{
  protected:
  
    // suboperator object describing the projected hamiltonian
    MatrixPencil<complex<double> > *matrix_pencil_;
    
  public:
    
    // matrix pencil access
    void printPencil(void) { matrix_pencil_->print("part matrix pencil"); }
    vector<int> range(void) { return matrix_pencil_->range(); }
    vector<int> domain(void) { return matrix_pencil_->domain(); }
    MatrixPencil<complex<double> >* matrixPencilPtr(void) { return matrix_pencil_; }
    
    // access the local solution for a given energy 
    virtual Subspace<complex<double> >* getLocalSolution(vector<int> &index_list, double energy);
    
    // allow to check a local solution for a given energy
    virtual void checkLocalSolution(Subspace<double> &subspace, double energy, double threshold=0); 
    virtual void checkLocalSolution(Subspace<complex<double> > &subspace, double energy, double threshold=0); 
    
    // constructor
    template <class BasisType>
    L2Part(BasisType &basis, Context &context, int ipart);
    
    // destructor
    ~L2Part(){ delete matrix_pencil_; }
};

// constructor
template <class SolutionType, class StateType>
template <class BasisType>
L2Part<SolutionType,StateType>::L2Part(BasisType &basis, Context &context, int ipart) :
Part<SolutionType>(basis,context,ipart)
{
  //
  // get the domain and range of the sub-operator associated to this part
  //
  vector<int> range=basis.getDefectPartIndices(ipart);
  vector<int> domain=basis.getNeighborhoodStateIndexList(ipart);
  //
  // create the suboperator object for this part
  //
  matrix_pencil_=new MatrixPencil<SolutionType>(range,domain,context,basis.hasOverlaps());
}

// constructor for float
template <class StateType>
template <class BasisType>
L2Part<float,StateType>::L2Part(BasisType &basis, Context &context, int ipart) :
Part<float>(basis,context,ipart)
{
  //
  // get the domain and range of the sub-operator associated to this part
  //
  vector<int> range=basis.getDefectPartIndices(ipart);
  vector<int> domain=basis.getNeighborhoodStateIndexList(ipart);
  //
  // create the suboperator object for this part
  //
  matrix_pencil_=new MatrixPencil<float>(range,domain,context,basis.hasOverlaps());
}

// constructor for double
template <class StateType>
template <class BasisType>
L2Part<double,StateType>::L2Part(BasisType &basis, Context &context, int ipart) :
Part<double>(basis,context,ipart)
{
  //
  // get the domain and range of the sub-operator associated to this part
  //
  vector<int> range=basis.getDefectPartIndices(ipart);
  vector<int> domain=basis.getNeighborhoodStateIndexList(ipart);
  //
  // create the suboperator object for this part
  //
  matrix_pencil_=new MatrixPencil<double>(range,domain,context,basis.hasOverlaps());
}

// constructor for complex float
template <class StateType>
template <class BasisType>
L2Part<complex<float>,StateType>::L2Part(BasisType &basis, Context &context, int ipart) :
Part<complex<float> >(basis,context,ipart)
{
  //
  // get the domain and range of the sub-operator associated to this part
  //
  vector<int> range=basis.getDefectPartIndices(ipart);
  vector<int> domain=basis.getNeighborhoodStateIndexList(ipart);
  //
  // create the suboperator object for this part
  //
  matrix_pencil_=new MatrixPencil<complex<float> >(range,domain,context,basis.hasOverlaps());
}

// constructor for complex double
template <class StateType>
template <class BasisType>
L2Part<complex<double>,StateType>::L2Part(BasisType &basis, Context &context, int ipart) :
Part<complex<double> >(basis,context,ipart)
{
  //
  // get the domain and range of the sub-operator associated to this part
  //
  vector<int> range=basis.getDefectPartIndices(ipart);
  vector<int> domain=basis.getNeighborhoodStateIndexList(ipart);
  //
  // create the suboperator object for this part
  //
  matrix_pencil_=new MatrixPencil<complex<double> >(range,domain,context,basis.hasOverlaps());
}

//
// solution access
//

// solution access for float
template <class StateType>
Subspace<float>* L2Part<float,StateType>::getLocalSolution(vector<int> &index_list, double energy)
{
  // start kernel finding
  matrix_pencil_->startKernel(energy);
  // return the result only for the requested indices
  return matrix_pencil_->endKernel(index_list);
}

// solution access for double
template <class StateType>
Subspace<double>* L2Part<double,StateType>::getLocalSolution(vector<int> &index_list, double energy)
{
  // start kernel finding
  matrix_pencil_->startKernel(energy);
  // return the result only for the requested indices
  return matrix_pencil_->endKernel(index_list);
}

// solution access for complex float
template <class StateType>
Subspace<complex<float> >* L2Part<complex<float>,StateType>::getLocalSolution(vector<int> &index_list, double energy)
{
  // start kernel finding
  matrix_pencil_->startKernel(energy);
  // return the result only for the requested indices
  return matrix_pencil_->endKernel(index_list);
}

// solution access for complex double
template <class StateType>
Subspace<complex<double> >* L2Part<complex<double>,StateType>::getLocalSolution(vector<int> &index_list, double energy)
{
  // start kernel finding
  matrix_pencil_->startKernel(energy);
  // return the result only for the requested indices
  return matrix_pencil_->endKernel(index_list);
}

//
// subspace verification templates for type float
//

// solution verification for real subspaces
template <class StateType>
void L2Part<float,StateType>::checkLocalSolution(Subspace<float> &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  double e=energy;
  Subspace<float>* projection = (Subspace<float> *)matrix_pencil_->projectSubspace<float,float>(subspace, e);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

// solution verification for complex subspaces
template <class StateType>
void L2Part<float,StateType>::checkLocalSolution(Subspace<complex<float> > &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<complex<float> >* projection = matrix_pencil_->projectSubspace<complex<float>,complex<float> >(subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

//
// end of float specialization
//

//
// subspace verification templates for type complex<float>
//

// solution verification for real subspaces
template <class StateType>
void L2Part<complex<float>,StateType>::checkLocalSolution(Subspace<float> &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<complex<float> > *projection;
  projection = (*matrix_pencil_).projectSubspace<complex<float>,float>(subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

// solution verification for complex subspaces
template <class StateType>
void L2Part<complex<float>,StateType>::checkLocalSolution(Subspace<complex<float> > &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<complex<float> >* projection = matrix_pencil_->projectSubspace<complex<float>,complex<float> >(subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

//
// end of complex<float> specialization
//

//
// subspace verification templates for type double
//

// solution verification for real subspaces
template <class StateType>
void L2Part<double,StateType>::checkLocalSolution(Subspace<double> &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<double>* projection = matrix_pencil_->projectSubspace<double,double>(subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

// solution verification for complex subspaces
template <class StateType>
void L2Part<double,StateType>::checkLocalSolution(Subspace<complex<double> > &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<complex<double> >* projection = matrix_pencil_->projectSubspace<complex<double>,complex<double> >(subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

//
// end of double specialization
//

//
// subspace verification templates for type complex<double>
//

// solution verification for real subspaces
template <class StateType>
void L2Part<complex<double>,StateType>::checkLocalSolution(Subspace<double> &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<complex<double> >* projection = (Subspace<complex<double> >*)matrix_pencil_->projectSubspace<complex<double>,double>((Subspace<double> &)subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

// solution verification for complex subspaces
template <class StateType>
void L2Part<complex<double>,StateType>::checkLocalSolution(Subspace<complex<double> > &subspace, double energy, double threshold)
{
  //
  // project the subspace onto the reduced pencil for the given energy
  //
  Subspace<complex<double> >* projection = matrix_pencil_->projectSubspace<complex<double>,complex<double> >(subspace, energy);
  //
  // if a threshold has been set
  //
  if ( threshold>0 )
  {
    //
    // test the max absolute value of projection coefficients
    //
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( max_abs_coeff > threshold ) 
    {
      cout << "Error: found residual error in - L2Part::checkLocalSolution - of value " << scientific << max_abs_coeff << " > " << threshold << endl;
      delete projection;
      exit(0);
    }
  }
  //
  // otherwise, just display the error
  //
  else
  {
    // 
    // find the max absolute value or the projections
    // 
    double max_abs_coeff=projection->getMaxAbsoluteCoeff();
    if ( context_.myProc()==0 ) cout << "L2Part::checkLocalSolution - found residual error of " << scientific << max_abs_coeff << endl;
    delete projection;
  }
}

//
// end of complex<double> specialization
//

#endif

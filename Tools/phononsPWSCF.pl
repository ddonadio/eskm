#!/usr/bin/perl 
#
# Create a force constant matrix for eskm using pw.x
# 

$dx = 0.02;
$conversion = -13.6058  * 96.485336521 * 100. / 0.529177;
$mass1 = 12.011;
$mass2 = 1.044;

$froot=$ARGV[0];
$findif = "$froot.dyn";
$posf = "$froot.xyz";
open (FND,">$findif");

# Read position file
open (POS,"<$posf");
$natoms=<POS>;
$natmove=$natoms;
$nmod = $natmove*3;
<POS>;
for($i=0;$i<$natoms;$i++){
    $xyz=<POS>;
    @aa =split(' ',$xyz);
    $s[$i]= $aa[0];
    $x[3*$i+1]=$aa[1];
    $x[3*$i+2]=$aa[2];
    $x[3*$i+3]=$aa[3];
}
# read the cell
<POS>;
<POS>;
$xyz=<POS>;
@cellx =split(' ',$xyz);
$xyz=<POS>;
@celly =split(' ',$xyz);
$cellz[0]=0.;
$cellz[1]=0.;
$cellz[2]=10.;

# Loop over degrees of freedom ($nmod) to perform finite displacements
for $i (1..$nmod) {
    $jat = int( ($i-1)/3 );
    if($s[$jat]=="C"){
      $sqrtmassj = sqrt($mass1);
    }
    else{
      $sqrtmassj = sqrt($mass2);
    }
    for $bb (-1..1){
       for $k (1..$nmod) {
         $xx[$k]= $x[$k];
       } 
       if($bb!=0){
         $fin = "scf$i.$bb.in";
         $fout = "scf.$i.$bb.out";
       
         $xx[$i]= $x[$i]+$bb*$dx;
         setinputfile();

#        system "mpirun -np 8 /people/thnfs/homes/donadio/Programs/espresso-5.0.1/bin/pw.x  < $fin > $fout\n";
         readoutputfile();
       }
    }
}

#############################################################################
#############################################################################
sub setinputfile {
    
    print STDOUT "$fin  ";
    open(FIN,">$fin");

print FIN <<ENDOFMOD;
 &CONTROL
    calculation='scf',
    restart_mode='from_scratch',
    prefix='$froot'
    pseudo_dir = '/people/thnfs/homes/donadio/Programs/espresso-4.2.1/pseudo/',
    outdir='./tmp/'
    tprnfor = .true.
    wf_collect = .true.
 /
 &SYSTEM
  ibrav     = 0,
  ntyp      = 2,
  ecutwfc   = 35.d0,
  ecutrho =220.0,
  occupations='smearing',
  degauss=0.02,
  smearing='mp',
  nat       = $natoms
 /
 &ELECTRONS
    mixing_beta = 0.25,
    conv_thr =  1.0d-10,
    startingpot = 'file',
 /
ATOMIC_SPECIES
C     $mass1   C.pbe-rrkjus.UPF
H     $mass2   H.pbe-rrkjus_psl.0.1.UPF
CELL_PARAMETERS (angstrom)
$cellx[0]  $cellx[1] $cellx[2]
$celly[0]  $celly[1] $celly[2]
$cellz[0]  $cellz[1] $cellz[2]
K_POINTS GAMMA
ATOMIC_POSITIONS (angstrom)
ENDOFMOD

for($ii=0;$ii<$natoms;$ii++){
   print FIN "$s[$ii]  $xx[3*$ii+1] $xx[3*$ii+2] $xx[3*$ii+3] \n";
}    
    close(FIN);
    return;
#$cell1[0]  $cell1[1]  $cell1[2]
#$cell2[0]  $cell2[1]  $cell2[2]
#$cell3[0]  $cell3[1]  $cell3[2]

} # End Subroutine setinputfile

#############################################################################
sub readoutputfile {
    system "grep force $fout |grep atom  > forces.$bb \n";
    if ($bb==1){
      $forcn="forces.-1";
      $forcp="forces.1";
      open(FN,"<$forcn");
      open(FP,"<$forcp");
      $iat = 0;
      while(<FN>){
         $iat++;
         @a=split(' ');
         $fnx[$iat] = $a[6];
         $fny[$iat] = $a[7];
         $fnz[$iat] = $a[8];
      }
      $iat=0;
      while(<FP>){
         $iat++;
         @a=split(' ');
         $fpx[$iat] = $a[6];
         $fpy[$iat] = $a[7];
         $fpz[$iat] = $a[8];
      }
      close(FN);
      close(FP);
      for $iat (1..$natoms){
         if($s[$iat]=="C"){
           $sqrtmassi = sqrt($mass1);
         }
         else{
           $sqrtmassi = sqrt($mass2);
         }
         $fknx = ($fpx[$iat] - $fnx[$iat])/$dx/2 * $conversion /$sqrtmassi/$sqrtmassj;
         $fkny = ($fpy[$iat] - $fny[$iat])/$dx/2 * $conversion /$sqrtmassi/$sqrtmassj; 
         $fknz = ($fpz[$iat] - $fnz[$iat])/$dx/2 * $conversion /$sqrtmassi/$sqrtmassj;
         print STDOUT "$fknx   $fkny   $fknz    $iat\n";
         if($iat>($natoms-$natmove)) {
            print FND "$fknx $fkny $fknz  \n";
         }
      }

    }
} # End Subrutine readoutputfile


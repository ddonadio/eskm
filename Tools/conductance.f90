 program getconduct

 implicit none

 integer iargc, i, nbid, itempmax, ideltatemp, it
 real(8) :: temperature, tempmax, deltatemp
 real(8) :: expbose, betaomega, integrand, conductance, deltaomega
 real(8), allocatable :: transmission(:), omega(:)
 real(8), PARAMETER :: cnv = 47.992374d0  !conversion THz --> Kelvin
 real(8), PARAMETER :: kboltz = 0.013806504 !gives output in nW/K
 character(len=100) :: ftrans, buffer

 if (iargc()<3) stop "Use: conductance.x file tempmax dtemp"
 CALL GETARG(1,ftrans)
 CALL GETARG(2,buffer)
 READ(buffer,*) tempmax
 CALL GETARG(3,buffer)
 READ(buffer,*) deltatemp

 open(unit=19, file=ftrans)
 i= 0

 do while(.true.)
   read(19,*,end=100) deltaomega
   i=i+1
 enddo

 100 continue
 
 nbid = i
 allocate(transmission(nbid),omega(nbid))

 rewind(19)
 do i = 1, nbid
   read(19,*) omega(i), transmission(i)
 enddo 

 itempmax = int(tempmax)
 ideltatemp = int(deltatemp)
 do it = ideltatemp, itempmax, ideltatemp
   temperature = dble(it)
   conductance = 0.d0
   do i = 1, nbid
     if (i>1) then
       deltaomega = omega(i)-omega(i-1)
     else
       deltaomega = omega(1)
     endif 
     betaomega = min(omega(i)*cnv/temperature , 250.) 
     expbose = exp( betaomega )
     integrand = expbose * betaomega**2 * transmission(i) / (expbose - 1.d0)**2
     !write(*,'(i5,3g16.8)') i, integrand, betaomega, expbose
     conductance = conductance + kboltz*integrand*deltaomega
   enddo
   write(*,*) temperature, conductance
 enddo

 

 end program getconduct

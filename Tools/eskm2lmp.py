#!/usr/bin/python 

import sys

# dictionary for atom labels: it is system dependent and has to be updated!
atomidx={"C":1 , "H":2 , "Si":3}

argv = sys.argv
if len(argv) != 2:
  print "Syntax: eskm2lmp.py in.lammps"
  sys.exit()

infile = sys.argv[1]+".xyz"
outfile = sys.argv[1] + ".lammps"

fout = open(outfile,'w')


with open(infile,'r') as f:
   numatoms = int(f.readline())
   empty = f.readline()
   for i in range (numatoms): 
      line = f.readline()
      atom = line.split()
      print >>fout, i+1, atomidx[atom[0]], atom[1], atom[2], atom[3]
      
   empty = f.readline()
   empty = f.readline()
   for i in range (3):
      line = f.readline()
      cell = line.split()
      print cell

f.closed

sys.exit()

#!/usr/bin/perl


$ThztoK = 47.992376;
$kboltz = 1.3806503e-2; # frequencies are in Thz, then power is in nW

if($#ARGV<2){
   print STDOUT "landauer.pl length ly lz < transmission.dat \n";
   exit();
}

system "grep T12 transmission_part_0 | cut -b9-24,27- > transmission.dat \n";

#$length = 135.75;   in nm!
$length = $ARGV[0];
$celly = $ARGV[1];
$cellz = $ARGV[2];
$area = $celly * $cellz;

$i=0;
$freqThz[0]=0.;
$trans[0]= 3.;

while(<STDIN>){
   $i++;
   @_=split(' ');
   $freqThz[$i]= $_[0];
   $trans[$i] = $_[1];
   #  $x[$i] = $freqThz[$i] * $ThztoK / $temperature;
   #  print STDOUT "$freqThz[$i]  $x[$i] \n";
}

$fcum = "cumulative300.dat";
open(CUM,">$fcum");

for ($temperature = 10; $temperature<4001; $temperature++){
   $conduct = 0.;
   for($j = 1;$j<$i;$j++){
      $x = $freqThz[$j] * $ThztoK / $temperature;
      $dnu = $freqThz[$j] - $freqThz[$j-1];
      $derivbose = $x**2 * exp($x)/(exp($x) - 1.)**2; 
      $conduct+= $dnu * $derivbose * ($trans[$j]+$trans[$j-1]) * $kboltz/2.;
      if($temperature==300){
          $c300 = $conduct/$area * $length;
          print CUM "$freqThz[$j] $c300 \n"; 
      }
   }
   $conductivity = $conduct/$area * $length;
#  length in nm, temperature in K and conductance in nW/K
   $condnorm = $conduct/$area;  # in GW/m^2 K
   print STDOUT "$temperature   $condnorm $conductivity\n";
}

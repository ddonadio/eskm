#!/usr/bin/perl

$filein = $ARGV[0];
for ($i=1;$i<=3;$i++) {
    $nrep[$i] = $ARGV[$i];
}
$concentration=$ARGV[4];
open (INP, "<$filein");

$keynum = <INP>;
$acell = <INP>;
@num = split(' ',$keynum);
@cell = split(' ',$acell);
@cella[0] = $cell[0];
@cella[1] = $cell[1];
@cella[2] = $cell[2];
@cellb[0] = $cell[3]; 
@cellb[1] = $cell[4]; 
@cellb[2] = $cell[5]; 
@cellc[0] = $cell[6];
@cellc[1] = $cell[7];
@cellc[2] = $cell[8];

for($k=0;$k<=2;$k++) {
    $ca[$k] = $cella[$k]*($nrep[1]);
    $cb[$k] = $cellb[$k]*($nrep[2]);
    $cc[$k] = $cellc[$k]*($nrep[3]);
}

$newnum = $num[0] * $nrep[1]*$nrep[2]*$nrep[3] ;
$nbase = $num[0];
print STDOUT "$newnum   vacancies: $concentration  \n";
print STDOUT "$ca[0] $ca[1] $ca[2]  ";
print STDOUT "$cb[0] $cb[1] $cb[2]  ";
print STDOUT "$cc[0] $cc[1] $cc[2]\n";
$idb = 0;
while (<INP>) {
  @atom = split(' ');
  $symb[$idb] = $atom[0];
  $x[$idb] = $atom[1]; $y[$idb] = $atom[2]; $z[$idb] = $atom[3];
  $idb++;
}
for($k1=0;$k1<($nrep[1]);$k1++) {
   for($k2=0;$k2<($nrep[2]);$k2++) {
      for($k3=0;$k3<($nrep[3]);$k3++) {
         for($idb=0;$idb<$nbase;$idb++){
              $sy = $symb[$idb];
              $xnew = $x[$idb] + $k1*$cella[0] + $k2*$cellb[0] + $k3*$cellc[0];
              $ynew = $y[$idb] + $k1*$cella[1] + $k2*$cellb[1] + $k3*$cellc[1];
              $znew = $z[$idb] + $k1*$cella[2] + $k2*$cellb[2] + $k3*$cellc[2];
             if($sy eq 'Si'){
                 $vac=rand();
                 if($vac>$concentration){
                    printf STDOUT "%-3s%16.8f%16.8f%16.8f\n", $sy, $xnew, $ynew, $znew;
                 }
             }
             else{
                printf STDOUT "%-3s%16.8f%16.8f%16.8f\n", $sy, $xnew, $ynew, $znew;
             }
          }
      }
   }
}


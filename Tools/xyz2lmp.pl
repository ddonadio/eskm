#!/usr/bin/perl

$filein = $ARGV[0];

open(FIN,"<$filein");
$_=<FIN>;
@i=split(' ');
$natms=$i[0];
$_=<FIN>;
@cell=split(' ');
print STDOUT "$natms atoms \n 1 atom types \n";
print STDOUT " 0.  $cell[0] xlo xhi \n";
print STDOUT " 0.  $cell[4] ylo yhi \n";
print STDOUT " 0.  $cell[8] zlo zhi \n";
print STDOUT "\n  Atoms  \n \n";
$n=0;
while(<FIN>){
  @xyz=split(' ');
  $n++;
  print STDOUT " $n     1  $xyz[1]  $xyz[2]   $xyz[3]\n";
}

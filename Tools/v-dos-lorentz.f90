 program vdos

  implicit none 

  integer i, iargc, nbid, polar, ibid
  real(8)  :: xmax, frequency, dummy, dx, omega, pi, delta, lorentz
  real(8), allocatable  :: totaldos(:), polardos(:,:)

  character(len=100) :: buffer,feigv
 
  pi = dacos(-1.d0)
  nbid = 2000

  if(iargc()<1) stop "Usage: vdos.x filename xmax deltalorentz"

  CALL GETARG(1,feigv)
  CALL GETARG(2,buffer)
  READ(buffer,*) xmax
  CALL GETARG(3,buffer)
  READ(buffer,*) delta

  dx = xmax/dble(nbid)

  open(unit=10,file=feigv,form='formatted')

  allocate(totaldos(0:nbid))
  allocate(polardos(3,0:nbid))
  totaldos = 0.d0
  polardos = 0.d0

  do while (.true.)
    read(10,*,end=100) dummy, dummy, frequency, polar
    do ibid = 0, nbid
      omega = dble(ibid)*dx
      lorentz = delta/((omega-frequency)**2 + delta**2)/pi
      totaldos(ibid) = totaldos(ibid) + lorentz
      polardos(polar,ibid) = polardos(polar,ibid) + lorentz
    enddo
  enddo
 100 continue
 
! print *, xmax, dx, dble(nbid)
  do ibid = 0, nbid
    omega = dble(ibid)*dx
    !write(*,*) omega, dx, ibid
    write(*,'(f14.5,4g16.8)') omega, totaldos(ibid), polardos(:,ibid) 
  enddo
 
  stop
 end program vdos

#!/bin/bash

np=4
export MKL_NUM_THREADS=${np}
mkdir data

# locate DLPOLY executable and link it to the dlpoly dir.
dlp_exe=`which DLPOLY.X`
ln -s $dlp_exe dlpoly/

# run ESKM
../sources/eskm.x input.txt -npdlpoly $np > eskm.log

#compute the conductance using Landauer's formula
grep T12 transmission_part_0 | cut -b 9-24,27- > transmission.dat 
../Tools/landauer.pl 1. 4.9 1. < transmission.dat > conductance.dat

